#pragma once
#include "MovingEntity.h"
#include <cassert>
#include "Mail.h"
#include "FSM.h"
#include <list>

class Triangle;

class CarrierStates;

class Carrier :
	public MovingEntity
{
protected:
	
	//pointer to graphics object
	Triangle* m_gObject;
	
	//instance of fsm
	FiniteStateMachine<Carrier>* m_pStateMachine;

	//pointer to all available states
	CarrierStates *m_pState;

	//current state
	State<Carrier>* m_pCurrentState;
	StateName m_eStateName;
	State<Carrier>* m_pPreviousState;
	State<Carrier>* m_pGlobalState;

	//pathfinding vars
	std::list<int> m_lCurrentPath;
	std::list<int>::iterator m_lCurrentNode;
	int m_iCurrentNode,m_iNextNode;
	

public:
	
	Carrier(vector3 posititon, vector3 velocity);
	~Carrier();

	//overriding virtuals
	
	//run every frame
	void Update(real time_elapsed);
	
	//message handling	
	bool HandleMessage(const Mail& msg);
	
	//changes states
	void ChangeState(State<Carrier>* pNewState);

	void RevertToPreviousState();

	//accessors

	FiniteStateMachine<Carrier>* GetFSM()const{return m_pStateMachine;}
	Triangle* GetGraphicsObject() {return m_gObject;}
	StateName GetStateName();
	std::list<int>::iterator GetNodeIter(){return m_lCurrentNode;}
	std::list<int> GetCurrentPath(){return m_lCurrentPath;}
	int GetCurrentNode(){return m_iCurrentNode;}
	int GetNextNode(){return m_iNextNode;}

	//mutators
	void SetGraphicsObject(Triangle* graphicObj);
	void SetStateName(StateName newStateName);
	void IncrementNodeIter(){++m_lCurrentNode;}
	void SetNodeIter(std::list<int>::iterator iter){m_lCurrentNode = iter;}
	void SetCurrentPath(std::list<int> newPath){m_lCurrentPath = newPath;m_lCurrentNode = m_lCurrentPath.begin();}
	void SetCurrentNode(int node){m_iCurrentNode = node;}
	void SetNextNode(int node){m_iNextNode = node;}

	//states
	bool StateLogic();

	

};

