#include "CarrierStates.h"
#include "Carrier.h"
#include "Graphics\gspOut.h"
#include "MessageDispatcher.h"
#include "MessageTypes.h"
#include "MyGameWorld.h"
#include "WorldManager.h"
#include "AMMS.h"
#include "SparseGraph.h"
#include "Dijkstra.h"

//  ****************************
// ******   SEEK     **********
//****************************

CarrierStateFlock* CarrierStateFlock::Instance()
{
	static CarrierStateFlock instance;

	return &instance;
}

void CarrierStateFlock::Enter(Carrier* pCarrier)
{
	//change object color to white to show flocking
	pCarrier->GetGraphicsObject()->SetColor(255,255,255);

	//add the forces to the entity
	AMI->AddWanderForce(pCarrier,50,100,50,5.0f);
	AMI->AddAlignment(pCarrier,16.0f);
	AMI->AddCohesion(pCarrier, 15.0f);
	AMI->AddSeparation(pCarrier,16.0f);

	//change state name 
	pCarrier->SetStateName(FLOCK);

	
}

void CarrierStateFlock::Execute(Carrier* pCarrier)
{
	////decide if I should stay in this state or change to another
	//if (pCarrier->StateLogic())
	//{
	//	//set delayed message for 1.0 seconds to update state
	//	Dispatch->DispatchMsg(1,pCarrier->GetID(),pCarrier->GetID(),MSG_UPDATE,NULL);
	//	return;
	//}

	//set delayed message for 1.0 seconds to update now in seek state
	Dispatch->DispatchMsg(1,pCarrier->GetID(),pCarrier->GetID(),MSG_UPDATE,NULL);

	
}

void CarrierStateFlock::Exit(Carrier* pCarrier)
{
	//clear all forces for entity
	AMI->ClearAllEntForces(pCarrier);
}

bool CarrierStateFlock::OnMessage(Carrier* pCarrier, const Mail& message)
{
	switch(message.m_iMsg)
	{
	case MSG_FLOCKING:
		{
			
		}
		return true;
		break;
	case MSG_ESCAPE:
		{
			pCarrier->GetFSM()->ChangeState(CEscape);
		}
		return true;
		break;
	case MSG_AGGRESIVE:
		{
			pCarrier->GetFSM()->ChangeState(CAggresive);
		}
	case MSG_UPDATE:
		{
			pCarrier->GetFSM()->Update();
		}
		return true;
		break;
	
	
	default:
		return false;
		
	}

}

//  ****************************
// ******    ESCAPE    **********
//****************************

CarrierStateEscape* CarrierStateEscape::Instance()
{
	static CarrierStateEscape instance;

	return &instance;
}

void CarrierStateEscape::Enter(Carrier* pCarrier)
{
	//change object color to BLU. because that's how ESCAPE-ers roll
	//get pointer to graphics object
	pCarrier->GetGraphicsObject()->SetColor(0,0,255);
	
	//wout << "Carrier "<< pCarrier->GetID()<<" is RUNNING AWAY!!" << endl;

	//ADD FORCES
	//AMI->AddFleeForce(pCarrier,pCarrier->GetTargetsInRange());

	pCarrier->SetStateName(ESCAPE);

		
}

void CarrierStateEscape::Execute(Carrier* pCarrier)
{
	//decide if I should stay in this state or change to another
	if (pCarrier->StateLogic())
	{
		//set delayed message for 1.0 seconds to update state
		Dispatch->DispatchMsg(1,pCarrier->GetID(),pCarrier->GetID(),MSG_UPDATE,NULL);
		return;
	}

	wout << "Carrier "<< pCarrier->GetID()<<" is continuing in ESCAPE state." << endl;

	//set delayed message for 1.0 seconds to update state
	Dispatch->DispatchMsg(1,pCarrier->GetID(),pCarrier->GetID(),MSG_UPDATE,NULL);

	
}

void CarrierStateEscape::Exit(Carrier* pCarrier)
{
	wout << "Carrier "<< pCarrier->GetID()<<" is EXITING ESCAPE state." << endl;
}

bool CarrierStateEscape::OnMessage(Carrier* pCarrier, const Mail& message)
{
	switch(message.m_iMsg)
	{
	case MSG_SEEK:
		{
			pCarrier->GetFSM()->ChangeState(CFlock);
		}
		return true;
		break;
	case MSG_WANDER:
		{
			pCarrier->GetFSM()->ChangeState(CAggresive);
		}
		return true;
		break;
	case MSG_UPDATE:
		{
			pCarrier->GetFSM()->Update();
		}
		return true;
		break;
	case MSG_REQUEST_POSITION:
		{
			Dispatch->DispatchMsg(0,pCarrier->GetID(),message.m_iSender,MSG_RESPONSE_POSITION, pCarrier);
		}
		return true;
		break;
	case MSG_RESPONSE_POSITION:
		{
			BaseGameEntity* pEntity = (BaseGameEntity*)message.ExtraInfo;
			if (pCarrier->EnemyInRange(pEntity))
			{
				if (!pCarrier->InTargetList(pEntity))
					pCarrier->AddTarget(pEntity);
			}
			else
				if (pCarrier->InTargetList(pEntity))
					pCarrier->RemoveTarget(pEntity);
		}
		return true;
		break;
	default:
		return false;
		
	}
}


//  ****************************
// ******   WANDER   **********
//****************************

CarrierStateWander::CarrierStateWander() 
{

}



CarrierStateWander* CarrierStateWander::Instance()
{
	static CarrierStateWander instance;

	return &instance;
}

void CarrierStateWander::Enter(Carrier* pCarrier)
{
	
	
}

void CarrierStateWander::Execute(Carrier* pCarrier)
{

	//decide if I should stay in this state or change to another
	if (pCarrier->StateLogic())
	{
		//set delayed message for 1.0 seconds to update state
		Dispatch->DispatchMsg(1,pCarrier->GetID(),pCarrier->GetID(),MSG_UPDATE,NULL);
		return;
	}

	wout << "Carrier "<< pCarrier->GetID()<<" is continuing in WANDER state." << endl;

	//set delayed message for 1.0 seconds to update state
	Dispatch->DispatchMsg(1,pCarrier->GetID(),pCarrier->GetID(),MSG_UPDATE,NULL);

	
}

void CarrierStateWander::Exit(Carrier* pCarrier)
{
	wout << "Carrier "<< pCarrier->GetID()<<" is EXITING WANDER state." << endl;
}

bool CarrierStateWander::OnMessage(Carrier* pCarrier, const Mail& message)
{
	switch(message.m_iMsg)
	{
	case MSG_SEEK:
		{
			pCarrier->GetFSM()->ChangeState(CFlock);

		}
		return true;
		break;
	case MSG_ESCAPE:
		{
			pCarrier->GetFSM()->ChangeState(CEscape);
		}
		return true;
		break;
	case MSG_UPDATE:
		{
			pCarrier->GetFSM()->Update();
		}
		return true;
		break;
	case MSG_REQUEST_POSITION:
		{
			Dispatch->DispatchMsg(0,pCarrier->GetID(),message.m_iSender,MSG_RESPONSE_POSITION, pCarrier);
		}
		return true;
		break;
	case MSG_RESPONSE_POSITION:
		{
			BaseGameEntity* pEntity = (BaseGameEntity*)message.ExtraInfo;
			if (pCarrier->EnemyInRange(pEntity))
			{
				if (!pCarrier->InTargetList(pEntity))
					pCarrier->AddTarget(pEntity);
			}
			else
				if (pCarrier->InTargetList(pEntity))
					pCarrier->RemoveTarget(pEntity);
		}
		return true;
		break;
	default:
		return false;
		
	}
}


//  ****************************
// ******   PATH     **********
//****************************

CarrierStateFollowPath* CarrierStateFollowPath::Instance()
{
	static CarrierStateFollowPath instance;

	return &instance;
}

void CarrierStateFollowPath::Enter(Carrier* pCarrier)
{
	//get beginning and end nodes
	m_iStartingNode = 18;
	m_iTargetNode = 7;

	//create pathfinder
	m_Seeker = new Dijkstra(WMI->GetNavGraph(),m_iStartingNode,m_iTargetNode);
	
	//get initial path
	pCarrier->SetCurrentPath( m_Seeker->GetPathToTarget());

	//seek starting node
	AMI->AddPathSeekForce(pCarrier,m_iStartingNode, 20);

	//init current and next nodes
	pCarrier->SetCurrentNode(m_iStartingNode);
	pCarrier->SetNextNode(m_iStartingNode);;

	//change state name 
	pCarrier->SetStateName(PATH);

	
}

void CarrierStateFollowPath::Execute(Carrier* pCarrier)
{
#if 0	//trace message
	wout << "Unit: " << pCarrier->GetID() << " is in PATH mode." << endl;
#endif

	//if I'm close to next node
	if ((WMI->GetNavGraph()->GetNode(pCarrier->GetNextNode())->GetPosition() - pCarrier->GetPosition()).magnitudeSquared() < 200)
	{
		//and its not the goal node
		if (pCarrier->GetNextNode() != m_iTargetNode)
		{
			//make next node current node, increment iterator 
			pCarrier->SetCurrentNode(pCarrier->GetNextNode());
			pCarrier->IncrementNodeIter();

			//set new next node
			pCarrier->SetNextNode(*pCarrier->GetNodeIter());

			//remove current seek
			AMI->ClearAllEntForces(pCarrier);

			//add new seek
			AMI->AddPathSeekForce(pCarrier,pCarrier->GetNextNode(),10);

			//recalculate path
			//m_Seeker = new Dijkstra(WMI->GetNavGraph(),m_iCurrentNode,m_iTargetNode);
			//m_lCurrentPath = m_Seeker->GetPathToTarget();

		}
		else
		{
			AMI->ClearAllEntForces(pCarrier);
			pCarrier->GetFSM()->ChangeState(CWinLevel);
			
		}
	}


	//set delayed message for 2.0 seconds to update now in seek state
	Dispatch->DispatchMsg(.5,pCarrier->GetID(),pCarrier->GetID(),MSG_UPDATE,NULL);

	
}

void CarrierStateFollowPath::Exit(Carrier* pCarrier)
{
	//clear all forces for entity
	AMI->ClearAllEntForces(pCarrier);
}

bool CarrierStateFollowPath::OnMessage(Carrier* pCarrier, const Mail& message)
{
	switch(message.m_iMsg)
	{
	case MSG_FLOCKING:
		{
			
		}
		return true;
		break;
	case MSG_ESCAPE:
		{
			pCarrier->GetFSM()->ChangeState(CEscape);
		}
		return true;
		break;
	case MSG_AGGRESIVE:
		{
			pCarrier->GetFSM()->ChangeState(CAggresive);
		}
	case MSG_UPDATE:
		{
			pCarrier->GetFSM()->Update();
		}
		return true;
		break;
	
	
	default:
		return false;
		
	}

}



//  ****************************
// ******    WIN     **********
//****************************





CarrierStateWinLevel* CarrierStateWinLevel::Instance()
{
	static CarrierStateWinLevel instance;

	return &instance;
}

void CarrierStateWinLevel::Enter(Carrier* pCarrier)
{
	//stop entity
	pCarrier->SetVelocity(vector3(0,0,0));

	//spin around because you're happy!
	pCarrier->SetRotation(vector3(0,0,10));
	
}

void CarrierStateWinLevel::Execute(Carrier* pCarrier)
{

	

	//set delayed message for 1.0 seconds to update state
	Dispatch->DispatchMsg(1,pCarrier->GetID(),pCarrier->GetID(),MSG_UPDATE,NULL);

	
}

void CarrierStateWinLevel::Exit(Carrier* pCarrier)
{
	
}

bool CarrierStateWinLevel::OnMessage(Carrier* pCarrier, const Mail& message)
{
	switch(message.m_iMsg)
	{
	case MSG_SEEK:
		{
			pCarrier->GetFSM()->ChangeState(CFlock);

		}
		return true;
		break;
	case MSG_ESCAPE:
		{
			pCarrier->GetFSM()->ChangeState(CEscape);
		}
		return true;
		break;
	case MSG_UPDATE:
		{
			pCarrier->GetFSM()->Update();
		}
		return true;
		break;
	case MSG_REQUEST_POSITION:
		{
			Dispatch->DispatchMsg(0,pCarrier->GetID(),message.m_iSender,MSG_RESPONSE_POSITION, pCarrier);
		}
		return true;
		break;
	case MSG_RESPONSE_POSITION:
		{
			BaseGameEntity* pEntity = (BaseGameEntity*)message.ExtraInfo;
			if (pCarrier->EnemyInRange(pEntity))
			{
				if (!pCarrier->InTargetList(pEntity))
					pCarrier->AddTarget(pEntity);
			}
			else
				if (pCarrier->InTargetList(pEntity))
					pCarrier->RemoveTarget(pEntity);
		}
		return true;
		break;
	default:
		return false;
		
	}
}


CarrierStates::CarrierStates()
{
	Flock = CFlock;
	Aggresive = CAggresive;
	Escape = CEscape;
}


CarrierStates::~CarrierStates()
{

}
