#pragma once
#include "State.h"
#include "Mail.h"

/* Programming Game AI by Example - Full Text FSM template page 64-65*/


template<typename entity_type>
class FiniteStateMachine
{
private:
	//pointer to the agent that owns this instance 
	entity_type* m_pOwner;

	State<entity_type>* m_pCurrentState;

	//record of the last state the agent was in 
	State<entity_type>* m_pPreviousState;

	//state logic is called every time the FSM is updated
	State<entity_type>* m_pGlobalState;

protected:

public:

	FiniteStateMachine(entity_type* owner): m_pOwner(owner),
											m_pCurrentState(NULL),
											m_pPreviousState(NULL),
											m_pGlobalState(NULL)
	{}

	//FSM initialization
	void SetCurrentState(State<entity_type>* s) {m_pCurrentState = s;}
	void SetGlobalState(State<entity_type>* s) {m_pGlobalState = s;}
	void SetPreviousState(State<entity_type>* s) {m_pPreviousState = s;}

	//run updates the scene
	void Update(float dt)const
	{
		//if a global state exists, call its execute method
		/*if (m_pGlobalState)
			m_pGlobalState->Execute(m_pOwner);*/

		//same for the current state
		if (m_pCurrentState) 
			m_pCurrentState->Execute(m_pOwner, dt);
	}

	//renders the scene
	void Render()const
	{
		//if a global state exists, call its execute method
		/*if (m_pGlobalState)
			m_pGlobalState->RenderScene(m_pOwner);*/

		//same for the current state
		if (m_pCurrentState) 
			m_pCurrentState->RenderScene(m_pOwner);
	}

	void OnLostDevice()
	{
		if (m_pCurrentState) 
			m_pCurrentState->OnLostDevice(m_pOwner);
	}

	void OnResetDevice()
	{
		if (m_pCurrentState) 
			m_pCurrentState->OnResetDevice(m_pOwner);
	}

	//set beginning state
	void SetInitialState(State<entity_type>* pNewState)
	{
		assert(pNewState && 
			"<FiniteStateMachine::ChangeState>: trying to change to a null state");

		//record of prev state
		m_pPreviousState = pNewState;

		//change to new state
		m_pCurrentState = pNewState;

		//call the entry method of the new state
		m_pCurrentState->Enter(m_pOwner);
	}

	//change to a new state
	void ChangeState(State<entity_type>* pNewState)
	{
		assert(pNewState && 
			"<FiniteStateMachine::ChangeState>: trying to change to a null state");

		//record of prev state
		m_pPreviousState = m_pCurrentState;

		//call exit of the existing state
		m_pCurrentState->Exit(m_pOwner);

		//change to new state
		m_pCurrentState = pNewState;

		//call the entry method of the new state
		m_pCurrentState->Enter(m_pOwner);
	}

	//go back to previous state
	void RevertToPreviousState()
	{
		ChangeState(m_pPreviousState);
	}

	//accessors
	State<entity_type>* CurrentState() const{return m_pCurrentState;}
	State<entity_type>* GlobalState() const{return m_pGlobalState;}
	State<entity_type>* PreviousState() const{return m_pPreviousState;}

	//returns true if the current state's type is equal to the type of the 
	//class passed as a parameter
	bool isInState(const State<entity_type>& st)const
	{
		if (typeid(*m_pCurrentState) == typeid(st)) return true;
		return false;
	}

	bool HandleMessage(const Mail& msg)const
	{
		//see if the current state is valid and that it can handle the message
		if (m_pCurrentState && m_pCurrentState->OnMessage(m_pOwner, msg))
		{
			return true;
		}
		//if it isn't and there's a global state, send message to the global state
		if (m_pGlobalState && m_pGlobalState->OnMessage(m_pOwner, msg))
		{
			return true;
		}

		return false;
	}


	~FiniteStateMachine()
	{
		
	}

	static FiniteStateMachine * Instance(); // declaration
};

