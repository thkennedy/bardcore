#include "Boobs1138.h"
#include "Physics\CollisionDetector.h"
#include "MessageDispatcher.h"
#include <string>
#include "Mail.h"


BaseGameEntity::BaseGameEntity(int ID, EntityType type)
	:m_entityID(ID), m_eType(type)
{

}
BaseGameEntity::BaseGameEntity(EntityType type)
	: m_eType(type)
{

}


BaseGameEntity::~BaseGameEntity(void)
{
}

int BaseGameEntity::GetID()
{
	return m_entityID;
}

EntityType BaseGameEntity::GetEntityType()
{
	return m_eType;
}

string BaseGameEntity::PrintEntType(BaseGameEntity* entity)
{
	string sEntType;
	switch(entity->GetEntityType())
	{
	case CARRIER:
		sEntType = "CARRIER";
		return sEntType;
		break;
	case FIGHTER:
		sEntType = "FIGHTER";
		return sEntType;
		break;
	case OBSTACLE:
		sEntType = "OBSTACLE";
		return sEntType;
		break;
	default:
		sEntType = "TYPE NOT FOUND";
		return sEntType;
		break;
	}
}

//  **************************
// **        MASS         ***
//**************************

real BaseGameEntity::GetMass() const
{
    if (m_rInverseMass == 0) 
	{
        return REAL_MAX;
    } 
	
	else 
	{
        return ((real)1.0)/m_rInverseMass;
    }
}

real BaseGameEntity::GetInverseMass() const
{
    return m_rInverseMass;
}

bool BaseGameEntity::HasFiniteMass() const
{
    return m_rInverseMass >= 0.0f;
}

//targeting
bool BaseGameEntity::InTargetList(BaseGameEntity* target)
{
	std::vector<BaseGameEntity*>::iterator it = m_vTargetsInRange.begin();
	
	while(it != m_vTargetsInRange.end())
	{
		if ((*it) == target)
			return true;

		it ++;
	}

	return false;
		

}

//does quick dist check to see if enemy is in range
bool BaseGameEntity::EnemyInRange(BaseGameEntity* target)
{
	return (CMI->EntityVisionRadius(this, target));
}

std::vector<BaseGameEntity*> BaseGameEntity::GetTargetsInRangeByType(EntityType type)
{
	std::vector<BaseGameEntity*> returnVec;

	std::vector<BaseGameEntity*>::iterator it = m_vTargetsInRange.begin();

	while (it != m_vTargetsInRange.end())
	{
		if ((*it)->GetEntityType() == type)
			returnVec.push_back(*it);

		++it;
	}

	return returnVec;
}

	//targeting
void BaseGameEntity::AddTarget(BaseGameEntity* addTarget)
{
	m_vTargetsInRange.push_back(addTarget);
	//wout << PrintEntType(this) <<  " ID: " << this->GetID() <<" successfully added target, ID: "<< addTarget->GetID() << ", which is a " <<PrintEntType(addTarget) << endl;
}

void BaseGameEntity::RemoveTarget(BaseGameEntity* removeTarget)
{
	std::vector<BaseGameEntity*>::iterator it = m_vTargetsInRange.begin();
	
	while(it != m_vTargetsInRange.end())
	{
		if ((*it) == removeTarget)
		{
			
			//wout << PrintEntType(this) <<  " ID: " << this->GetID() <<" Removed Target, ID: "<< (*it)->GetID() << ", which is a " <<PrintEntType(*it) << endl;
			m_vTargetsInRange.erase(it);
			return;
		}
		it ++;

		
	}

	//wout << PrintEntType(this) <<  " ID: " << this->GetID() <<" Failed to remove target, ID: "<< removeTarget->GetID() << ", which is a " <<PrintEntType(removeTarget) << endl;
	
}



