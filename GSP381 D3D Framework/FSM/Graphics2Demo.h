#pragma once


#include "State.h"

/*
	Clean Demo State for Graphics 2

*/

//forward declarations
class D3DApp;
class D3DSphere;
class D3DCamera;
class D3DBox;
class D3DShip;
class D3DTorus;
class FireRing;
class Rain;
class ParticleGun;
class FlameThrower;

#define Gfx2Demo Graphics2Demo::Instance()

class Graphics2Demo : public State<D3DApp>
{
private:
	Graphics2Demo(void);

	//free-camera
	//D3DCamera *m_pCamera;

	// list of shapes
	std::vector<int> m_vShapes;

	// particle systems
	FireRing* m_psFire;
	Rain* m_psRain;

	// pointer to gun for quick access
	ParticleGun* m_pGun;
	FlameThrower* m_pFlame;

public:

	static Graphics2Demo* Instance();

	static DWORD WINAPI ThreadQuadCreation(LPVOID arg);
	
	~Graphics2Demo(void);

	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);

	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);
};

