#pragma once
#include "Mail.h"
#include "../Physics/functions.h"



template <typename entity_type>
class State
{
public:

	//game entity methods
	virtual void Enter(entity_type*) = 0;
	virtual void Execute(entity_type*, float) = 0;
	virtual void Exit(entity_type*) = 0;

	//directX methods
	virtual void InitializeState(entity_type*) = 0;
	virtual void UpdateScene(entity_type*, float) = 0;
	virtual void RenderScene(entity_type*) = 0;
	virtual void OnResetDevice(entity_type*) = 0;
	virtual void OnLostDevice(entity_type*) = 0;
	virtual void LeaveState(entity_type*) = 0;


	//handles received messages
	virtual bool OnMessage(entity_type*, const Mail&) = 0;

	
	

	


	
};