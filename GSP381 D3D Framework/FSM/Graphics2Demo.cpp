#include "../Networking/MTClient.h"
#include "Graphics2Demo.h"
#include "../Graphics/d3dUtility.h"
//#include "../Graphics/D3DCamera.h"
#include "../Graphics/D3DBox.h"
#include "../Graphics/D3DApp.h"
#include "../Input/DirectInput.h"
#include "../Graphics/GfxStats.h"
#include "../Graphics/ShapeManager.h"
#include "../Graphics/D3DSphere.h"
#include "../Physics/MovementManager.h"
#include "../Graphics/D3DVertex.h"
#include "../PRNG/RandomNumber.h"
#include <random>
#include "../Graphics/FireRing.h"
#include "../Graphics/Rain.h"
#include "../Graphics/CameraManager.h"
#include "../Graphics/ParticleManager.h"
#include "../Graphics/ParticleGun.h"


//instance mehtod
Graphics2Demo* Graphics2Demo::Instance()
{
	static Graphics2Demo instance;

	return &instance;

}

//global state methods
//calls the D3D specific methods so that FSM stays modular(usable) for game entities
void Graphics2Demo::Enter(D3DApp* pD3DApp)
{
	InitializeState(pD3DApp);
}
void Graphics2Demo::Execute(D3DApp* pD3DApp, float dt)
{
	UpdateScene(pD3DApp, dt);
	
}
void Graphics2Demo::Exit(D3DApp* pD3DApp)
{
	LeaveState(pD3DApp);
}

//D3D specific methods
void Graphics2Demo::InitializeState(D3DApp* pD3DApp)
{
	int id, id1, id2;

	// seed rand
	RandNum->SeedRand(42);

	//set up camera first
	// add camera to cam mgr
	CamMgr->CreateCamera();
	CamMgr->m_pCurrentCamView->SetPos(0,0,-15);
	CamMgr->m_pCurrentCamView->SetSpeed(50.0f);
	CamMgr->m_pCurrentCamView->SetLookAt(Vector3(0,0,0),Vector3(0,1,0));

	//give shape mgr the camera
	SMI->SetCameraView(CamMgr->m_pCurrentCamView);

	// init all vert declarations
	InitAllVertexDeclarations();

	// init particle system manager
	PMan->Init();
	
	

#if 0
	float x,y,z;
		
	for (int i = 0; i < 10; ++i)
	{
		x = RandNum->Uniform(-10,10);
		y = RandNum->Uniform(-10,10);
		z = RandNum->Uniform(-10,10);

		id = SMI->CreateInstancedQuad(1,1,1);
		SMI->SetPosition(id,Vector3(x,y,z) );
		
	}

#endif
	AABBG psysBox;
	PMan->CreateFireRing("Graphics/Techniques/FireTech.fx", 
		"FireRingTech", "Graphics/Textures/Particles/torch.dds",
		D3DXVECTOR3(0.0f, 0.0f, -2.0f), psysBox, 20000, 0.00025f);

	//PMan->CreateRain("Graphics/Techniques/RainTech.fx", "RainTech", 
	//	"Graphics/Textures/IceTexture.png",
 //     D3DXVECTOR3(-1.0f, -9.8f, 0.0f), psysBox, 100000, 0.001f);
	
	/*id = PMan->CreateSprinkler("Graphics/Techniques/SprinklerTech.fx", "SprinklerTech",
		"Graphics/Textures/Particles/smoke.dds",
		D3DXVECTOR3(2.0f, -1.0f, 0.0f), psysBox, 20000, 0.003f);*/

	//id = PMan->CreateGun("Graphics/Techniques/GunTech.fx", "GunTech", 
	//	"Graphics/Textures/Particles/bolt.dds", 
	//	D3DXVECTOR3(0, -9.8f, 0.0f), psysBox, 100, -1.0f);
	float x,z;
	/*for (int i = 1000; i >= 0; --i)
	{
		x = RandNum->Uniform(-80.0f,80.0f);
		z = RandNum->Uniform(-80.0f,80.0f);

		id = PMan->CreateFlameThrower("Graphics/Techniques/GunTech.fx", "GunTech",
			"Graphics/Textures/Particles/torch.dds",
			D3DXVECTOR3(0.0f, -59.8f, 0.0f), psysBox, 1000, 0.0005f);

		PMan->GetParticleSystem(id)->SetPosition(x,0,z);
	}*/

	id = PMan->CreateFlameThrower("Graphics/Techniques/GunTech.fx", "GunTech",
			"Graphics/Textures/Particles/torch.dds",
			D3DXVECTOR3(0.0f, 20.0f, 0.0f), psysBox, 1000, -1.0f);

	m_pFlame = (FlameThrower*)PMan->GetParticleSystem(id);

	OnResetDevice(D3DAPPI);
	
}

DWORD WINAPI Graphics2Demo::ThreadQuadCreation(LPVOID arg)
{
	for (int i = 0; i < 50000; ++i)
	{
		SMI->CreateParticle(0.25f);
		
	}

	return 1;
}


void Graphics2Demo::UpdateScene(D3DApp* pD3DApp, float dt)
{
	static float counter = 999.0f;

	// Check input.
	if( gDInput->keyDown(DIK_1) && counter > 0.01f )
	{
		SMI->RotateShape(m_vShapes[0],0.0f,0.1f,0.0f);
		counter = 0.0f;
	}

	if( gDInput->keyDown(DIK_2) && counter > 0.01f)
	{
		SMI->RotateShape(m_vShapes[0],0.1f,0.0f,0.0f);
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_3) && counter > 0.3f)
	{
		
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_4) && counter > 0.3f)
	{
		
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_5) && counter > 0.3f)
	{
		
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_8) && counter > 0.3f)
	{
		
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_7) && counter > 0.3f)
	{
		
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_0) && counter > 0.3f )
	{
		
		counter = 0.0f;
	}
	if(gDInput->mouseButtonDown(0) && counter > 0.0f )
	{
		m_pFlame->AddParticle();
		counter = 0.0f;
	}

		
	//update physics objects
	//MMI->Update(dt);
	//update graphics objects
	SMI->Update(dt);

	// update camera
	CamMgr->Update(dt);

	// update psys
	//m_psFire->Update(dt);
	//m_psRain->Update(dt);
	PMan->Update(dt);


	//increment dbounce counter
	counter += dt;
}

void Graphics2Demo::RenderScene(D3DApp* pD3DApp)
{

	// clear the window to a deep blue
	D3DAPPI->GetD3DDevice()->Clear(0, 0, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 10, 100), 1.0f, 0);
	D3DAPPI->GetD3DDevice()->BeginScene();    // begins the 3D scene

	
#if 0
	SMI->InstancedQuadPreRender();
	SMI->InstancedQuadRender();
#endif


	//draw objects
#if 0
	SMI->Render();
#endif

	//m_psFire->Draw();
	//m_psRain->Draw();
	PMan->Render();


#if 1
	 /////////////////
	// Draw FPS
	//////////////////
	//display stats

	GStats->display();
#endif
	
    D3DAPPI->GetD3DDevice()->EndScene();    // ends the 3D scene

    D3DAPPI->GetD3DDevice()->Present(NULL, NULL, NULL, NULL);   

}

void Graphics2Demo::OnResetDevice(D3DApp* pD3DApp)
{
	SMI->OnResetDevice();
	
	GStats->onResetDevice();
	
	CamMgr->OnResetDevice();

	//m_psFire->OnResetDevice();

	//m_psRain->OnResetDevice();

	PMan->OnResetDevice();
	
}

void Graphics2Demo::OnLostDevice(D3DApp* pD3DApp)
{
	SMI->OnLostDevice();
	
	GStats->onLostDevice();

	CamMgr->OnLostDevice();

	//m_psFire->OnLostDevice();

	//m_psRain->OnLostDevice();

	PMan->OnLostDevice();
}

void Graphics2Demo::LeaveState(D3DApp* pD3DApp)
{
	
}

bool Graphics2Demo::OnMessage(D3DApp* pD3DApp, const Mail& message)
{
	return 1;
}




Graphics2Demo::Graphics2Demo(void)
{
}


Graphics2Demo::~Graphics2Demo(void)
{
}
