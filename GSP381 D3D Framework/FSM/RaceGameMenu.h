#pragma once
#include "D3DStates.h"

//////////////////////
//	RACEGAME MENU STATE
/////////////////////

// forward declarations
class D3DMenu;
class D3DSprite;


#define RGMenu RaceGameMenu::Instance()

class RaceGameMenu : public State<D3DApp>
{
private:
	// private const 
	RaceGameMenu(void);

	//Menus
	D3DMenu *menu1,*menu2, *menu3, *menu4, *menu5, *menu6, *menu7;
	std::vector<D3DMenu*> m_vMenus;

	D3DSprite *m_pBackground;

	// button textures
	IDirect3DTexture9 *m_pButtonTexture;

	// scratch pad
	int i,j,k,l;
	float x,y,z;

public:
	static RaceGameMenu* Instance();
	~RaceGameMenu(void);

	///////////////
	// INIT
	//////////////


	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);

	// menu specific functions
	void InitMenus();

	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);
};

