#pragma once

#include "State.h"




/*
	Demo3: RAGDOLL!!!

	
*/

//forward declarations
class D3DApp;
class D3DSphere;
class D3DCamera;
class D3DBox;
class ForceRegistry;
class ContactGenRegistry;

//#defines for singleton access
#define PhysDemo3 PhysicsDemo3::Instance()



class PhysicsDemo3 : public State<D3DApp>
{
private:
	PhysicsDemo3(void){}

	//free-camera
	D3DCamera *m_pCamera;

	// force registry
	ForceRegistry *m_pForces;

	// selected object to shoot
	int m_iSelectedAmmo;

		
public:
	
	//static method for singleton
	static PhysicsDemo3* Instance();

	

	~PhysicsDemo3(void){}

	void BallBallExample();
	void BoxBoxAABBExample();
	void BallBoxAABBExample();
	void BoxBoxOBBExample();
	void CollisionRotationExample();
	void ForcesExample();
	void BlastForce();
	void RagDoll();
	void RagDoll2();
	void Blocks();
	void ReferenceDots();
	void BVHExample(float xpos);
	void FireSelected();
	void ShootBullet(float speed);
	void ShootBall(float speed);
	void ShootBallGravity(float speed);
	void ShootBox(float speed);
	void ShootBoxGravity(float speed);
	void ShootGrenade(float speed);


	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);

	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);

};

