#include "../Networking/MTClient.h"
#include "NetworkingDemo.h"
#include "../Graphics/d3dUtility.h"
#include "../Graphics/D3DApp.h"
#include "../Input/DirectInput.h"
#include "../Graphics/GfxStats.h"
#include "../Graphics/ShapeManager.h"
#include "../Physics/MovementManager.h"
#include "../Objects/GameObjects/Score.h"
#include "../Networking/PacketInfo.h"
#include "../Objects/GameObjects/Racer.h"
#include "../Graphics/D3DVertex.h"
#include "../Graphics/Cube Map/CubeMap.h"
#include "../PRNG/RandomNumber.h"
#include "../Graphics/Sprites/D3DSpriteManager.h"
#include "../Objects/D3DMenu.h"
#include "../Graphics/D3DSprite.h"
#include "../Graphics/ParticleManager.h"
#include "../Graphics/CameraManager.h"
#include <random>


//instance mehtod
NetworkingDemo* NetworkingDemo::Instance()
{
	static NetworkingDemo instance;

	return &instance;

}

//global state methods
//calls the D3D specific methods so that FSM stays modular(usable) for game entities
void NetworkingDemo::Enter(D3DApp* pD3DApp)
{
	InitializeState(pD3DApp);
}
void NetworkingDemo::Execute(D3DApp* pD3DApp, float dt)
{
	UpdateScene(pD3DApp, dt);
	
}
void NetworkingDemo::Exit(D3DApp* pD3DApp)
{
	LeaveState(pD3DApp);
}

//D3D specific methods
void NetworkingDemo::InitializeState(D3DApp* pD3DApp)
{
	int id, id1, id2;

	// seed rand
	srand(time(NULL));

	//set up camera first
	
	CamMgr->CreateCamera();
	CamMgr->m_pCurrentCamView->SetPos(0,0,-15);
	CamMgr->m_pCurrentCamView->SetSpeed(50.0f);
	CamMgr->m_pCurrentCamView->SetLookAt(Vector3(0,0,0),Vector3(0,1,0));

	//give shape mgr the camera
	SMI->SetCameraView(CamMgr->m_pCurrentCamView);

	// init all vert declarations
	InitAllVertexDeclarations();

	// init game
	InitGameObjects();

	OnResetDevice(D3DAPPI);


}
void NetworkingDemo::CheckInput(float &fCounter)
{
	// Check input.
	if( gDInput->keyDown(DIK_1) && fCounter > 0.5f )
	{
		RestartGame();
		//myClient->SendPacket(5);
		fCounter = 0.0f;
	}

	if( gDInput->keyDown(DIK_2) && fCounter > 0.5f)
	{

		StartRace();	
		//myClient->SendPacket(2);
		fCounter = 0.0f;
	}
	if( gDInput->keyDown(DIK_3) && fCounter > 0.5f)
	{
		StopBoost();
		//myClient->SendPacket(3);
		fCounter = 0.0f;
	}
	if( gDInput->keyDown(DIK_4) && fCounter > 0.5f)
	{
		/*for (auto i = 7; i>=0;--i)
		{
			m_Sync->m_bPosition[i] = 100;
			m_Sync->m_bAcceleration[i] = 0;
			m_Sync->m_bVelocity[i] = 10;

			if (i == 0)
				m_Sync->m_bPosition[i] = 150;
		}
		SyncRacers();*/
		myClient->SendPacket(1);
		fCounter = 0.0f;
	}
	if( gDInput->keyDown(DIK_5) && fCounter > 0.5f)
	{
		EndRace();
		//myClient->SendPacket(5);
		fCounter = 0.0f;
	}
	if( gDInput->keyDown(DIK_8) && fCounter > 0.3f)
	{
		myClient->Shutdown();
		m_iPlayerID = -1;
		m_bIsRunning = false;
		fCounter = 0.0f;
	}
	if( gDInput->keyDown(DIK_9) && fCounter > 0.3f)
	{
		// registration
		myClient->SendPacket(4);
		fCounter = 0.0f;
	}
	if( gDInput->keyDown(DIK_0) && fCounter > 0.3f )
	{
		
		if (m_bIsRunning == false)
		{
			myClient->Run();
			m_bIsRunning = true;
		}

		
		fCounter = 0.0f;
	}

	if( gDInput->keyDown(DIK_SPACE) && fCounter > 0.5f)
	{
		//StartBoost();
		myClient->SendPacket(2);
		SPRMGR->GetSprite(L"Boost")->fDisplayTime = 0.0f;
		fCounter = 0.0f;
	}
	if(gDInput->mouseButtonDown(0) && fCounter > 0.3f )
	{
		

		fCounter = 0.0f;
	}
}

void NetworkingDemo::UpdateScene(D3DApp* pD3DApp, float dt)
{
	static float counter = 999.0f;
	static Vector3 vCamPos;

	CheckInput(counter);

	//update physics objects
	MMI->Update(dt);

	// update game objects
	for (auto it = m_vRacers.begin(); it != m_vRacers.end(); ++it)
	{
		(*it)->Update(dt);
	}

	//update graphics objects
	SMI->Update(dt);

	// update particles
	PMan->Update(dt);

	// update sprites
	SPRMGR->Update(dt);

	// get messages
	CheckMessages();

	// update roster
	UpdateRoster(dt);
	
	// update scoring
	Scoring->Update(dt);

	// update camera
	CamMgr->Update(dt);

	for (int i = m_vMenus.size()-1; i >= 0; --i)
		m_vMenus[i]->Update(dt);
	
	// timer for boost mode
	if (m_bBoosting == true)
	{
		// declare it if it doesn't exist already
		static float timer = 0.0;

		// add time
		timer += dt;

		// if 5 secs have passed, leave boost mode
		if (timer >= 5.0f)
		{
			timer = 0.0f;
			m_bBoosting = false;
			StopBoost();
		}
	}


	// set cam behind player's racer if its been assigned, else leave free
	if (m_iPlayerID != -1)
	{
		// update player's speed display
		Scoring->SetSpeed(MMI->GetVelocity(m_iPlayerID).z);
		Scoring->SetDistance(m_vRacers[m_iPlayerID]->GetPos().z);

		vCamPos = MMI->GetPosition(m_iPlayerID);
		CamMgr->m_pCurrentCamView->SetPos(vCamPos.x, vCamPos.y+8,vCamPos.z-50);
	}

	// update stats
	GStats->update(dt);
		
	//increment dbounce counter
	counter += dt;
		
}

void NetworkingDemo::RenderScene(D3DApp* pD3DApp)
{
	// If the device was not created successfully, return
	if(!D3DAPPI->GetD3DDevice())
		return;

	HRESULT hr;

    // clear the window to a deep blue
	hr = D3DAPPI->GetD3DDevice()->Clear(0, 0, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 10, 100), 1.0f, 0);
	hr = D3DAPPI->GetD3DDevice()->BeginScene();    // begins the 3D scene
	

	

	// draw sky
	m_pSky->Render();



	//draw objects
#if 1
	SMI->Render();
#endif

#if 1
	SMI->InstancedQuadPreRender();
	SMI->InstancedQuadRender();
#endif

#if 1 // draw sprites
	// Call Sprite's Begin to start rendering 2D sprite objects
	D3DAPPI->GetD3DSpriteObject()->Begin(D3DXSPRITE_ALPHABLEND);
	
	SPRMGR->Render();
	
	// End drawing 2D sprites
	D3DAPPI->GetD3DSpriteObject()->End();
	
#endif

	// render particles
	PMan->Render();

#if 1
	 /////////////////
	// Draw FPS
	//////////////////
	//display stats

	Scoring->Render();
	GStats->display();

	
#endif


	
    D3DAPPI->GetD3DDevice()->EndScene();    // ends the 3D scene

    D3DAPPI->GetD3DDevice()->Present(NULL, NULL, NULL, NULL);    // displays the created frame
	


}

void NetworkingDemo::OnResetDevice(D3DApp* pD3DApp)
{
	SMI->OnResetDevice();

	GStats->onResetDevice();

	Scoring->onResetDevice();
	
	CamMgr->OnResetDevice();

	SPRMGR->OnResetDevice();

	PMan->OnResetDevice();

	m_pSky->OnResetDevice();

	
	
}

void NetworkingDemo::OnLostDevice(D3DApp* pD3DApp)
{
	SMI->OnLostDevice();
	
	GStats->onLostDevice();

	Scoring->onLostDevice();

	SPRMGR->OnLostDevice();

	PMan->OnLostDevice();

	m_pSky->OnLostDevice();
}

void NetworkingDemo::LeaveState(D3DApp* pD3DApp)
{
	
}

bool NetworkingDemo::OnMessage(D3DApp* pD3DApp, const Mail& message)
{
	return 1;
}



// game methods
void NetworkingDemo::InitGameObjects()
{
	// init vars
	m_Roster = new Roster();
	m_Sync = new Sync();
	m_GameEvent = new GameEvent();
	
	m_iPlayerID = -1;

	// seed random
	RandNum->SeedRand(42);

	// init particle manager
	PMan->Init();
	

	//skybox
	//set up skybox
	m_pSky = new CubeMap("Graphics/Cube Map/hills.dds", 1000.0f);
	// give it the camera
	m_pSky->SetCamera(CamMgr->m_pCurrentCamView);

	// init sprites
	InitSprites();

	// create road texture
	IDirect3DTexture9* texture;
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/road.jpg", &texture);
	int id;
	float length = 400,
		width = 300,
		roadLength = 500,
		roadWidth = 1,
		xStart = 0,
		zStart = 0;

	// ROAD
	for (auto i = roadLength-1; i >= 0; --i)
	{
		
		// create road quad
		id = SMI->CreateInstancedQuad(width/2,length/2, 1.0f);
		// set road texture
		SMI->SetInstancedQuadTexture(texture);
		//rotate around x axis 90 deg
		SMI->RotateShape(id, 0.0f,1.570795f,0.0f);
		// move quad to its position
		SMI->SetPosition(id, Vector3(xStart, 0,zStart));
		// increment z values
		zStart += length;
	}


	////////////////
	// BUILDINGS
	////////////////
	// re-init vars
	xStart = 0;
	zStart = 0;

	int randNum;
	for (auto i = roadLength-1; i >=0; --i)
	{
		// need width negative
		width *= -1;;

		// one building on each side
		for (auto j = 1; j >=0; --j)
		{	

			id = SMI->CreateBox(10,30,150);
			SMI->SetPosition(id, Vector3(xStart+width/2,20,zStart));
		
			// random color
			randNum = RandNum->Uniform(1,5);
			switch(randNum)
			{
				case 1:
					SMI->SetColor(id, RED);
				break;
				case 2:
					SMI->SetColor(id, GREEN);
				break;
				case 3:
					SMI->SetColor(id, BLUE);
				break;
				case 4:
					SMI->SetColor(id, GRAY);
				break;
				case 5:
					SMI->SetColor(id, YELLOW);
				break;
			}

			// reset width to positive
			width *= -1;
		}
		// add to zstart
		zStart += length;
	}
	

	// create 8 ships on "starting line"

	// first 4 left of 0
	for (auto i = 3; i >= 0; --i)
	{
		// create racer game object
		Racer* temp = new Racer();

		// make it not able to sleep
		MMI->SetCanSleep(temp->GetID(), false);

		// set damping to 1 (don't drag)
		MMI->SetLinearDamping(temp->GetID(), 1.0f);
		
		// create graphics
		id = SMI->CreateShip(1, 1, 1);

		// link graphics and game object
		temp->SetGraphicsID(id);

		// set object's position
		MMI->SetPosition(temp->GetID(), Vector3(i*-25, 10, 0) );
		
		// rotate it 90 deg
		MMI->SetOrientation(temp->GetID(), 3.14f,0.0f,0.0f);

		// add it to racer vec
		m_vRacers.push_back(temp);


	}
	
	// next 4 right of 0
	for (auto i = 4; i >= 1; --i)
	{
		// create racer game object
		Racer* temp = new Racer();

		// make it not able to sleep
		MMI->SetCanSleep(temp->GetID(), false);

		// set damping to 1 (don't drag)
		MMI->SetLinearDamping(temp->GetID(), 1.0f);
		
		// create graphics
		id = SMI->CreateShip(1, 1, 1);

		// link graphics and game object
		temp->SetGraphicsID(id);

		// set object's position
		MMI->SetPosition(temp->GetID(), Vector3(i*25, 10, 0) );
		
		// rotate it 90 deg
		MMI->SetOrientation(temp->GetID(), 3.14f,0.0f,0.0f);

		// add it to racer vec
		m_vRacers.push_back(temp);


	}

	Scoring->Init();

}

//state methods
void NetworkingDemo::InitSprites()
{
	float x = D3DAPPI->m_D3Dpp.BackBufferWidth / 2,
		y = D3DAPPI->m_D3Dpp.BackBufferHeight / 3;
	// load in-race sprites
	SPRMGR->CreateSprite(false,L"Boost", L"Graphics/Sprites/RaceGame/Boost.png");
	SPRMGR->GetSprite(L"Boost")->SetPosition(x,y);

	SPRMGR->CreateSprite(false,L"GetReady", L"Graphics/Sprites/RaceGame/GetReady.png");
	SPRMGR->GetSprite(L"GetReady")->SetPosition(x,y);

	SPRMGR->CreateSprite(false,L"StartRace", L"Graphics/Sprites/RaceGame/StartRace.png");
	SPRMGR->GetSprite(L"StartRace")->SetPosition(x,y);

	SPRMGR->CreateSprite(false,L"YouLose", L"Graphics/Sprites/RaceGame/YouLose.png");
	SPRMGR->GetSprite(L"YouLose")->SetPosition(x,y);

	SPRMGR->CreateSprite(false,L"YouWin", L"Graphics/Sprites/RaceGame/YouWin.png");
	SPRMGR->GetSprite(L"YouWin")->SetPosition(x,y);
	
}

void NetworkingDemo::CheckMessages()
{
	// copies message vec into ours
	myClient->GetNextMessage(m_cNextMessage);

	// if nothing there, exit out
	if (m_cNextMessage == NULL)
		return;

	// read first in char array
	BYTE type = m_cNextMessage[0];

	switch (type)
	{
		case 0:
		{

			break;
		}			
		case 1: // sync
		{
			m_Sync->ConvertFromChar(m_cNextMessage);
			
			SyncRacers();
			break;
		}
		case 2: // boost
		{
			StartBoost();
			break;
		}
		case 3: // game event
		{
			// convert into readable data
			m_GameEvent->ConvertFromChar(m_cNextMessage);


			if (m_GameEvent->m_bState == 2)
				EndRace();
			else if (m_GameEvent->m_bState == 1)
				StartRace();
			else
				SPRMGR->GetSprite(L"GetReady")->fDisplayTime = 5.0f;
				

			break;
		}
		case 5: // roster
		{
			m_Roster->ConvertFromChar(m_cNextMessage);
			// update displayed values
			m_iPlayerID = m_Roster->yourID;
			Scoring->SetID(m_iPlayerID);
			// update the timeleft
			m_fTimeLeft = m_Roster->timeLeft;
			Scoring->SetTime(m_fTimeLeft);
			break;
		}
		default:
			break;
			
	}


}

void NetworkingDemo::UpdateRoster(float dt)
{
	// decrement timeleft
	if (m_fTimeLeft > 0.0f)
	{
		m_fTimeLeft -= dt; 
		Scoring->SetTime(m_fTimeLeft);
	}

}

void NetworkingDemo::RestartGame()
{
	// init score
	Scoring->Init();

	// reset appropriate vars
	m_iPlayerID = -1;
	// roster no longer good, delete
	delete m_Roster;
	m_Roster = new Roster();

	// reset racer positions
	for (i = m_vRacers.size() -1 ; i >= 0; --i)
	{
		// get positions
		vScratch1 = MMI->GetPosition(m_vRacers[i]->GetID());
		// reset z, as its the only one that changed 
		vScratch1.z = 0;
		MMI->SetPosition(m_vRacers[i]->GetID(),vScratch1);
	}

	myClient->SendPacket(4);

}

void NetworkingDemo::SyncRacers()
{
	int id;
	for (auto i = 7; i >= 0; --i)
	{
		// set the network offsets
		m_vRacers[i]->fNetworkOffset = m_Sync->m_bPosition[i];
		// set speed
		m_vRacers[i]->SetSpeed(m_Sync->m_bVelocity[i]);
		// set Acceleration
		m_vRacers[i]->SetAccel(m_Sync->m_bAcceleration[i]);
		
	}
}

void NetworkingDemo::StartBoost()
{
	SPRMGR->GetSprite(L"Boost")->fDisplayTime = 5.0f;
		
}

void NetworkingDemo::StopBoost()
{
	//SPRMGR->GetSprite(L"Boost")->SetPosition(-256, 0);
	//m_vRacers[m_iPlayerID]->SetAccel(0);
}

void NetworkingDemo::Adjust()
{
	m_vRacers[m_iPlayerID]->fNetworkOffset = m_vRacers[m_iPlayerID+1]->fNetworkOffset;
}

void NetworkingDemo::StartRace()
{
	// get rid of get ready sprite
	SPRMGR->GetSprite(L"GetReady")->fDisplayTime = -1.0f;

	// put StartRace sprite up there
	SPRMGR->GetSprite(L"StartRace")->fDisplayTime = 2.0f;

	// give all racers init velocity
	for (int i = m_vRacers.size()-1; i >= 0; --i)
	{
		m_vRacers[i]->SetSpeed(1);
	}


}

void NetworkingDemo::EndRace()
{
	// scratch 1 is biggest Z (winning dist)
	fScratch1 = -1;

	// j is the id of the winner
	j = 0;

	// figure out who won and stop all racers
	for (i = m_vRacers.size()-1; i >= 0; --i)
	{
		// scratch 2 is used to compare
		// get z of racer
		fScratch2 = m_vRacers[i]->GetPos().z;
		// compare against scratch 1
		if (fScratch2 > fScratch1)
		{
			// if its bigger, assign it
			fScratch1 = fScratch2;
			// store id of racer
			j = i;
		}

		m_vRacers[i]->SetSpeed(0);
		m_vRacers[i]->SetAccel(0);
		m_vRacers[i]->fNetworkOffset = MMI->GetPosition(m_vRacers[i]->GetID()).z;

	}

	// get rid of boost message just in case
	SPRMGR->GetSprite(L"Boost")->fDisplayTime = 0.0f;

	// if # in j matched the player's id, play won
	if (m_iPlayerID == j)
		SPRMGR->GetSprite(L"YouWin")->fDisplayTime = 999.0f;
	else
		SPRMGR->GetSprite(L"YouLose")->fDisplayTime = 999.0f;
	

}



NetworkingDemo::NetworkingDemo(void)
{
}


NetworkingDemo::~NetworkingDemo(void)
{
}
