#include "RaceGameMenu.h"
#include "../Graphics/D3DApp.h"
#include "../Graphics/Sprites/D3DSpriteManager.h"
#include "../Graphics/GfxStats.h"
#include "../Objects/D3DMenu.h"
#include "../Graphics/D3DSprite.h"
#include "../Input/DirectInput.h"
#include "../FSM/NetworkingDemo.h"


RaceGameMenu::RaceGameMenu(void)
{
}

RaceGameMenu* RaceGameMenu::Instance()
{
	static RaceGameMenu instance;
	
	return &instance;
}

RaceGameMenu::~RaceGameMenu(void)
{
}

//global state methods
void RaceGameMenu::Enter(D3DApp* pD3DApp)
{
	InitializeState(pD3DApp);
}
void RaceGameMenu::Execute(D3DApp* pD3DApp, float dt)
{
	UpdateScene(pD3DApp, dt);
}
void RaceGameMenu::Exit(D3DApp* pD3DApp)
{
	LeaveState(pD3DApp);
}

// menu specific functions
void RaceGameMenu::InitMenus()
{
	// create texture
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(),L"Graphics/Sprites/Buttons/greybutton.png",&m_pButtonTexture);
	
	// get middle of screen

	
	menu1 = new D3DMenu();

	menu1->SetPosition(Vector3(0,0,0));


	menu1->CreateButton(m_pButtonTexture, L"Graphics/Sprites/Buttons/greybutton.png",L"Button1","Play Game",0,Vector3(400,400,0));


	m_vMenus.push_back(menu1);

	/*i = SPRMGR->CreateSprite(1,L"Button1",L"Graphics/Sprites/Buttons/greybutton.png");
	SPRMGR->GetSprite(i)->SetPosition(D3DAPPI->m_D3Dpp.BackBufferWidth/2,D3DAPPI->m_D3Dpp.BackBufferHeight/2);*/
}

//D3D specific methods
void RaceGameMenu::InitializeState(D3DApp* pD3DApp)
{	
	// load background last
	i = SPRMGR->CreateSprite(1,L"Background",L"Graphics/Sprites/RaceGame/Menubackground.png");
	m_pBackground = SPRMGR->GetSprite(i);
	m_pBackground->SetPosition(D3DAPPI->m_D3Dpp.BackBufferWidth/2,D3DAPPI->m_D3Dpp.BackBufferHeight/2);
	m_pBackground->SetScale(D3DXVECTOR3(.78125f,1.17185f,0));

}
void RaceGameMenu::UpdateScene(D3DApp* pD3DApp, float dt)
{
	if( gDInput->keyDown(DIK_SPACE) )
	{
		D3DAPPI->GetFSM()->ChangeState(Networking);
	}

	// update sprites
	SPRMGR->Update(dt);

	// update menus
	for (i = m_vMenus.size()-1; i>=0;--i)
		m_vMenus[i]->Update(dt);

	// update stats
	GStats->update(dt);
}
void RaceGameMenu::RenderScene(D3DApp* pD3DApp)
{
	// If the device was not created successfully, return
	if(!pD3DApp->GetD3DDevice())
		return;

    // clear the window to a deep blue
    pD3DApp->GetD3DDevice()->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 40, 100), 1.0f, 0);

	pD3DApp->GetD3DDevice()->BeginScene();    // begins the 3D scene

	// Call Sprite's Begin to start rendering 2D sprite objects
	pD3DApp->GetD3DSpriteObject()->Begin(D3DXSPRITE_ALPHABLEND);
	
		
	SPRMGR->Render();

	
	// End drawing 2D sprites
	pD3DApp->GetD3DSpriteObject()->End();

#if 0
	GStats->display();
#endif

	
	

	

    pD3DApp->GetD3DDevice()->EndScene();    // ends the 3D scene

    pD3DApp->GetD3DDevice()->Present(NULL, NULL, NULL, NULL);  
}
void RaceGameMenu::OnResetDevice(D3DApp* pD3DApp)
{
	SPRMGR->OnResetDevice();
	GStats->onResetDevice();

	for (i = m_vMenus.size()-1; i>=0; --i)
	{
		m_vMenus[i]->OnResetDevice();
	}

	// background needs special scaling
	m_pBackground->SetScale(D3DXVECTOR3((float)pD3DApp->m_D3Dpp.BackBufferWidth / 1024.0f,
		(float)pD3DApp->m_D3Dpp.BackBufferHeight / 512.0f,0));


}
void RaceGameMenu::OnLostDevice(D3DApp* pD3DApp)
{
	SPRMGR->OnLostDevice();
	GStats->onLostDevice();

	for (i = m_vMenus.size()-1; i>=0; --i)
	{
		m_vMenus[i]->OnLostDevice();
	}
}
void RaceGameMenu::LeaveState(D3DApp* pD3DApp)
{
	SPRMGR->ClearRegistry();
}

bool RaceGameMenu::OnMessage(D3DApp* pD3DApp, const Mail& message)
{
	return 1;
}