#pragma once
#include "State.h"
//#include "../Networking/PacketInfo.h"

/*
	Clean Demo State for Graphics 2

*/

//forward declarations
class D3DApp;
class D3DCamera;
class Roster;
class Sync;
class GameEvent;
class Racer;
class CubeMap;
class D3DMenu;
class D3DSprite;

#define Networking NetworkingDemo::Instance()

class NetworkingDemo : public State<D3DApp>
{
private:
	NetworkingDemo(void);

	

	// list of shapes
	std::vector<int> m_vShapes;

	// Player's ID
	int m_iPlayerID;

	// time until match starts
	float m_fTimeLeft;

	// connected to server
	bool m_bConnected;

	// messages from client
	char* m_cNextMessage;

	// current roster
	Roster* m_Roster;

	// current sync packet
	Sync * m_Sync;

	// current game event packet
	GameEvent* m_GameEvent;

	// vec of racers
	std::vector<Racer*> m_vRacers;

	// cube map
	CubeMap* m_pSky;

	// start client
	bool m_bIsRunning;

	// sprite controllers
	//D3DMenu* m_pBoostSprite;

	// menus, for updating, render, etc.
	std::vector<D3DMenu*> m_vMenus;

	// bool to know when I'm in boost mode
	bool m_bBoosting;

	///////////////////
	// SCRATCH PAD
	//////////////////
	// loop vars
	int i,j,k,l;
	// data vars
	float fScratch1, fScratch2, fScratch3;
	Vector3 vScratch1, vScratch2;


public:

	static NetworkingDemo* Instance();
	
	~NetworkingDemo(void);

	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);

	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);



	// game methods

	// inits
	void InitGameObjects();
	void InitSprites();
	
	// updates
	void UpdateRoster(float dt);
	
	// shutdown
	void RestartGame();

	// networking
	void SyncRacers();			// actually performs the sync when a packet arrives
	void CheckMessages();
	
	// input
	void CheckInput(float &fCounter);

	// events
	void StartBoost();
	void StopBoost();
	void Adjust();
	void StartRace();
	void EndRace();




};

