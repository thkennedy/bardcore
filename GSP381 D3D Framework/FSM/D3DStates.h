#pragma once
#include "State.h"
#include "../Graphics/D3DText.h"
#include "../Physics/Vector3.h"
#include <fstream>
#include <d3d9.h>
#include <d3dx9.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include "../Physics/Vector3.h"
#pragma comment(lib, "winmm.lib")

// include the Direct3D Library file
#pragma comment (lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dinput8.lib")



//forward declarations
class D3DApp;
class D3DText;
class D3DSprite;
class MenuButton;
class D3DMenu;
class SoundSys;
class SoundEffect;
class D3DPyramid;
class D3DCamera;
class CubeMap;



//#defines for singleton access
#define Idle D3DStateIdle::Instance()
#define D3DTextBounce D3DStateTextBounce::Instance()
#define D3DMenu1 D3DStateWk2Menu::Instance()
#define IntroMovie D3DStateIntroMovie::Instance()
#define Credits D3DStateCredits::Instance()
#define Pyramid D3DStatePyramid::Instance()



class D3DStateIdle:public State<D3DApp>
{
private:
	D3DStateIdle(){}

	D3DText* m_oText;
	Vector3 m_vVelocity;
	
public:
	static D3DStateIdle* Instance()
{
	D3DStateIdle instance;
	return &instance;
}

	//global state methods
	virtual void Enter(D3DApp* pD3DApp){}
	virtual void Execute(D3DApp* pD3DApp, float dt){}
	virtual void Exit(D3DApp* pD3DApp){}

	

	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp){}
	virtual void UpdateScene(D3DApp* pD3DApp, float dt){}
	virtual void RenderScene(D3DApp* pD3DApp){}
	virtual void OnResetDevice(D3DApp* pD3DApp){}
	virtual void OnLostDevice(D3DApp* pD3DApp){}
	virtual void LeaveState(D3DApp* pD3DApp){}

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message){return 1;}

	
	
};

//D3D States - singleton
class D3DStateTextBounce:public State<D3DApp>
{
private:
	D3DStateTextBounce(){}

	D3DText* m_oText;
	Vector3 m_vVelocity;
	
public:
	static D3DStateTextBounce* Instance();

	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);

	//state methods
	bool OutsideScreen(D3DApp* pD3DApp);

	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);

	
	
};


//D3D States - singleton
//needs - buttons texture/sprite
class D3DStateWk2Menu:public State<D3DApp>
{
private:
	D3DStateWk2Menu(){}

	//////////////////////////////////////////////////////////////////////////
	// Sprite Variables
	//////////////////////////////////////////////////////////////////////////
			
	int m_iNextButtonID;
	D3DSprite* m_pBackground;
	//d3dmenus
	D3DMenu *menu1,*menu2, *menu3, *menu4, *menu5, *menu6, *menu7;
	std::vector<D3DMenu*> m_vMenus;

	//sounds
	SoundSys* m_sAmbient;
	SoundEffect *m_sOnClick, *m_sOnMouseOver;

	//video
	bool m_bVideoPlaying, m_bVideoPaused;
	
public:
	static D3DStateWk2Menu* Instance();

	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);

	//state methods
	void PlayIntroVideo(D3DApp* pD3DApp);
	void LoadMenuButtons(D3DApp* pD3DApp);
	void ClickedButtons(D3DApp* pD3DApp, int buttonClicked );

	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);



	
	
};


//D3D States - singleton
//needs - buttons texture/sprite
class D3DStateIntroMovie:public State<D3DApp>
{
private:
	D3DStateIntroMovie(){}

	
	//video
	bool m_bVideoPlaying, m_bVideoPaused;
	
public:
	static D3DStateIntroMovie* Instance();

	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);

	
	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);

	
};


//D3D States - singleton
//needs - buttons texture/sprite
class D3DStateCredits:public State<D3DApp>
{
private:
	//sounds
	SoundEffect* m_sAmbient;

	ifstream inFile;
	D3DStateCredits(){}

	D3DMenu *m_pCredits;
	
	
public:
	static D3DStateCredits* Instance();

	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);

	//utility methods
	void InitCredits();
	
	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);

	
};

//D3D States - singleton
class D3DStatePyramid:public State<D3DApp>
{
private:
	//sounds
	SoundSys* m_sAmbient;

	ifstream inFile;
	D3DStatePyramid(){}


	//camera vars
	float m_CameraRadius, m_CameraRotationY, m_CameraHeight;

		
	// a matrix to store the rotation information
	D3DXMATRIX matRotateY, matRotateX, matRotateZ;    
	D3DXVECTOR3 rotation;
	
	IDirect3DTexture9* m_Texture;


	

	////////////////////
	// SHADER INFO
	///////////////////
	ID3DXEffect*			m_pEffect;	//effects interface
	D3DXHANDLE              m_hTech;	//technique handle
	D3DXHANDLE              m_hWVP;		//World*View*Projection matrix handle
	D3DXHANDLE				m_hTexture;	//object texture


	D3DPyramid *m_pPyramid;

	CubeMap* m_pSky;
	
	D3DCamera *m_pCamera;
	
public:
	static D3DStatePyramid* Instance();

	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);
	

	//utility methods
	void BuildFX();
	void InitVerts();
	/*void BuildViewMatrix();
	void BuildProjectionMatrix();*/




	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);

	
};




class D3DStates :
	public State<D3DApp>
{

public:
	D3DStates(void);
	~D3DStates(void);

	D3DStateTextBounce* m_pTextBounce;
};
