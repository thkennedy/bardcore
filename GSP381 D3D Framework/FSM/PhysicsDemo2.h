#pragma once

#include "State.h"




/*
	Demo2: Basic Collisions complete

	Circle-Circle
	Circle-AABB
	AABB-AABB

*/

//forward declarations
class D3DApp;
class D3DSphere;
class D3DCamera;
class D3DBox;
class ForceRegistry;

//#defines for singleton access
#define PhysDemo2 PhysicsDemo2::Instance()



class PhysicsDemo2 : public State<D3DApp>
{
private:
	PhysicsDemo2(void){}

	//free-camera
	D3DCamera *m_pCamera;

	// force registry
	ForceRegistry *m_pForces;

	
public:
	
	//static method for singleton
	static PhysicsDemo2* Instance();

	

	~PhysicsDemo2(void){}

	void BallBallExample();
	void BoxBoxAABBExample();
	void BallBoxAABBExample();
	void BoxBoxOBBExample();
	void CollisionRotationExample();
	void ForcesExample();
	void BlastForce();


	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);

	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);

};

