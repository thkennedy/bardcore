#include "GameState.h"
#include "MenuState.h"
#include "../Graphics/D3DApp.h"
#include "../Graphics/d3dUtility.h"
#include "../Graphics/GfxStats.h"
#include "../Sound/SoundSys.h"
#include "../Sound/SoundEffect.h"
#include "../Input/DirectInput.h"
#include "../Graphics/D3DVertex.h"
#include "../Graphics/D3DPyramid.h"
#include "../Graphics/D3DCamera.h"
#include "../Graphics/Cube Map/CubeMap.h"
#include "../Graphics/D3DShip.h"
#include <iostream>
#include <string>
#include "../Graphics/D3DTorus.h"
#include "../Physics/functions.h"
#include "../Objects/GameObjects/Score.h"

GameState::GameState(void)
{
}

GameState* GameState::Instance()
{
	static GameState instance;

	return &instance;
}

GameState::~GameState(void)
{
}

//global state methods
//calls the D3D specific methods so that FSM stays modular(usable) for game entities
void GameState::Enter(D3DApp* pD3DApp)
{
	InitializeState(pD3DApp);
}
void GameState::Execute(D3DApp* pD3DApp, float dt)
{
	UpdateScene(pD3DApp, dt);
	
}
void GameState::Exit(D3DApp* pD3DApp)
{
	LeaveState(pD3DApp);
}

//utility methods
void GameState::BuildFX()
{
	HRESULT hr;

	// Create the FX from a .fx file.
	ID3DXBuffer* errors = 0;
	hr = D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), "Graphics/Techniques/pyramid.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &m_pEffect, &errors);
	if( errors ) 
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);	

	// Obtain handles.
	m_hTech = m_pEffect->GetTechniqueByName("PyramidTech");
	m_hWVP  = m_pEffect->GetParameterByName(0, "gWVP");
	m_hTexture = m_pEffect->GetParameterByName(0,"gTex");
	m_hWorldInvTrans = m_pEffect->GetParameterByName(0, "gWorldInvTrans");
	m_hLightVecW     = m_pEffect->GetParameterByName(0, "gLightVecW");
	m_hDiffuseMtrl   = m_pEffect->GetParameterByName(0, "gDiffuseMtrl");
	m_hDiffuseLight  = m_pEffect->GetParameterByName(0, "gDiffuseLight");
	m_hAmbientMtrl   = m_pEffect->GetParameterByName(0, "gAmbientMtrl");
	m_hAmbientLight  = m_pEffect->GetParameterByName(0, "gAmbientLight");
	m_hSpecularMtrl  = m_pEffect->GetParameterByName(0, "gSpecularMtrl");
	m_hSpecularLight = m_pEffect->GetParameterByName(0, "gSpecularLight");
	m_hSpecularPower = m_pEffect->GetParameterByName(0, "gSpecularPower");
	m_hEyePos        = m_pEffect->GetParameterByName(0, "gEyePosW");
	m_hWorld         = m_pEffect->GetParameterByName(0, "gWorld");
		
	m_pPyramid->SetFX(m_pEffect);
	m_pPyramid->SetTexture(m_hTexture, m_Texture);

}

void GameState::InitVerts()
{
	HRESULT hr;
	
	//set up ship and give it a camera
	m_pShip = new D3DShip(0);
	m_pShip->SetCamera(m_pCamera);
	m_pShip->SetVelocity(0,0,-5);

	D3DTorus* tempRing;
	float x = 0.0f, y= 0.0f, z= -200.0f;

	//set up rings and their camera
	for (int i = 0; i < 10; i++)
	{
		
		tempRing = new D3DTorus();
		tempRing->SetCamera(m_pCamera);
		tempRing->SetPosition(x,y,z);
		tempRing->ChangeVelocityZ(3);
		tempRing->SetPlayerPos(m_pShip->GetPosition());

		m_vRings.push_back(tempRing);

		z -= 150;

		/*x = RandInRange(-50,50);
		y = RandInRange(-50,50);*/
	}

	//BuildFX();
	OnResetDevice(D3DAPPI);

	InitAllVertexDeclarations();

}


//D3D specific methods
void GameState::InitializeState(D3DApp* pD3DApp)
{

	Scoring->Init();


	//set up camera first
	m_pCamera = new D3DCamera();
	m_pCamera->SetPos(0,5,60);
	m_pCamera->SetLookAt(D3DXVECTOR3(0,0,0), D3DXVECTOR3 (0,1,0));
	
	

	//initialize sounds
	m_sAmbient = new SoundEffect();
	m_sAmbient->initialise();
	m_sAmbient->load("Sound/dragonborn.mp3");


	//set up skybox
	m_pSky = new CubeMap("Graphics/Cube Map/hills.dds", 10000.0f);
	
	InitVerts();

	m_sAmbient->playLooped();


}

void GameState::UpdateScene(D3DApp* pD3DApp, float dt)
{
	static float index = 0.0f; /*index+=0.001f;    // an ever-increasing float value*/

	// Check input.
	if( gDInput->keyDown(DIK_W) )
	{
		//rotation.x   -= 10 * dt;
		m_pShip->SetRotationX(m_pShip->GetOrientationV()->x + 1 * dt);
		m_pShip->ChangeVelocityY(5);
	}
	if( gDInput->keyDown(DIK_S) )	 
	{
		//rotation.x += 10* dt;
		m_pShip->SetRotationX(m_pShip->GetOrientationV()->x - 1 * dt);
		m_pShip->ChangeVelocityY(-5);
	}
	if(gDInput->keyDown(DIK_A) )
	{
		//rotation.y -= 10 * dt;
		m_pShip->SetRotationY(m_pShip->GetOrientationV()->y - 1 * dt);
		m_pShip->ChangeVelocityX(5);
	}
	if(gDInput->keyDown(DIK_D) )
	{
		//rotation.y += 10 * dt;
		m_pShip->SetRotationY(m_pShip->GetOrientationV()->y + 1 * dt);
		m_pShip->ChangeVelocityX(-5);
	}

	if(!gDInput->keyDown(DIK_W) && !gDInput->keyDown(DIK_S) && !gDInput->keyDown(DIK_A) && !gDInput->keyDown(DIK_D))
	{
		//find home orientation and move towards it
		D3DXVECTOR3 homeOrientation = D3DXVECTOR3(0,0,0) - *m_pShip->GetOrientationV();
		D3DXVec3Normalize(&homeOrientation, &homeOrientation);
		D3DXVec3Scale(&homeOrientation, &homeOrientation, 1*dt);
		m_pShip->SetRotation(*m_pShip->GetOrientationV() + homeOrientation);
		
		//do same for velocity
		m_pShip->SetVelocity(D3DXVECTOR3(0,0,0));
	}

	m_pShip->ChangeVelocityZ(0);

	

	//m_pPyramid->Update();
	m_pShip->Update(dt);
	m_pCamera->Update(dt);
	for (auto it = m_vRings.begin(); it != m_vRings.end(); it++)
	{
		(*it)->Update(dt);
		(*it)->SetPlayerPos(m_pShip->GetPosition());
	}

	////////////////////////
	//	QUIT BACK TO MENU
	///////////////////////
	if(gDInput->keyDown(DIK_Q) )
	{
		pD3DApp->GetFSM()->ChangeState(GameMenu);
	}
}


void GameState::RenderScene(D3DApp* pD3DApp)
{
	// If the device was not created successfully, return
	if(!pD3DApp->GetD3DDevice())
		return;

	HRESULT hr;

    hr = pD3DApp->GetD3DDevice()->BeginScene();    // begins the 3D scene


	//draw objects
#if 1
	m_pSky->Render();
	//m_pPyramid->Render();
	m_pShip->Render();
	for (auto it = m_vRings.begin(); it != m_vRings.end(); it++)
	{
		(*it)->Render();
	}
	

#endif

	 //////////////////////////////////////////////////////////////////////////
	// Draw FPS
	//////////////////////////////////////////////////////////////////////////
	//display stats
#if 1
	GStats->display();
	Scoring->Render();
#endif
	
    pD3DApp->GetD3DDevice()->EndScene();    // ends the 3D scene

    pD3DApp->GetD3DDevice()->Present(NULL, NULL, NULL, NULL);    // displays the created frame

}

void GameState::OnResetDevice(D3DApp* pD3DApp)
{
	
	for (auto it = m_vRings.begin(); it != m_vRings.end(); it++)
	{
		(*it)->OnResetDevice();
	}
	m_pShip->OnResetDevice();
	GStats->onResetDevice();
	Scoring->onResetDevice();
	m_pSky->OnResetDevice();
	m_pSky->SetCamera(m_pCamera);
	m_pCamera->CalculateProjectionMatrix();
	
}

void GameState::OnLostDevice(D3DApp* pD3DApp)
{
	for (auto it = m_vRings.begin(); it != m_vRings.end(); it++)
	{
		(*it)->OnLostDevice();
	}
	m_pSky->OnLostDevice();
	GStats->onLostDevice();
	Scoring->onLostDevice();
	m_pShip->OnLostDevice();

}

void GameState::LeaveState(D3DApp* pD3DApp)
{
	for (auto it = m_vRings.begin(); it != m_vRings.end(); it++)
	{
		(*it)->OnLostDevice();
	}
	m_pSky->OnLostDevice();
	GStats->onLostDevice();
	Scoring->onLostDevice();
	m_pShip->OnLostDevice();

	m_vRings.clear();

	delete m_sAmbient;

}

bool GameState::OnMessage(D3DApp* pD3DApp, const Mail& message)
{
	return 1;
}
