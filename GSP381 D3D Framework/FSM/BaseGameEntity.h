#pragma once
#include "Physics\Quaternion.h"
#include "Physics\Matrix4.h"


struct Mail;


//entity types enum
enum EntityType 
{
	FIGHTER,
	CARRIER,
	OBSTACLE,

};

enum StateName
{
	FLOCK,
	AGGRESSIVE,
	ESCAPE,
	DEAD,
	PATH
};

enum TeamName
{
	FIGHTER_TEAM,
	CARRIER_TEAM,
	HUMANE,
	ALBATROSS,
	DOLPHINE,
	OBSTACLES
};

class BaseGameEntity
{
protected:
	int m_entityID;

	vector3 m_vPosition;
	
	EntityType m_eType;
	
	//game vars
	real m_rStrength; // unit strength

	real m_rCollisionRadius; // collision radius of the object
	
	real m_rVisionRadius; // what can I see?

	//current target
	BaseGameEntity* m_bCurrentTarget;

	//all targets in vision range
	std::vector<BaseGameEntity*> m_vTargetsInRange;
	//mass
	real m_rInverseMass;

	//elasticity
	real m_restitution;

	//team 
	TeamName m_eTeam;

public:
	BaseGameEntity(int ID, EntityType type);
	BaseGameEntity(EntityType type);
	
	
	virtual ~BaseGameEntity();
		
	virtual void Update(real time_elapsed)=0;
	
	virtual bool HandleMessage(const Mail& msg) = 0;

	string PrintEntType(BaseGameEntity* entity);

	//Accessors
	int GetID();

	//position
	vector3 GetPosition() {return m_vPosition;}

	//radius
	real GetColRadius() {return m_rCollisionRadius;}
	real GetVisionRadius() {return m_rVisionRadius;}
	
	EntityType GetEntityType();

	//strength
	real GetStrength() {return m_rStrength;}

	// mass        
	real GetMass() const;
	real GetInverseMass() const;
	bool HasFiniteMass() const;

	//targeting
	BaseGameEntity* GetCurrentTarget() {return m_bCurrentTarget;}
	bool InTargetList(BaseGameEntity* target);
	//does quick dist check to see if enemy is in range
	bool EnemyInRange(BaseGameEntity* target);
	std::vector<BaseGameEntity*> GetTargetsInRange() {return m_vTargetsInRange;}
	std::vector<BaseGameEntity*> GetTargetsInRangeByType(EntityType type);
	
	//MUTATORS
	void SetID(int id){m_entityID = id;}
	void SetColRadius(real radius){m_rCollisionRadius = radius;}
	
	//mass
	void SetMass(real mass){assert(mass != 0); m_rInverseMass = ((real)1.0)/mass;}
	void SetInverseMass(real iMass) {m_rInverseMass = iMass;}
	
	//targeting
	void AddTarget(BaseGameEntity* addTarget);
	void RemoveTarget(BaseGameEntity* removeTarget);
	void SetCurrentTarget(BaseGameEntity* target) {m_bCurrentTarget = target;}



};

