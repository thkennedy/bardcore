#pragma once
#include "D3DStates.h"

//////////////////////
//	GAME MENU STATE
/////////////////////

#define GameMenu MenuState::Instance()


class MenuState:public State<D3DApp>
{
private:
	MenuState(){}

	//////////////////////////////////////////////////////////////////////////
	// Sprite Variables
	//////////////////////////////////////////////////////////////////////////
				
	int m_iNextButtonID;
	D3DSprite* m_pBackground;
	//d3dmenus
	D3DMenu *menu1,*menu2, *menu3, *menu4, *menu5, *menu6, *menu7;
	std::vector<D3DMenu*> m_vMenus;

	//sounds
	SoundEffect* m_sAmbient;
	SoundEffect *m_sOnClick, *m_sOnMouseOver;

	//video
	bool m_bVideoPlaying, m_bVideoPaused;

	//leave state
	bool m_bLeaveState;
	
public:
	static MenuState* Instance();

	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);

	//state methods
	void PlayIntroVideo(D3DApp* pD3DApp);
	void LoadMenuButtons(D3DApp* pD3DApp);
	void ClickedButtons(D3DApp* pD3DApp, int buttonClicked );

	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);



	
	
};

