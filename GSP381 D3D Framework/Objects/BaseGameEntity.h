#pragma once
#include "../Physics/Vector3.h"



class BaseGameEntity
{
protected:

	Vector3 m_vPosition;		// entity's position in 3D space

	
	
public:
	BaseGameEntity(void);
	BaseGameEntity(Vector3 vPosition, bool bCanSleep = true);

	virtual ~BaseGameEntity(void);

	/// <summary>
	/// All Entity Updates Processed Here
	/// </summary>
	/// <param name="dt">Change in time from last frame</param>
	/// <returns>No Return</returns>
	virtual void VUpdate(float dt) = 0;


	////////////////////////
	// ACCESSORS
	///////////////////////
	Vector3 GetPosition() const {return m_vPosition;}
	

	////////////////////////
	// MUTATORS
	///////////////////////
	
	void SetPosition(float x, float y, float z);
	void SetPosition(Vector3 vPosition);
	
	

};

