#pragma once

#include "../../Graphics/d3dUtility.h"

class SoundEffect;

#define Scoring Score::Instance()

class Score
{
private:
	Score();
	
    ID3DXFont* mFont;
    float m_fSpeed;
    int m_iPlayerID;
	int m_iTimeLeft;
	float mMultiplier;
	float m_fDistance;
	SoundEffect* m_sHitRing;
	SoundEffect* m_sMissRing;
	int m_bbWidth, m_bbHeight;
	
public:
	
	static Score* Instance();
    ~Score();
    void onLostDevice();
    void onResetDevice();
	
	////////////////////
	// UTILS
	///////////////////
	void Init();

	///////////////////
	// MUTATORS
	//////////////////
	void SetID(int id);
	void SetTime(int time);
	void SetSpeed(float speed);
	void SetDistance(float dist);

	void HitRing();
    void MissedRing();
    
    

    void Update(float dt);
    void Render();


};

