#pragma once
#include "../../Physics/functions.h"

#define MAXPARTICLES 2000

// forward declarations
class RigidBody;
class FireRing;

class Racer
{
private:
	// racer's body id
	int m_iBodyID;

	// racer's graphics id
	int m_iGraphicsID;

	// racer's speed factor - multiply vel by this to give speed
	float m_fSpeedFactor;

	// racer's max speed
	float m_fSpeed;

	// ptr to racer's rigid body
	RigidBody* m_pRacerBody;

	// left rocket
	FireRing* m_psLeftRocket;

	// right rocket
	FireRing* m_psRightRocket;

	/////////////////
	// SCRATCH PAD
	////////////////
	Vector3 direction;
	int i,j,k;




public:
	Racer(void);
	~Racer(void);

	////////////////////
	// PUBLIC VARS
	///////////////////
	float fNetworkOffset;
	float fNetworkAcceleration;
	

	//////////////////////
	// UTILITIES
	/////////////////////
	void Update(float dt);
	
	
	/////////////////////////
	// ACCESSORS
	////////////////////////
	float GetSpeedFactor(){return m_fSpeedFactor;}
	float GetSpeed(){return m_fSpeed;}
	int GetGraphicsID(){return m_iGraphicsID;}
	int GetID(){return m_iBodyID;}
	Vector3 GetPos();
	

	///////////////////////
	// MUTATORS
	//////////////////////
	void SetSpeedFactor(float fSpeedFactor){m_fSpeedFactor = fSpeedFactor;}
	void SetSpeed(float fSpeed);
	void SetAccel(float fAccel);
	void SetGraphicsID(int iNewID){m_iGraphicsID = iNewID;}

	//void SetSpeed(float speed){m_fSpeed = speed;}
};

