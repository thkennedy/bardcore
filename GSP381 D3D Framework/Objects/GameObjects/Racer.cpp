#include "Racer.h"
#include "../../Physics/MovementManager.h"
#include "../../Graphics/ShapeManager.h"
#include "../../Physics/RigidBody.h"
#include "../../Graphics/ParticleManager.h"


Racer::Racer(void)
{
	// create rigid body
	m_iBodyID = MMI->CreateRigidBody(Vector3(0,0,0));

	// get ptr to body
	m_pRacerBody = MMI->GetBody(m_iBodyID);

	// doesn't have a graphics object yet, so set id to -1
	m_iGraphicsID = -1;

	// set speed factor
	m_fSpeedFactor = 1000.0f;

	// set speed
	m_fSpeed = 0.0f;

	fNetworkOffset = 0.0f;
	m_fSpeed = 0.0f;
	fNetworkAcceleration = 0.0f;

	AABBG psysBox;
	// create the rockets
	i = PMan->CreateFireRing("Graphics/Techniques/FireTech.fx", 
		"FireRingTech", "Graphics/Textures/Particles/torch.dds",
		D3DXVECTOR3(0.0f, 0.0f, 0.0f), psysBox, MAXPARTICLES, 0.0005f);

	m_psLeftRocket = (FireRing*)PMan->GetParticleSystem(i);
	m_psLeftRocket->SetScale(.09,.09,.09);

	i = PMan->CreateFireRing("Graphics/Techniques/FireTech.fx", 
		"FireRingTech", "Graphics/Textures/Particles/torch.dds",
		D3DXVECTOR3(0.0f, 0.0f, 0.0f), psysBox, MAXPARTICLES, 0.0005f);

	m_psRightRocket = (FireRing*)PMan->GetParticleSystem(i);
	m_psRightRocket->SetScale(.09,.09,.09);

	

}


Racer::~Racer(void)
{
}

//////////////////////
// UTILITIES
/////////////////////
void Racer::Update(float dt)
{
	// cache Velocity and position
	Vector3 vPos = m_pRacerBody->GetPosition();
	Vector3 vVel = m_pRacerBody->GetVelocity();

	// if network offset and position.z do not match, get the closer
	if (fNetworkOffset >vPos.z*m_fSpeedFactor+1.5f || fNetworkOffset <vPos.z*m_fSpeedFactor-1.5f)
	{
		// get direction that racer is off by
		direction = (Vector3(0,0,fNetworkOffset) - vPos);	
		// remove x and y portions of it
		direction.x = 0;
		direction.y = 0;
		// normalize it
		direction.Normalize();
		
		// apply a small force in that direction
		m_pRacerBody->AddForce(direction * 10);
	}


	
	// physics update on speed
	m_fSpeed += (fNetworkAcceleration * dt);
	
	// physics update on offset
	fNetworkOffset += (m_fSpeed * dt);
	
	// align graphics with position
	SMI->SetPosition(m_iGraphicsID, vPos);
	SMI->SetOrientation(m_iGraphicsID, m_pRacerBody->GetOrientation() );

	if (m_pRacerBody->GetVelocity().MagnitudeSquared() > 0)
	{
		m_psLeftRocket->SetAccel(Vector3(0,0,-10) );	
		m_psRightRocket->SetAccel(Vector3(0,0,-10) );
	}
	else
	{
		m_psLeftRocket->SetAccel(Vector3(0,0, 0) );	
		m_psRightRocket->SetAccel(Vector3(0,0, 0) );
	}


	// align particle system with pos
	m_psLeftRocket->SetPosition(m_pRacerBody->GetPosition().x-4.8,m_pRacerBody->GetPosition().y+2.1,m_pRacerBody->GetPosition().z-2);
	
	m_psRightRocket->SetPosition(m_pRacerBody->GetPosition().x+4.8,m_pRacerBody->GetPosition().y+2.1,m_pRacerBody->GetPosition().z-2);
	

}

Vector3 Racer::GetPos()
{
	return m_pRacerBody->GetPosition();
}

void Racer::SetSpeed(float fSpeed)
{
	m_fSpeed = fSpeed*m_fSpeedFactor;

	// set racer's new velocity
	m_pRacerBody->SetVelocity(0,0,m_fSpeed);

	
}
	
void Racer::SetAccel(float fAccel)
{
	m_pRacerBody->SetAcceleration(0,0,fAccel);
	fNetworkAcceleration = fAccel;
}
