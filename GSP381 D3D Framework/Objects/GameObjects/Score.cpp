#include "../../Graphics/D3DApp.h"
#include "../../Sound/SoundEffect.h"
#include "Score.h"
#include <tchar.h>

Score* Score::Instance()
{
	static Score instance;

	return &instance;
}

Score::Score()
: mFont(0)
{
	D3DXFONT_DESC fontDesc;
	fontDesc.Height          = 24;
    fontDesc.Width           = 0;
    fontDesc.Weight          = 0;
    fontDesc.MipLevels       = 1;
    fontDesc.Italic          = false;
    fontDesc.CharSet         = DEFAULT_CHARSET;
    fontDesc.OutputPrecision = OUT_DEFAULT_PRECIS;
    fontDesc.Quality         = DEFAULT_QUALITY;
    fontDesc.PitchAndFamily  = DEFAULT_PITCH | FF_DONTCARE;
    wcscpy_s(fontDesc.FaceName, _T("Times New Roman"));

	D3DXCreateFontIndirect(D3DAPPI->GetD3DDevice(), &fontDesc, &mFont);

	mMultiplier = 1.0f;

	m_sHitRing = new SoundEffect();
	m_sHitRing->initialise();
	m_sHitRing->load("Sound/ring.wav");

	m_sMissRing = new SoundEffect();
	m_sMissRing->initialise();
	m_sMissRing->load("Sound/doh.mp3");
}

Score::~Score()
{
	ReleaseCOM(mFont);

	delete m_sHitRing;
	delete m_sMissRing;
}

void Score::onLostDevice()
{
	mFont->OnLostDevice();
}

void Score::onResetDevice()
{
	mFont->OnResetDevice();

	// get new width and height
	m_bbHeight = D3DAPPI->GetD3Dpp().BackBufferHeight* 6.0f/8.0f;
	m_bbWidth = D3DAPPI->GetD3Dpp().BackBufferWidth * 5.5/8.0f;
}

void Score::Init()
{
	m_fSpeed = m_iPlayerID = m_iTimeLeft =  0;

	mMultiplier = 1.0f;
}

///////////////////
// MUTATORS
//////////////////
void Score::SetID(int id)
{
	m_iPlayerID = id;
}
void Score::SetTime(int time)
{
	m_iTimeLeft = time;
}
void Score::SetSpeed(float speed)
{
	m_fSpeed = speed;
}

void Score::SetDistance(float dist)
{
	m_fDistance = dist;
}

void Score::HitRing()
{
	m_sHitRing->play();

	/*mRingsHit++;

	mScore += 100 * mMultiplier;*/

	mMultiplier += .5;
}

void Score::MissedRing()
{
	m_sMissRing->play();
	
	/*mRingsMissed += 1;*/

	mMultiplier = 1.0f;
}


void Score::Update(float dt)
{
	
	
}

void Score::Render()
{
	// Make static so memory is not allocated every frame.
	static char buffer[256];

	sprintf_s(buffer, "Player Speed = %.2f\n"
		"Player ID: %i\n"
		"Dist. Travelled: %.2f\n"
		"Time Until Race: %i\n" , m_fSpeed, m_iPlayerID, m_fDistance, m_iTimeLeft, mMultiplier);

	



	RECT R = {m_bbWidth, m_bbHeight , 0, 0};
	//getting display rect


	
	mFont->DrawTextA(NULL, buffer,-1,&R,DT_CALCRECT, D3DCOLOR_XRGB(255,255,255));
	mFont->DrawTextA(NULL, buffer, -1, &R, DT_NOCLIP, D3DCOLOR_XRGB(255,255,255));
}