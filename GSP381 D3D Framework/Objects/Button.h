#pragma once

template<class menu_type>
class Button{
private:

public:
	void (menu_type::*ButtonFunc)();

	menu_type *ButtonObj;

	Button(menu_type *Menu, void(menu_type::*ButtonFunc)());
	void OnClick();
};

template<class menu_type>
Button<menu_type>::Button(menu_type *Menu, void(menu_type::*ButtonFunc)())
	{
		this->ButtonObj = Menu;
		this->ButtonFunc = ButtonFunc;
	}

template<class menu_type>
void Button<menu_type>::OnClick()
	{
		((ButtonObj)->*(ButtonFunc))();
	}

