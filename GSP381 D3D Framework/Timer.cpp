#include "Timer.h"

Timer::Timer()
{

	__int64 cntsPerSec = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&cntsPerSec);
	secsPerCnt = 1.0f / (float)cntsPerSec;

	__int64 prevTimeStamp = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&prevTimeStamp);

	
}

Timer* Timer::Instance()
{
	static Timer inst;
	return &inst;
}

void Timer::Start()
{
	QueryPerformanceCounter((LARGE_INTEGER*)&prevTimeStamp);
	
}

void Timer::Stop()
{
	QueryPerformanceCounter(&Count);
	end = ((double)Count.QuadPart / (double)Freq.QuadPart);
}

float Timer::GetDT()
{
	
	QueryPerformanceCounter((LARGE_INTEGER*)&currentTimeStamp);
	dt = (currentTimeStamp - prevTimeStamp)*secsPerCnt;

	return dt;

}

double Timer::GetTime()
{
	elapsed = end - begin;
	return elapsed;
}

void Timer::Clear()
{
	begin = end = elapsed = 0;
	QueryPerformanceFrequency(&Freq);
	
}

void Timer::Lap()
{
	prevTimeStamp = currentTimeStamp;
}