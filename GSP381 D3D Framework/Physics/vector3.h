#pragma once

#include <cmath>
#include "functions.h"
#include "Matrix3.h"
#include <d3dx9.h>

/*
	Adapeted to derive from D3DXVECTOR3 and use its built-in functions when possible
*/

class Vector3	// the physics kind, not the stl kind
	:public D3DXVECTOR3
{
private:
	
public:
	
	//////////////
	// const/dest
	/////////////
	Vector3();  //constructor
	Vector3(float xval, float yval, float zval);  //para const.
	Vector3( const Vector3& other);		// copy const
	Vector3( const D3DXVECTOR3& other); // copy const
	~Vector3(); // destructor


	/////////////////////
	// ACCESSORS - directly returns a value
	////////////////////
	float Magnitude();			// returns the magnitude of the vector
	float MagnitudeSquared() const;	// returns the magnitude of the vector squared
	

	
	///////////////////////
	/// MUTATORS - directly affects the stored value
	//////////////////////
	void Scale(float c);			// used to scale the vector by factor c
	void Normalize();			// uses the magnitude () function to Normalize the vector
	void Clear();				// clears x,y,z to 0
	void Invert();				// scales all values by -1
	void AddScaledVector(const Vector3& vector, float scale); // adds a scaled vector to the stored value
	void Average(int divisor);	// averages the vector by the divisor 


	//////////////////////
	// MATH OPERATIONS - returns a value using the stored value (does not edit)
	/////////////////////
	Vector3 ScaleReturn(float c);	// returns scaled vector 
	Vector3 NormalizeRet();			// normalizes the vector
	Vector3 CrossProductReturn (const Vector3 &rhs); // returns cross product value
	Vector3 CrossProduct (Vector3 vectA, Vector3 vectB); // crosses the two vectors
	Vector3 InvertReturn();			// returns inverted value
	Vector3 ComponentProduct(const Vector3 &vector) const; // multiplies components of vector
	void ComponentProductUpdate(const Vector3 &vector); // mult components of vectors, sets var as result
	
	

	
	//overloaded operators


	Vector3 operator - (const Vector3 &rhs) const;
	void operator -= (const Vector3 &v);
	Vector3 operator + (const Vector3& v) const;
	void operator += (const Vector3& v);
	float operator * (const Vector3 &rhs) const ;
	Vector3 operator * (const float &rhs) const;
	void operator *= (const float v);
	
	Vector3 operator % (const Vector3 &rhs);
	Vector3 operator %= (const Vector3 &rhs);
	Vector3 operator = (const Vector3 &rhs);
	Vector3 operator = (const D3DXVECTOR3 &rhs);
	float& operator[](unsigned int pInd);

	//bools
	bool operator <		(const Vector3 &rhs);
	bool operator <=	(const Vector3 &rhs);
	bool operator >		(const Vector3 &rhs);
	bool operator >=	(const Vector3 &rhs);
	bool operator ==	(const Vector3 &rhs);
	bool operator !=	(const Vector3 &rhs);
	


};


/*
D3DXVec3BaryCentric
D3DXVec3CatmullRom
D3DXVec3Hermite
D3DXVec3Normalize
D3DXVec3Project
D3DXVec3ProjectArray
D3DXVec3Transform
D3DXVec3TransformArray
D3DXVec3TransformCoord
D3DXVec3TransformCoordArray
D3DXVec3TransformNormal
D3DXVec3TransformNormalArray
D3DXVec3Unproject
D3DXVec3UnprojectArray
*/