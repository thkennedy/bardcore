#include "AdvCollisions.h"
#include "Contacts.h"
#include "MovementManager.h"
#include "Circle.h"
#include "OBB.h"
#include "Plane.h"
#include "Ball.h"
#include "Box.h"
#include "ContactGenerator.h"
#include "AdvancedBV.h"
#include "../Graphics/ShapeManager.h"





bool IntersectionTests::SphereAndHalfSpace(const Circle &sphere, const Plane &plane)
{
    // Find the distance from the origin
    float ballDistance;
	ballDistance = plane.GetNormal() * sphere.GetPosition() - sphere.GetRadius();

    // Check for the intersection
    return ballDistance <= plane.GetOffset();
}

bool IntersectionTests::SphereAndSphere(
    const Circle &one,
    const Circle &two)
{
    // Find the vector between the objects
	Vector3 midline = one.GetPosition() - two.GetPosition();

    // See if it is large enough.
    return midline.MagnitudeSquared() <
        (one.GetRadius()+two.GetRadius())*(one.GetRadius()+two.GetRadius());
}

static inline float TransformToAxis(const OBB &box, const Vector3 &axis )
{
    return
		box.GetHalfExtents().x * fabs(axis * box.GetAxis(0)) +
        box.GetHalfExtents().y * fabs(axis * box.GetAxis(1)) +
        box.GetHalfExtents().z * fabs(axis * box.GetAxis(2));
}

/**
 * This function checks if the two boxes overlap
 * along the given axis. The final parameter toCentre
 * is used to pass in the vector between the boxes centre
 * points, to avoid having to recalculate it each time.
 */
static inline bool OverlapOnAxis(
    const OBB &one,
    const OBB &two,
    const Vector3 &axis,
    const Vector3 &toCentre
    )
{
    // Project the half-size of one onto axis
    float oneProject = TransformToAxis(one, axis);
    float twoProject = TransformToAxis(two, axis);

    // Project this onto the axis
    float distance = fabs(toCentre * axis);

    // Check for overlap
    return (distance < oneProject + twoProject);
}

// This preprocessor definition is only used as a convenience
// in the boxAndBox intersection  method.
#define TEST_OVERLAP(axis) OverlapOnAxis(one, two, (axis), toCentre)

bool IntersectionTests::BoxAndBox(
    const OBB &one,
    const OBB &two
    )
{

	// Find the vector between the two centres
	Vector3 toCentre = two.GetPosition() - one.GetPosition();




	//if (
 //       // Check on box one's axes first
 //       TEST_OVERLAP(one.GetAxis(0)) &&
 //       TEST_OVERLAP(one.GetAxis(1)) &&
 //       TEST_OVERLAP(one.GetAxis(2)) )
	//{
	//	Sleep(1);
	//}

 //       // And on two's
 //    if (   TEST_OVERLAP(two.GetAxis(0)) &&
 //       TEST_OVERLAP(two.GetAxis(1)) &&
 //       TEST_OVERLAP(two.GetAxis(2)) )
	// {
	//	 Sleep(1);
	// }

 //    if (   // Now on the cross products
 //       TEST_OVERLAP(one.GetAxis(0) % two.GetAxis(0) + Vector3(.01,.01,.01)) &&
 //       TEST_OVERLAP(one.GetAxis(0) % two.GetAxis(1) + Vector3(.01,.01,.01)) &&
 //       TEST_OVERLAP(one.GetAxis(0) % two.GetAxis(2) + Vector3(.01,.01,.01)) )
	// {
	//	 Sleep(1);
	// }
	// if (
 //       TEST_OVERLAP(one.GetAxis(1) % two.GetAxis(0) + Vector3(.01,.01,.01)) &&
 //       TEST_OVERLAP(one.GetAxis(1) % two.GetAxis(1) + Vector3(.01,.01,.01)) &&
 //       TEST_OVERLAP(one.GetAxis(1) % two.GetAxis(2) + Vector3(.01,.01,.01))  )
	// {
	//	Sleep(1);
	// }

	// if (

 //       TEST_OVERLAP(one.GetAxis(2) % two.GetAxis(0) + Vector3(.01,.01,.01)) &&
 //       TEST_OVERLAP(one.GetAxis(2) % two.GetAxis(1) + Vector3(.01,.01,.01)) &&
 //       TEST_OVERLAP(one.GetAxis(2) % two.GetAxis(2) + Vector3(.01,.01,.01)))
	// {
	//	 Sleep(1);
	// }

	// return 0;

    return (
        // Check on box one's axes first
        TEST_OVERLAP(one.GetAxis(0)) &&
        TEST_OVERLAP(one.GetAxis(1)) &&
        TEST_OVERLAP(one.GetAxis(2)) &&

        // And on two's
        TEST_OVERLAP(two.GetAxis(0)) &&
        TEST_OVERLAP(two.GetAxis(1)) &&
        TEST_OVERLAP(two.GetAxis(2)) &&

        // Now on the cross products
        TEST_OVERLAP(one.GetAxis(0) % two.GetAxis(0)+ Vector3(.01f,.01f,.01f)) &&
        TEST_OVERLAP(one.GetAxis(0) % two.GetAxis(1)+ Vector3(.01f,.01f,.01f)) &&
        TEST_OVERLAP(one.GetAxis(0) % two.GetAxis(2)+ Vector3(.01f,.01f,.01f)) &&
        TEST_OVERLAP(one.GetAxis(1) % two.GetAxis(0)+ Vector3(.01f,.01f,.01f)) &&
        TEST_OVERLAP(one.GetAxis(1) % two.GetAxis(1)+ Vector3(.01f,.01f,.01f)) &&
        TEST_OVERLAP(one.GetAxis(1) % two.GetAxis(2)+ Vector3(.01f,.01f,.01f)) &&
        TEST_OVERLAP(one.GetAxis(2) % two.GetAxis(0)+ Vector3(.01f,.01f,.01f)) &&
        TEST_OVERLAP(one.GetAxis(2) % two.GetAxis(1)+ Vector3(.01f,.01f,.01f)) &&
        TEST_OVERLAP(one.GetAxis(2) % two.GetAxis(2)+ Vector3(.01f,.01f,.01f))
    );
}
#undef TEST_OVERLAP

bool IntersectionTests::BoxAndHalfSpace(
    const OBB &box,
    const Plane &plane
    )
{
    // Work out the projected radius of the box onto the plane direction
    float projectedRadius = TransformToAxis(box, plane.GetNormal());

    // Work out how far the box is from the origin
    float boxDistance = plane.GetNormal() * box.GetPosition() - projectedRadius;

    // Check for the intersection
    return boxDistance <= plane.GetOffset();
}

Vector3 ClosestPtOBB(const Vector3* p,const OBB b, Vector3 &q)
{

	Vector3 d = *p - b.GetPosition();

	// Start result at center of box; make steps from there
	q = b.GetPosition();

	// For each OBB axis...
	for (int i= 0; i < 3; i++) 
	{
		// project d onto that axis to get distance of d along that axis from the box center
		float dist = d * b.GetLocalAxis(i);
		// If distance farther than the box extents, clamp to the box
		if (dist > b.GetHalfExtents(i))
		dist = b.GetHalfExtents(i);
		if (dist < - b.GetHalfExtents(i))
			dist = - b.GetHalfExtents(i);
		// Step that distance along the axis to get world coordinate
		q += dist * b.GetLocalAxis(i);
	}

	return q;
}

bool IntersectionTests::CircleOBB(const Circle * Circle,const OBB * Box)
{
	Vector3 p;
		
	//find point p on AABB closest to sphere center
	ClosestPtOBB(&Circle->GetPosition(), *Box, p);

		
	// Sphere and AABB intersect if the (squared) distance from sphere center to
	// point p is less than the (squared) sphere radius
	Vector3 v = p - Circle->GetPosition();
	return v * v <= Circle->GetRadius() * Circle->GetRadius();
	

	
}

bool IntersectionTests::CircleCircleDet(const BoundingVolume *BV1,const BoundingVolume *BV2)
{


	Vector3 vObject1 = BV1->GetPosition(),
			vObject2 = BV2->GetPosition();
	float	r2 = BV2->GetRadius(),
			r1 = BV1->GetRadius();

	if (	(vObject2.x-vObject1.x)*(vObject2.x-vObject1.x) + 
			(vObject2.y-vObject1.y)*(vObject2.y-vObject1.y) +
			(vObject2.z-vObject1.z)*(vObject2.z-vObject1.z) <= (r2 + r1)*(r2 + r1)	)
	{
		
		

		
		return true;

	}//end if

				
	return false;
}

bool IntersectionTests::CircleOBB2(const Circle * Ball ,const OBB * Box)
{
	
	// level 1 test
	if (CircleCircleDet(Ball,Box)) 
	{
		// if inside radius
		// translate ball into box local
		Vector3 vBallLocalPos = Box->GetTransform().TransformInverse(Ball->GetPosition());

		// if ball abs x,y,z is < half extents, then its colliding
		if (fabs(vBallLocalPos.x) < Box->GetHalfExtents().x)
			if (fabs(vBallLocalPos.y) < Box->GetHalfExtents().y)
				if (fabs(vBallLocalPos.z) < Box->GetHalfExtents().z)
					return true;
		
	}
	
	return 0;
	
}









unsigned ACollisionDetector::SphereAndTruePlane(
    const Circle &sphere,
    const Plane &plane,
    CollisionData *data
    )
{
    // Make sure we have m_pContacts
    if (data->m_iContactsLeft <= 0) return 0;

    // Cache the sphere position
    Vector3 position = sphere.GetPosition();

    // Find the distance from the plane
    float centreDistance = plane.GetNormal() * position - plane.GetOffset();

    // Check if we're within radius
    if (centreDistance*centreDistance > sphere.GetRadius()*sphere.GetRadius())
    {
        return 0;
    }

    // Check which side of the plane we're on
    Vector3 normal = plane.GetNormal();
    float penetration = -centreDistance;
    if (centreDistance < 0)
    {
        normal *= -1;
        penetration = -penetration;
    }
    penetration += sphere.GetRadius();

    // Create the contact - it has a normal in the plane direction.
    Contact* contact = data->m_pContacts;
    contact->m_vContactNormal = normal;
    contact->m_fPenetration = penetration;
    contact->m_vContactPoint = position - plane.GetNormal() * centreDistance;
	contact->SetBodyData(MMI->GetBody(sphere.GetBodyID()), NULL,
		data->m_fFriction, data->m_fRestitution);

    data->AddContacts(1);
    return 1;
}

unsigned ACollisionDetector::SphereAndHalfSpace(
    const Circle &sphere,
    const Plane &plane,
    CollisionData *data
    )
{
    // Make sure we have m_pContacts
    if (data->m_iContactsLeft <= 0) return 0;

    // Cache the sphere position
    Vector3 position = sphere.GetPosition();

    // Find the distance from the plane
    float ballDistance =
        plane.GetNormal() * position -
        sphere.GetRadius() - plane.GetOffset();

    if (ballDistance >= 0) return 0;

    // Create the contact - it has a normal in the plane direction.
    Contact* contact = data->m_pContacts;
    contact->m_vContactNormal = plane.GetNormal();
    contact->m_fPenetration = -ballDistance;
    contact->m_vContactPoint = position - plane.GetNormal() * (ballDistance + sphere.GetRadius());
    contact->SetBodyData(MMI->GetBody(sphere.GetBodyID()), NULL,
        data->m_fFriction, data->m_fRestitution);

    data->AddContacts(1);
    return 1;
}

unsigned ACollisionDetector::SphereAndSphere(
    const Circle &one,
    const Circle &two,
    CollisionData *data
    )
{
    // Make sure we have m_pContacts
    if (data->m_iContactsLeft <= 0) return 0;

    // Cache the sphere positions
	Vector3 positionOne = one.GetPosition();
    Vector3 positionTwo = two.GetPosition();

    // Find the vector between the objects
    Vector3 midline = positionOne - positionTwo;
    float size = midline.Magnitude();

    // See if it is large enough.
    if (size <= 0.0f || size >= one.GetRadius()+two.GetRadius())
    {
        return 0;
    }

    // We manually create the normal, because we have the
    // size to hand.
    Vector3 normal = midline * (((float)1.0)/size);

    Contact* contact = data->m_pContacts;
    contact->m_vContactNormal = normal;
    contact->m_vContactPoint = positionOne + midline * (float)0.5;
    contact->m_fPenetration = (one.GetRadius()+two.GetRadius() - size);
    contact->SetBodyData(MMI->GetBody(one.GetBodyID()), MMI->GetBody(two.GetBodyID()),
		data->m_fFriction, data->m_fRestitution);

    data->AddContacts(1);
    return 1;
}

unsigned ACollisionDetector::SphereAndAdvBV(
    const Circle &one,
    const AdvancedBV &two,
    CollisionData *data
    )
{
	//vector that will be used for the children of the circle
	std::vector<OBB*> vChildren;
	OBB pBone;
	bool bContactCreated = false;

	//first layer
	if (IntersectionTests::SphereAndSphere(one, *two.GetOutterCircle()) )
	{
		
		tree<BoundingVolume*>::iterator Iter = two.GoToRoot();
		
		if (ACollisionDetector::AdvSphereAndBox(two, one, Iter, data, pBone, bContactCreated)) 
		{
			//SMI->SetColor(pBone.m_iGraphicsID, RED);
			return 1;
		}
		
	}

	return 0;
}

bool ACollisionDetector::AdvSphereAndBox(const AdvancedBV &advBV, const Circle &sphere, 
	tree<BoundingVolume*>::iterator &Iter, CollisionData *data, OBB &pBone, bool &bContactCreated)
{
	if (Iter == NULL) 
		return 0;

	//vector that will be used for the children of the advanced bv
	std::vector<OBB*> vChildren;

	// get children
	vChildren = advBV.GetChildren(Iter);

	// if there are no more children, vec size will be 0
	if (vChildren.size() == 0) 
		return true;

	//set Iterator to its child being searched
		Iter = Iter.node->first_child;

		// keep track of bone being checked
			if (Iter != NULL)
			{
				OBB* temp = (OBB*)Iter.node->data;
				pBone = *temp;
			}

		//// keep track of bone being checked
		//OBB* temp = (OBB*)Iter.node->data;
		//pBone = *temp;

	for (auto it = vChildren.begin(); it != vChildren.end(); ++it)
	{

		if (Iter == NULL) 
		return 0;

		OBB box = *(*it);

		 // Transform the centre of the sphere into box coordinates
		Vector3 centre = sphere.GetPosition();
		Vector3 relCentre = box.GetTransform().TransformInverse(centre);

		// Early out check to see if we can exclude the contact
		if (fabs(relCentre.x) - sphere.GetRadius() > box.GetHalfExtents().x ||
			fabs(relCentre.y) - sphere.GetRadius() > box.GetHalfExtents().y ||
			fabs(relCentre.z) - sphere.GetRadius() > box.GetHalfExtents().z)
		{
			Iter = Iter.node->next_sibling;
			// keep track of bone being checked
			if (Iter != NULL)
			{
				OBB* temp = (OBB*)Iter.node->data;
				pBone = *temp;
			}
			continue;
			
			//return 0;
		}

		Vector3 closestPt(0,0,0);
		float dist;

		// Clamp each coordinate to the box.
		dist = relCentre.x;
		if (dist > box.GetHalfExtents().x) dist = box.GetHalfExtents().x;
		if (dist < -box.GetHalfExtents().x) dist = -box.GetHalfExtents().x;
		closestPt.x = dist;

		dist = relCentre.y;
		if (dist > box.GetHalfExtents().y) dist = box.GetHalfExtents().y;
		if (dist < -box.GetHalfExtents().y) dist = -box.GetHalfExtents().y;
		closestPt.y = dist;

		dist = relCentre.z;
		if (dist > box.GetHalfExtents().z) dist = box.GetHalfExtents().z;
		if (dist < -box.GetHalfExtents().z) dist = -box.GetHalfExtents().z;
		closestPt.z = dist;

		// Check we're in contact
		dist = (closestPt - relCentre).MagnitudeSquared();
		if (dist > sphere.GetRadius() * sphere.GetRadius()) 
		{
			Iter = Iter.node->next_sibling;
			// keep track of bone being checked
			if (Iter != NULL)
			{
				OBB* temp = (OBB*)Iter.node->data;
				pBone = *temp;
			}

			continue;
		}
			//return false;
		
		// recursion to go deeper until no children left
		// another iterator to keep our place
		tree<BoundingVolume*>::iterator iterate = Iter;

		// if no children, we are contacting the last body - return true
		if (iterate == NULL)
		{
			//SMI->SetColor(pBone->m_iGraphicsID,RED);

			// Compile the contact
			Vector3 closestPtWorld = box.GetTransform().Transform(closestPt);

			Contact* contact = data->m_pContacts;
			contact->m_vContactNormal = (closestPtWorld - centre);
			contact->m_vContactNormal.Normalize();
			contact->m_vContactPoint = closestPtWorld;
			contact->m_fPenetration = sphere.GetRadius() - sqrtf(dist);
			contact->SetBodyData(NULL, MMI->GetBody(sphere.GetBodyID()),
				data->m_fFriction, data->m_fRestitution);

			data->AddContacts(1);
			return true;
		}
		
		//call function recursively
		if (AdvSphereAndBox(advBV,sphere,iterate, data, pBone, bContactCreated))
		{
			if (bContactCreated == false)
			{
			
				// Compile the contact
				Vector3 closestPtWorld = box.GetTransform().Transform(closestPt);

				Contact* contact = data->m_pContacts;
				contact->m_vContactNormal = (closestPtWorld - centre);
				contact->m_vContactNormal.Normalize();
				contact->m_vContactPoint = closestPtWorld;
				contact->m_fPenetration = sphere.GetRadius() - sqrtf(dist);
				contact->SetBodyData(NULL, MMI->GetBody(sphere.GetBodyID()),
					data->m_fFriction, data->m_fRestitution);

				data->AddContacts(1);

				bContactCreated = true;
			}
			
			
			return true;


		}
					
	}

	return false;
	

}

/*
 * This function checks if the two boxes overlap
 * along the given axis, returning the ammount of overlap.
 * The final parameter toCentre
 * is used to pass in the vector between the boxes centre
 * points, to avoid having to recalculate it each time.
 */
static inline float PenetrationOnAxis(
    const OBB &one,
    const OBB &two,
    const Vector3 &axis,
    const Vector3 &toCentre
    )
{
    // Project the half-size of one onto axis
    float oneProject = TransformToAxis(one, axis);
    float twoProject = TransformToAxis(two, axis);

    // Project this onto the axis
    float distance = fabs(toCentre * axis);

    // Return the overlap (i.e. positive indicates
    // overlap, negative indicates separation).
    return oneProject + twoProject - distance;
}


static inline bool TryAxis(
    const OBB &one,
    const OBB &two,
    Vector3& axis,
    const Vector3& toCentre,
    unsigned index,

    // These values may be updated
    float& smallestPenetration,
    unsigned &smallestCase
    )
{
    // Make sure we have a normalized axis, and don't check almost parallel axes
    if (axis.MagnitudeSquared() < 0.0001) return true;
    axis.Normalize();

    float penetration = PenetrationOnAxis(one, two, axis, toCentre);

    if (penetration < 0) return false;
    if (penetration < smallestPenetration) 
	{
        smallestPenetration = penetration;
        smallestCase = index;
    }
    return true;
}

void FillPointFaceBoxBox(
    const OBB &one,
    const OBB &two,
    const Vector3 &toCentre,
    CollisionData *data,
    unsigned best,
    float pen
    )
{
    // This method is called when we know that a vertex from
    // box two is in contact with box one.

    Contact* contact = data->m_pContacts;

    // We know which axis the collision is on (i.e. best),
    // but we need to work out which of the two faces on
    // this axis.
    Vector3 normal = one.GetAxis(best);
    if (one.GetAxis(best) * toCentre > 0)
    {
        normal = normal * -1.0f;
    }

    // Work out which vertex of box two we're colliding with.
    // Using toCentre doesn't work!
    Vector3 vertex = two.GetHalfExtents();
    if (two.GetAxis(0) * normal < 0) vertex.x = -vertex.x;
    if (two.GetAxis(1) * normal < 0) vertex.y = -vertex.y;
    if (two.GetAxis(2) * normal < 0) vertex.z = -vertex.z;

	if (one.GetBodyID() != -1)
	{
		// Create the contact data
		contact->m_vContactNormal = normal;
		contact->m_fPenetration = pen;
		contact->m_vContactPoint = two.GetTransform() * vertex;
		contact->SetBodyData(MMI->GetBody(one.GetBodyID()), MMI->GetBody(two.GetBodyID()),
			data->m_fFriction, data->m_fRestitution);
	}

	else
	{
		// Create the contact data
		contact->m_vContactNormal = normal;
		contact->m_fPenetration = pen;
		contact->m_vContactPoint = two.GetTransform() * vertex;
		contact->SetBodyData(NULL, MMI->GetBody(two.GetBodyID()),
			data->m_fFriction, data->m_fRestitution);
	}
}

static inline Vector3 ContactPoint(
    const Vector3 &pOne,
    const Vector3 &dOne,
    float oneSize,
    const Vector3 &pTwo,
    const Vector3 &dTwo,
    float twoSize,

    // If this is true, and the contact point is outside
    // the edge (in the case of an edge-face contact) then
    // we use one's midpoint, otherwise we use two's.
    bool useOne)
{
    Vector3 toSt, cOne, cTwo;
    float dpStaOne, dpStaTwo, dpOneTwo, smOne, smTwo;
    float denom, mua, mub;

    smOne = dOne.MagnitudeSquared();
    smTwo = dTwo.MagnitudeSquared();
    dpOneTwo = dTwo * dOne;

    toSt = pOne - pTwo;
    dpStaOne = dOne * toSt;
    dpStaTwo = dTwo * toSt;

    denom = smOne * smTwo - dpOneTwo * dpOneTwo;

    // Zero denominator indicates parrallel lines
    if (fabs(denom) < 0.0001f) {
        return useOne?pOne:pTwo;
    }

    mua = (dpOneTwo * dpStaTwo - smTwo * dpStaOne) / denom;
    mub = (smOne * dpStaTwo - dpOneTwo * dpStaOne) / denom;

    // If either of the edges has the nearest point out
    // of bounds, then the edges aren't crossed, we have
    // an edge-face contact. Our point is on the edge, which
    // we know from the useOne parameter.
    if (mua > oneSize ||
        mua < -oneSize ||
        mub > twoSize ||
        mub < -twoSize)
    {
        return useOne?pOne:pTwo;
    }
    else
    {
        cOne = pOne + dOne * mua;
        cTwo = pTwo + dTwo * mub;

        return cOne * 0.5 + cTwo * 0.5;
    }
}



// This preprocessor definition is only used as a convenience
// in the boxAndBox contact generation method.
#define CHECK_OVERLAP(axis, index) \
    if (!TryAxis(one, two, (axis), toCentre, (index), pen, best)) return 0;

void TimeStepping (OBB &one, OBB &two, float dt, unsigned iterations)
{
	//divide last dt into number of iterations
	float fTimeStep = dt / iterations;

	// use the inverse velocity to go back and find when penetration 
	// is the closest to 0 without penetration
	// get inverse velocities of one and two
	Vector3 invOne, invTwo;
	invOne = MMI->GetVelocity(one.GetBodyID()).ScaleReturn(-1); 
	invTwo = MMI->GetVelocity(two.GetBodyID()).ScaleReturn(-1); 

	//from the first iteration until I get the first < 0 answer, I move and re-check penetration
	Vector3 onePos, twoPos;
	onePos.Clear(); twoPos.Clear();
	for (unsigned i = 0; i < iterations; ++i)
	{
		one.SetPosition(one.GetPosition() + invOne.ScaleReturn(fTimeStep));
		two.SetPosition(two.GetPosition() + invTwo.ScaleReturn(fTimeStep));
		
	}
}

unsigned ACollisionDetector::BoxAndBox(
    const OBB &one,
    const OBB &two,
    CollisionData *data
    )
{
    if (!IntersectionTests::BoxAndBox(one, two)) return 0;

    // Find the vector between the two centres
    Vector3 toCentre = two.GetPosition() - one.GetPosition();

    // We start assuming there is no contact
    float pen = FLT_MAX;
    unsigned best = 0xffffff;

    // Now we check each axes, returning if it gives us
    // a separating axis, and keeping track of the axis with
    // the smallest penetration otherwise.
    CHECK_OVERLAP(one.GetAxis(0), 0);
    CHECK_OVERLAP(one.GetAxis(1), 1);
    CHECK_OVERLAP(one.GetAxis(2), 2);

    CHECK_OVERLAP(two.GetAxis(0), 3);
    CHECK_OVERLAP(two.GetAxis(1), 4);
    CHECK_OVERLAP(two.GetAxis(2), 5);

    // Store the best axis-major, in case we run into almost
    // parallel edge collisions later
    unsigned bestSingleAxis = best;

    CHECK_OVERLAP(one.GetAxis(0) % two.GetAxis(0), 6);
    CHECK_OVERLAP(one.GetAxis(0) % two.GetAxis(1), 7);
    CHECK_OVERLAP(one.GetAxis(0) % two.GetAxis(2), 8);
    CHECK_OVERLAP(one.GetAxis(1) % two.GetAxis(0), 9);
    CHECK_OVERLAP(one.GetAxis(1) % two.GetAxis(1), 10);
    CHECK_OVERLAP(one.GetAxis(1) % two.GetAxis(2), 11);
    CHECK_OVERLAP(one.GetAxis(2) % two.GetAxis(0), 12);
    CHECK_OVERLAP(one.GetAxis(2) % two.GetAxis(1), 13);
    CHECK_OVERLAP(one.GetAxis(2) % two.GetAxis(2), 14);

    // Make sure we've got a result.
    assert(best != 0xffffff);

    // We now know there's a collision, and we know which
    // of the axes gave the smallest penetration. We now
    // can deal with it in different ways depending on
    // the case.
    if (best < 3)
    {
        // We've got a vertex of box two on a face of box one.
        FillPointFaceBoxBox(one, two, toCentre, data, best, pen);
        data->AddContacts(1);
        return 1;
    }
    else if (best < 6)
    {
        // We've got a vertex of box one on a face of box two.
        // We use the same algorithm as above, but swap around
        // one and two (and therefore also the vector between their
        // centres).
        FillPointFaceBoxBox(two, one, toCentre*-1.0f, data, best-3, pen);
        data->AddContacts(1);
        return 1;
    }
    else
    {
        // We've got an edge-edge contact. Find out which axes
        best -= 6;
        unsigned oneAxisIndex = best / 3;
        unsigned twoAxisIndex = best % 3;
        Vector3 oneAxis = one.GetAxis(oneAxisIndex);
        Vector3 twoAxis = two.GetAxis(twoAxisIndex);
        Vector3 axis = oneAxis % twoAxis;
        axis.Normalize();

        // The axis should point from box one to box two.
        if (axis * toCentre > 0) axis = axis * -1.0f;

        // We have the axes, but not the edges: each axis has 4 edges parallel
        // to it, we need to find which of the 4 for each object. We do
        // that by finding the point in the centre of the edge. We know
        // its component in the direction of the box's collision axis is zero
        // (its a mid-point) and we determine which of the extremes in each
        // of the other axes is closest.
        Vector3 ptOnOneEdge = one.GetHalfExtents();
        Vector3 ptOnTwoEdge = two.GetHalfExtents();
        for (unsigned i = 0; i < 3; i++)
        {
            if (i == oneAxisIndex) ptOnOneEdge[i] = 0;
            else if (one.GetAxis(i) * axis > 0) ptOnOneEdge[i] = -ptOnOneEdge[i];

            if (i == twoAxisIndex) ptOnTwoEdge[i] = 0;
            else if (two.GetAxis(i) * axis < 0) ptOnTwoEdge[i] = -ptOnTwoEdge[i];
        }

        // Move them into world coordinates (they are already oriented
        // correctly, since they have been derived from the axes).
        ptOnOneEdge = one.GetTransform() * ptOnOneEdge;
        ptOnTwoEdge = two.GetTransform() * ptOnTwoEdge;

        // So we have a point and a direction for the colliding edges.
        // We need to find out point of closest approach of the two
        // line-segments.
        Vector3 vertex = ContactPoint(
            ptOnOneEdge, oneAxis, one.GetHalfExtents()[oneAxisIndex],
            ptOnTwoEdge, twoAxis, two.GetHalfExtents()[twoAxisIndex],
            bestSingleAxis > 2
            );

        // We can fill the contact.
        Contact* contact = data->m_pContacts;

        contact->m_fPenetration = pen;
        contact->m_vContactNormal = axis;
        contact->m_vContactPoint = vertex;
        contact->SetBodyData(MMI->GetBody(one.GetBodyID()), MMI->GetBody(two.GetBodyID()),
            data->m_fFriction, data->m_fRestitution);
        data->AddContacts(1);
        return 1;
    }
    return 0;
}
#undef CHECK_OVERLAP




unsigned ACollisionDetector::BoxAndPoint(
    const OBB &box,
    const Vector3 &point,
    CollisionData *data
    )
{
    // Transform the point into box coordinates
    Vector3 relPt = box.GetTransform().TransformInverse(point);

    Vector3 normal;

    // Check each axis, looking for the axis on which the
    // penetration is least deep.
    float min_depth = box.GetHalfExtents().x - fabs(relPt.x);
    if (min_depth < 0) return 0;
    normal = box.GetAxis(0) * ((relPt.x < 0)?(float)-1:(float)1);

    float depth = box.GetHalfExtents().y - fabs(relPt.y);
    if (depth < 0) return 0;
    else if (depth < min_depth)
    {
        min_depth = depth;
        normal = box.GetAxis(1) * ((relPt.y < 0)?(float)-1:(float)1);
    }

    depth = box.GetHalfExtents().z - fabs(relPt.z);
    if (depth < 0) return 0;
    else if (depth < min_depth)
    {
        min_depth = depth;
        normal = box.GetAxis(2) * ((relPt.z < 0)?(float)-1:(float)1);
    }

    // Compile the contact
    Contact* contact = data->m_pContacts;
    contact->m_vContactNormal = normal;
    contact->m_vContactPoint = point;
    contact->m_fPenetration = min_depth;

    // Note that we don't know what rigid body the point
    // belongs to, so we just use NULL. Where this is called
    // this value can be left, or filled in.
    contact->SetBodyData(MMI->GetBody(box.GetBodyID()), NULL,
        data->m_fFriction, data->m_fRestitution);

    data->AddContacts(1);
    return 1;
}

unsigned ACollisionDetector::BoxAndSphere(
    const OBB &box,
    const Circle &sphere,
    CollisionData *data
    )
{
    // Transform the centre of the sphere into box coordinates
	Vector3 centre = sphere.GetPosition();
    Vector3 relCentre = box.GetTransform().TransformInverse(centre);

    // Early out check to see if we can exclude the contact
    if (fabs(relCentre.x) - sphere.GetRadius() > box.GetHalfExtents().x ||
        fabs(relCentre.y) - sphere.GetRadius() > box.GetHalfExtents().y ||
        fabs(relCentre.z) - sphere.GetRadius() > box.GetHalfExtents().z)
    {
        return 0;
    }

    Vector3 closestPt(0,0,0);
    float dist;

    // Clamp each coordinate to the box.
    dist = relCentre.x;
    if (dist > box.GetHalfExtents().x) 
		dist = box.GetHalfExtents().x;
    
	if (dist < -box.GetHalfExtents().x) 
		dist = -box.GetHalfExtents().x;
    
	closestPt.x = dist;

    dist = relCentre.y;
    if (dist > box.GetHalfExtents().y) 
		dist = box.GetHalfExtents().y;
    
	if (dist < -box.GetHalfExtents().y) 
		dist = -box.GetHalfExtents().y;
    closestPt.y = dist;

    dist = relCentre.z;
    if (dist > box.GetHalfExtents().z) 
		dist = box.GetHalfExtents().z;
    if (dist < -box.GetHalfExtents().z) 
		dist = -box.GetHalfExtents().z;

    closestPt.z = dist;

    // Check we're in contact with outter point
    dist = (closestPt - relCentre).MagnitudeSquared();
    if (dist > sphere.GetRadius() * sphere.GetRadius())
	{
		// check to see if ball is inside box
		if (IntersectionTests::CircleOBB2(&sphere,&box))
		{
			Sleep(0);
		}
		else
			return 0;
	}

    // Compile the contact
	Vector3 closestPtWorld = box.GetTransform().Transform(closestPt);

    Contact* contact = data->m_pContacts;
    contact->m_vContactNormal = (closestPtWorld - centre);
	contact->m_vContactNormal.Normalize();
    contact->m_vContactPoint = closestPtWorld;
    contact->m_fPenetration = sphere.GetRadius() - sqrtf(dist);
    contact->SetBodyData(MMI->GetBody(box.GetBodyID()), MMI->GetBody(sphere.GetBodyID()),
        data->m_fFriction, data->m_fRestitution);

    data->AddContacts(1);
    return 1;
}

unsigned ACollisionDetector::BoxAndAdvBV(
        const OBB &box,
        const AdvancedBV &AdvBv,
        CollisionData *data
        )
{
	OBB pBone;
	bool bContactCreated = false;

	// test outter sphere against obb
	if (IntersectionTests::CircleOBB(AdvBv.GetOutterCircle(), &box) )
	{
		tree<BoundingVolume*>::iterator Iter = AdvBv.GoToRoot();
		
		if (AdvBoxAndBox(AdvBv,box,Iter,data,pBone, bContactCreated) )
		{
//			SMI->SetColor(pBone.m_iGraphicsID, RED);
			return 1;
		}
		
		
	}

	return 0;
}

// This preprocessor definition is only used as a convenience
// in the boxAndBox contact generation method.
#define CHECK_OVERLAP(axis, index) \
    if (!TryAxis(pBox, box, (axis), toCentre, (index), pen, best)) return 0;

/** Recursive Method for BVH Methods **/
bool ACollisionDetector::AdvBoxAndBox(
		const AdvancedBV &advBV, 
		const OBB &pBox, 
		tree<BoundingVolume*>::iterator &Iter,
		CollisionData *data,
		OBB &pBone,
		bool &bContactCreated
		)
{
	if (Iter == NULL) 
		return 0;

	//vector that will be used for the children of the advanced bv
	std::vector<OBB*> vChildren;

	// get children
	vChildren = advBV.GetChildren(Iter);

	// if there are no more children, vec size will be 0
	if (vChildren.size() == 0) 
		return true;

	//set Iterator to its child being searched
		Iter = Iter.node->first_child;

		// keep track of bone being checked
			if (Iter != NULL)
			{
				OBB* temp = (OBB*)Iter.node->data;
				pBone = *temp;
			}

		//// keep track of bone being checked
		//OBB* temp = (OBB*)Iter.node->data;
		//pBone = *temp;

	for (auto it = vChildren.begin(); it != vChildren.end(); ++it)
	{

		if (Iter == NULL) 
		return 0;

		OBB box = *(*it);

		//early out boolean
		if (!IntersectionTests::BoxAndBox(pBox, box))
		// if not in contact
		{
			Iter = Iter.node->next_sibling;
			// keep track of bone being checked
			if (Iter != NULL)
			{
				OBB* temp = (OBB*)Iter.node->data;
				pBone = *temp;
			}

			continue;
		}
		
		// recursion to go deeper until no children left
		// another iterator to keep our place
		tree<BoundingVolume*>::iterator iterate = Iter;

		// if no children, we are contacting the last body - return true
		if (iterate == NULL)
		{
			//SMI->SetColor(pBone->m_iGraphicsID,RED);

			//// Compile the contact
			//Vector3 closestPtWorld = box.GetTransform().Transform(closestPt);

			//Contact* contact = data->m_pContacts;
			//contact->m_vContactNormal = (closestPtWorld - centre);
			//contact->m_vContactNormal.Normalize();
			//contact->m_vContactPoint = closestPtWorld;
			//contact->m_fPenetration = sphere.GetRadius() - sqrtf(dist);
			//contact->SetBodyData(NULL, MMI->GetBody(sphere.GetBodyID()),
			//	data->m_fFriction, data->m_fRestitution);

			//data->AddContacts(1);
			return true;
		}
		
		//call function recursively
		if (AdvBoxAndBox(advBV,pBox,iterate, data, pBone, bContactCreated))
		{
			if (bContactCreated == false)
			{
				// Find the vector between the two centres
				Vector3 toCentre = box.GetPosition() - pBox.GetPosition();

				// We start assuming there is no contact
				float pen = FLT_MAX;
				unsigned best = 0xffffff;

				// Now we check each axes, returning if it gives us
				// a separating axis, and keeping track of the axis with
				// the smallest penetration otherwise.
				CHECK_OVERLAP(pBox.GetAxis(0), 0);
				CHECK_OVERLAP(pBox.GetAxis(1), 1);
				CHECK_OVERLAP(pBox.GetAxis(2), 2);

				CHECK_OVERLAP(box.GetAxis(0), 3);
				CHECK_OVERLAP(box.GetAxis(1), 4);
				CHECK_OVERLAP(box.GetAxis(2), 5);

				// Store the best axis-major, in case we run into almost
				// parallel edge collisions later
				unsigned bestSingleAxis = best;

				CHECK_OVERLAP(pBox.GetAxis(0) % box.GetAxis(0), 6);
				CHECK_OVERLAP(pBox.GetAxis(0) % box.GetAxis(1), 7);
				CHECK_OVERLAP(pBox.GetAxis(0) % box.GetAxis(2), 8);
				CHECK_OVERLAP(pBox.GetAxis(1) % box.GetAxis(0), 9);
				CHECK_OVERLAP(pBox.GetAxis(1) % box.GetAxis(1), 10);
				CHECK_OVERLAP(pBox.GetAxis(1) % box.GetAxis(2), 11);
				CHECK_OVERLAP(pBox.GetAxis(2) % box.GetAxis(0), 12);
				CHECK_OVERLAP(pBox.GetAxis(2) % box.GetAxis(1), 13);
				CHECK_OVERLAP(pBox.GetAxis(2) % box.GetAxis(2), 14);

				// Make sure we've got a result.
				assert(best != 0xffffff);

				// We now know there's a collision, and we know which
				// of the axes gave the smallest penetration. We now
				// can deal with it in different ways depending on
				// the case.
				if (best < 3)
				{
					// We've got a vertex of box two on a face of box pBox.
					FillPointFaceBoxBox(box, pBox, toCentre, data, best, pen);
					data->AddContacts(1);
					bContactCreated = true;
					return 1;
				}
				else if (best < 6)
				{
					// We've got a vertex of box one on a face of box box.
					// We use the same algorithm as above, but swap around
					// one and two (and therefore also the vector between their
					// centres).
					FillPointFaceBoxBox(box, pBox, toCentre*-1.0f, data, best-3, pen);
					data->AddContacts(1);
					bContactCreated = true;
					return 1;
				}
				else
				{
					// We've got an edge-edge contact. Find out which axes
					best -= 6;
					unsigned oneAxisIndex = best / 3;
					unsigned twoAxisIndex = best % 3;
					Vector3 oneAxis = pBox.GetAxis(oneAxisIndex);
					Vector3 twoAxis = box.GetAxis(twoAxisIndex);
					Vector3 axis = oneAxis % twoAxis;
					axis.Normalize();

					// The axis should point from box one to box box.
					if (axis * toCentre > 0) axis = axis * -1.0f;

					// We have the axes, but not the edges: each axis has 4 edges parallel
					// to it, we need to find which of the 4 for each object. We do
					// that by finding the point in the centre of the edge. We know
					// its component in the direction of the box's collision axis is zero
					// (its a mid-point) and we determine which of the extremes in each
					// of the other axes is closest.
					Vector3 ptOnOneEdge = pBox.GetHalfExtents();
					Vector3 ptOnTwoEdge = box.GetHalfExtents();
					for (unsigned i = 0; i < 3; i++)
					{
						if (i == oneAxisIndex) ptOnOneEdge[i] = 0;
						else if (pBox.GetAxis(i) * axis > 0) ptOnOneEdge[i] = -ptOnOneEdge[i];

						if (i == twoAxisIndex) ptOnTwoEdge[i] = 0;
						else if (box.GetAxis(i) * axis < 0) ptOnTwoEdge[i] = -ptOnTwoEdge[i];
					}

					// Move them into world coordinates (they are already oriented
					// correctly, since they have been derived from the axes).
					ptOnOneEdge = pBox.GetTransform() * ptOnOneEdge;
					ptOnTwoEdge = box.GetTransform() * ptOnTwoEdge;

					// So we have a point and a direction for the colliding edges.
					// We need to find out point of closest approach of the two
					// line-segments.
					Vector3 vertex = ContactPoint(
						ptOnOneEdge, oneAxis, pBox.GetHalfExtents()[oneAxisIndex],
						ptOnTwoEdge, twoAxis, box.GetHalfExtents()[twoAxisIndex],
						bestSingleAxis > 2
						);

					// We can fill the contact.
					Contact* contact = data->m_pContacts;

					contact->m_fPenetration = pen;
					contact->m_vContactNormal = axis;
					contact->m_vContactPoint = vertex;
					contact->SetBodyData(MMI->GetBody(pBox.GetBodyID()), NULL,
						data->m_fFriction, data->m_fRestitution);
					data->AddContacts(1);
					bContactCreated = true;
					return 1;
				}
			}
			
			
			return true;


		}
					
	} // end for loop

	return false;
}

#undef CHECK_OVERLAP

unsigned ACollisionDetector::BoxAndHalfSpace(
    const OBB &box,
    const Plane &plane,
    CollisionData *data
    )
{
    // Make sure we have m_pContacts
    if (data->m_iContactsLeft <= 0) return 0;

    // Check for intersection
    if (!IntersectionTests::BoxAndHalfSpace(box, plane))
    {
        return 0;
    }

    // We have an intersection, so find the intersection points. We can make
    // do with only checking vertices. If the box is resting on a plane
    // or on an edge, it will be reported as four or two contact points.

    // Go through each combination of + and - for each half-size
    static float mults[8][3] = {{1,1,1},{-1,1,1},{1,-1,1},{-1,-1,1},
                               {1,1,-1},{-1,1,-1},{1,-1,-1},{-1,-1,-1}};

    Contact* contact = data->m_pContacts;
    unsigned contactsUsed = 0;
    for (unsigned i = 0; i < 8; i++) {

        // Calculate the position of each vertex
        Vector3 vertexPos(mults[i][0], mults[i][1], mults[i][2]);
        vertexPos.ComponentProductUpdate(box.GetHalfExtents());
        vertexPos = box.GetTransform().Transform(vertexPos);

        // Calculate the distance from the plane
        float vertexDistance = vertexPos * plane.GetNormal();

        // Compare this to the plane's distance
        if (vertexDistance <= plane.GetOffset())
        {
            // Create the contact data.

            // The contact point is halfway between the vertex and the
            // plane - we multiply the direction by half the separation
            // distance and add the vertex location.
            contact->m_vContactPoint = plane.GetNormal();
            contact->m_vContactPoint *= (vertexDistance-plane.GetOffset());
            contact->m_vContactPoint += vertexPos;
			//contact->m_vContactPoint *= .5f;
            contact->m_vContactNormal = plane.GetNormal();
            contact->m_fPenetration = plane.GetOffset() - vertexDistance;

            // Write the appropriate data
            contact->SetBodyData(MMI->GetBody(box.GetBodyID()), NULL,
                data->m_fFriction, data->m_fRestitution);

            // Move onto the next contact
            contact++;
            contactsUsed++;
            if (contactsUsed == data->m_iContactsLeft) return contactsUsed;
        }
    }

    data->AddContacts(contactsUsed);
    return contactsUsed;
}

///////////
// ADV COLLISIONS
////////////
AdvCollisions::AdvCollisions()
	: m_Resolver((maxContacts)/*, 0.2f,0.2f*/)
{
	// setting id system to start at 0
	m_iBoundingVolumeID = 0;	

	// setting the collision data structure contact array
	m_cData.m_pContactArray = m_Contacts;

	// create a new contactgenregistry
	m_pCGens = new ContactGenRegistry();


}
AdvCollisions::~AdvCollisions()
{
	Clear();

	delete m_pCGens;
}
AdvCollisions * AdvCollisions::Instance()
{
	static AdvCollisions instance;

	return &instance;
}

/** Processes the contact generation code. */
void AdvCollisions::GenerateContacts()
{

	// Set up the collision data structure
    m_cData.Reset(maxContacts);
	m_cData.m_fFriction = 0.6f;
	m_cData.m_fRestitution = (float)0.0;
    m_cData.m_fTolerance = (float)0.1;
	
	//counters
	unsigned int i, j;


	//*********************
	//CIRCLE-CIRCLE DETECTION
	//*********************

	for(i = 0; i <  m_vCircle.size() ; i++)
	{		
		for(j = i; j < m_vCircle.size(); j++)
		{
			if (i==j)
				continue;
			// if no more contacts left, leave
			if (!m_cData.HasMoreContacts()) return;

			m_cData.m_fRestitution = 0.6f;

			ACollisionDetector::SphereAndSphere(*m_vCircle[i], *m_vCircle[j], &m_cData);
			

		}//end nested for

	}//end outter for

	//*********************
	//Circle-Plane DETECTION
	//*********************

	for(i = 0; i <  m_vCircle.size() ; i++)
	{		
		for(j = 0; j < m_vPlane.size(); j++)
		{
			// if no more contacts left, leave
			if (!m_cData.HasMoreContacts()) return;

			m_cData.m_fRestitution = 0.6f;
			
			ACollisionDetector::SphereAndHalfSpace(*m_vCircle[i], *m_vPlane[j], &m_cData);
			

		}//end nested for

	}//end outter for

	//*********************
	//OBB-OBB DETECTION
	//*********************
	for(i = 0; i <  m_vOBB.size() ; i++)
	{		
		for(j = i; j < m_vOBB.size(); j++)
		{
			if (i==j)
				continue;

			m_cData.m_fRestitution = 0.5f;
			m_cData.m_fFriction = 0.8f;
			ACollisionDetector::BoxAndBox(*m_vOBB[i], *m_vOBB[j], &m_cData);
			

		}//end nested for

	}//end outter for


	//*********************
	//Box-Plane DETECTION
	//*********************

	for(i = 0; i <  m_vOBB.size() ; i++)
	{		
		for(j = 0; j < m_vPlane.size(); j++)
		{
			// if no more contacts left, leave
			if (!m_cData.HasMoreContacts()) return;

			m_cData.m_fRestitution = 0.0f;
			m_cData.m_fFriction = 0.8f;

			ACollisionDetector::BoxAndHalfSpace(*m_vOBB[i], *m_vPlane[j], &m_cData);
			

		}//end nested for

	}//end outter for



	//*********************
	//Circle-OBB DETECTION
	//*********************
	for(i = 0; i <  m_vOBB.size() ; i++)
	{		
		for(j = 0; j < m_vCircle.size(); j++)
		{
			if (!m_cData.HasMoreContacts()) return;

			m_cData.m_fRestitution = 0.0f;

#if 1
			ACollisionDetector::BoxAndSphere(*m_vOBB[i], *m_vCircle[j], &m_cData);
#endif

#if 0
			if (IntersectionTests::CircleOBB2(m_vCircle[j], m_vOBB[i])	)

			{
				//test
				Sleep(1);
			}
#endif
			
			
		}//end nested for

	}//end outter for


	//*********************
	//BALL-ADVANCED BV ALL DETECTION
	//*********************
	for(size_t i = 0; i <  m_vCircle.size() ; i++)
	{		
		for(size_t j = 0; j < m_vAdvancedBVs.size(); j++)
		{
			if (!m_cData.HasMoreContacts()) return;

			m_cData.m_fRestitution = 0.6f;

			ACollisionDetector::SphereAndAdvBV(*m_vCircle[i], *m_vAdvancedBVs[j], &m_cData);
			
		}//end nested for

	}//end outter for

	//*********************
	//BOX-ADVANCED BV ALL DETECTION
	//*********************
	for(size_t i = 0; i <  m_vOBB.size() ; i++)
	{		
		for(size_t j = 0; j < m_vAdvancedBVs.size(); j++)
		{
			if (!m_cData.HasMoreContacts()) return;

			m_cData.m_fRestitution = 0.6f;

			ACollisionDetector::BoxAndAdvBV(*m_vOBB[i], *m_vAdvancedBVs[j], &m_cData);
			
		}//end nested for

	}//end outter for


	// contact generators
	m_pCGens->updateCGens(m_cData);

}

void AdvCollisions::Update(float dt)
{
	
	
	
	
	// contact generation
	GenerateContacts();

	// Resolve detected contacts
    m_Resolver.ResolveContacts(m_cData.m_pContactArray, m_cData.m_uContactCount, dt );

	

	
	
	
}

////////////////////////
//	BV CREATION
////////////////////////

int AdvCollisions::CreateCircle(Vector3 vPosition, float fRadius, int iBodyID)
{
	//create temp circle ptr
	Circle* pTemp = new Circle(m_iBoundingVolumeID, vPosition, fRadius, iBodyID);

	//add to full listing of bv's
	m_mBoundingVolumes.insert(pair<int, BoundingVolume*>(m_iBoundingVolumeID,pTemp) );

	// add to circle bv list
	m_vCircle.push_back(pTemp);

	// return and post-increment bv id
	return m_iBoundingVolumeID++;
}

int AdvCollisions::CreateOBB(Vector3 vPosition, Vector3 vHalfExtents, int iBodyID)
{
	//create temp OBB
	OBB* pTemp = new OBB(m_iBoundingVolumeID, vPosition, vHalfExtents, iBodyID);

	//add to full listing of bv's
	m_mBoundingVolumes.insert(pair<int, BoundingVolume*>(m_iBoundingVolumeID,pTemp) );

	// add to circle bv list
	m_vOBB.push_back(pTemp);

	// return and post-increment bv id
	return m_iBoundingVolumeID++;
}

int AdvCollisions::CreatePlane(Vector3 vNormal, float fOffset)
{
	//create temp OBB
	Plane* pTemp = new Plane(m_iBoundingVolumeID, vNormal, fOffset);

	//add to full listing of bv's
	m_mBoundingVolumes.insert(pair<int, BoundingVolume*>(m_iBoundingVolumeID,pTemp) );

	// add to plane bv list
	m_vPlane.push_back(pTemp);

	// return and post-increment bv id
	return m_iBoundingVolumeID++;
}
AdvancedBV* AdvCollisions::CreateAdvancedBV(Vector3 vPosition, float fRadius, bool bVisible)
{
	// create temp advanced bv
	AdvancedBV* pTemp = new AdvancedBV(m_iBoundingVolumeID, -1, vPosition, fRadius, bVisible);

	// add to full listing of bv's
	m_mBoundingVolumes.insert(pair<int, BoundingVolume*>(m_iBoundingVolumeID++,pTemp) );

	// add to advanced bv list
	m_vAdvancedBVs.push_back(pTemp);

	// return pointer to advanced bv so it can be added to
	return pTemp;

}

////////////////////////
//	BV DELETION
////////////////////////
//allows for deletion of a bounding volume
void AdvCollisions::DeleteBV(int id)
{
	//delete from map
	m_mBoundingVolumes.erase(id);

	//delete from vector
	//look in circles
	for (unsigned int i = 0; i < m_vCircle.size(); i++)
	{
		if (m_vCircle[i]->GetID() == id)
		{
			delete m_vCircle[i];
			m_vCircle.erase(m_vCircle.begin() + i);
			return;
		}
			
	}
	//look in obb
	for (unsigned int i = 0; i < m_vOBB.size(); i++)
	{
		if (m_vOBB[i]->GetID() == id)
		{
			delete m_vOBB[i];
			m_vOBB.erase(m_vOBB.begin() + i);
			return;
		}
	}
	//look in planes
	for (unsigned int i = 0; i < m_vPlane.size(); i++)
	{
		if (m_vPlane[i]->GetID() == id)
		{
			delete m_vPlane[i];
			m_vPlane.erase(m_vPlane.begin() + i);
			return;
		}
	}
}

void AdvCollisions::Clear()
{
	// TODO: RB- This is a shitty way of doing this, but it's on purpose.
	// It's probably not great that we're holding/updating multiple lists
	// of the same thing here... But if we clean up based on the individual lists,
	// it should point out places where the two lists aren't kept in sync, as anything
	// in the map that's still around after this should generate a leak...right?
	// REMOVE ME once these lists have been consolidated!!!

	for(size_t i = 0; i <  m_vCircle.size(); i++)
		delete m_vCircle[i];

	m_vCircle.clear();

	for(size_t i = 0; i <  m_vAdvancedBVs.size(); i++)
		delete m_vAdvancedBVs[i];

	m_vAdvancedBVs.clear();

	for(size_t i = 0; i <  m_vPlane.size(); i++)
		delete m_vPlane[i];

	m_vPlane.clear();

	for(size_t i = 0; i <  m_vOBB.size(); i++)
		delete m_vOBB[i];

	m_vOBB.clear();
}

////////////////////////
//	BV ACCESS
////////////////////////
//returns ptr to BV for basic updates
BoundingVolume* AdvCollisions::GetBV(int id)
{
	return m_mBoundingVolumes.find(id)->second;
}
//returns ptr to circle bv based on id
Circle* AdvCollisions::GetCircle(int id)
{
	return (Circle*)m_mBoundingVolumes.find(id)->second;	
}
// returns ptr to aabb bv based on id
AABB* AdvCollisions::GetAABB(int id)
{
	return (AABB*)m_mBoundingVolumes.find(id)->second;
}
// return ptr to obb bv based on id
OBB* AdvCollisions::GetOBB(int id)
{
	return (OBB*)m_mBoundingVolumes.find(id)->second;
}
// return ptr to plane bv based on id
Plane* AdvCollisions::GetPlane(int id)
{
	return (Plane*)m_mBoundingVolumes.find(id)->second;
}

///////////////////////
// CONTACT GENERATORS 
///////////////////////
void AdvCollisions::AddJointCGen(unsigned int bodyID, Vector3 vBodyConnectionPt, unsigned int otherID, 
	Vector3 vOtherConnectionPt, float error)
{
	m_pCGens->AddJointCGen(bodyID,vBodyConnectionPt,otherID,vOtherConnectionPt, error);
}

void AdvCollisions::AddCableCGen(unsigned int bodyID, Vector3 vBodyConnectionPt, unsigned int otherID, 
	Vector3 vOtherConnectionPt, float length)
{
	m_pCGens->AddJointCGen(bodyID,vBodyConnectionPt,otherID,vOtherConnectionPt, length);
}