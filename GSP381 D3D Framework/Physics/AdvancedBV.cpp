#include "AdvancedBV.h"
#include "BoundingVolume.h"
#include "Circle.h"
#include "OBB.h"
#include "../Graphics/ShapeManager.h"
#include "../Graphics/D3DBox.h"
#include "../Graphics/D3DSphere.h"


AdvancedBV::AdvancedBV(void)
{
}


/**
Full const for Advanced BV - only thing required is the id - other params
iBodyID - Rigid Body ID
vCenter - center of mass for model. default is the center computed by D3DXComputeBoundingSphere
**NOTE** this is m_vPosition of the entire advanced BV, but not necessarilly the center of 
fRadius - radius of bounding sphere as computed by D3DXComputeBoundingSphere
vMin - lower left corner of bounding box calculated by D3DXComputeBoundingBox 
vMax - upper right corner of bounding box calculated by D3DXComputeBoundingBox 

**/
AdvancedBV::AdvancedBV(int id, int iBodyID, Vector3 vCenter, float fRadius,
	 bool bVisible, Vector3 vMin, Vector3 vMax)
	:BoundingVolume(id, vCenter, iBodyID)
{
	static int iNodeID = 0;

	//create temp circle bv
	Circle *temp = new Circle(iNodeID ++, vCenter, fRadius, iBodyID);
		
	//create root node for tree, putting circle in it
	m_tRoot = m_tBoundingVolumes.set_head(temp);

	// no rotation, so go to translation
	m_mTransform(0,3) = vCenter.x;
	m_mTransform(1,3) = vCenter.y;
	m_mTransform(2,3) = vCenter.z;


#if 1	// graphics creation - only for demo
	if (bVisible == true)
	{
		int id = SMI->CreateSphere(fRadius);
		SMI->GetSphere(id)->SetPosition(vCenter);
		SMI->SetRandomColor(id);
		//m_vGBoxes.push_back(id);
	}
		
#endif
	
}

AdvancedBV::~AdvancedBV(void)
{
	//need to go through map and delete all bv's
	for (auto it = m_tBoundingVolumes.begin(); it != m_tBoundingVolumes.end(); ++it)
	{
		delete (*it);
		(*it) = NULL;
	}

	// clear the tree
	m_tBoundingVolumes.clear();
}

void AdvancedBV::VUpdate(float dt)
{


}
void AdvancedBV::CalculateInternals()
{

}

//////////////////
// CREATION
/////////////////
//adds a circle bv to the bvh tree
int AdvancedBV::AddCircle(int iParentID, Vector3 vOffsetFromCenter, float fRadius)
{
	return 0;
}
//adds a obb bv to the bvh tree- can pass transform in here or set manually later
int AdvancedBV::AddOBB(	LPCSTR sParentName, 
						Vector3 vOffsetFromCenter, 
						Vector3 vHalfExtents, 
						LPCSTR sBoneName, 
						bool bVisible,
						Matrix4 &mTransform
						)
{
	//iterator 
	tree<BoundingVolume*>::iterator it;
	
	// outter circle isn't in the box list, so have to single it out
	if (sParentName == "Circle")
	{
		it = m_tRoot;		
	}
	else
	{
		it = m_mBones.find(sParentName)->second;
	}
	
	// make sure parent exists
	if (it == NULL) return -1;

	//offset is translation part of matrix
	Matrix4 tempTransform;

	// if a matrix was NOT passed in, or has 0 translation, we create our own tranform matrix
	if (mTransform == Matrix4())
	{
		// set values manually 
		// set axis normals
		tempTransform._11 = 1.0f;
		tempTransform._22 = 1.0f;
		tempTransform._33 = 1.0f;
		tempTransform._44 = 1.0f;
		
		// no rotation, so go to translation
		tempTransform(0,3) = vOffsetFromCenter.x;
		tempTransform(1,3) = vOffsetFromCenter.y;
		tempTransform(2,3) = vOffsetFromCenter.z;
	}
	else
	{
		tempTransform = mTransform;
	}
		//create an obb bv
		OBB* temp = new OBB(m_iCircleCount, m_vPosition,vHalfExtents);

		// set the transform

		temp->SetTransform(tempTransform * m_mTransform);

		//move the obb from the center by the transform
		temp->SetPosition(tempTransform.Transform(m_vPosition));
		
		//  Add it to the tree to its parent
		m_tIterator = m_tBoundingVolumes.append_child(it,temp);

		// add both name and bv to the map as well
		m_mBones.insert(pair<LPCSTR, tree<BoundingVolume*>::iterator>(sBoneName,m_tIterator) );

#if 1	// graphics creation - only for demo
		if (bVisible == true)
		{
			int id = SMI->CreateBox(vHalfExtents.x, vHalfExtents.y, vHalfExtents.z);
			temp->m_iGraphicsID = id;
			SMI->GetBox(id)->SetPosition(tempTransform.Transform(m_vPosition));
			SMI->SetRandomColor(id);
			m_vGBoxes.push_back(id);
			m_vBoxes.push_back(temp);
		}
		temp->m_sBoneName = sBoneName;
#endif

	return m_iCircleCount++;
}
// lets you submit a vector of Matrix4 transform matrices from the 
//	animation system to auto-create the boxes. columns 1-3 are x,y,z 
//	rotation/scale and column 4 is translation
std::vector<int>* AdvancedBV::CreateBones(int &id, Vector3 &vMeshPosition, 
							std::vector<Matrix4*> &vBones, 
							int iBodyID)
{
	return NULL;
}

/////////////////
// ACCESSORS
/////////////////
OBB* AdvancedBV::GetBone(LPCSTR sBoneName)
{
	tree<BoundingVolume*>::iterator it = m_mBones.find(sBoneName)->second;
	return (OBB*)(*it);
}
OBB* AdvancedBV::GetBone(tree<BoundingVolume*>::iterator &iter) const
{
	return (OBB*) iter.node->data;
}

Circle* AdvancedBV::GetOutterCircle() const
{
	return (Circle*)m_tRoot.node->data;
}

std::vector<OBB*> AdvancedBV::GetChildren(LPCSTR sBoneName) const
{
	// return vector
	std::vector<OBB*> returnMe;

	// outter circle isn't in the box list, so have to single it out
	if (sBoneName == "Circle")
	{
		for (auto it = m_tRoot.begin(); it != m_tRoot.end(); ++it)
		{
			returnMe.push_back((OBB*)(*it));
		}

		return returnMe;
	}
	
	// otherwise, get the iterator
	tree<BoundingVolume*>::iterator iter = m_mBones.find(sBoneName)->second;

	//and find its children and return them
	for (auto it = iter.begin(); it != iter.end(); ++it)
		{
			returnMe.push_back((OBB*)(*it));
		}

		return returnMe;
}

std::vector<OBB*> AdvancedBV::GetChildren(tree<BoundingVolume*>::iterator &iter) const
{
	// return vector
	std::vector<OBB*> returnMe;

	//and find its children and return them
	for (auto it = iter.begin(); it != iter.end(); ++it)
		{
			returnMe.push_back((OBB*)(*it));
		}

	

		return returnMe;
}


/////////////////
// MUTATORS
/////////////////
void AdvancedBV::UpdateBoneTransform(LPCSTR sBoneName, Matrix4 mTransform)
{
	tree<BoundingVolume*>::iterator it = m_mBones.find(sBoneName)->second;

	(*it)->SetTransform(mTransform);
}