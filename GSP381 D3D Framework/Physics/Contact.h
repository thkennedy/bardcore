#pragma once
#include "RigidBody.h"


class Contact
{
private:

	//two rigid bodies in contact
	RigidBody m_aContactingBodies[2]; 
	
	// contact normal for the two bodies
	Vector3 m_vContactNormal;
	
	// closing velocity for the two bodies
	Vector3 m_vClosingVelocity;
	
	// impulse 
	float m_fImpulse[2];

public:
	Contact();
	Contact(Vector3 vContactNormal, Vector3 vClosingVelocity, Vector3 vContactNormalVelocity);
	~Contact();

	void Resolve(){}
};

