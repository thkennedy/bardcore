#pragma once
#include <vector>
#include "BoundingVolume.h"
#include "WorldManager.h"
#include <map>

//forward declarations
class BoundingVolume;
class Circle;
class AABB;
class OBB;


#define CMI CollisionDetector::Instance()



class CollisionDetector
{
private:
	CollisionDetector(); // singleton, so constructor is private


	// map of all bv's for lookup (quicker than iter through)
	std::map<int, BoundingVolume *> m_mBoundingVolumes;
	
	//object vectors for collision detection
	std::vector<Circle *> m_vCircle;
	std::vector<AABB *>m_vAABB;
	std::vector<OBB *>m_vOBB;
	
	int m_iBoundingVolumeID;


public:
	
	static CollisionDetector * Instance(); // declaration
	~CollisionDetector();

	////////////////////////
	//	BV CREATION
	////////////////////////

	int CreateCircle(Vector3 vPosition, float fRadius, int iBodyID);
	int CreateAABB(Vector3 vPosition, Vector3 vHalfExtents, int iBodyID);
	int CreateOBB(Vector3 vPosition, Vector3 vHalfExtents, int iBodyID);

	////////////////////////
	//	BV DELETION
	////////////////////////
	//allows for deletion of a bounding volume
	void DeleteBV(int id);

	////////////////////////
	//	BV ACCESS
	////////////////////////
	//returns ptr to bv based on id
	BoundingVolume* GetBV(int id);
	//returns ptr to circle bv based on id
	Circle* GetCircle(int id);
	// returns ptr to aabb bv based on id
	AABB* GetAABB(int id);
	// return ptr to obb bv based on id
	OBB* GetOBB(int id);

	//***********************
	//COLLISION CHECKS
	//***********************
	void CheckCollisions();
	void CheckOutOfBounds(float maxX, float maxY, float maxZ, Vector3 center);
	
	//***********************
	//CIRCLES
	//***********************
	void OutOfBoundsCirCir(float maxX, float maxY, float maxZ, Vector3 center);
	bool CircleCircleDet(BoundingVolume *BV1, BoundingVolume *BV2);
	bool CircleAABB(Circle* Circle, AABB* Box);
	bool CircleOBBDet(Circle * Circle, OBB * Box);

	//***********************
	//AABB
	//***********************
	void OutOfBoundsAABB(float maxBoundX, float maxBoundY, float maxBoundZ, Vector3 center);
	bool BoxBoxAABBDet(AABB* BoxA, AABB* BoxB);
	Vector3 ClosestPointAABB(Vector3* p, AABB* b, Vector3 &q);
	bool BoxBoxAABBOBB(AABB *box1, OBB *box2);


	//***********************
	//OBB
	//***********************
	
	void OutOfBoundsOBB(float maxBoundX, float maxBoundY, float maxBoundZ, Vector3 center);
	
	// Separating Axis Theorem Methods 
	float TransformToAxis(  OBB &box, Vector3 &projectionAxis);
	bool OverlapOnAxis( OBB &one, OBB &two, Vector3 &projectionAxis, Vector3 &toCenter);
	bool OBBOBB (OBB &one, OBB &two);
	bool BoxBoxOBB(OBB *box1, OBB *box2);
	Vector3 ClosestPtOBB(Vector3* p, OBB* b, Vector3 &q);


	//***********************
	//PLANE
	//***********************
	
	//Vector3 ClosestPtPlane(ParticlePlane * p, Vector3 point);

	
	//***********************
	//INTERPENETRATION CALCS
	//***********************
	void InterpenetrationCheck(BoundingVolume *BV1, BoundingVolume *BV2);
	//void InterpenetrationCheck(AABB* Box1, AABB* Box2);



	//***********************
	//COLLISION RESPONSE - MOVE TO CONTACT LOGIC
	//***********************
	
	void ImpulseResponse(BoundingVolume * BV1, BoundingVolume * BV2);
	void ImpulseResponseWithRotation(BoundingVolume * BV1, BoundingVolume * BV2, Vector3 *ContactPoint);
	void InterpenetrationFix(BoundingVolume * BV1, BoundingVolume * BV2, float penetrationDepth);
	

	



};

