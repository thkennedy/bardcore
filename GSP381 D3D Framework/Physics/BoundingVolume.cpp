#include "BoundingVolume.h"



BoundingVolume::BoundingVolume(void)
{
	m_vPosition = Vector3(0.0f,0.0f,0.0f);
	
	m_bActive = true;

}

BoundingVolume::BoundingVolume(int id, Vector3 vPosition, int iBodyID)
	:m_iBoundingVolumeID(id), m_vPosition(vPosition), m_bActive(true), m_iBodyID(iBodyID)
{

}


BoundingVolume::~BoundingVolume(void)
{

}

void BoundingVolume::VUpdate(float dt)
{
	// sync position with rigid body
	//m_vPosition = MMI->GetBody(m_iBodyID)->GetPosition();

	// calculate internals
	//CalculateInternals();
}

void BoundingVolume::CalculateInternals()
{
	// shouldn't cheat - calc my own transform matrix
	//m_mTransform = MMI->GetBody(m_iBodyID)->GetTransform();// * m_mOffset;
}



////////////////////////
// ACCESSORS
///////////////////////

float BoundingVolume::GetPosition(int iValue)
{
	switch(iValue)
	{
	case 1:
		return m_vPosition.x;
	case 2:
		return m_vPosition.y;
	case 3:
		return m_vPosition.z;
	};

	return 0;
}

/**
    * This is a convenience function to allow access to the
    * axis vectors in the transform for this primitive.
    */
Vector3 BoundingVolume::GetAxis(unsigned index) const
{
    return m_mTransform.GetAxisVector(index);
}

/**
    * Returns the resultant transform of the primitive, calculated from
    * the combined offset of the primitive and the transform
    * (orientation + position) of the rigid body to which it is
    * attached.
    */
const Matrix4& BoundingVolume::GetTransform() const
{
    return m_mTransform;
}

////////////////////////
// MUTATORS
///////////////////////
void BoundingVolume::SetPosition(float x, float y, float z)
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;
}
void BoundingVolume::SetPosition(Vector3 vPosition)
{
	m_vPosition = vPosition;
}
void BoundingVolume::SetTransform (Matrix4 mNewTransform)
{
	m_mTransform = mNewTransform;
}

void BoundingVolume::SetOrientationQ(Quaternion qOrientation)
{
	m_qOrientation = qOrientation;

	
}
void BoundingVolume::SetOrientationQ(Vector3 vAxis, float fRotation)
{
	Quaternion qTempOrientation(vAxis,fRotation);


}