#include "AABB.h"


AABB::AABB()
{
	m_vHalfExtents = Vector3(1.0f,1.0f,1.0f);
}

AABB::AABB(int id, Vector3 vPosition, float x, float y, float z)
	:BoundingVolume(id, vPosition)
{
	m_vHalfExtents = Vector3(x,y,z);

	m_fRadius = sqrt((m_vHalfExtents.x*m_vHalfExtents.x) +
		(m_vHalfExtents.y*m_vHalfExtents.y) + (m_vHalfExtents.z*m_vHalfExtents.z)) * (.7f);

}
AABB::AABB(int id, Vector3 vPosition, Vector3 vHalfExtents, int iBodyID)
	:BoundingVolume(id, vPosition, iBodyID), m_vHalfExtents(vHalfExtents)
{
	m_fRadius = sqrt((m_vHalfExtents.x*m_vHalfExtents.x) +
		(m_vHalfExtents.y*m_vHalfExtents.y) + (m_vHalfExtents.z*m_vHalfExtents.z));
}

AABB::~AABB()
{
}

void AABB::VUpdate(float dt)
{
	//perform global updates
	BoundingVolume::VUpdate(dt);

	// aabb specific updates

}

////////////////
// ACCESSORS
////////////////
float AABB::GetHalfExtents(int iValue) const
{
	switch(iValue)
	{
	case 1:
		return m_vHalfExtents.x;
	case 2:
		return m_vHalfExtents.y;
	case 3:
		return m_vHalfExtents.z;
	};

	return 0;
}

// computes the min and max on x,y,z axes
float AABB::Max(int i)
{
	return (this->GetPosition(i) + this->GetHalfExtents(i));
}
float AABB::Min(int i)
{
	return (this->GetPosition(i) - this->GetHalfExtents(i));
}
////////////////
// MUTATORS
////////////////
void AABB::SetHalfExtents(float x, float y, float z)
{
	m_vHalfExtents = Vector3(x,y,z);
}