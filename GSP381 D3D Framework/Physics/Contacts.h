#pragma once
#include "vector3.h"

/*
	This wrapper contains the pertinent files to utilize the contact generation system
	outlined by ian millington in his book

	Classes within:
	- Contact
	- ContactGenerator
	- ContactResolver
*/

class ContactResolver;
class RigidBody;

//for ease of calculations, members made public
class Contact
{
    //friend class to allow direct member access
    friend ContactResolver;

public:
	//Bodies involved in the contact
    RigidBody* body[2];

    //m_fFriction coefficient
    float m_fFriction;

    //restitution coeff
    float m_fRestitution;

    // contact pt in WORLD coords.
    Vector3 m_vContactPoint;

    // direction of contact normal in WORLD coords.
    Vector3 m_vContactNormal;

   //interpenetration amount
    float m_fPenetration;

    // sets body data
    void SetBodyData(RigidBody* one, RigidBody *two,
                        float m_fFriction, float m_fRestitution);

protected:

    // matrix that converts coors from contact's local to world coords
    Matrix3 m_mContactToWorld;

    // closing velocity at point of contact
    Vector3 m_vContactVelocity;

    // min change in velocity to resolve contact
    float m_fDesiredDeltaVelocity;

    // world space contact relative to center of each body
    Vector3 m_vRelativeContactPosition[2];

protected:
    // calculates internal data for bodies
    void CalculateInternals(float dt);

    // swaps rigid bodies and reverses contact normal - don't forget to re-
	// calculate internal data
    void SwapBodies();

    // wakes up body if it comes into contact with another awake body
    void MatchAwakeState();

	// Calcs desired delta velocity
    void CalculateDesiredDeltaVelocity(float dt);

    // calculates velocity on a body point
    Vector3 CalculateLocalVelocity(unsigned bodyIndex, float dt);

    // calcs the orthonormal basis for contact pt based on friction direction
    void CalculateContactBasis();

    // regular impulse response (not contact based)
    void ApplyImpulse(const Vector3 &impulse, RigidBody *body,
                        Vector3 *velocityChange, Vector3 *rotationChange);

    // contact-based impulse
    void ApplyVelocityChange(Vector3 velocityChange[2],
                                Vector3 rotationChange[2]);

	// interpenetration fix
    void ApplyPositionChange(Vector3 linearChange[2],
                                Vector3 angularChange[2],
                                float m_fPenetration);

    // does not use friction in impulse calcs
    Vector3 CalculateFrictionlessImpulse(Matrix3 *inverseInertiaTensor);

    // calculation of impulse using friction
    Vector3 CalculateFrictionImpulse(Matrix3 *inverseInertiaTensor);
};


class ContactResolver
{
protected:
    // iterations used when resolving velocity
    unsigned m_uVelocityIterations;

    // iterations used when resolving velocity
    unsigned m_uPositionIterations;

    // velocities smaller than this are considered to be zero. 
	// book recommends default value 0.01
    float m_fVelocityEpsilon;

    // position diff smaller than this are considered 0 - 
	// book recommends default of .01 to start
    float m_fPositionEpsilon;

public:
	// number of iters used to resolve velocities
    unsigned m_uVelocityIterationsUsed;

    // number of iters used to resolve interpenetrations
    unsigned m_uPositionIterationsUsed;

private:
    
    //Keeps track of whether the internal settings are valid.
    bool m_bValidSettings;

public:
    
    /*Creates a new contact resolver with the given number of iterations
    * per resolution call, and optional epsilon values. */
    
    ContactResolver(unsigned iterations,
        float m_fVelocityEpsilon=(float)0.01,
        float m_fPositionEpsilon=(float)0.01);

    /**
        * Creates a new contact resolver with the given number of iterations
        * for each kind of resolution, and optional epsilon values.
        */
    ContactResolver(unsigned uVelocityIterations,
        unsigned uPositionIterations,
        float fVelocityEpsilon=(float)0.01,
        float fPositionEpsilon=(float)0.01);

    // makes sure all settings are valid
    bool IsValid()
    {
        return (m_uVelocityIterations > 0) &&
                (m_uPositionIterations > 0) &&
                (m_fPositionEpsilon >= 0.0f) &&
                (m_fPositionEpsilon >= 0.0f);
    }

    // lets you set iterations for each resolution stage
    void SetIterations(unsigned m_uVelocityIterations,
                        unsigned m_uPositionIterations);

    // sets iterations for resolution stages
    void SetIterations(unsigned iterations);

    // sets the near 0 values for velocity and position
    void SetEpsilon(float m_fVelocityEpsilon,
                    float m_fPositionEpsilon);

    /**
		book comments left because they're helpful

        * Resolves a set of contacts for both m_fPenetration and velocity.
        *
        * Contacts that cannot interact with
        * each other should be passed to separate calls to ResolveContacts,
        * as the resolution algorithm takes much longer for lots of
        * contacts than it does for the same number of contacts in small
        * sets.
        *
        * @param contactArray Pointer to an array of contact objects.
        *
        * @param numContacts The number of contacts in the array to resolve.
        *
        * @param numIterations The number of iterations through the
        * resolution algorithm. This should be at least the number of
        * contacts (otherwise some constraints will not be resolved -
        * although sometimes this is not noticable). If the iterations are
        * not needed they will not be used, so adding more iterations may
        * not make any difference. In some cases you would need millions
        * of iterations. Think about the number of iterations as a bound:
        * if you specify a large number, sometimes the algorithm WILL use
        * it, and you may drop lots of frames.
        *
        * @param dt The dt of the previous integration step.
        * This is used to compensate for forces applied.
        */
    void ResolveContacts(Contact *contactArray, unsigned numContacts, float dt);

protected:
    // Gets contact ready for processing
    void PrepareContacts(Contact *contactArray, unsigned numContacts, float dt);

    //Resolves velocities
    void AdjustVelocities(Contact *contactArray, unsigned numContacts, float dt);

    /**
        * Resolves the positional issues with the given array of constraints,
        * using the given number of iterations.
        */
    void AdjustPositions(Contact *contacts, unsigned numContacts, float dt);
};



