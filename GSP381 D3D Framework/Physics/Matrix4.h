#pragma once

#include "Vector3.h"
#include <d3dx9.h>

/*
	Optimized Matrix4 by inheriting from D3DXMATRIX - calls used are simplified within 
	helper methods
*/

class Quaternion;


class Matrix4 :
	public D3DXMATRIX
{

public:

	

	Matrix4(); // default constructor - creates identity matrix


	///////////////////////////
	//		UTILITIES
	//////////////////////////
	Matrix4 LookAtLH(Vector3 vEye, Vector3 vLookAt, Vector3 vUp);	//creates and returns a left-hand look at matrix
	Vector3 Transform(const Vector3 &vector) const;		//transforms a vector by the current matrix
	Vector3 TransformInverse(const Vector3 &vector) const;//transforms a vector by the current matrix's inverse
	Vector3 TransformDirection(const Vector3 &vector) const;
	Vector3 TransformInverseDirection(const Vector3 &vector) const;
	Vector3 LocalToWorld(const Vector3 &localpt,const Matrix4 &transform);
	Matrix4 WorldToLocal(Vector3 &worldPt, Vector3 origin);
	Matrix4 LocalToWorld(Vector3 &localPt, Vector3 origin);
	
	
	///////////////////////////
	//		ACCESSORS
	//////////////////////////
	float GetDeterminant()const;		// returns determinant of the matrix
	Matrix4 Inverse() const;			// returns the inverse value of the matrix, but does not change values
	static inline void CalculateTransformMatrix(Matrix4 &tMatrix,
												const Vector3 &position,
												const Quaternion &orientation);
	//Matrix4 ComboMatrix(Vector3 transXYZ, float rotateW, Vector3 rotateAxis, float scaleX, float scaleY, float scaleZ);
	Matrix4 TranslateMatrix(Vector3 deltaXYZ);	//returns just the translation part of the matrix
	Vector3 GetAxisVector(int i) const;		// returns the column i - 0-2 is rotation, 3 is translation

	///////////////////////////
	//		MUTATORS
	//////////////////////////
	void Invert();						// inverts matrix
	void SetDiagonal(float a, float b, float c);
	void SetOrientationAndPos(const Quaternion &q, const Vector3 &pos);
	void SetInverse(const Matrix4 &m);
	

	
		
	
	
	Vector3 operator*(const Vector3 &vector) const;
	Matrix4 operator*(const Matrix4 &o);
	/*Matrix4 operator = (const Matrix3 &rhs);*/

	
	




};

//void setInverse(Matrix4 &m);		// calculates the inverse of a matrix (legacy)

