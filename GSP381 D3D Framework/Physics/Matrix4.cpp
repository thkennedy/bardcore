#include "Matrix4.h"
#include "Quaternion.h"
#include "Vector3.h"

Matrix4::Matrix4()
	:D3DXMATRIX()
{
	// init to identity matrix
	_12 = _13 = _14 =
    _21 = _23 = _24 =
	_31 = _32 = _34 =
	_41 = _42 = _43 = 0;

	_11 = _22 = _33 = _44  = 1;
}

///////////////////////////
//		UTILITIES
//////////////////////////
Matrix4 Matrix4::LookAtLH(Vector3 vEye, Vector3 vLookAt, Vector3 vUp)	//creates and returns a left-hand look at matrix
{
	D3DXMatrixLookAtLH(this,&vEye,&vLookAt,&vUp);

	return *this;
}

Vector3 Matrix4::Transform(const Vector3 &vector) const //transforms a vector by the current matrix
{
    return (*this) * vector;
}

Vector3 Matrix4::TransformInverse(const Vector3 &vector) const
{
	
        
            Vector3 tmp = vector;
            tmp.x -= _14;
            tmp.y -= _24;
            tmp.z -= _34;
            
			return Vector3(
                tmp.x * _11 +
                tmp.y * _21 +
                tmp.z * _31,

                tmp.x * _12 +
                tmp.y * _22 +
                tmp.z * _32,

                tmp.x * _13 +
                tmp.y * _23 +
                tmp.z * _33
            );
}

/**
    * Transform the given direction vector by this matrix.
    *
    * @note When a direction is converted between frames of
    * reference, there is no translation required.
    *
    * @param vector The vector to transform.
    */
Vector3 Matrix4::TransformDirection(const Vector3 &vector) const
{
    return Vector3(
        vector.x * _11 +
        vector.y * _12 +
        vector.z * _13,

        vector.x * _21 +
        vector.y * _22 +
        vector.z * _23,

        vector.x * _31 +
        vector.y * _32 +
        vector.z * _33
    );
}

/**
    * Transform the given direction vector by the
    * transformational inverse of this matrix.
    *
    * @note This function relies on the fact that the inverse of
    * a pure rotation matrix is its transpose. It separates the
    * translational and rotation components, transposes the
    * rotation, and multiplies out. If the matrix is not a
    * scale and shear free transform matrix, then this function
    * will not give correct results.
    *
    * @note When a direction is converted between frames of
    * reference, there is no translation required.
    *
    * @param vector The vector to transform.
    */
Vector3 Matrix4::TransformInverseDirection(const Vector3 &vector) const
{
    return Vector3(
        vector.x * _11 +
        vector.y * _21 +
        vector.z * _31,

        vector.x * _12 +
        vector.y * _22 +
        vector.z * _32,

        vector.x * _13 +
        vector.y * _23 +
        vector.z * _33
    );
}

void Matrix4::Invert()
{
	 SetInverse(*this);

}

//Sets the matrix to be a diagonal matrix with the given coefficients.
void Matrix4::SetDiagonal(float a, float b, float c)
{
    _11 = a;
    _22 = b;
    _33 = c;
}

 /**
    * Sets this matrix to be the rotation matrix corresponding to
    * the given quaternion.
    */
void Matrix4::SetOrientationAndPos(const Quaternion &q, const Vector3 &pos)
{
    _11 = 1 - (2*q.y*q.y + 2*q.z*q.z);
    _12 = 2*q.x*q.y + 2*q.z*q.w;
    _13 = 2*q.x*q.z - 2*q.y*q.w;
    _14 = pos.x;

    _21 = 2*q.x*q.y - 2*q.z*q.w;
    _22 = 1 - (2*q.x*q.x  + 2*q.z*q.z);
    _23 = 2*q.y*q.z + 2*q.x*q.w;
    _24 = pos.y;

    _31 = 2*q.x*q.z + 2*q.y*q.w;
    _32 = 2*q.y*q.z - 2*q.x*q.w;
    _33 = 1 - (2*q.x*q.x  + 2*q.y*q.y);
    _34 = pos.z;
}

void Matrix4::SetInverse(const Matrix4 &m)
{
    // Make sure the determinant is non-zero.
    float det = GetDeterminant();
    if (det == 0) return;
    det = ((float)1.0)/det;

    _11 = (-m._32*m._23+m._22*m._33)*det;
    _21 = (m._31*m._23-m._21*m._33)*det;
    _31 = (-m._31*m._22+m._21*m._32)*det;

    _12 = (m._32*m._13-m._12*m._33)*det;
    _22 = (-m._31*m._13+m._11*m._33)*det;
    _32 = (m._31*m._12-m._11*m._32)*det;

    _13 = (-m._22*m._13+m._12*m._23)*det;
    _23 = (+m._21*m._13-m._11*m._23)*det;
    _33 = (-m._21*m._12+m._11*m._22)*det;

    _14 = (m._32*m._23*m._14
               -m._22*m._33*m._14
               -m._32*m._13*m._24
               +m._12*m._33*m._24
               +m._22*m._13*m._34
               -m._12*m._23*m._34)*det;
    _24 = (-m._31*m._23*m._14
               +m._21*m._33*m._14
               +m._31*m._13*m._24
               -m._11*m._33*m._24
               -m._21*m._13*m._34
               +m._11*m._23*m._34)*det;
    _34 =(m._31*m._22*m._14
               -m._21*m._32*m._14
               -m._31*m._12*m._24
               +m._11*m._32*m._24
               +m._21*m._12*m._34
               -m._11*m._22*m._34)*det;
}





Matrix4 Matrix4::Inverse()const
{

	Matrix4 result;
	result.Invert();
    return result;

}

float Matrix4::GetDeterminant()const
{
	return _31*_22*_13+
        _21*_32*_13+
        _31*_12*_23-
        _11*_32*_23-
        _21*_12*_33+
        _11*_22*_33;


}

//void Matrix4::CalculateTransformMatrix(Matrix4 &tMatrix,
//												const Vector3 &position,
//												const Quaternion &orientation)
//{
//	
//
//	tMatrix._11 = 1-(2*(orientation.y*orientation.y)+2*(orientation.z*orientation.z));
//	tMatrix._12 = 2*orientation.x*orientation.y+2*orientation.z*orientation.w;
//	tMatrix._13 = 2*orientation.x*orientation.z-2*orientation.y*orientation.w;
//	tMatrix._14 = position.x;
//
//	tMatrix._21 = 2*orientation.x*orientation.y-2*orientation.z*orientation.w;
//	tMatrix._22 = 1-(2*(orientation.x*orientation.x)+2*(orientation.z*orientation.z));
//	tMatrix._23 = 2*orientation.y*orientation.z+2*orientation.x*orientation.w;
//	tMatrix._24 = position.y;
//
//	tMatrix._31 = 2*orientation.x*orientation.z+2*orientation.y*orientation.w;
//	tMatrix._32 = 2*orientation.y*orientation.z-2*orientation.x*orientation.w;
//	tMatrix._33 = 1-(2*(orientation.x*orientation.x)+2*(orientation.x*orientation.y));
//	tMatrix._34 = position.z;
//
//	
//
//
//}


Vector3 Matrix4::operator*(const Vector3 &vector) const
{
	return Vector3(
                vector.x * _11 +
                vector.y * _12 +
                vector.z * _13 + _14,

                vector.x * _21 +
                vector.y * _22 +
                vector.z * _23 + _24,

                vector.x * _31 +
                vector.y * _32 +
                vector.z * _33 + _34
            );
}



Matrix4 Matrix4::operator*(const Matrix4 &o)
{

 Matrix4 result;
    result._11 = (o._11*_11) + (o._21*_12) + (o._31*_13);
    result._21 = (o._11*_21) + (o._21*_22) + (o._31*_23);
    result._31 = (o._11*_31) + (o._21*_32) + (o._31*_33);

    result._12 = (o._12*_11) + (o._22*_12) + (o._32*_13);
    result._22 = (o._12*_21) + (o._22*_22) + (o._32*_23);
    result._32 = (o._12*_31) + (o._22*_32) + (o._32*_33);

    result._13 = (o._13*_11) + (o._23*_12) + (o._33*_13);
    result._23 = (o._13*_21) + (o._23*_22) + (o._33*_23);
    result._33 = (o._13*_31) + (o._23*_32) + (o._33*_33);

    result._14 = (o._14*_11) + (o._24*_12) + (o._34*_13) + _14;
    result._24 = (o._14*_21) + (o._24*_22) + (o._34*_23) + _24;
    result._34 = (o._14*_31) + (o._24*_32) + (o._34*_33) + _34;

    return result;
	       
   
}

//Matrix4 Matrix4::operator = (const Matrix3 &rhs)
//{
//
//	//rotation part
//	_11 = rhs.m_data[0][0];
//	_12 = rhs.m_data[0][1];
//	_13 = rhs.m_data[0][2];
//	
//	_21 = rhs.m_data[1][0];
//	_22 = rhs.m_data[1][1];
//	_23 = rhs.m_data[1][2];
//	
//	_31 = rhs.m_data[2][0];
//	_32 = rhs.m_data[2][1];
//	_33= rhs.m_data[2][2];
//
//	return *this;
//
//	     
//}


Matrix4 Matrix4::WorldToLocal(Vector3 &worldPt, Vector3 origin)
{
	Matrix4 temp;

	float worldLocalX = -(worldPt.x - origin.x);
	float worldLocalY = -(worldPt.y - origin.y);
	float worldLocalZ = -(worldPt.z - origin.z);
	
	temp._14 = worldLocalX;
	temp._24 = worldLocalY;
	temp._34= worldLocalZ;
	

	return temp;
}
Matrix4 Matrix4::LocalToWorld(Vector3 &localPt, Vector3 origin)
{
	Matrix4 temp;

	float localWorldX = (localPt.x - origin.x);
	float localWorldY = (localPt.y - origin.y);
	float localWorldZ = (localPt.z - origin.z);

	temp._14 = localWorldX;
	temp._24 = localWorldY;
	temp._34= localWorldZ;
	

	return temp;
}

Matrix4 Matrix4::TranslateMatrix(Vector3 deltaXYZ)
{
	Matrix4 temp;

	temp._14 = deltaXYZ.x;
	temp._24 = deltaXYZ.y;
	temp._34= deltaXYZ.z;
	

	return temp;
}

/**
    * Gets a vector representing one axis (i.e. one column) in the matrix.
    *
    * @param i The row to return. Row 3 corresponds to the position
    * of the transform matrix.
    *
    * @return The vector.
    */
Vector3 Matrix4::GetAxisVector(int i) const
{
	return Vector3(this->m[0][i],this->m[1][i],this->m[2][i]);
}

//Matrix4 Matrix4::ComboMatrix(Vector3 transXYZ, float rotateW, Vector3 rotateAxis, 
//		float scaleX, float scaleY, float scaleZ)
//{
//	Matrix4 combo;
//	Matrix4 translate,rotate;
//
//	if (transXYZ.x != 0 || transXYZ.y != 0 || transXYZ.z != 0)
//	{
//		translate = translate.TranslateMatrix(transXYZ);
//	}
//
//	if (rotateW != 0 && rotateAxis != Vector3(0,0,0))
//	{
//		Quaternion qRotate;
//
//		qRotate = Quaternion(rotateW,rotateAxis);
//
//		rotate = qRotate.GetRotationMatrix();
//
//	}
//
//	combo = translate * rotate;
//
//	return combo;
//
//}

