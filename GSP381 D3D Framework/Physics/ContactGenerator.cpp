#include "ContactGenerator.h"
#include "RigidBody.h"
#include "Contacts.h"
#include "AdvCollisions.h"
#include "MovementManager.h"
#include "../Graphics/ShapeManager.h"

//*************************
//FORCE REGISTRY
//*************************
void ContactGenRegistry::add(ContactGenerator* AddCGen)
{
	//create an entry for the registry
	ContactGenRegistration temp;

	//assign values
	temp.contactGenerator = AddCGen;

	//push entry onto CGenList
	m_CGenList.push_back(temp);
}

//removes a registered pair from registry(CGenList). if the pair is not registered, does nothing
void ContactGenRegistry::remove(int CGenID)
{
	for (unsigned i = 0; i < m_CGenList.size(); i++)
	{
		if (m_CGenList[i].contactGenerator->m_iCGenID == CGenID)
		{
			m_CGenList.erase(m_CGenList.begin() + i);
			return;
		}
	}

}

//clears all CGens from CGen list (does not deleteBody the objects themselves, just clears the list)
void ContactGenRegistry::clear()
{
	m_CGenList.clear();
}

void ContactGenRegistry::updateCGens(CollisionData &cData)
{
	//iterates through list of CGens and calls the update CGen method
	Registry::iterator i = m_CGenList.begin();
	for (i; i != m_CGenList.end(); i++)
	{
		unsigned added = i->contactGenerator->AddContact(cData.m_pContacts, cData.m_iContactsLeft);
		cData.AddContacts(added);

	}
}

void ContactGenRegistry::AddJointCGen(float bodyID, Vector3 vBodyConnectionPt, float otherID, Vector3 vOtherConnectionPt, float error)
{
	//create spring CGen generator
	Joint *jointCGen = new Joint();
	
	jointCGen->Set(MMI->GetBody(bodyID),vBodyConnectionPt, MMI->GetBody(otherID), vOtherConnectionPt, error);
	
	//add CGens to registry, connecting the bodies to each other
	add(jointCGen);
	
	//graphics representation with a line
	jointCGen->m_iGraphicsID = SMI->CreateLine(vBodyConnectionPt,vOtherConnectionPt);
	
}

void ContactGenRegistry::AddCableCGen(float bodyID, Vector3 vBodyConnectionPt, float otherID, Vector3 vOtherConnectionPt, float length)
{
	//create spring CGen generator
	Cable *cableCGen = new Cable();
	
	cableCGen->Set(MMI->GetBody(bodyID),vBodyConnectionPt, MMI->GetBody(otherID), vOtherConnectionPt, length);
	
	//add CGens to registry, connecting the bodies to each other
	add(cableCGen);
	
	//graphics representation with a line
	cableCGen->m_iGraphicsID = SMI->CreateLine(vBodyConnectionPt,vOtherConnectionPt);
	
}



///////////////////
// JOINTS
//////////////////
unsigned Joint::AddContact(Contact *contact, unsigned limit) const
{
	float test = 10.00f;
    // Calculate the position of each connection point in world coordinates
    Vector3 a_pos_world = body[0]->GetPointInWorldSpace(position[0]);
    Vector3 b_pos_world = body[1]->GetPointInWorldSpace(position[1]);

	//update the line
	SMI->SetLine(m_iGraphicsID,a_pos_world,b_pos_world);

	// Calculate the length of the joint
    Vector3 a_to_b = b_pos_world - a_pos_world;
    Vector3 normal = a_to_b;
    normal.Normalize();
    float length = a_to_b.Magnitude();

    // Check if it is violated
    if (float_abs(length) > error)
    {
        contact->body[0] = body[0];
        contact->body[1] = body[1];
        contact->m_vContactNormal = normal;
        contact->m_vContactPoint = (a_pos_world + b_pos_world) * 0.5f;
        contact->m_fPenetration = length-error;
		if (contact->m_fPenetration > test)
		{

		}

        contact->m_fFriction = 1.0f;
        contact->m_fRestitution = 0;
        return 1;
    }
	// Check if it is violated
    if (float_abs(length) < error)
    {
        contact->body[0] = body[0];
        contact->body[1] = body[1];
        contact->m_vContactNormal = normal * -1;
        contact->m_vContactPoint = (a_pos_world + b_pos_world) * 0.5f;
        contact->m_fPenetration = error - length;
		if (contact->m_fPenetration > test)
		{

		}

        contact->m_fFriction = 1.0f;
        contact->m_fRestitution = 0;
        return 1;
    }

    return 0;
}

void Joint::Set(RigidBody *a, const Vector3& a_pos,
                RigidBody *b, const Vector3& b_pos,
                float error)
{
    body[0] = a;
    body[1] = b;

    position[0] = a_pos;
    position[1] = b_pos;

    Joint::error = error;

	//create a line to show the joint


}


///////////////////
// Cable
//////////////////
unsigned Cable::AddContact(Contact *contact, unsigned limit) const
{
	float test = 10.00f;
    // Calculate the position of each connection point in world coordinates
    Vector3 a_pos_world = body[0]->GetPointInWorldSpace(position[0]);
    Vector3 b_pos_world = body[1]->GetPointInWorldSpace(position[1]);

	//update the line
	SMI->SetLine(m_iGraphicsID,a_pos_world,b_pos_world);

	// Calculate the length of the joint
    Vector3 a_to_b = b_pos_world - a_pos_world;
    Vector3 normal = a_to_b;
    normal.Normalize();
    float length = a_to_b.Magnitude();

    // Check if it is violated
    if (float_abs(length) > length)
    {
        contact->body[0] = body[0];
        contact->body[1] = body[1];
        contact->m_vContactNormal = normal;
        contact->m_vContactPoint = (a_pos_world + b_pos_world) * 0.5f;
        contact->m_fPenetration = length-length;
		if (contact->m_fPenetration > test)
		{

		}

        contact->m_fFriction = 1.0f;
        contact->m_fRestitution = 0;
        return 1;
    }
	

    return 0;
}

void Cable::Set(RigidBody *a, const Vector3& a_pos,
                RigidBody *b, const Vector3& b_pos,
                float length)
{
    body[0] = a;
    body[1] = b;

    position[0] = a_pos;
    position[1] = b_pos;

    Cable::length = length;

	//create a line to show the joint


}