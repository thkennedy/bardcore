#include "OBB.h"


OBB::OBB()
{
	m_vHalfExtents = Vector3(1.0f,1.0f,1.0f);

	m_qOrientation = Quaternion();

	m_fRadius = sqrt((m_vHalfExtents.x*m_vHalfExtents.x) +
		(m_vHalfExtents.y*m_vHalfExtents.y) + (m_vHalfExtents.z*m_vHalfExtents.z));

	CalculateLocalAxes();
}

OBB::OBB(int id, Vector3 vPosition, Vector3 vHalfExtents, int iBodyID)
	:AABB(id, vPosition, vHalfExtents,iBodyID)
{
	m_qOrientation = Quaternion();

	m_fRadius = sqrt((m_vHalfExtents.x*m_vHalfExtents.x) +
		(m_vHalfExtents.y*m_vHalfExtents.y) + (m_vHalfExtents.z*m_vHalfExtents.z));

	//setting local axies
	// x
	m_vLocalAxes[0] = Vector3(1.0f, 0.0f, 0.0f);
	m_vLocalAxes[1] = Vector3(0.0f, 1.0f, 0.0f);
	m_vLocalAxes[2] = Vector3(0.0f, 0.0f, 1.0f);

	CalculateLocalAxes();

}

OBB::OBB(AABB &box)
	:AABB(box)
{
	//setting local axies
	// x
	m_vLocalAxes[0] = Vector3(1.0f, 0.0f, 0.0f);
	m_vLocalAxes[1] = Vector3(0.0f, 1.0f, 0.0f);
	m_vLocalAxes[2] = Vector3(0.0f, 0.0f, 1.0f);

	CalculateLocalAxes();
}

OBB::~OBB()
{
}

/////////////////
// UTILITIES
////////////////

void OBB::VUpdate(float dt)
{
	// global update
	BoundingVolume::VUpdate(dt);

	//// obb specific updates
	////update orientation to body
	//m_qPrevOrientation = m_qOrientation;
	//m_qOrientation = MMI->GetBody(m_iBodyID)->GetOrientation();


	//re-calculate axis'
	CalculateLocalAxes();
	
}
void OBB::CalculateLocalAxes()
{
	D3DXMATRIX rMat;

	D3DXMatrixRotationQuaternion(&rMat, &m_qOrientation);

	//x axis
	m_vLocalAxes[0].x = rMat._11;
	m_vLocalAxes[0].y = rMat._12;
	m_vLocalAxes[0].z = rMat._13;

	//y axis
	m_vLocalAxes[1].x = rMat._21;
	m_vLocalAxes[1].y = rMat._22;
	m_vLocalAxes[1].z = rMat._23;

	//z axis
	m_vLocalAxes[2].x = rMat._31;
	m_vLocalAxes[2].y = rMat._32;
	m_vLocalAxes[2].z = rMat._33;



	

}

////////////////
// ACCESSORS
////////////////

////////////////
// MUTATORS
////////////////


void OBB::AdjustLocalAxis(int iAxis, float fAmount)
{
	m_vLocalAxes[iAxis].x += fAmount;
	m_vLocalAxes[iAxis].y += fAmount;
	m_vLocalAxes[iAxis].z += fAmount;

	m_vLocalAxes[iAxis].Normalize();
}