#pragma once
#include "BoundingVolume.h"
//#include "../Containers/tree-2.81/src/xinlin.hh"
#include "../Containers/tree-2.81/src/tree.hh"
#include <vector>
#include <map>

/*
	Advanced BV uses a BVH tree for detailed hit detection
	important items

	Rigid body id - points to rigid body that the advanced bv owns

	tree of bounding volumes

	counts of circles and obb's



*/

class Circle;
class OBB;

class AdvancedBV
	: public BoundingVolume
{
private:
	// id of body which this advanced bv belongs to
	int m_iBodyID;

	// Tree structure holding all bounding volumes for the rigid body
	tree<BoundingVolume*> m_tBoundingVolumes;

	// iterator to the root node
	tree<BoundingVolume*>::iterator m_tRoot;
	
	// used to traverse the tree for search functions
	tree<BoundingVolume*>::iterator m_tIterator;

	////vector of vector3 offsets for where bv's are
	std::map<LPCSTR, tree<BoundingVolume*>::iterator> m_mBones;

	// counts of object in the bvh
	int m_iCircleCount;
	int m_iOBBCount;

	
	// graphics ONLY FOR DEMONSTRATION
	std::vector<int> m_vGBoxes;
	std::vector<OBB*> m_vBoxes;
	

	
public:
	AdvancedBV(void);
	/**
	Para const for Advanced BV - only thing required is the id - other params
	iBodyID - Rigid Body ID
	vCenter - center of mass for model. default is the center computed by D3DXComputeBoundingSphere
	**NOTE** this is m_vPosition of the entire advanced BV, but not necessarilly the center of 
	fRadius - radius of bounding sphere as computed by D3DXComputeBoundingSphere
	vMin - lower left corner of bounding box calculated by D3DXComputeBoundingBox 
	vMax - upper right corner of bounding box calculated by D3DXComputeBoundingBox 

	**/
	AdvancedBV(int id, int iBodyID = -1, Vector3 vCenter = Vector3(0,0,0), float fRadius = 1.0f, 
		 bool bVisible = 0, Vector3 vMin = Vector3(0,0,0), Vector3 vMax = Vector3(0,0,0));

	~AdvancedBV(void);

	virtual void VUpdate(float dt);
	void CalculateInternals();





	//////////////////
	// CREATION
	/////////////////
	//adds a circle bv to the bvh tree
	int AddCircle(int iParentID, Vector3 vOffsetFromCenter, float fRadius);
	//adds a obb bv to the bvh tree- can pass transform in here or set manually later
	int AddOBB(LPCSTR sParentName, 
		Vector3 vOffsetFromCenter, 
		Vector3 vHalfExtents, 
		LPCSTR sBoneName = "Spleen",
		bool bVisible = true,
		Matrix4 &mTransform = Matrix4()
		);
	// lets you submit a vector of Matrix4 transform matrices from the 
	//	animation system to auto-create the boxes. columns 1-3 are x,y,z 
	//	rotation/scale and column 4 is translation
	std::vector<int>* CreateBones(int &id, Vector3 &vMeshPosition, 
								std::vector<Matrix4*> &vBones, 
								int iBodyID = -1);

	/////////////////
	// ACCESSORS
	/////////////////
	OBB* GetBone(LPCSTR sBoneName);
	OBB* GetBone(tree<BoundingVolume*>::iterator &iter) const;
	Circle* GetOutterCircle() const;
	std::vector<OBB*> GetChildren(LPCSTR sBoneName) const;
	std::vector<OBB*> GetChildren(tree<BoundingVolume*>::iterator &iter) const;
	int GetOBBCount() {return m_iOBBCount;}
	


	/////////////////
	// MUTATORS
	/////////////////
	void UpdateBoneTransform(LPCSTR sBoneName, Matrix4 mTransform);
	tree<BoundingVolume*>::iterator GoToRoot() const{tree<BoundingVolume*>::iterator Iter = m_tRoot; return Iter;}

};

