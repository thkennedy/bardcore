#pragma once
#include "BoundingVolume.h"


class Circle
	: public BoundingVolume
{
private:
	

public:
	Circle();
	Circle(float fRadius);
	Circle(int id, Vector3 vPosition, float fRadius, int iBodyID = -1);
	~Circle();


	virtual void VUpdate(float dt);



	////////////////
	// ACCESSORS
	////////////////
	

	////////////////
	// MUTATORS
	////////////////
	





};

