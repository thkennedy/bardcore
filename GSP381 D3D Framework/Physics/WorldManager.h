#pragma once

/*
	centralized control for demos
*/

#define WMI WorldManager::Instance()

//forward declarations


class WorldManager
{
private:
	WorldManager();

public:

	static WorldManager* Instance();
	
	~WorldManager();


	//////////////////
	// Utilities
	/////////////////
	void Init(int demoNum = 0);
	void Update(float dt);
	void CleanUp();


};

