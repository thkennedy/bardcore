#pragma once
#include "RigidBody.h"
#include "vector3.h"

/*
	This is my Box class - is can be axis-aligned or have an 
	orientation. 


*/ 
class Box
	: public RigidBody
{
protected:

	//box's half-extents
	Vector3 m_vHalfExtents;

	
	

public:
	Box();
	Box(int id, Vector3 vPosition, Vector3 vHalfExtents, int iBVid = -1);
	~Box();

	virtual void VUpdate(float dt);


};

