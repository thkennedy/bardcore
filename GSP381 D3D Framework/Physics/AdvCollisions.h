#pragma once
#include "Contacts.h"
#include <map>
#include <vector>
#include "Matrix4.h"
#include "../Containers/tree-2.81/src/tree.hh"
/*
	Advanced Collision Detector
	Most of the concepts and formatting come from Millington's book - integration
	came from myself
*/


 // Forward declarations of primitive friends
class IntersectionTests;
class CollisionDetector;
class RigidBody;
class Circle;
class AABB;
class OBB;
class Plane;
class BoundingVolume;
class ContactGenRegistry;
class AdvancedBV;


class IntersectionTests
{
public:

    static bool SphereAndHalfSpace(const Circle &sphere, const Plane &plane);

    static bool SphereAndSphere(const Circle &one, const Circle &two);

    static bool BoxAndBox(const OBB &one, const OBB &two);

    /**
        * Does an intersection test on an arbitrarily aligned box and a
        * half-space.
        *
        * The box is given as a transform matrix, including
        * position, and a vector of half-sizes for the extend of the
        * box along each local axis.
        *
        * The half-space is given as a direction (i.e. unit) vector and the
        * offset of the limiting plane from the origin, along the given
        * direction.
        */
    static bool BoxAndHalfSpace(
        const OBB &box,
        const Plane &plane);

	static bool CircleOBB(
		const Circle * Circle, 
		const OBB * Box);

	static bool CircleCircleDet(
		const BoundingVolume *BV1, 
		const BoundingVolume *BV2);

	static bool CircleOBB2(
		const Circle * Circle, 
		const OBB * Box);


};


/**
    * A helper structure that contains information for the detector to use
    * in building its contact data.
    */
struct CollisionData
{
    /**
        * Holds the base of the collision data: the first contact
        * in the array. This is used so that the contact pointer (below)
        * can be incremented each time a contact is detected, while
        * this pointer points to the first contact found.
        */
    Contact *m_pContactArray;

    /** Holds the contact array to write into. */
    Contact *m_pContacts;

    /** Holds the maximum number of m_pContacts the array can take. */
    int m_iContactsLeft;

    /** Holds the number of m_pContacts found so far. */
    unsigned m_uContactCount;

    /** Holds the m_fFriction value to write into any collisions. */
    float m_fFriction;

    /** Holds the m_fRestitution value to write into any collisions. */
    float m_fRestitution;

    /**
        * Holds the collision m_fTolerance, even uncolliding objects this
        * close should have collisions generated.
        */
    float m_fTolerance;

    /**
        * Checks if there are more m_pContacts available in the contact
        * data.
        */
    bool HasMoreContacts()
    {
        return m_iContactsLeft > 0;
    }

    /**
        * Resets the data so that it has no used m_pContacts recorded.
        */
    void Reset(unsigned maxContacts)
    {
        m_iContactsLeft = maxContacts;
        m_uContactCount = 0;
        m_pContacts = m_pContactArray;
    }

    /**
        * Notifies the data that the given number of m_pContacts have
        * been added.
        */
    void AddContacts(unsigned count)
    {
        // Reduce the number of m_pContacts remaining, add number used
        m_iContactsLeft -= count;
        m_uContactCount += count;

        // Move the array forward
        m_pContacts += count;
    }
};

/**
    * A wrapper class that holds the fine grained collision detection
    * routines.
    *
    * Each of the functions has the same format: it takes the details
    * of two objects, and a pointer to a contact array to fill. It
    * returns the number of m_pContacts it wrote into the array.
    */
class ACollisionDetector
{
public:

    static unsigned SphereAndHalfSpace(
        const Circle &sphere,
        const Plane &plane,
        CollisionData *data
        );

    static unsigned SphereAndTruePlane(
        const Circle &sphere,
        const Plane &plane,
        CollisionData *data
        );

    static unsigned SphereAndSphere(
        const Circle &one,
        const Circle &two,
        CollisionData *data
        );
	static unsigned SphereAndAdvBV(
		const Circle &one,
		const AdvancedBV &two,
		CollisionData *data
		);

	/** Recursive Method for BVH Methods **/
	static bool AdvSphereAndBox(
		const AdvancedBV &advBV, 
		const Circle &sphere, 
		tree<BoundingVolume*>::iterator &Iter,
		CollisionData *data,
		OBB &pBone,
		bool &bContactCreated
		);



    /**
        * Does a collision test on a collision box and a plane representing
        * a half-space (i.e. the normal of the plane
        * points out of the half-space).
        */
    static unsigned BoxAndHalfSpace(
        const OBB &box,
        const Plane &plane,
        CollisionData *data
        );

    static unsigned BoxAndBox(
        const OBB &one,
        const OBB &two,
        CollisionData *data
        );

    static unsigned BoxAndPoint(
        const OBB &box,
        const Vector3 &point,
        CollisionData *data
        );

    static unsigned BoxAndSphere(
        const OBB &box,
        const Circle &sphere,
        CollisionData *data
        );

	static unsigned BoxAndAdvBV(
        const OBB &box,
        const AdvancedBV &AdvBv,
        CollisionData *data
        );


	/** Recursive Method for BVH Methods **/
	static bool AdvBoxAndBox(
		const AdvancedBV &advBV, 
		const OBB &box, 
		tree<BoundingVolume*>::iterator &Iter,
		CollisionData *data,
		OBB &pBone,
		bool &bContactCreated
		);
};


/*
	wrapper class - contains the separate parts of the collision system
	1 - intersection tests
	2 - contact generation
	3 - contact resolution
*/
#define ACMI AdvCollisions::Instance()

class AdvCollisions
{
private:
	//private const for singleton
	AdvCollisions();

	/** Holds the maximum number of contacts. */
    const static unsigned maxContacts = 256* 8;

    /** Holds the array of contacts. */
    Contact m_Contacts[maxContacts];

    /** Holds the collision data structure for collision detection. */
    CollisionData m_cData;

    /** Holds the contact resolver. */
    ContactResolver m_Resolver;

	/** Processes the contact generation code. */
    void GenerateContacts();

	// map of all bv's for lookup (quicker than iter through)
	std::map<int, BoundingVolume *> m_mBoundingVolumes;
	
	//object vectors for collision detection
	std::vector<Circle *> m_vCircle;
	std::vector<OBB *>m_vOBB;
	std::vector<Plane *> m_vPlane;
	std::vector<AdvancedBV *> m_vAdvancedBVs;
	
	// current id for next bv creations
	int m_iBoundingVolumeID;
	
	// contact generator registry
	ContactGenRegistry *m_pCGens;

	
	    
public:

	static AdvCollisions* Instance();
	~AdvCollisions();

	/** Update the objects. */
    void Update(float dt);

	////////////////////////
	//	BV CREATION
	////////////////////////

	int CreateCircle(Vector3 vPosition, float fRadius, int iBodyID);
	int CreateOBB(Vector3 vPosition, Vector3 vHalfExtents, int iBodyID);
	int CreatePlane(Vector3 vNormal, float fOffset);
	AdvancedBV* CreateAdvancedBV(Vector3 vPosition, float fRadius = 30.0f , bool bVisible = 0);
	void AddBone(LPCSTR sParentName, Vector3 vOffsetFromCenter, Vector3 vHalfExtents, 
		LPCSTR sBoneName, Matrix4 mTransform);

	////////////////////////
	//	BV DELETION
	////////////////////////
	//allows for deletion of a bounding volume
	void DeleteBV(int id);
	void Clear();

	////////////////////////
	//	BV ACCESS
	////////////////////////
	//returns ptr to bv based on id
	BoundingVolume* GetBV(int id);
	//returns ptr to circle bv based on id
	Circle* GetCircle(int id);
	// returns ptr to aabb bv based on id
	AABB* GetAABB(int id);
	// return ptr to obb bv based on id
	OBB* GetOBB(int id);
	// return ptr to plane bv based on id
	Plane* GetPlane(int id);

	///////////////////////
	// CONTACT GENERATORS 
	///////////////////////
	void AddJointCGen(unsigned int bodyID, Vector3 vBodyConnectionPt, unsigned int otherID, Vector3 vOtherConnectionPt, float error);
	void AddCableCGen(unsigned int bodyID, Vector3 vBodyConnectionPt, unsigned int otherID, Vector3 vOtherConnectionPt, float length);

	

	

};