#pragma once

#include <math.h>
#include "Matrix3.h"
#include "Vector3.h"
#include <d3dx9.h>



class Quaternion
	: public D3DXQUATERNION
{

public:
	
		
	float data[4];

	Quaternion(Vector3 axis,float theta);
	Quaternion(float w =1.0f,float x=0.0f,float=0.0f,float=0.0f);
	Quaternion(float iw, Vector3 qVec);
	Quaternion(const D3DXQUATERNION &quat);
	Quaternion(const Quaternion &quat);
	
	void Normalize();
	Quaternion operator *(Quaternion &multiplier);
	void rotateByVector(const Vector3& vector);
	void AddScaledVector(const Vector3& vector,float scale);
	void operator*=(Quaternion q);
	Quaternion operator *(float);
	void operator +=(Quaternion q);
	Quaternion operator +(Quaternion q2);
	void CreateBasis(Vector3* pA, Vector3* pB, Vector3* pC);
	Quaternion GetConjugate();

	/*Matrix3 Rotate(Quaternion pQuat);*/

	/*Matrix3 GetRotationMatrix();*/




};


