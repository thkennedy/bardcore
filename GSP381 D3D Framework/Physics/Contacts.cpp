#include "Contacts.h"
#include "RigidBody.h"

// Contact implementation

void Contact::SetBodyData(RigidBody* one, RigidBody *two,
                          float friction, float restitution)
{
    Contact::body[0] = one;
    Contact::body[1] = two;
    Contact::m_fFriction = friction;
    Contact::m_fRestitution = restitution;
}

void Contact::MatchAwakeState()
{
    // Collisions with the world never cause a body to wake up.
    if (!body[1]) return;

    bool body0awake = body[0]->GetAwake();
    bool body1awake = body[1]->GetAwake();

    // Wake up only the sleeping one
    if (body0awake ^ body1awake) {
        if (body0awake) body[1]->SetAwake();
        else body[0]->SetAwake();
    }
}

void Contact::SwapBodies()
{
    m_vContactNormal *= -1;

    RigidBody *temp = body[0];
    body[0] = body[1];
    body[1] = temp;
}


inline
void Contact::CalculateContactBasis()
{
    Vector3 contactTangent[2];

    // Check whether the Z-axis is nearer to the X or Y axis
    if (fabs(m_vContactNormal.x) > fabs(m_vContactNormal.y))
    {
        // Scaling factor to ensure the results are normalised
        const float s = (float)1.0f/sqrtf(m_vContactNormal.z*m_vContactNormal.z +
            m_vContactNormal.x*m_vContactNormal.x);

        // The new X-axis is at right angles to the world Y-axis
        contactTangent[0].x = m_vContactNormal.z*s;
        contactTangent[0].y = 0;
        contactTangent[0].z = -m_vContactNormal.x*s;

        // The new Y-axis is at right angles to the new X- and Z- axes
        contactTangent[1].x = m_vContactNormal.y*contactTangent[0].x;
        contactTangent[1].y = m_vContactNormal.z*contactTangent[0].x -
            m_vContactNormal.x*contactTangent[0].z;
        contactTangent[1].z = -m_vContactNormal.y*contactTangent[0].x;
    }
    else
    {
        // Scaling factor to ensure the results are normalised
        const float s = (float)1.0/sqrtf(m_vContactNormal.z*m_vContactNormal.z +
            m_vContactNormal.y*m_vContactNormal.y);

        // The new X-axis is at right angles to the world X-axis
        contactTangent[0].x = 0;
        contactTangent[0].y = -m_vContactNormal.z*s;
        contactTangent[0].z = m_vContactNormal.y*s;

        // The new Y-axis is at right angles to the new X- and Z- axes
        contactTangent[1].x = m_vContactNormal.y*contactTangent[0].z -
            m_vContactNormal.z*contactTangent[0].y;
        contactTangent[1].y = -m_vContactNormal.x*contactTangent[0].z;
        contactTangent[1].z = m_vContactNormal.x*contactTangent[0].y;
    }

    // Make a matrix from the three vectors.
    m_mContactToWorld.SetComponents(
        m_vContactNormal,
        contactTangent[0],
        contactTangent[1]);
}

Vector3 Contact::CalculateLocalVelocity(unsigned bodyIndex, float dt)
{
    RigidBody *thisBody = body[bodyIndex];

    // Work out the velocity of the contact point.
    Vector3 velocity =
        thisBody->GetRotation() % m_vRelativeContactPosition[bodyIndex];
    velocity += thisBody->GetVelocity();

    // Turn the velocity into contact-coordinates.
    Vector3 m_vContactVelocity = m_mContactToWorld.transformTranspose(velocity);

    // Calculate the ammount of velocity that is due to forces without
    // reactions.
    Vector3 accVelocity = thisBody->GetLastFrameAcceleration();//* dt;

    // Calculate the velocity in contact-coordinates.
    accVelocity = m_mContactToWorld.transformTranspose(accVelocity);

    // We ignore any component of acceleration in the contact normal
    // direction, we are only interested in planar acceleration
    accVelocity.x = 0;

    // Add the planar velocities - if there's enough m_fFriction they will
    // be removed during velocity resolution
    m_vContactVelocity += accVelocity;

    // And return it
    return m_vContactVelocity;
}


void Contact::CalculateDesiredDeltaVelocity(float dt)
{
    const static float velocityLimit = (float)0.01f;

    // Calculate the acceleration induced velocity accumulated this frame
    float velocityFromAcc = 0;

    if (body[0]->GetAwake())
    {
		velocityFromAcc =
        body[0]->GetLastFrameAcceleration() * dt * m_vContactNormal;
    }

    if (body[1] && body[1]->GetAwake())
    {
        velocityFromAcc -=
            body[1]->GetLastFrameAcceleration() * dt * m_vContactNormal;
    }

    // If the velocity is very slow, limit the m_fRestitution
    float thisRestitution = m_fRestitution;
    if (fabs(m_vContactVelocity.x) < velocityLimit)
    {
        thisRestitution = (float)0.0f;
    }

    // Combine the bounce velocity with the removed
    // acceleration velocity.
    m_fDesiredDeltaVelocity =
        -m_vContactVelocity.x
        -thisRestitution * (m_vContactVelocity.x - velocityFromAcc);
}


void Contact::CalculateInternals(float dt)
{
    // Check if the first object is NULL, and swap if it is.
    if (!body[0]) SwapBodies();
    assert(body[0]);

    // Calculate an set of axis at the contact point.
    CalculateContactBasis();

    // Store the relative position of the contact relative to each body
    m_vRelativeContactPosition[0] = m_vContactPoint - body[0]->GetPosition();
    if (body[1]) {
        m_vRelativeContactPosition[1] = m_vContactPoint - body[1]->GetPosition();
    }

    // Find the relative velocity of the bodies at the contact point.
    m_vContactVelocity = CalculateLocalVelocity(0, dt);
    if (body[1]) {
        m_vContactVelocity -= CalculateLocalVelocity(1, dt);
    }

    // Calculate the desired change in velocity for resolution
    CalculateDesiredDeltaVelocity(dt);
}

void Contact::ApplyVelocityChange(Vector3 velocityChange[2],
                                  Vector3 rotationChange[2])
{
    // Get hold of the Inverse mass and Inverse inertia tensor, both in
    // world coordinates.
    Matrix3 InverseInertiaTensor[2];
    body[0]->GetInverseInertiaTensorWorld(InverseInertiaTensor[0]);
    if (body[1])
        body[1]->GetInverseInertiaTensorWorld(InverseInertiaTensor[1]);

    // We will calculate the impulse for each contact axis
    Vector3 impulseContact;

    if (m_fFriction == (float)0.0)
    {
        // Use the short format for frictionless contacts
        impulseContact = CalculateFrictionlessImpulse(InverseInertiaTensor);
    }
    else
    {
        // Otherwise we may have impulses that aren't in the direction of the
        // contact, so we need the more complex version.
        impulseContact = CalculateFrictionImpulse(InverseInertiaTensor);
    }

    // Convert impulse to world coordinates
    Vector3 impulse = m_mContactToWorld.Transform(impulseContact);

    // Split in the impulse into linear and rotational components
    Vector3 impulsiveTorque = m_vRelativeContactPosition[0] % impulse;
    rotationChange[0] = InverseInertiaTensor[0].Transform(impulsiveTorque);
	if (body[0]->GetInverseMass() <= 0.0f)
		rotationChange[0] = Vector3(0,0,0);
	velocityChange[0].Clear();
    velocityChange[0].AddScaledVector(impulse, body[0]->GetInverseMass());

    // Apply the changes
    body[0]->AddVelocity(velocityChange[0]);
    body[0]->AddRotation(rotationChange[0]);

    if (body[1])
    {
        if (body[1]->GetInverseMass() > 0.0f)
			{
			// Work out body one's linear and angular changes
			Vector3 impulsiveTorque = impulse % m_vRelativeContactPosition[1];
			rotationChange[1] = InverseInertiaTensor[1].Transform(impulsiveTorque);
			/*if (body[1]->GetInverseMass() <= 0.0f)
				rotationChange[1] = Vector3(0,0,0);*/
			velocityChange[1].Clear();
			velocityChange[1].AddScaledVector(impulse, -body[1]->GetInverseMass());
		
			// And apply them.
			body[1]->AddVelocity(velocityChange[1]);
			body[1]->AddRotation(rotationChange[1]);
		}
    }
}

inline
Vector3 Contact::CalculateFrictionlessImpulse(Matrix3 * InverseInertiaTensor)
{
    Vector3 impulseContact;

    // Build a vector that shows the change in velocity in
    // world space for a unit impulse in the direction of the contact
    // normal.
    Vector3 deltaVelWorld = m_vRelativeContactPosition[0] % m_vContactNormal;
    deltaVelWorld = InverseInertiaTensor[0].Transform(deltaVelWorld);
    deltaVelWorld = deltaVelWorld % m_vRelativeContactPosition[0];

    // Work out the change in velocity in contact coordiantes.
    float deltaVelocity = deltaVelWorld * m_vContactNormal;

    // Add the linear component of velocity change
    deltaVelocity += body[0]->GetInverseMass();

    // Check if we need to the second body's data
    if (body[1])
    {
        // Go through the same transformation sequence again
        Vector3 deltaVelWorld = m_vRelativeContactPosition[1] % m_vContactNormal;
        deltaVelWorld = InverseInertiaTensor[1].Transform(deltaVelWorld);
        deltaVelWorld = deltaVelWorld % m_vRelativeContactPosition[1];

        // Add the change in velocity due to rotation
        deltaVelocity += deltaVelWorld * m_vContactNormal;

        // Add the change in velocity due to linear motion
        deltaVelocity += body[1]->GetInverseMass();
    }

    // Calculate the required size of the impulse
    impulseContact.x = m_fDesiredDeltaVelocity / deltaVelocity;
    impulseContact.y = 0;
    impulseContact.z = 0;
    return impulseContact;
}

inline
Vector3 Contact::CalculateFrictionImpulse(Matrix3 * InverseInertiaTensor)
{
    Vector3 impulseContact;
    float InverseMass = body[0]->GetInverseMass();

    // The equivalent of a cross product in matrices is multiplication
    // by a skew symmetric matrix - we build the matrix for converting
    // between linear and angular quantities.
    Matrix3 impulseToTorque;
    impulseToTorque.SetSkewSymmetric(m_vRelativeContactPosition[0]);


    // Build the matrix to convert contact impulse to change in velocity
    // in world coordinates.
    Matrix3 deltaVelWorld = impulseToTorque;
    deltaVelWorld *= InverseInertiaTensor[0];
    deltaVelWorld *= impulseToTorque;
    deltaVelWorld *= -1;

    // Check if we need to add body two's data
    if (body[1])
    {
        // Set the cross product matrix
        impulseToTorque.SetSkewSymmetric(m_vRelativeContactPosition[1]);

        // Calculate the velocity change matrix
        Matrix3 deltaVelWorld2 = impulseToTorque;
        deltaVelWorld2 *= InverseInertiaTensor[1];
        deltaVelWorld2 *= impulseToTorque;
        deltaVelWorld2 *= -1;

        // Add to the total delta velocity.
        deltaVelWorld += deltaVelWorld2;

        // Add to the Inverse mass
        InverseMass += body[1]->GetInverseMass();
    }

    // Do a change of basis to convert into contact coordinates.
	Matrix3 deltaVelocity = m_mContactToWorld.Transpose();
    deltaVelocity *= deltaVelWorld;
    deltaVelocity *= m_mContactToWorld;

    // Add in the linear velocity change
    deltaVelocity.data[0] += InverseMass;
    deltaVelocity.data[4] += InverseMass;
    deltaVelocity.data[8] += InverseMass;

    // Invert to get the impulse needed per unit velocity
    Matrix3 impulseMatrix = deltaVelocity.Inverse();

    // Find the target velocities to kill
    Vector3 velKill(m_fDesiredDeltaVelocity,
        -m_vContactVelocity.y,
        -m_vContactVelocity.z);

    // Find the impulse to kill target velocities
    impulseContact = impulseMatrix.Transform(velKill);

    // Check for exceeding m_fFriction
    float planarImpulse = sqrtf(
        impulseContact.y*impulseContact.y +
        impulseContact.z*impulseContact.z
        );
    if (planarImpulse > impulseContact.x * m_fFriction)
    {
        // We need to use dynamic m_fFriction
        impulseContact.y /= planarImpulse;
        impulseContact.z /= planarImpulse;

        impulseContact.x = deltaVelocity.data[0] +
            deltaVelocity.data[1]*m_fFriction*impulseContact.y +
            deltaVelocity.data[2]*m_fFriction*impulseContact.z;

        impulseContact.x = m_fDesiredDeltaVelocity / impulseContact.x;
        impulseContact.y *= m_fFriction * impulseContact.x;
        impulseContact.z *= m_fFriction * impulseContact.x;
    }
    return impulseContact;
}

void Contact::ApplyPositionChange(Vector3 linearChange[2],
                                  Vector3 angularChange[2],
                                  float fPenetration)
{
    const float angularLimit = (float)0.2f;
    float angularMove[2];
    float linearMove[2];

    float totalInertia = 0;
    float linearInertia[2];
    float angularInertia[2];

    // We need to work out the inertia of each object in the direction
    // of the contact normal, due to angular inertia only.
    for (unsigned i = 0; i < 2; i++) if (body[i])
    {
        Matrix3 InverseInertiaTensor;
        body[i]->GetInverseInertiaTensorWorld(InverseInertiaTensor);

        // Use the same procedure as for calculating frictionless
        // velocity change to work out the angular inertia.
        Vector3 angularInertiaWorld =
            m_vRelativeContactPosition[i] % m_vContactNormal;
        angularInertiaWorld =
            InverseInertiaTensor.Transform(angularInertiaWorld);
        angularInertiaWorld =
            angularInertiaWorld % m_vRelativeContactPosition[i];
        angularInertia[i] =
            angularInertiaWorld * m_vContactNormal;

        // The linear component is simply the Inverse mass
        linearInertia[i] = body[i]->GetInverseMass();

        // Keep track of the total inertia from all components
        totalInertia += linearInertia[i] + angularInertia[i];

        // We break the loop here so that the totalInertia value is
        // completely calculated (by both iterations) before
        // continuing.
    }

    // Loop through again calculating and applying the changes
    for (unsigned i = 0; i < 2; i++) if (body[i])
    {
        // The linear and angular movements required are in proportion to
        // the two Inverse inertias.
        float sign = (i == 0)?1:-1.0f;
        angularMove[i] =
            sign * fPenetration * (angularInertia[i] / totalInertia);
        linearMove[i] =
            sign * fPenetration * (linearInertia[i] / totalInertia);

        // To avoid angular projections that are too great (when mass is large
        // but inertia tensor is small) limit the angular move.
        Vector3 projection = m_vRelativeContactPosition[i];
        projection.AddScaledVector(m_vContactNormal, 
			-(m_vRelativeContactPosition[i] * m_vContactNormal )
			);

        // Use the small angle approximation for the sine of the angle (i.e.
        // the magnitude would be sine(angularLimit) * projection.magnitude
        // but we approximate sine(angularLimit) to angularLimit).
		float maxMagnitude = angularLimit * projection.Magnitude();

        if (angularMove[i] < -maxMagnitude)
        {
            float totalMove = angularMove[i] + linearMove[i];
            angularMove[i] = -maxMagnitude;
            linearMove[i] = totalMove - angularMove[i];
        }
        else if (angularMove[i] > maxMagnitude)
        {
            float totalMove = angularMove[i] + linearMove[i];
            angularMove[i] = maxMagnitude;
            linearMove[i] = totalMove - angularMove[i];
        }

        // We have the linear amount of movement required by turning
        // the rigid body (in angularMove[i]). We now need to
        // calculate the desired rotation to achieve that.
        if (angularMove[i] == 0)
        {
            // Easy case - no angular movement means no rotation.
            angularChange[i].Clear();
        }
        else
        {
            // Work out the direction we'd like to rotate in.
            Vector3 targetAngularDirection =
                m_vRelativeContactPosition[i] % m_vContactNormal;

            Matrix3 InverseInertiaTensor;
            body[i]->GetInverseInertiaTensorWorld(InverseInertiaTensor);

            // Work out the direction we'd need to rotate to achieve that
            angularChange[i] =
                InverseInertiaTensor.Transform(targetAngularDirection) *
                (angularMove[i] / angularInertia[i]);

			if (body[i]->GetInverseMass() <= 0.0f)
				angularChange[i].Clear();
        }

        // Velocity change is easier - it is just the linear movement
        // along the contact normal.
        linearChange[i] = m_vContactNormal * linearMove[i];

        // Now we can start to apply the values we've calculated.
        // Apply the linear movement
        if (body[i]->GetInverseMass() > 0.0f)
		{
		
			Vector3 pos;
			pos = body[i]->GetPosition();
			pos.AddScaledVector(m_vContactNormal, linearMove[i]);
			body[i]->SetPosition(pos);

       // And the change in orientation
		
			Quaternion q;
			body[i]->GetOrientation(q);
			q.AddScaledVector(angularChange[i], ((float)1.0));
			body[i]->SetOrientation(q);
		}

        // We need to calculate the derived data for any body that is
        // asleep, so that the changes are reflected in the object's
        // data. Otherwise the resolution will not change the position
        // of the object, and the next collision detection round will
        // have the same m_fPenetration.
        if (!body[i]->GetAwake()) body[i]->CalculateDerivedData();
    }
}





// Contact resolver implementation

ContactResolver::ContactResolver(unsigned iterations,
                                 float m_fVelocityEpsilon,
                                 float m_fPositionEpsilon)
{
    SetIterations(iterations, iterations);
    SetEpsilon(m_fVelocityEpsilon, m_fPositionEpsilon);
}

ContactResolver::ContactResolver(unsigned m_uVelocityIterations,
                                 unsigned m_uPositionIterations,
                                 float m_fVelocityEpsilon,
                                 float m_fPositionEpsilon)
{
    SetIterations(m_uVelocityIterations);
    SetEpsilon(m_fVelocityEpsilon, m_fPositionEpsilon);
}

void ContactResolver::SetIterations(unsigned iterations)
{
    SetIterations(iterations, iterations);
}

void ContactResolver::SetIterations(unsigned m_uVelocityIterations,
                                    unsigned m_uPositionIterations)
{
    ContactResolver::m_uVelocityIterations = m_uVelocityIterations;
    ContactResolver::m_uPositionIterations = m_uPositionIterations;
}

void ContactResolver::SetEpsilon(float m_fVelocityEpsilon,
                                 float PositionEpsilon)
{
    ContactResolver::m_fVelocityEpsilon = m_fVelocityEpsilon;
    ContactResolver::m_fPositionEpsilon = PositionEpsilon;
}

void ContactResolver::ResolveContacts(Contact *contacts,
                                      unsigned numContacts,
                                      float dt)
{
    // Make sure we have something to do.
    if (numContacts == 0) return;
    if (!IsValid()) return;

    // Prepare the contacts for processing
    PrepareContacts(contacts, numContacts, dt);

    // Resolve the interpenetration problems with the contacts.
    AdjustPositions(contacts, numContacts, dt);

    // Resolve the velocity problems with the contacts.
    AdjustVelocities(contacts, numContacts, dt);
}

void ContactResolver::PrepareContacts(Contact* contacts,
                                      unsigned numContacts,
                                      float dt)
{
    // Generate contact velocity and axis information.
    Contact* lastContact = contacts + numContacts;
    for (Contact* contact=contacts; contact < lastContact; contact++)
    {
        // Calculate the internal contact data (inertia, basis, etc).
        contact->CalculateInternals(dt);
    }
}

void ContactResolver::AdjustVelocities(Contact *c,
                                       unsigned numContacts,
                                       float dt)
{
    Vector3 velocityChange[2], rotationChange[2];
    Vector3 deltaVel;

    // iteratively handle impacts in order of severity.
    m_uVelocityIterationsUsed = 0;
    while (m_uVelocityIterationsUsed < m_uVelocityIterations)
    {
        // Find contact with maximum magnitude of probable velocity change.
        float max = m_fVelocityEpsilon;
        unsigned index = numContacts;
        for (unsigned i = 0; i < numContacts; i++)
        {
            if (c[i].m_fDesiredDeltaVelocity > max)
            {
                max = c[i].m_fDesiredDeltaVelocity;
                index = i;
            }
        }
        if (index == numContacts) break;

        // Match the awake state at the contact
        c[index].MatchAwakeState();

        // Do the resolution on the contact that came out top.
        c[index].ApplyVelocityChange(velocityChange, rotationChange);

        // With the change in velocity of the two bodies, the update of
        // contact velocities means that some of the relative closing
        // velocities need recomputing.
        for (unsigned i = 0; i < numContacts; i++)
        {
            // Check each body in the contact
            for (unsigned b = 0; b < 2; b++) if (c[i].body[b])
            {
                // Check for a match with each body in the newly
                // resolved contact
                for (unsigned d = 0; d < 2; d++)
                {
                    if (c[i].body[b] == c[index].body[d])
                    {
                        deltaVel = velocityChange[d] +
                            (rotationChange[d] % c[i].m_vRelativeContactPosition[b]);

                        // The sign of the change is negative if we're dealing
                        // with the second body in a contact.
                        c[i].m_vContactVelocity +=
                            c[i].m_mContactToWorld.transformTranspose(deltaVel)
                            * (b?-1.0f:1.0f);
                        c[i].CalculateDesiredDeltaVelocity(dt);
                    }
                }
            }
        }
        m_uVelocityIterationsUsed++;
    }
}

void ContactResolver::AdjustPositions(Contact *c,
                                      unsigned numContacts,
                                      float dt)
{
    unsigned i,index;
    Vector3 linearChange[2], angularChange[2];
    float max;
    Vector3 deltaPosition;

    // iteratively resolve interpenetrations in order of severity.
    m_uPositionIterationsUsed = 0;
    while (m_uPositionIterationsUsed < m_uPositionIterations)
    {
        // Find biggest m_fPenetration
        max = m_fPositionEpsilon;
        index = numContacts;
        for (i=0; i<numContacts; i++)
        {
            if (c[i].m_fPenetration > max)
            {
                max = c[i].m_fPenetration;
                index = i;
            }
        }
        if (index == numContacts) break;

        // Match the awake state at the contact
        c[index].MatchAwakeState();

        // Resolve the m_fPenetration.
        c[index].ApplyPositionChange(
            linearChange,
            angularChange,
            max);

        // Again this action may have changed the m_fPenetration of other
        // bodies, so we update contacts.
        for (i = 0; i < numContacts; i++)
        {
            // Check each body in the contact
            for (unsigned b = 0; b < 2; b++) if (c[i].body[b])
            {
                // Check for a match with each body in the newly
                // resolved contact
                for (unsigned d = 0; d < 2; d++)
                {
                    if (c[i].body[b] == c[index].body[d])
                    {
                        deltaPosition = linearChange[d] +
                            (angularChange[d] %  c[i].m_vRelativeContactPosition[b]);

                        // The sign of the change is positive if we're
                        // dealing with the second body in a contact
                        // and negative otherwise (because we're
                        // subtracting the resolution)..
                        c[i].m_fPenetration +=
                            deltaPosition * c[i].m_vContactNormal
                            * (b?1:-1);
                    }
                }
            }
        }
        m_uPositionIterationsUsed++;
    }
}

