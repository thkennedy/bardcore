#include "Box.h"
#include "../Graphics/ShapeManager.h"
#include "CollisionDetector.h"
#include "../Graphics/D3DBox.h"
#include "RigidBody.h"
#include "AdvCollisions.h"



Box::Box()
	:RigidBody(-1, Vector3(0,0,0))
{
	m_iGraphicsID = SMI->CreateBox(1,1,1);
}

Box::Box(int id, Vector3 vPosition, Vector3 vHalfExtents,int iBVid)
	:RigidBody(id, vPosition, iBVid), m_vHalfExtents(vHalfExtents)
{
	m_iGraphicsID = SMI->CreateBox(vHalfExtents.x,vHalfExtents.y,vHalfExtents.z);

	m_qOrientation = Quaternion();

	m_fInverseMass = 1.0f / 10.0f;

	// inertial tensor for solid cube
	
	Matrix3 InertiaTensor;

	InertiaTensor.SetBlockInertiaTensor(vHalfExtents, GetMass());

	/*float x = 0.08333f * (1.0f/m_fInverseMass) * (powf(vHalfExtents.y,2) + powf(m_vHalfExtents.z,2));
	float y = 0.08333f * (1.0f/m_fInverseMass) * (powf(vHalfExtents.x,2) + powf(m_vHalfExtents.z,2));
	float z = 0.08333f * (1.0f/m_fInverseMass) * (powf(vHalfExtents.x,2) + powf(m_vHalfExtents.y,2));

	InertiaTensor.data[0] = x;
	InertiaTensor.data[4] = y;
	InertiaTensor.data[8] = z;*/

	SetInertiaTensor(InertiaTensor);

	CalculateDerivedData();
			
}

Box::~Box(void)
{
}

void Box::VUpdate(float dt)
{
	//update physics
	RigidBody::VUpdate(dt);

	// update bv loc
	ACMI->GetBV(m_iBoundingVolumeID)->VUpdate(dt);
	
	//update graphics position
	SMI->GetBox(m_iGraphicsID)->SetPosition(m_vPosition);

	//update graphics orientation
	SMI->GetBox(m_iGraphicsID)->SetHeading(m_qOrientation);
	
}