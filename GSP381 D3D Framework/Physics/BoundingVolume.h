#pragma once
#include "Vector3.h"
#include "Quaternion.h"
#include <vector>
#include "Matrix4.h"


/*
base bv class - handles base data values such as position, id's, active
Also, handles features such as registration/deregistration for octants

*/

//forward declarations
class OctreeNode;	//for future use - node(s) object is in


class BoundingVolume
{
protected:

	// used for getting the bv
	int m_iBoundingVolumeID;

	// body id so I know which body to move
	int m_iBodyID;

	Vector3 m_vPosition;		//BV's center point in 3d space

	float m_fRadius;			//All BV's have a radius for rough collision detection and interpenetration fixes
	
	std::vector<OctreeNode*> m_vRegisteredNodes;	//nodes of octree I'm registered in

	bool m_bActive;				// Only active BV's do collision checks - default is TRUE

	// Offset of this primitive from the given rigid body (for building custom bv's)
	Matrix4 m_mOffset;

	// transform of the bv - offset of bv * tranform of body
	Matrix4 m_mTransform;	

	Quaternion m_qOrientation;	// BV's orientation in Quaternion form
	Quaternion m_qPrevOrientation;	// BV's previous orientation - used to save calcs


	
public:
	BoundingVolume(void);
	BoundingVolume(int BVid, Vector3 vPosition, int iBodyID = -1);
	~BoundingVolume(void);


	virtual void VUpdate(float dt);
	virtual void CalculateInternals();
	void CalculateTransformMatrix(Matrix4 &transformMatrix, Vector3 &position,
			Quaternion &orientation);

	////////////////////////
	// ACCESSORS
	///////////////////////
	int GetID() const {return m_iBoundingVolumeID;}
	int GetBodyID() const {return m_iBodyID;}
	Vector3 GetPosition() const {return m_vPosition;}
	float GetPosition(int i) ;
	bool GetActive() const {return m_bActive;}
	float GetRadius() const {return m_fRadius;}
	Vector3 GetAxis(unsigned index) const;
	const Matrix4& GetTransform() const;
	Quaternion	GetOrientationQ() {return m_qOrientation;}
	


	////////////////////////
	// MUTATORS
	///////////////////////
	void SetID(int id) {m_iBoundingVolumeID = id;}
	void SetBodyID(int id) {m_iBodyID = id;}
	void SetPosition(float x, float y, float z);
	void SetPosition(Vector3 vPosition);
	void SetActive(bool bActive) {m_bActive = bActive;}
	void SetRadius(float fRadius) {m_fRadius = fRadius;}
	void SetTransform (Matrix4 mNewTransform);
	void SetOrientationQ(Quaternion qOrientation);
	void SetOrientationQ(Vector3 vAxis, float fRotation);
	

	
};

