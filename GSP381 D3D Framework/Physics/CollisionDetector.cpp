#include "CollisionDetector.h"
#include "Circle.h"
#include "AABB.h"
#include "OBB.h"
#include "MovementManager.h"
#include "RigidBody.h"

CollisionDetector::CollisionDetector()
{
	m_iBoundingVolumeID = 0;	
}
CollisionDetector::~CollisionDetector()
{

}
CollisionDetector * CollisionDetector::Instance()
{
	static CollisionDetector instance;

	return &instance;
}

////////////////////////
//	BV CREATION
////////////////////////

int CollisionDetector::CreateCircle(Vector3 vPosition, float fRadius, int iBodyID)
{
	//create temp circle ptr
	Circle* pTemp = new Circle(m_iBoundingVolumeID, vPosition, fRadius, iBodyID);

	//add to full listing of bv's
	m_mBoundingVolumes.insert(pair<int, BoundingVolume*>(m_iBoundingVolumeID,pTemp) );

	// add to circle bv list
	m_vCircle.push_back(pTemp);

	// return and post-increment bv id
	return m_iBoundingVolumeID++;
}
int CollisionDetector::CreateAABB(Vector3 vPosition, Vector3 vHalfExtents, int iBodyID)
{
	//create temp AABB
	AABB* pTemp = new AABB(m_iBoundingVolumeID, vPosition, vHalfExtents, iBodyID);

	//add to full listing of bv's
	m_mBoundingVolumes.insert(pair<int, BoundingVolume*>(m_iBoundingVolumeID,pTemp) );

	// add to circle bv list
	m_vAABB.push_back(pTemp);

	// return and post-increment bv id
	return m_iBoundingVolumeID++;
}
int CollisionDetector::CreateOBB(Vector3 vPosition, Vector3 vHalfExtents, int iBodyID)
{
	//create temp OBB
	OBB* pTemp = new OBB(m_iBoundingVolumeID, vPosition, vHalfExtents, iBodyID);

	//add to full listing of bv's
	m_mBoundingVolumes.insert(pair<int, BoundingVolume*>(m_iBoundingVolumeID,pTemp) );

	// add to circle bv list
	m_vOBB.push_back(pTemp);

	// return and post-increment bv id
	return m_iBoundingVolumeID++;
}

////////////////////////
//	BV DELETION
////////////////////////
//allows for deletion of a bounding volume
void CollisionDetector::DeleteBV(int id)
{
	//delete from map
	m_mBoundingVolumes.erase(id);

	//delete from vector
	//look in circles
	for (unsigned int i = 0; i < m_vCircle.size(); i++)
	{
		if (m_vCircle[i]->GetID() == id)
		{
			delete m_vCircle[i];
			m_vCircle.erase(m_vCircle.begin() + i);
			return;
		}
			
	}
	// look in AABBs
	for (unsigned int i = 0; i < m_vAABB.size(); i++)
	{
		if (m_vAABB[i]->GetID() == id)
		{
			delete m_vAABB[i];
			m_vAABB.erase(m_vAABB.begin() + i);
			return;
		}
	}
	//look in obb
	for (unsigned int i = 0; i < m_vOBB.size(); i++)
	{
		if (m_vOBB[i]->GetID() == id)
		{
			delete m_vOBB[i];
			m_vOBB.erase(m_vOBB.begin() + i);
			return;
		}
	}

	
}

////////////////////////
//	BV ACCESS
////////////////////////
//returns ptr to BV for basic updates
BoundingVolume* CollisionDetector::GetBV(int id)
{
	return m_mBoundingVolumes.find(id)->second;
}
//returns ptr to circle bv based on id
Circle* CollisionDetector::GetCircle(int id)
{
	return (Circle*)m_mBoundingVolumes.find(id)->second;	
}
// returns ptr to aabb bv based on id
AABB* CollisionDetector::GetAABB(int id)
{
	return (AABB*)m_mBoundingVolumes.find(id)->second;
}
// return ptr to obb bv based on id
OBB* CollisionDetector::GetOBB(int id)
{
	return (OBB*)m_mBoundingVolumes.find(id)->second;
}

//***********************
//COLLISION CHECKS
//***********************

//new collision checking logic
void CollisionDetector::CheckCollisions()
{
	Vector3 collidingPoint;

	//counters
	unsigned int i, j;


	//*********************
	//CIRCLE-CIRCLE DETECTION
	//*********************

	for(i = 0; i <  m_vCircle.size() ; i++)
	{		
		for(j = i; j < m_vCircle.size(); j++)
		{
			if (i==j)
				continue;

			if (CircleCircleDet(m_vCircle[i], m_vCircle[j]))
			{
				//check for interpenetration and resolve if necessary
				InterpenetrationCheck(m_vCircle[i],m_vCircle[j]);

				// resolve collision
				ImpulseResponse(m_vCircle[i],m_vCircle[j]);
			}

		}//end nested for

	}//end outter for




	//*********************
	//AABB-AABB DETECTION
	//*********************

	for(i = 0; i <  m_vAABB.size() ; i++)
	{		
		for(j = i; j < m_vAABB.size(); j++)
		{
			if (i==j)
				continue;

			if (BoxBoxAABBDet(m_vAABB[i], m_vAABB[j]))
			{
				//check for interpenetration and resolve if necessary
				InterpenetrationCheck(m_vAABB[i],m_vAABB[j]);

				// resolve collision
				ImpulseResponse(m_vAABB[i],m_vAABB[j]);
			}

		}//end nested for

	}//end outter for


	//*********************
	//AABB-Circle DETECTION
	//*********************

	for(i = 0; i <  m_vCircle.size() ; i++)
	{		
		for(j = 0; j < m_vAABB.size(); j++)
		{
			if (CircleAABB(m_vCircle[i], m_vAABB[j]))
			{
				//check for interpenetration and resolve if necessary
				InterpenetrationCheck(m_vCircle[i],m_vAABB[j]);

				// resolve collision
				ImpulseResponse(m_vCircle[i],m_vAABB[j]);
			}

		}//end nested for

	}//end outter for


	//*********************
	//OBB-OBB DETECTION
	//*********************
	for(i = 0; i <  m_vOBB.size() ; i++)
	{		
		for(j = i; j < m_vOBB.size(); j++)
		{
			if (i==j)
				continue;

			if (BoxBoxOBB(m_vOBB[i], m_vOBB[j]))
			{
				//check for interpenetration and resolve if necessary
				InterpenetrationCheck(m_vOBB[i],m_vOBB[j]);

				// contact point is mid-way between both objects' center
				Vector3 contactPoint = m_vOBB[j]->GetPosition() - m_vOBB[i]->GetPosition();
				contactPoint.Scale(0.5f);

				//put it in world space
				//contactPoint = MMI->GetBody(m_vOBB[i]->GetBodyID())->GetTransform() * contactPoint;

				// resolve collision
				ImpulseResponseWithRotation(m_vOBB[i],m_vOBB[j], &contactPoint);
			}

		}//end nested for

	}//end outter for

	//*********************
	//Circle-OBB DETECTION
	//*********************
	for(unsigned int i = 0; i <  m_vCircle.size() ; i++)
	{		
		for(unsigned int j = 0; j < m_vOBB.size(); j++)
		{
			if (CircleOBBDet(m_vCircle[i], m_vOBB[j]))
				{
					//check interpenetration
					InterpenetrationCheck(m_vCircle[i],m_vOBB[j]);
					
					ImpulseResponse(m_vCircle[i], m_vOBB[j]);
				}//end if
			
		}//end nested for

	}//end outter for


	//*********************
	//AABB-OBB DETECTION
	//*********************
	for(unsigned int i = 0; i <  m_vAABB.size() ; i++)
	{		
		for(unsigned int j = 0; j < m_vOBB.size(); j++)
		{
			if (BoxBoxAABBOBB(m_vAABB[i], m_vOBB[j]))
				{
					//check interpenetration
					InterpenetrationCheck(m_vAABB[i],m_vOBB[j]);
					
					ImpulseResponse(m_vAABB[i], m_vOBB[j]);
				}//end if
			
		}//end nested for

	}//end outter for


}

void CollisionDetector::CheckOutOfBounds(float maxX, float maxY, float maxZ, Vector3 center)
{

	OutOfBoundsCirCir(maxX, maxY, maxZ, center);
	OutOfBoundsAABB(maxX, maxY, maxZ, center);
	OutOfBoundsOBB(maxX, maxY, maxZ, center);
}

//***********************
//CIRCLES
//***********************



void CollisionDetector::OutOfBoundsCirCir(float maxX, float maxY, float maxZ, Vector3 center)
{
	float rebound = -1 * .8f;
//	float CircleWallDiff;

	//for(unsigned int i = 0; i < m_vCircle.size(); i++)
	for (auto it = m_vCircle.begin(); it != m_vCircle.end(); it++)
	{
		//check x range
		if ((*it)->GetPosition().x + (*it)->GetRadius() > center.x + maxX || (*it)->GetPosition().x -  (*it)->GetRadius()< center.x-maxX)
		{
			Vector3 vec = MMI->GetVelocity((*it)->GetBodyID());
			vec.x *= -1;
			MMI->SetVelocity((*it)->GetBodyID(),vec);
			
		}
		if ((*it)->GetPosition().y + (*it)->GetRadius() > center.y + maxY || (*it)->GetPosition().y -  (*it)->GetRadius()< center.y-maxY)
		{
			Vector3 vec = MMI->GetVelocity((*it)->GetBodyID());
			vec.y *= -1;
			MMI->SetVelocity((*it)->GetBodyID(),vec);
		}
		if ((*it)->GetPosition().z + (*it)->GetRadius() > center.z + maxZ || (*it)->GetPosition().z -  (*it)->GetRadius()< center.z-maxZ)
		{
			Vector3 vec = MMI->GetVelocity((*it)->GetBodyID());
			vec.z *= -1;
			MMI->SetVelocity((*it)->GetBodyID(),vec);
		}
	}//end for loops
}
bool CollisionDetector::CircleCircleDet(BoundingVolume *BV1, BoundingVolume *BV2)
{

#if 1
	Vector3 vObject1 = BV1->GetPosition(),
			vObject2 = BV2->GetPosition();
	float	r2 = BV2->GetRadius(),
			r1 = BV1->GetRadius();

	if (	(vObject2.x-vObject1.x)*(vObject2.x-vObject1.x) + 
			(vObject2.y-vObject1.y)*(vObject2.y-vObject1.y) +
			(vObject2.z-vObject1.z)*(vObject2.z-vObject1.z) <= (r2 + r1)*(r2 + r1)	)
	{
		return true;

	}//end if

#endif

#if 0
	
	float x2, x1, y2, y1, z1, z2, r2, r1;
	
	x2 = BV2->GetPosition().x;
	x1 = BV1->GetPosition().x;
	y2 = BV2->GetPosition().y;
	y1 = BV1->GetPosition().y;
	z2 = BV2->GetPosition().z;
	z1 = BV1->GetPosition().z;
	r2 = BV2->GetRadius();
	r1 = BV1->GetRadius();

	if ( (x2 - x1 )*(x2-x1) + (y2 - y1)*(y2-y1) + (z2 - z1)*(z2 - z1) <= (r2+r1)*(r2 + r1) )
	{
		return true;

	}//end if

#endif





					
	return false;
}
bool CollisionDetector::CircleAABB(Circle* Circle, AABB* Box)
{
	// 2 stage detection
	
	//stage 1 - circle-circle
	if (CircleCircleDet(Circle, Box))
	{
		// stage 2 if rough collision passes, do deeper check

		//closest point
		Vector3 q;
		
		//find closest point
		ClosestPointAABB(&Circle->GetPosition(),Box,q);

		//if closest pt is colliding with sphere, collision exists
		Vector3 vObject1 = Circle->GetPosition();
				
		float	r2 = Circle->GetRadius();
				

		if (	(q.x-vObject1.x)*(q.x-vObject1.x) + 
				(q.y-vObject1.y)*(q.y-vObject1.y) +
				(q.z-vObject1.z)*(q.z-vObject1.z) <= (r2)*(r2)	)
		{
			return true;

		}//end if
	}

	return false;

}

bool CollisionDetector::CircleOBBDet(Circle * Circle, OBB * Box)
{
	// 2 stage collisions
	//stage 1 - circle-circle
	if (CircleCircleDet(Circle, Box))
	{
		// if sphere test works, test closest pt

		Vector3 p;
		
		//find point p on AABB closest to sphere center
		ClosestPtOBB(&Circle->GetPosition(), Box, p);
		
		// Sphere and AABB intersect if the (squared) distance from sphere center to
		// point p is less than the (squared) sphere radius
		Vector3 v = p - Circle->GetPosition();
		return v * v <= Circle->GetRadius() * Circle->GetRadius();
	}

	return false;
}



//***********************
//AABB
//***********************
void CollisionDetector::OutOfBoundsAABB(float maxBoundX, float maxBoundY, float maxBoundZ, Vector3 center)
{
	std::vector<AABB*>::iterator i = m_vAABB.begin();
	for(i; i != m_vAABB.end(); i++)
	{
		float minX = (*i)->GetPosition().x - (*i)->GetHalfExtents().x,
		maxX = (*i)->GetPosition().x + (*i)->GetHalfExtents().x,
		minY = (*i)->GetPosition().y - (*i)->GetHalfExtents().y,
		maxY = (*i)->GetPosition().y + (*i)->GetHalfExtents().y,
		minZ = (*i)->GetPosition().z - (*i)->GetHalfExtents().z,
		maxZ = (*i)->GetPosition().z + (*i)->GetHalfExtents().z;

		//check left/right bounds
		if(minX <= center.x-maxBoundX || maxX >= center.x+maxBoundX)
		{
			Vector3 vec = MMI->GetVelocity((*i)->GetBodyID());
			vec.x *= -1;
			MMI->SetVelocity((*i)->GetBodyID(),vec);
		}
		
		//check bottom/top bounds
		if( minY <= center.y - maxBoundY || maxY >= center.y + maxBoundY)
		{
			
			Vector3 vec = MMI->GetVelocity((*i)->GetBodyID());
			vec.y *= -1;
			MMI->SetVelocity((*i)->GetBodyID(),vec);
		}
		if( minZ <= center.z - maxBoundZ || maxZ >= center.z + maxBoundZ)
		{
			
			Vector3 vec = MMI->GetVelocity((*i)->GetBodyID());
			vec.z *= -1;
			MMI->SetVelocity((*i)->GetBodyID(),vec);
		}
		
	}//end for
}
bool CollisionDetector::BoxBoxAABBDet(AABB* BoxA, AABB* BoxB)
{
	// simple SAT for x, y, z axis

	//step 1 - get vec from a to b
	Vector3 vAtoB = BoxB->GetPosition() - BoxA->GetPosition();

	//step 2 check separation on X, Y, Z axis
	return (	(fabs(vAtoB.x) <= (BoxA->GetHalfExtents().x + BoxB->GetHalfExtents().x)) &&
				(fabs(vAtoB.y) <= (BoxA->GetHalfExtents().y + BoxB->GetHalfExtents().y)) &&
				(fabs(vAtoB.z) <= (BoxA->GetHalfExtents().z + BoxB->GetHalfExtents().z))	);



}

Vector3 CollisionDetector::ClosestPointAABB(Vector3* p, AABB* b, Vector3 &q)
{
	// For each coordinate axis, if the point value is outside box, clamp it to the box, else keep it as is

	//set v to the x component of the point
	float v = p->x;

	// if the point is less than the x min, clamp to x min
	if (v < b->Min(1))
		v = (b->Min(1));
	// if the point is greater than the x max, clamp to x max
	else if (v > (b->Max(1)))
		v = b->Max(1);
	
	// y component of closest point stored in q
	q.x= v;

	//set v to the y component of the point
	v = p->y;

	// if the point is less than the y min, clamp to y min
	if (v < b->Min(2))
		v = (b->Min(2));
	// if the point is greater than the y max, clamp to y max
	else if (v > (b->Max(2)))
		v = b->Max(2);
	
	// y component of closest point stored in q
	q.y= v;
	
	//set v to the z component of the point
	v = p->z;

	// if the point is less than the z min, clamp to z min
	if (v < b->Min(3))
		v = (b->Min(3));
	// if the point is greater than the z max, clamp to z max
	else if (v > b->Max(3))
		v = b->Max(3);
	
	// z component of closest point stored in q
	q.z= v;

	return q;
}



//***********************
//OBB
//***********************

void CollisionDetector::OutOfBoundsOBB(float maxBoundX, float maxBoundY, float maxBoundZ, Vector3 center)
{
	std::vector<OBB*>::iterator i = m_vOBB.begin();
	for(i; i != m_vOBB.end(); i++)
	{
		float minX = (*i)->GetPosition().x - (*i)->GetHalfExtents().x,
		maxX = (*i)->GetPosition().x + (*i)->GetHalfExtents().x,
		minY = (*i)->GetPosition().y - (*i)->GetHalfExtents().y,
		maxY = (*i)->GetPosition().y + (*i)->GetHalfExtents().y,
		minZ = (*i)->GetPosition().z - (*i)->GetHalfExtents().z,
		maxZ = (*i)->GetPosition().z + (*i)->GetHalfExtents().z;

		//check left/right bounds
		if(minX <= center.x-maxBoundX || maxX >= center.x+maxBoundX)
		{
			Vector3 vec = MMI->GetVelocity((*i)->GetBodyID());
			vec.x *= -1;
			MMI->SetVelocity((*i)->GetBodyID(),vec);
		}
		
		//check bottom/top bounds
		if( minY <= center.y - maxBoundY || maxY >= center.y + maxBoundY)
		{
			
			Vector3 vec = MMI->GetVelocity((*i)->GetBodyID());
			vec.y *= -1;
			MMI->SetVelocity((*i)->GetBodyID(),vec);
		}
		if( minZ <= center.z - maxBoundZ || maxZ >= center.z + maxBoundZ)
		{
			
			Vector3 vec = MMI->GetVelocity((*i)->GetBodyID());
			vec.z *= -1;
			MMI->SetVelocity((*i)->GetBodyID(),vec);
		}
		
	}//end for

}

//projects half-sizes of an OBB to any axis
float CollisionDetector::TransformToAxis( OBB &box,  Vector3 &projectionAxis)
{
	return (	box.GetHalfExtents().x * abs(projectionAxis * box.GetLocalAxis(0)) +
				box.GetHalfExtents().y * abs(projectionAxis * box.GetLocalAxis(1)) +
				box.GetHalfExtents().z * abs(projectionAxis * box.GetLocalAxis(2)) );

	
}

// projects half-sizes and separation vector of OBBs to any axis
bool CollisionDetector::OverlapOnAxis( OBB &one, OBB &two, Vector3 &projectionAxis, Vector3 &toCenter)
{
	// Project the half-size of one onto axis
	float oneProject = TransformToAxis(one, projectionAxis);
	float twoProject = TransformToAxis(two, projectionAxis);
	
	// Project separating vector onto the projection axis
	float distance = abs(toCenter * projectionAxis);
	
	// Check for overlap
	return(distance < oneProject + twoProject);
}

bool CollisionDetector::OBBOBB (OBB &one, OBB &two)
{
	// Find the vector between the two centers
	Vector3 toCenter = two.GetPosition() - one.GetPosition();

	//if any axes are parallel (equal) add a rotate b slightly
	if ( one.GetLocalAxis(0) == two.GetLocalAxis(0))
	{
		// x is aligned - adjust it slightly
		MMI->GetBody(two.GetBodyID())->RotateBody(Quaternion(Vector3(1.0f,0.0f,0.0f),0.1f));
	}
	if ( one.GetLocalAxis(1) == two.GetLocalAxis(1))
	{
		// y is aligned - adjust it slightly
		MMI->GetBody(two.GetBodyID())->RotateBody(Quaternion(Vector3(0.0f,0.0f,1.0f),0.1f));
	}
	if (one.GetLocalAxis(2) == two.GetLocalAxis(2) )
	{
		// z is aligned adjust it slightly
		MMI->GetBody(two.GetBodyID())->RotateBody(Quaternion(Vector3(1.0f,0.0f,0.0f),0.1f));
	}

	return (
		OverlapOnAxis(one, two, one.GetLocalAxis(0), toCenter) && // Check on box one' axes
		OverlapOnAxis(one, two, one.GetLocalAxis(1), toCenter) &&
		OverlapOnAxis(one, two, one.GetLocalAxis(2), toCenter) &&
		OverlapOnAxis(one, two, two.GetLocalAxis(0),toCenter)  && // And on two's
		OverlapOnAxis(one, two, two.GetLocalAxis(1), toCenter) &&
		OverlapOnAxis(one, two, two.GetLocalAxis(2) ,toCenter) &&
		OverlapOnAxis(one, two, one.GetLocalAxis(0) % two.GetLocalAxis(0) ,toCenter) &&	// Now on the cross products
		OverlapOnAxis(one, two, one.GetLocalAxis(0) % two.GetLocalAxis(1), toCenter) &&
		OverlapOnAxis(one, two, one.GetLocalAxis(0) % two.GetLocalAxis(2), toCenter) &&
		OverlapOnAxis(one, two, one.GetLocalAxis(1) % two.GetLocalAxis(0), toCenter) &&
		OverlapOnAxis(one, two, one.GetLocalAxis(1) % two.GetLocalAxis(1), toCenter) &&
		OverlapOnAxis(one, two, one.GetLocalAxis(1) % two.GetLocalAxis(2), toCenter) &&
		OverlapOnAxis(one, two, one.GetLocalAxis(2) % two.GetLocalAxis(0), toCenter) &&
		OverlapOnAxis(one, two, one.GetLocalAxis(2) % two.GetLocalAxis(1), toCenter) &&
		OverlapOnAxis(one, two, one.GetLocalAxis(2) % two.GetLocalAxis(2), toCenter) );
		
}

bool CollisionDetector::BoxBoxOBB(OBB *box1, OBB *box2)
{
	//two stage detection

	//stage 1 Circle-Circle detection (cheap)
	if(CircleCircleDet(box1,box2))
	{
		//stage 2 - SAT test
		if (OBBOBB (*box1, *box2))
			return true;
	}

	return false;
}

bool CollisionDetector::BoxBoxAABBOBB(AABB *box1, OBB *box2)
{
	//two stage detection

	//stage 1 Circle-Circle detection (cheap)
	if(CircleCircleDet(box1,box2))
	{
		//need to create an obb with the attributes of an AABB
		OBB* aBox = new OBB(*box1);
		//stage 2 - SAT test
		if (OBBOBB (*aBox, *box2))
			return true;
	}

	return false;
}

Vector3 CollisionDetector::ClosestPtOBB(Vector3* p, OBB* b, Vector3 &q)
{

	Vector3 d = *p - b->GetPosition();

	// Start result at center of box; make steps from there
	q = b->GetPosition();

	// For each OBB axis...
	for (int i= 0; i < 3; i++) 
	{
		// project d onto that axis to get distance of d along that axis from the box center
		float dist = d * b->GetLocalAxis(i);
		// If distance farther than the box extents, clamp to the box
		if (dist > b->GetHalfExtents(i))
		dist = b->GetHalfExtents(i);
		if (dist < - b->GetHalfExtents(i))
			dist = - b->GetHalfExtents(i);
		// Step that distance along the axis to get world coordinate
		q += dist * b->GetLocalAxis(i);
	}

	return q;
}




#if 0
//***********************
//PLANE
//***********************
void CollisionDetector::AddPlane(ParticlePlane * planeObject)
{
	m_vPlaneObjects.push_back(planeObject);
}

#endif





//***********************
//INTERPENETRATION CALCS
//***********************

void CollisionDetector::InterpenetrationCheck(BoundingVolume *BV1, BoundingVolume *BV2)
{
	//fix penetration
	// find relative distance
	float relativeDist = (BV2->GetPosition() - BV1->GetPosition()).Magnitude();
		
	//calculate penetration distance
	float penetrationDepth = (BV1->GetRadius()+BV2->GetRadius()) - relativeDist;

	Vector3 contactNormal = BV2->GetPosition() - BV1->GetPosition();
		contactNormal.Normalize();
	
	//if there is penetration
	if (penetrationDepth > 0)
	{
		//fix it
		InterpenetrationFix(BV1, BV2, penetrationDepth);
	}
	
}



#if 0



Vector3 CollisionDetector::ClosestPtPlane(ParticlePlane * p, Vector3 point)
{
	float t = p->GetNormal() * point - p->GetOffSet();
	return point - p->GetNormal().scaleReturn(t);
}


#endif

void CollisionDetector::InterpenetrationFix(BoundingVolume * BV1, BoundingVolume * BV2, float penetrationDepth)
{
	//get penetration contact normal
	Vector3 contactNormal = (BV2->GetPosition() - BV1->GetPosition());
	contactNormal.Normalize();

	//for ease of math, m1+m2
	float massCombo1 = ( MMI->GetMass(BV2->GetBodyID()) ) / ( MMI->GetMass(BV1->GetBodyID()) + MMI->GetMass(BV2->GetBodyID()) );
	float massCombo2 = ( MMI->GetMass(BV1->GetBodyID()) ) / ( MMI->GetMass(BV1->GetBodyID()) + MMI->GetMass(BV1->GetBodyID()) );

	Vector3 obj1change = contactNormal.ScaleReturn(penetrationDepth * massCombo1) , 
			obj2change = contactNormal.ScaleReturn(penetrationDepth * massCombo2);
						
	//updating locations			
	MMI->SetPosition(BV1->GetBodyID(),(BV1->GetPosition() - obj1change));
	
	MMI->SetPosition(BV2->GetBodyID(), (BV2->GetPosition() + obj2change));
	
	
}

//***********************
//COLLISION RESPONSE - MOVE TO CONTACT LOGIC
//***********************



void CollisionDetector::ImpulseResponse(BoundingVolume * BV1, BoundingVolume * BV2)
{
	
	Vector3 contactNormal, closingVelocity;
	float j;
	float contactNormalVelocity,impulse1,impulse2;

	//step 1 get contact normal
	contactNormal = (BV1->GetPosition() - BV2->GetPosition());
	contactNormal.Normalize();
	
	
	//step 2 get closing velocity
	closingVelocity = ( MMI->GetVelocity(BV1->GetBodyID()) - MMI->GetVelocity(BV2->GetBodyID()) );

	//step 3 get contact normal velocity
	contactNormalVelocity = closingVelocity * contactNormal;

	//get J
	j = ( (-(1+ELASTICITY)) * contactNormalVelocity) / 
		((1/MMI->GetMass(BV1->GetBodyID()))+(1/MMI->GetMass(BV2->GetBodyID())) );

	//calculate impuse
	impulse1 = j/MMI->GetMass(BV1->GetBodyID());
	impulse2 = j/MMI->GetMass(BV2->GetBodyID());
	
	MMI->SetVelocity(BV1->GetBodyID(), (MMI->GetVelocity(BV1->GetBodyID()) + contactNormal.ScaleReturn(impulse1)) );

	MMI->SetVelocity(BV2->GetBodyID(), (MMI->GetVelocity(BV2->GetBodyID()) - contactNormal.ScaleReturn(impulse2)) );
	
	

}

void CollisionDetector::ImpulseResponseWithRotation(BoundingVolume * BV1, BoundingVolume * BV2, Vector3 *ContactPoint)
{
	
	Vector3 contactNormal, closingVelocity;
	float j;
	float contactNormalVelocity,impulse1,impulse2;

	//step 1 get contact normal
	contactNormal = (BV1->GetPosition() - BV2->GetPosition());
	contactNormal.Normalize();
	
	
	//step 2 get closing velocity - added in with rotational velocity of contact point -verified value
	closingVelocity = ( MMI->GetVelocity(BV1->GetBodyID()) - MMI->GetVelocity(BV2->GetBodyID()) +
		((MMI->GetRotation(BV1->GetBodyID()) % *ContactPoint) - (MMI->GetRotation(BV2->GetBodyID()) % *ContactPoint)) );

	//step 3 get contact normal velocity
	contactNormalVelocity = closingVelocity * contactNormal;



	//calculating values for easy use
	Vector3 rq1 = *ContactPoint - BV1->GetPosition();
	Vector3 rq2 = *ContactPoint - BV2->GetPosition();

	// summing inverse masses
	float inverseMasses = MMI->GetInverseMass(BV1->GetBodyID()) + MMI->GetInverseMass(BV2->GetBodyID());

	// (inverse inertia tensor (rq1 & contactNormal))
	Vector3 iinv1 = (MMI->GetInverseInertiaTensor(BV1->GetBodyID()) * (rq1 % contactNormal));
	iinv1 =	iinv1 % rq1;
	Vector3 iinv2 = (MMI->GetInverseInertiaTensor(BV2->GetBodyID()) * (rq2 % contactNormal));
	iinv2 = iinv2 % rq2;

	// add them together
	iinv1 += iinv2;


	//get J
	j = (	(-(1+ELASTICITY)) * contactNormalVelocity) / 
		(	(inverseMasses) + (iinv1 * contactNormal)	);

	// calculate linear impulse
	impulse1 = j/MMI->GetMass(BV1->GetBodyID());
	impulse2 = j/MMI->GetMass(BV2->GetBodyID());

	// calculate angular impulse
	Vector3 rotation1 = MMI->GetRotation(BV1->GetBodyID()) + MMI->GetInverseInertiaTensor(BV1->GetBodyID()) * (rq1 % (contactNormal * j));
	Vector3 rotation2 = MMI->GetRotation(BV2->GetBodyID()) - MMI->GetInverseInertiaTensor(BV2->GetBodyID()) * (rq2 % (contactNormal * j));

	rotation1.Scale(0.7f);
	rotation2.Scale(-0.7f);

	
	// update velocities
	MMI->SetVelocity(BV1->GetBodyID(), (MMI->GetVelocity(BV1->GetBodyID()) + contactNormal.ScaleReturn(impulse1)) );
	MMI->SetVelocity(BV2->GetBodyID(), (MMI->GetVelocity(BV2->GetBodyID()) - contactNormal.ScaleReturn(impulse2)) );

	// update rotations
	MMI->SetRotation(BV1->GetBodyID(), rotation1);
	MMI->SetRotation(BV2->GetBodyID(), rotation2);
	
	
}
