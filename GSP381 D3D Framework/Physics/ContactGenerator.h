#pragma once

#include "vector3.h"

class Contact;
class RigidBody;
struct CollisionData;


/**
    * This is the basic polymorphic interface for contact generators
    * applying to rigid bodies.
    */
class ContactGenerator
{
public:

	//id of CGen created (used for deletion/access)
	int m_iCGenID;
	// id of graphics object associated with foce (most often a line)
	int m_iGraphicsID;
   
    virtual unsigned AddContact(Contact *contact, unsigned limit) const = 0;
};

//*************************
//CONTACT GENERATOR REGISTRY
//*************************

class ContactGenRegistry
{
protected:
	struct ContactGenRegistration //one CGen genreator and the RigidBody it applies to
	{
		ContactGenerator *contactGenerator;
	};

	typedef std::vector<ContactGenRegistration> Registry;

	Registry m_CGenList; //list of all registered CGen

	// id system for keeping track of contact generators. used for add/delete
	int m_iCGenID;

public:

	//register the given CGen generator to apply the given RigidBody
	void add(ContactGenerator *addCGen);

	//removes a registered pair from registry(CGenList). if the pair is not registered, does nothing
	void remove(int CGenID);

	//clears all CGens from CGen list (does not deleteBody the objects themselves, just clears the list)
	void clear();

	//calls all the CGen generators to update the CGens of their corresponding particles
	void updateCGens(CollisionData &cData);

	/////////////////////////
	// SIMPLIFIED INTERFACE
	////////////////////////

	void AddJointCGen(float bodyID, Vector3 vBodyConnectionPt, 
		float otherBodyID, Vector3 vOtherConnectionPoint, float error);

	void AddCableCGen(float bodyID, Vector3 vBodyConnectionPt, 
		float otherID, Vector3 vOtherConnectionPt, float error);

	
};

class Joint : public ContactGenerator
    {
    public:
        /**
         * Holds the two rigid bodies that are connected by this joint.
         */
        RigidBody* body[2];

        /**
         * Holds the relative location of the connection for each
         * body, given in local coordinates.
         */
        Vector3 position[2];

        /**
         * Holds the maximum displacement at the joint before the
         * joint is considered to be violated. This is normally a
         * small, epsilon value.  It can be larger, however, in which
         * case the joint will behave as if an inelastic cable joined
         * the bodies at their joint locations.
         */
        float error;

        /**
         * Configures the joint in one go.
         */
        void Set(
            RigidBody *a, const Vector3& a_pos,
            RigidBody *b, const Vector3& b_pos,
            float error
            );

        /**
         * Generates the contacts required to restore the joint if it
         * has been violated.
         */
        unsigned AddContact(Contact *contact, unsigned limit) const;
    };

class Cable : public ContactGenerator
    {
    public:
        /**
         * Holds the two rigid bodies that are connected by this joint.
         */
        RigidBody* body[2];

        /**
         * Holds the relative location of the connection for each
         * body, given in local coordinates.
         */
        Vector3 position[2];

        /**
         * Holds the maximum displacement at the joint before the
         * joint is considered to be violated. This is normally a
         * small, epsilon value.  It can be larger, however, in which
         * case the joint will behave as if an inelastic cable joined
         * the bodies at their joint locations.
         */
        float length;

        /**
         * Configures the joint in one go.
         */
        void Set(
            RigidBody *a, const Vector3& a_pos,
            RigidBody *b, const Vector3& b_pos,
            float length
            );

        /**
         * Generates the contacts required to restore the joint if it
         * has been violated.
         */
        unsigned AddContact(Contact *contact, unsigned limit) const;
    };