#pragma once
#include "BoundingVolume.h"

/*
	Derived from BV, this is Axis-Aligned Bounding Box. It does not utilize orientation 
	As it will always be locked to the world Axis
*/


class AABB 
	:public BoundingVolume
{
protected:
	Vector3 m_vHalfExtents;

public:
	AABB();
	AABB(int id, Vector3 vPosition, float x, float y, float z);
	AABB(int id, Vector3 vPosition, Vector3 vHalfExtents, int iBodyID = -1);

	~AABB();

	virtual void VUpdate(float dt);

	////////////////
	// ACCESSORS
	////////////////
	Vector3 GetHalfExtents() const {return m_vHalfExtents;}
	float GetHalfExtents(int iValue) const;
	// computes the min and max on x,y,z axes
	float Max(int i);
	float Min(int i);

	////////////////
	// MUTATORS
	////////////////
	void SetHalfExtents(Vector3 vHalfExtents){m_vHalfExtents = vHalfExtents;}
	void SetHalfExtents(float x, float y, float z);

};

