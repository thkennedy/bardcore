#pragma once
#include <Windows.h>

class CriticalSection
{
public:
	CriticalSection(void){InitializeCriticalSection(&m_cs);}
	~CriticalSection(void){DeleteCriticalSection(&m_cs);}
	void Lock()	{EnterCriticalSection( &m_cs);}
	void Unlock() {LeaveCriticalSection( &m_cs);}

protected:
	mutable CRITICAL_SECTION m_cs;
};

class ScopedCriticalSection
{
private:
	CriticalSection & m_csResource;
public:
	ScopedCriticalSection( CriticalSection & csResource)
		: m_csResource( csResource)
	{m_csResource.Lock();}
	~ScopedCriticalSection() {m_csResource.Unlock();}

};

