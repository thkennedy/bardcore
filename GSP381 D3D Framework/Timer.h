#ifndef TIMER_H
#define TIMER_H

#include <Windows.h>

#define DXTIMER Timer::Instance()

class Timer
{
private:
	LARGE_INTEGER Freq, Count;
	double begin, end, elapsed;
	__int64 cntsPerSec, prevTimeStamp,currentTimeStamp;
	float secsPerCnt, dt;
	Timer();
public:
	static Timer* Instance();

	void Start();
	void Stop();
	float GetDT();
	void Lap();
	double GetTime();
	void Clear();

};

#endif