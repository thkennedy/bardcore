#include "RandomNumber.h"



RandomNumber::RandomNumber()
{
	//acceptance-rejection method
	
	
}

RandomNumber* RandomNumber::Instance()
{
	static RandomNumber instance;

	return &instance;
}

void RandomNumber::SeedRand(int seed)
{
	m_iSeed = seed;
}

float RandomNumber::LCG()
{
	m_iModulus = 2147483647;
	m_iIncrement = 0;
	m_iMultiplier = 16807;

	m_iSeed = (m_iMultiplier * m_iSeed + m_iIncrement) % m_iModulus;

	float randNum = (float)m_iSeed / (float)m_iModulus;

	return randNum;

}

double RandomNumber::Uniform(float a, float b)
{
	assert( a < b );
	return a + ( b - a ) * LCG();

}

void RandomNumber::GetRandomVec(float &x, float &y, float &z)
{
	x = LCG();
	y = LCG();
	z = LCG();
}



float RandomNumber::Exponential(float beta)
{
	//beta must be greater than 0
	if (!(beta >= 0.0f))
	{
		std::cout << "Beta must be at least 0.\n";
		return 0;
	}

	return -beta * log(LCG());
		
}

long double RandomNumber::ExponF(double a, double b)
{

assert(b > 0);
#if 1
	
	long double x = LCG();
	if (x >= 0)
		return 1.0 - exp( -(x-a)/b );
	else
		return 0;
#endif

#if 0
	return -log(1-LCG())/(b-a);
#endif


}



float RandomNumber::Weibull(int a, int b, int c)
{
	//b and c must be greater than 0
	if (b < 0 && c < 0)
	{
		std::cout << "B or C is less than 0.\n";
		return 0;
	}

	//x = b * ln(-1/U-1)^((1/c))+a
	return (float)b * pow(log(-1/(LCG() - 1)), (1/(float)c)) + a;
}

float RandomNumber::Triangular(float xMin, float xMax, float c)
{
	//make sure max is greater than min
	if ( xMin >= xMax)
	{
		std::cout << "In the world of math you're using Min is greater than Max. Interesting.\n";
		return 0;
	}
	
	//make sure c (mode) is between min and max
	if (c < xMin || c > xMax)
	{
		std::cout << "C (mode) does not fall between the min and max you have defined\n";
		return 0;
	}

	//calc f(c)
	float beta = ( c- xMin) / (xMax - xMin);
	float randNum1 = LCG();
	float randNum2 = LCG();

	//two cases - U less than f(c) or U is greater than f(c)
	if (randNum1 <= beta)
	{
		return xMax + (c - xMax) * sqrt(randNum1);
	}
	else
	{
		return xMin - (xMin - c) * sqrt(randNum2);
	}

	
}


int RandomNumber::Normal()
{
	
	while (true)
	{
		//step 1 generate random variable Y with p.d.f g - exponential 
		float Y = Exponential(1);

		//step 2 generate a uniform random variable between 0 and 1
		float U = LCG();
	
		//step 3 if U <= -(((Y-1)^2)/2) then X = Y, else reject and start over - goal is to get a variable in the curve (under the Y val)
		if (U <= -(((pow(Y-1,2)))/2))
		{
			float X = Y;
			//step 4 generate V between 0,1
			int V = LCG();

			//step 5 if V <= .5, set Z = X
			if (V <= .5)
			{
				return V;
			}
			//otherwise set Z = -X
			else
				return V * -1;
		}

	}
}

long double RandomNumber::Pois(long double mu)
{


	assert(mu > 0.00000);
	double p = 1.0;
	int k;
	for (k = 0; p >= exp(-mu); k++)
		p *= LCG();
	return k - 1;


}


double RandomNumber::normal( double mean, double variance )
{
	assert( variance > 0. );
	double p, p1, p2;
	do {
		p1 = Uniform( -1, 1 );
		p2 = Uniform( -1, 1 );
		p = p1 * p1 + p2 * p2;
	} while ( p >= 1. );
	return mean + variance * p1 * sqrt( -2. * log( p ) / p );
}