#pragma once
#include <iostream>
#include <math.h>
#include <cassert>


#define RandNum RandomNumber::Instance()

class RandomNumber
{
private:
	//values for the LCG
	unsigned long long m_iSeed, m_iModulus, m_iMultiplier, m_iIncrement;
	RandomNumber(void);

public:
	
	~RandomNumber(void){}

	static RandomNumber* Instance();

	//utilities
	float LCG();
	double Uniform(float a, float b);
	float Exponential(float beta);
	long double ExponF(double a, double b);
	float Weibull(int a, int b, int c);
	float Triangular (float xMin, float xMax, float c);
	int Normal ();
	long double Pois(long double mu);
	double normal( double mean, double variance );
	void GetRandomVec(float &x, float &y, float &z);
	//mutators
	void SeedRand(int seed);
	
	

};

