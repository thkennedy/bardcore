#pragma once

#include "ParticleSystem.h"

class FireRing : public ParticleSystem
{
public:
	FireRing(void){}
	FireRing(const std::string& fxName,
            const std::string& techName,
            const std::string& texName,

            const D3DXVECTOR3& accel,
            const AABBG& box,
            int maxNumParticles,
            float timePerParticle);
	~FireRing(void);

	void InitParticle(Particle& out);
};

