#include "Sprinkler.h"
#include "../PRNG/RandomNumber.h"


Sprinkler::Sprinkler(const std::string& fxName, 
	const std::string& techName, 
	const std::string& texName, 
	const D3DXVECTOR3& accel, 
	const AABBG& box,
	int maxNumParticles,
	float timePerSecond)
	: ParticleSystem(fxName, techName, texName, accel, box, maxNumParticles,
	timePerSecond)
{
}

void Sprinkler::InitParticle(Particle& out)
{
	// Generate about the origin.
	out.m_vInitialPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
 
	out.m_fInitialTime		= m_rTime;
	out.m_fLifeTime			= RandNum->Uniform(4.0f, 5.0f);
	out.m_cInitialColor		= D3DWhite;
	out.m_fInitialSize		= RandNum->Uniform(8.0f, 12.0f);
	out.m_fMass				= RandNum->Uniform(0.8f, 1.2f);

	out.m_vInitialVelocity.x = RandNum->Uniform(-2.5f, 2.5f);
	out.m_vInitialVelocity.y = RandNum->Uniform(5.0f, 6.0f);
	out.m_vInitialVelocity.z = RandNum->Uniform(-2.5f, 2.5f);
}


Sprinkler::~Sprinkler(void)
{
}
