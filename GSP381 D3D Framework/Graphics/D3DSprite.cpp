#include "D3DSprite.h"
#include "d3dUtility.h"
#include "D3DApp.h"


D3DSprite::D3DSprite(LPDIRECT3D9 d3dObj, LPDIRECT3DDEVICE9 d3dDevice, ID3DXSprite* spriteObj)
{
	m_pD3DObject = d3dObj;
	m_pD3DDevice = d3dDevice;
	m_pD3DSprite = spriteObj;

	//default center is top left corner
	m_vCenter = D3DXVECTOR3(0,0,0);
	
	//getting focus window specs
	m_pD3DDevice->GetCreationParameters(&m_d3dcpWindowParams);
	GetWindowRect(m_d3dcpWindowParams.hFocusWindow, &m_rDisplay);

	//default position is the center of the screen
	m_vPosition = D3DXVECTOR3((m_rDisplay.right-m_rDisplay.left)/2,(m_rDisplay.bottom-m_rDisplay.top)/2,0);
	m_bOnScreen = true;

	//sprite given invalid texture so it'll throw error if trying to render
	m_pTexture = NULL;

	//sprite given invalid ID to show it has not been registered
	m_iSpriteID = -1;

	//default dimensions is 128x128
	m_vSpriteDimensions = Vector3(128,128,0);

	m_bMouseOver = false;

	//set default scale,translate,etc

	m_vScale = D3DXVECTOR3(1,1,1);
	m_vRotation = D3DXVECTOR3(0,0,0);

	// set the render position
	m_vRenderPos = m_vPosition;
	m_vRenderPos.x *= m_vScale.x;
	m_vRenderPos.y *= m_vScale.y;

}

D3DSprite::D3DSprite()
{
	m_pD3DObject = D3DAPPI->GetD3DObject();
	m_pD3DDevice = D3DAPPI->GetD3DDevice();
	m_pD3DSprite = D3DAPPI->GetD3DSpriteObject();

	//default center is top left corner
	m_vCenter = D3DXVECTOR3(0,0,0);
	
	//getting focus window specs
	m_pD3DDevice->GetCreationParameters(&m_d3dcpWindowParams);
	GetWindowRect(m_d3dcpWindowParams.hFocusWindow, &m_rDisplay);

	//default position is the center of the screen
	m_vPosition = D3DXVECTOR3((m_rDisplay.right-m_rDisplay.left)/2,(m_rDisplay.bottom-m_rDisplay.top)/2,0);
	m_bOnScreen = true;

	//sprite given invalid texture so it'll throw error if trying to render
	m_pTexture = NULL;

	//sprite given invalid ID to show it has not been registered
	m_iSpriteID = -1;

	//default dimensions is 128x128
	m_vSpriteDimensions = Vector3(128,128,0);

	m_bMouseOver = false;

	//set default scale,translate,etc

	m_vScale = D3DXVECTOR3(1,1,1);
	m_vRotation = D3DXVECTOR3(0,0,0);

	// set the render position
	m_vRenderPos = m_vPosition;
	m_vRenderPos.x *= m_vScale.x;
	m_vRenderPos.y *= m_vScale.y;

	OnReset();
}


D3DSprite::~D3DSprite(void)
{

}


//////////////////////////////////////////////////////////////////////////
// Utilities
//////////////////////////////////////////////////////////////////////////
void D3DSprite::Update(float time_elapsed)
{
	//Update rotation based on angular velocity here later
	
	if (m_bAlwaysRender == false)
	{
		// update visible bool for render
		// if sprite is offscreen or display time has expired
		if (m_vRenderPos.x < 0.0f || m_vRenderPos.y < 0.0f || fDisplayTime <= 0.0f )
		{
			m_bOnScreen = false;
		}
		else
		{
			// decrease fDisplayTime
			fDisplayTime -= time_elapsed;
			m_bOnScreen = true;
		}
	}
	else
		m_bOnScreen = true;

   
}

void D3DSprite::Render()
{
	//////////////
	//	ROTATION
	//////////////

	// a matrix to store the rotation information
	D3DXMATRIX matRotateY, matRotateX, matRotateZ;    
	
	// build a matrix to rotate the model based on the increasing float value
    D3DXMatrixRotationX(&matRotateX, m_vRotation.x);
	D3DXMatrixRotationY(&matRotateY, m_vRotation.y);
	D3DXMatrixRotationZ(&matRotateZ, m_vRotation.z);
	

	D3DXMATRIX rotationMatrix = matRotateX * matRotateY * matRotateZ;


	//////////////
	//	SCALE
	//////////////

	D3DXMATRIX matScale;

	D3DXMatrixScaling(&matScale,m_vScale.x,m_vScale.y,m_vScale.z);

	//////////////
	//	TRANSLATE
	//////////////

	D3DXMATRIX matTranslate;

	D3DXMatrixTranslation(&matTranslate,m_vRenderPos.x,m_vRenderPos.y,m_vRenderPos.z);

	///////////////
	//	TRANSFORM
	///////////////
	m_mTransform = matScale * rotationMatrix * matTranslate ;

	m_pD3DSprite->SetTransform(&m_mTransform);
	
	if (!m_bMouseOver)
		m_pD3DSprite->Draw(m_pTexture,NULL,&m_vCenter,
				&D3DXVECTOR3(0,0,0),D3DWhite);
	else
		m_pD3DSprite->Draw(m_pTexture,NULL,&m_vCenter,
				&D3DXVECTOR3(0,0,0),D3DBlack);

	m_pD3DSprite->Flush();

}

void D3DSprite::ReleaseSprite()
{
	if (m_pTexture)
	{
		m_pTexture->Release();
	}

	
}

void D3DSprite::OnLostDevice()
{
	
}
void D3DSprite::OnReset()
{
	// 
	m_pD3DDevice->GetCreationParameters(&m_d3dcpWindowParams);
	GetWindowRect(m_d3dcpWindowParams.hFocusWindow, &m_rDisplay);

	SetScale(D3DXVECTOR3(D3DAPPI->m_D3Dpp.BackBufferWidth * .00125f, 
		D3DAPPI->m_D3Dpp.BackBufferHeight * .0016667f,1) );

	// set the render position
	m_vRenderPos = m_vPosition;
	m_vRenderPos.x *= m_vScale.x;
	m_vRenderPos.y *= m_vScale.y;

	//get files info
	//set the file particulars
	//m_vCenter = D3DXVECTOR3(m_imageInfo.Width/2*m_vScale.x, m_imageInfo.Height/2*m_vScale.y, 0);
	//m_vSpriteDimensions = D3DXVECTOR3(m_imageInfo.Width*m_vScale.x, m_imageInfo.Height*m_vScale.y,0);
}
	

//////////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////////

	


//////////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////////
void D3DSprite::SetPosition(D3DXVECTOR3 newPos) 
{
	m_vPosition = newPos;
	// set the render position
	m_vRenderPos = m_vPosition;
	m_vRenderPos.x *= m_vScale.x;
	m_vRenderPos.y *= m_vScale.y;

}

void D3DSprite::SetTexture(LPCTSTR NewTexture)
{
	HRESULT hr;
	hr = D3DXCreateTextureFromFile(m_pD3DDevice,NewTexture,&m_pTexture);

	//get files info
	D3DXGetImageInfoFromFile(NewTexture, &m_imageInfo);

	

	//set the file particulars
	m_vCenter = D3DXVECTOR3(m_imageInfo.Width/2, m_imageInfo.Height/2, 0);
	m_vSpriteDimensions = D3DXVECTOR3(m_imageInfo.Width, m_imageInfo.Height,0);

}
void D3DSprite::SetTexture(IDirect3DTexture9 *pTexture, LPCTSTR texturePath)
{
	m_pTexture = pTexture;

	D3DXGetImageInfoFromFile(texturePath, &m_imageInfo);
	//set the file particulars
	m_vCenter = D3DXVECTOR3(m_imageInfo.Width/2, m_imageInfo.Height/2, 0);
	m_vSpriteDimensions = D3DXVECTOR3(m_imageInfo.Width, m_imageInfo.Height,0);
	
}