#include "Particle.h"
#include "ShapeManager.h"
#include "D3DQuad.h"
#include "InstancedQuad.h"
#include <random>

Particle::Particle(int id)
	:D3DShape(id)
{
	// seed rand
	srand(time(NULL));

	
	ResetParticle();

	
}


Particle::~Particle(void)
{
}

void Particle::Update(float dt)
{
	//// update position
	//m_vPosition += m_vVelocity.ScaleReturn(dt);

	//// apply  "gravity to y velocity
	//m_vVelocity.y -= 0.5f;

	//// if y position is < 0, reset particle
	//if (m_vPosition.y < 0.0f)
	//	ResetParticle();

	//// update quad
	//myQuad->SetPosition(m_vPosition);
	
}
void Particle::Render()
{

	

}
void Particle::OnResetDevice()
{

}
void Particle::OnLostDevice()
{

}
void Particle::ResetParticle()
{
	////all start at 0,0,0
	//m_vPosition = Vector3(0.0f,0.0f,0.0f);

	//// give initial rand x velocity
	//m_vVelocity.x = rand() % 5;

	//// init z velocity
	//m_vVelocity.z = rand() % 5;

	//// init y velocity
	//m_vVelocity.y = rand() % 20;
	//
	//// even numbers go left.
	//if ((int)m_vVelocity.x % 2 > 0)
	//	m_vVelocity.x *= -1;

	//// even numbers go forward
	//if ((int)m_vVelocity.z % 2 > 0)
	//	m_vVelocity.z *= -1;
}