//=============================================================================
// pyramid.fx by Tim Kennedy
//
// 
// 
//=============================================================================

uniform extern texture	gTex;
uniform extern float4x4 gView;
uniform extern float4x4 gProj;
uniform extern float4x4 gViewProj;
	
//my sampler
sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
};



// input structure describes the vertex that is input into the shader
// Vert Decl is PNT - Pos, Normal, Texture
struct InputVS
{
	// stream 0
	
	float3 posL		: POSITION0;
	float3 Normal	: NORMAL0;
	float2 tex0		: TEXCOORD0;

	// stream 1
	float4 model_matrix0 : TEXCOORD1;
	float4 model_matrix1 : TEXCOORD2;
	float4 model_matrix2 : TEXCOORD3;
	float4 model_matrix3 : TEXCOORD4;
	
	float4 instance_color : D3DCOLOR;
	
};


//my vert shader declaration
struct OutputVS
{
    float4 posH		: POSITION0;
	float4 Normal	: NORMAL0;
	float2 tex0		: TEXCOORD0;
	


};

struct InputPS
{
	float2 base		: TEXCOORD0;
};



//vert shader method
OutputVS PyramidVS(InputVS input)
{

	// construct the model matrix

	float4x4 modelMatrix =
	{
		input.model_matrix0,
		input.model_matrix1,
		input.model_matrix2,
		input.model_matrix3

	};

    // Zero out our output.
	OutputVS outVS = (OutputVS)0;

	// transform input position to world space
	// with the instance model matrix
	float4 worldPosition = mul(float4(input.posL,1.0f), modelMatrix);
			 	
	// Transform to homogeneous clip space.
	outVS.posH = mul(worldPosition, gViewProj);

	// pass on texture coords
	outVS.tex0 = input.tex0;
	 
	// Done--return the output.
    return outVS;
}


//pixel shader method
float4 PyramidPS(InputPS input) : COLOR
{
	float4 texColor = tex2D(TexS, input.base);
    return texColor;
	
}

technique InstancedVSTech
{
    pass P0
    {
		Lighting = FALSE;
		// Specify the render/device states associated with this pass.
		//FillMode = Wireframe;
		 // Alpha blending
        AlphaBlendEnable = true;
        SrcBlend = SrcAlpha;
        DestBlend = InvSrcAlpha;
		

        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_3_0 PyramidVS();
        pixelShader  = compile ps_3_0 PyramidPS();

		
    }
}
