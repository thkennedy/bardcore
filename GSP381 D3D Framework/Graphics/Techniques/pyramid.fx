//=============================================================================
// pyramid.fx by Tim Kennedy
//
// 
// 
//=============================================================================

uniform extern float4x4 gWVP;
uniform extern texture	gTex;
	
//my sampler
sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};


//my vert shader declaration
struct OutputVS
{
    float4 posH		: POSITION0;
	float4 diffuse	: COLOR0;
	float2 tex0		: TEXCOORD0;
	

};

//vert shader method
OutputVS PyramidVS(float3 posL : POSITION0, float3 Normal : NORMAL, float2 tex0: TEXCOORD0)
{
    // Zero out our output.
	OutputVS outVS = (OutputVS)0;

	
	
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(posL, 1.0f), gWVP);

	// pass on texture coords
	outVS.tex0 = tex0;
	 
	// Done--return the output.
    return outVS;
}


//pixel shader method
float4 PyramidPS(float2 tex0 : TEXCOORD0) : COLOR
{
	float3 texColor = tex2D(TexS, tex0).rgb;
    return float4(texColor, 1.0f);
	
}

technique PyramidTech
{
    pass P0
    {
		Lighting = FALSE;
		// Specify the render/device states associated with this pass.
		//FillMode = Wireframe;
		

        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_3_0 PyramidVS();
        pixelShader  = compile ps_3_0 PyramidPS();

		
    }
}
