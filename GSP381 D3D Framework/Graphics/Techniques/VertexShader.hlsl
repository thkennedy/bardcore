//=============================================================================
// pyramid.fx by Tim Kennedy
//
// 
// 
//=============================================================================

uniform extern texture	gTex;
uniform extern float4x4 gViewProjectionMatrix;
	
//my sampler
sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
};



// input structure describes the vertex that is input into the shader
// Vert Decl is PNT - Pos, Normal, Texture
struct InputVS
{
	// stream 0
	
	float3 posL		: POSITION0;
	float3 Normal	: NORMAL0;
	float2 tex0		: TEXCOORD0;

	// stream 1
	float4 model_matrix0 : TEXCOORD1;
	float4 model_matrix1 : TEXCOORD2;
	float4 model_matrix2 : TEXCOORD3;
	float4 model_matrix3 : TEXCOORD4;
	
	float4 instance_color : D3DCOLOR;
	
};


//my vert shader declaration
struct OutputVS
{
    float4 posH		: POSITION0;
	float4 Normal	: NORMAL0;
	float2 tex0		: TEXCOORD0;
	


};

struct InputPS
{
	float2 base		: TEXCOORD0;
};



//vert shader method
OutputVS PyramidVS(InputVS input)
{

	// construct the model matrix

	float4x4 modelMatrix =
	{
		input.model_matrix0,
		input.model_matrix1,
		input.model_matrix2,
		input.model_matrix3

	};

    // Zero out our output.
	OutputVS outVS = (OutputVS)0;

	// transform input position and normal to world space
	// with the instance model matrix

	float4 worldPosition = mul(input.posL, modelMatrix);
	float3 worldNormal = mul(input.Normal, modelMatrix);
	
	 	
	// Transform to homogeneous clip space.
	outVS.posH = mul(worldPosition, gViewProjectionMatrix);

	// calculate normal
	//outVS.Normal = mul(worldNormal, gViewProjectionMatrix);

	// pass on texture coords
	outVS.tex0 = input.tex0;
	 
	// Done--return the output.
    return outVS;
}


//pixel shader method
float4 PyramidPS(InputPS input) : COLOR
{
	float3 texColor = tex2D(TexS, input.base).rgb;
    return float4(texColor, 1.0f);
	
}

technique InstancedVSTech
{
    pass P0
    {
		Lighting = FALSE;
		// Specify the render/device states associated with this pass.
		//FillMode = Wireframe;
		

        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_3_0 PyramidVS();
        pixelShader  = compile ps_3_0 PyramidPS();

		
    }
}
