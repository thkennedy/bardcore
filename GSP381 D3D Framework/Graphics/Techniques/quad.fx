//=============================================================================
// Quad.fx by Tim Kennedy
//
// 
// 
//=============================================================================

uniform extern texture	gTex;
uniform extern float4x4 gWVP;
	
//my sampler
sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};

// input for my verts
struct InputVS
{
	float3 posL		: POSITION;
	float3 Normal	: NORMAL;
	float2 tex0		: TEXCOORD;
};

//my vert shader declaration
struct OutputVS
{
    float4 posH		: POSITION0;
	float4 Normal	: NORMAL;
	float2 tex0		: TEXCOORD0;
	

};

// input for ps
struct InputPS
{
	float2 base		: TEXCOORD;
};

//vert shader method
OutputVS QuadVS(InputVS input)
{
    // Zero out our output.
	OutputVS outVS = (OutputVS)0;

	
		
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(input.posL,1.0f), gWVP);


	// calculate normal
	//outVS.Normal = mul(worldNormal, gViewProj);

	// pass on texture coords
	outVS.tex0 = input.tex0;
	 
	// Done--return the output.
    return outVS;
}


//pixel shader method
float4 QuadPS(InputPS input) : COLOR
{
	//float4 texColor = tex2D(TexS, input.base);
    return tex2D(TexS, input.base);
	
}

technique QuadTech
{
    pass P0
    {
		Lighting = FALSE;
		// Specify the render/device states associated with this pass.
		//FillMode = Wireframe;
		AlphaBlendEnable = true;
        SrcBlend = SrcAlpha;
        DestBlend = InvSrcAlpha;

        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_3_0 QuadVS();
        pixelShader  = compile ps_3_0 QuadPS();

		
    }
}
