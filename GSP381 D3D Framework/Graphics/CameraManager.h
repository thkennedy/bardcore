#pragma once
#include <vector>
#include "D3DCamera.h"

/*
	Controls creation/deletion of cameras, allows single update for multiple cams, easy switching of 
	views, active cam views, etc. Singleton, allowing for only one manager, multiple cameras

*/

#define CamMgr CameraManager::Instance()

// forward declarations
//class D3DCamera;


class CameraManager
{
private:
	// vec of cameras
	std::vector<D3DCamera*> m_vCameras;
	CameraManager(void);
	
public:

	// public cam views for easy access
	// ptr to current cam view
	D3DCamera* m_pCurrentCamView;

	// ptr to previous cam view
	D3DCamera* m_pPreviousCamView;

	// instance method
	static CameraManager* Instance();

	~CameraManager(void);

	//////////////////
	// UTILITIES
	/////////////////
	void Update(float dt);
	void OnLostDevice();	
	void OnResetDevice();
	void CreateCamera();

	///////////////////
	// ACCESSORS
	//////////////////

	/////////////////
	//	MUTATORS
	////////////////
};

