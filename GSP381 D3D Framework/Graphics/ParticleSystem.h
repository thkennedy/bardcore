#pragma once
#include <vector>
#include <string>
#include "../Physics/vector3.h"
#include "../Physics/functions.h"
#include "D3DVertex.h"


// forward declarations


// helper structs

//===============================================================
// Bounding Volumes

struct AABBG 
{
	// Initialize to an infinitely small bounding box.
	AABBG()
		: minPt(INFINITY, INFINITY, INFINITY),
		  maxPt(-INFINITY, -INFINITY, -INFINITY){}

    D3DXVECTOR3 center()
	{
		return 0.5f*(minPt+maxPt);
	}

	void xform (D3DXMATRIX mInput, D3DXMATRIX &mOutput)
	{
		mOutput *= mInput;
	}

	D3DXVECTOR3 minPt;
	D3DXVECTOR3 maxPt;
};

struct BoundingSphere 
{
	BoundingSphere()
		: pos(0.0f, 0.0f, 0.0f), radius(0.0f){}

	D3DXVECTOR3 pos;
	float radius;
};

class ParticleSystem
{
protected:
	HRESULT hr;

	// .fx pointer and shader handles
    ID3DXEffect* m_FX;
    D3DXHANDLE m_hTech;
    D3DXHANDLE m_hWVP;
    D3DXHANDLE m_hEyePosL;
    D3DXHANDLE m_hTex;
    D3DXHANDLE m_hTime;
    D3DXHANDLE m_hAccel;
    D3DXHANDLE m_hViewportHeight;

    IDirect3DTexture9* m_Tex;		// particle texture
    IDirect3DVertexBuffer9* m_VB;	// vertex buffer
    D3DXMATRIX m_mWorld;			// world matrix
    D3DXMATRIX m_mInvWorld;			// inverse world matrix
    real m_rTime;					// how long particle system has been running
    Vector3 m_vAccel;				// constant acceleration factor
    AABBG m_Box;					// AABBG used for culling - determines what particles are rendered
    int m_iMaxNumParticles;			// max particles system can contain - passed into vbuffer
    real m_rTimePerParticle;		// how often a particle should be emitted
	Vector3 m_vPosition;			// position of emitter
	Vector3 m_vScale;				// scale of system
	Vector3 m_vRotation;			// where its rotated

	std::vector<Particle> m_vParticles;			// all particles in the system, both alive and dead
    std::vector<Particle*> m_vAliveParticles;	// all alive particles - ones being rendered
    std::vector<Particle*> m_vDeadParticles;	// all dead particles - if we need more, just use these


	///////////////
	// SCRATCH PAD
	////////////////
	D3DXMATRIX rotationMatrix;
	D3DXMATRIX matRotX,matRotY,matRotZ;
	D3DXMATRIX matTranslate;
	D3DXMATRIX matScale;
	Quaternion tempQuat;
	Vector3 tempVec1;
	float i,j,k;

public:
	// particle system id
	int ID;
	int iNumVerts;


	ParticleSystem(void){}
	ParticleSystem(
        const std::string& fxName,
        const std::string& sTechName,
        const std::string& sTexName,
        const Vector3& vAccel,
        const AABBG& box,
        int iMaxNumParticles,
        real rTimePerParticle);

    virtual ~ParticleSystem();
	
	////////////////////
	// Utilities
	////////////////////
	// build FX file
	void BuildFX(const std::string& fxName,const std::string& sTechName);
	// adds a particle to the system, inits dead particle
	void AddParticle();
	virtual void InitParticle(Particle& out) = 0;
	void BuildTransform();
	virtual void Update(real dt);
    virtual void Draw();
	

	////////////////////
    // Accessors
	///////////////////
    real GetTime();
	const AABBG& GetAABBG()const;
	D3DMATRIX GetWorldMatrix();
	

	//////////////////
	// Mutators
	//////////////////
	void SetTime(real t);
    void SetWorldMtx(const D3DXMATRIX& world);
    void SetPosition(float x, float y, float z);
	void RotateSystem(float x, float y, float z);
	void SetRotation(float x, float y, float z);
	void SetScale(float x, float y, float z);
	void SetAccel(Vector3 vAccel){m_vAccel = vAccel;}
	
	//////////////////
	// D3D Methods
	/////////////////
    virtual void OnLostDevice();
    virtual void OnResetDevice();

    
    
};

