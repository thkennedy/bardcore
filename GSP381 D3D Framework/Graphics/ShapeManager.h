#pragma once
#include <vector>
#include <map>
#include "D3DShape.h"
#include "../Physics/vector3.h"
#include "../MultiThreading/CriticalSection.h"






enum EShapeColor
{
	RED,
	GREEN,
	BLUE,
	GRAY,
	YELLOW

};



/*
	Singleton Shape Manager - Allows for centralized creation, access, update, 
	rendering, onlost, onreset, and deleting of graphics objects
*/

#define SMI ShapeManager::Instance()

typedef std::vector<D3DShape*> ShapeVec;


//forward declarations

class D3DSphere;
class D3DBox;
class D3DLine;
class D3DCamera;
class D3DInstancedQuad;



class ShapeManager
{
private:
	ShapeManager(void);

	// map of shapes for random access

	
	//vector of shapes for iterating through and updating
	ShapeVec m_vAllShapes;
	std::vector<D3DInstancedQuad*> m_vQuads;

	// map of shape data, used for access
	std::map<int,D3DShape*> m_mapShapes;

	// INSTANCE DATA
	int m_iNumBoxes;	// number of box meshes
	int m_iNumSpheres;	// number of sphere meshes
	int m_iNumQuads;// number of particles
	
	// shape's rendered texture
	IDirect3DTexture9* m_InstancedQuadTexture;

	int m_iShapeID;

	D3DCamera *m_pCurrentCamView;

	// sphere used for all draw calls
	LPD3DXMESH	m_pSphere;
	// box used for all draw calls
	LPD3DXMESH	m_pBox;
	// line used for all draw calls
	LPD3DXLINE m_pLine;

	//////////////////////////
	// TEXTURES
	//////////////////////////
	IDirect3DTexture9* m_RedTexture;
	IDirect3DTexture9* m_GreenTexture;
	IDirect3DTexture9* m_BlueTexture;
	IDirect3DTexture9* m_GrayTexture;
	IDirect3DTexture9* m_YellowTexture;


	///////////////////////////////
	// VERTEX AND INDEX BUFFERS
	///////////////////////////////
	IDirect3DVertexBuffer9* m_QuadVB;
	IDirect3DIndexBuffer9*	m_QuadIB;
	IDirect3DVertexBuffer9*	m_InstanceDataVB;

	/////////////////////////
	// Vert declaration data
	/////////////////////////
	

	UINT numElements;


	////////////////////
	// SHADER INFO
	///////////////////
	ID3DXEffect*	m_pEffect;			//	effects interface
	D3DXHANDLE		m_hTexture;			//	texture handle
	D3DXHANDLE		m_hTech;
	D3DXHANDLE		m_hWVP ;			// world*view*proj matrix
	D3DXHANDLE		m_hVP;				// view * proj matrix
	
	D3DXHANDLE		m_hWorld;			// world matrix
	D3DXHANDLE		m_hProj;			// projection matrix
	D3DXHANDLE		m_hView;			// view matrix

	



	//////////////////////
	// THREAD UTILS
	/////////////////////
	CriticalSection m_csQuad;
	CriticalSection m_csShape;


public:
	~ShapeManager(void);

	static ShapeManager* Instance();

	/////////////////////
	// FX STUFF
	////////////////////
	void BuildFX();


	///////////////////////////
	// CREATE SHAPES
	//////////////////////////
	// create a ship mesh with a scale of 1 and the position entered.
	int CreateShip(float xPos = 0.0f, float yPos = 0.0f, float zPos = 0.0f);
	int CreateSphere(float radius = 1.0f);
	int CreateBox(float x = 1.0f,float y = 1.0f, float z = 1.0f);
	int CreateLine(D3DXVECTOR3 vStartPos, D3DXVECTOR3 vEndPos);
	int CreateQuad(float xHalfExt, float yHalfExt, float zHalfExt);
	int CreateInstancedQuad(float xHalfExt, float yHalfExt, float zHalfExt);
	int CreatePyramid();
	int CreateParticle(float fScale);
	int D3DXCreateQuad();

	///////////////////////
	// ACCESS SHAPES
	//////////////////////
	D3DShape*	GetShape(int id);
	D3DSphere*	GetSphere(int id);
	D3DBox*		GetBox(int id);
	D3DLine*	GetLine(int id);
	D3DInstancedQuad* GetQuad(int id);

	////////////////////
	// DELETE SHAPES
	///////////////////
	void DeleteShape(int id);

	/////////////////
	// CAMERA (VIEWS)
	/////////////////
	void SetCameraView(D3DCamera *camView);

	////////////////
	// COLORS
	///////////////
	void SetColor(int id, EShapeColor newColor);
	void SetLineColor(int id, D3DCOLOR newColor);
	void SetRandomColor(int id);
	
	////////////////////
	// MUTATORS
	///////////////////
	void SetLine(int id, Vector3 vStartPos, Vector3 vEndPos);
	void SetPosition(int id, Vector3 vPos);
	// sets orientation   
	void SetOrientation(int iBodyID, const Quaternion &orientation);
    // sets orientation by passing individual values
	void SetOrientation(int iBodyID, const float w, const float x,const float y, const float z);
	// sets heading using yaw, pitch roll values
	void SetHeading(int id, float yaw, float pitch, float roll);
	// manually rotates by the given quaternion
	void RotateShape(int iBodyID, Quaternion &qRotation);
	void RotateShape(int id, float yaw, float pitch, float roll);
	void SetInstancedQuadTexture(IDirect3DTexture9* texture);

	////////////////////
	// UTILITIES
	///////////////////
	void Init();
	void Update(float dt);
	void Render();
	//my instanced render function
	void InstancedQuadPreRender();
	void InstancedQuadRender();
	
	void OnResetDevice();
	void OnLostDevice();
};

