#pragma once
#include "D3DShape.h"

class D3DBox :
	public D3DShape
{
private:
	virtual void CalculateInternals();

	//mesh ptr
	LPD3DXMESH m_pMeshBox;

	D3DXVECTOR3 mLightVecW;		// light in world coords
	D3DXCOLOR   mAmbientMtrl;	// ambient mat color
	D3DXCOLOR   mAmbientLight;	// ambient light color
	D3DXCOLOR   mDiffuseMtrl;	// diffse mat color
	D3DXCOLOR   mDiffuseLight;	// diffuse light color
	D3DXCOLOR   mSpecularMtrl;	// specular mat color
	D3DXCOLOR   mSpecularLight;	// specular light color
	float       mSpecularPower;	// specular power

	
public:
	D3DBox(void);
	D3DBox(LPD3DXMESH pBox, float x, float y, float z, IDirect3DTexture9* pTexture, int id);
	~D3DBox(void);

	
	///////////////
	// UTILITIES
	//////////////
	void BuildFX();
	virtual void Update(float dt);
	virtual void Render();
	virtual void OnResetDevice();
	virtual void OnLostDevice();


	void SetHalfExtents(float x, float y, float z){m_vScale = D3DXVECTOR3(x,y,z);}


};

