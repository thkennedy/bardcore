#include "D3DQuad.h"
#include "D3DApp.h"
#include "D3DVertex.h"
#include "D3DCamera.h"


D3DQuad::D3DQuad(int id, float x, float y, float z)
	:D3DShape(id)
{

	/////////////////////////
	// INTERNAL VARS
	/////////////////////////
	
	m_vPosition = m_vCenter = m_vOrientation = D3DXVECTOR3(0.0f,0.0f,0.0f);

	m_vScale = D3DXVECTOR3(x,y,z);

	m_vPosition = D3DXVECTOR3(0.0f,0.0f,0.0f);

	
	///////////////////////////////////////
	//		VERTEX BUFFER
	//////////////////////////////////////
	HRESULT hr;
	// Obtain a pointer to a new vertex buffer.
	hr = D3DAPPI->GetD3DDevice()->CreateVertexBuffer(	4 * sizeof(PNT), 
														D3DUSAGE_WRITEONLY,
														0, 
														D3DPOOL_MANAGED, 
														&m_VertexBuffer, 
														0);
	
	PNT* v = 0;
	m_VertexBuffer->Lock(0, 0, (void**)&v, 0);
	
	D3DXVECTOR3 normal;

	
	normal = D3DXVECTOR3(0.0f,0.0f,1.0f);
	v[0] = PNT(	-1.0f,	-1.0f,	0.0f,	normal,	0.0f,1.0f);
	v[1] = PNT(	-1.0f,	1.0f,	0.0f,	normal,	0.0f,0.0f);
	v[2] = PNT(	1.0f,	1.0f,	0.0f,	normal, 1.0f,0.0f);
	v[3] = PNT(	1.0f,	-1.0f,	0.0f,	normal, 1.0f,1.0f);

	m_VertexBuffer->Unlock();


	///////////////////////////
	// INDEX BUFFER
	//////////////////////////

		
	// Obtain a pointer to a new index buffer.
	hr = D3DAPPI->GetD3DDevice()->CreateIndexBuffer(6 * sizeof(WORD), D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_IndexBuffer, 0);

	// Now lock it to obtain a pointer to its internal data, and write the data

	WORD* k = 0;

	m_IndexBuffer->Lock(0, 0, (void**)&k, 0);

	// triangles
	k[0] = 0; k[1] = 1; k[2] = 2;
	k[3] = 0; k[4] = 2; k[5] = 3;


	m_IndexBuffer->Unlock();

	// setting texture
	//D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/IceTexture.png", &m_Texture);

	BuildFX();

	OnResetDevice();
}


D3DQuad::~D3DQuad(void)
{
}

void D3DQuad::CalculateInternals()
{

	//////////////
	//	ROTATION
	//////////////

	//quaternion calcs


	D3DXMATRIX rotationMatrix;

	D3DXMatrixRotationQuaternion(&rotationMatrix, &m_qOrientation);



	//////////////
	//	TRANSLATE
	//////////////

	D3DXMATRIX matTranslate;

	D3DXMatrixTranslation(&matTranslate,m_vPosition.x,m_vPosition.y,m_vPosition.z);

	//////////////
	//	SCALE
	//////////////

	D3DXMATRIX matScale;

	D3DXMatrixScaling(&matScale,m_vScale.x,m_vScale.y,m_vScale.z);

	///////////////
	//	TRANSFORM
	///////////////
	m_mTransform = matScale * rotationMatrix * matTranslate ;


}

void D3DQuad::BuildFX()
{
	HRESULT hr;
	
	// Create the FX from a .fx file.
	ID3DXBuffer* errors = 0;
	hr = D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), "Graphics/Techniques/quad.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &m_pEffect, &errors);
	if( errors ) 
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);	

	// Obtain handles.
	m_hTech = m_pEffect->GetTechniqueByName("QuadTech");
	m_hWVP  = m_pEffect->GetParameterByName(0, "gWVP");
	m_hTexture = m_pEffect->GetParameterByName(0,"gTex");

	//m_pEffect->SetTexture(m_hTexture, m_Texture);

	


}

void D3DQuad::Update(float dt)
{
				
	CalculateInternals();

}

void D3DQuad::Render()
{
	HRESULT hr;
	
	// Setup the rendering FX
	hr = m_pEffect->SetTechnique(m_hTech);
	hr = m_pEffect->SetMatrix(m_hWVP, &(m_mTransform * m_pCamView->GetViewProj()));

	hr = D3DAPPI->GetD3DDevice()->SetStreamSource(0,m_VertexBuffer,0,sizeof(PNT));
	hr = D3DAPPI->GetD3DDevice()->SetIndices(m_IndexBuffer);
	hr = D3DAPPI->GetD3DDevice()->SetVertexDeclaration(PNT::Decl);

	
	//m_pEffect->SetTexture(m_hTexture, m_Texture);

	// Begin passes.
	UINT numPasses = 0;
	m_pEffect->Begin(&numPasses, 0);
	for(UINT i = 0; i < numPasses; ++i)
	{
		m_pEffect->BeginPass(i);
		hr = D3DAPPI->GetD3DDevice()->DrawIndexedPrimitive(
			D3DPT_TRIANGLELIST, 
			0, 
			0, 
			4, 
			0, 
			2);
		m_pEffect->EndPass();
	}
	m_pEffect->End();
	
	
}

void D3DQuad::OnResetDevice()
{
	//D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/pyramid.jpg", &m_Texture);
	m_pEffect->OnResetDevice();
}
void D3DQuad::OnLostDevice()
{
	m_pEffect->OnLostDevice();
}

