#pragma once

#include "ParticleSystem.h"

class ParticleGun : public ParticleSystem
{
public:
	ParticleGun(void){}
	ParticleGun(const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		const D3DXVECTOR3& accel, 
		const AABBG& box,
		int maxNumParticles,
		float timePerParticle);

	~ParticleGun(void);

	void InitParticle(Particle& out);
};

