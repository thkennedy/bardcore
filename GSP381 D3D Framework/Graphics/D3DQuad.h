#pragma once
#include "D3DShape.h"

class D3DQuad : public D3DShape
{
private:

	virtual void CalculateInternals();

public:
	D3DQuad(void){}
	D3DQuad(int id, float x, float y, float z);
	~D3DQuad(void);


	///////////////
	// UTILITIES
	//////////////
	void BuildFX();
	virtual void Update(float dt);
	virtual void Render();
	virtual void OnResetDevice();
	virtual void OnLostDevice();

	void SetHalfExtents(float x, float y, float z){m_vScale = D3DXVECTOR3(x,y,z);}
};

