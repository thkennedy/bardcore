#pragma once
#include "D3DShape.h"

class D3DPyramid : public D3DShape
{
private:

	virtual void CalculateInternals();

public:
	D3DPyramid(void){}
	D3DPyramid(int id);
	~D3DPyramid(void);

	void BuildFX();
	virtual void Update(float dt);
	virtual void Render();
	virtual void OnResetDevice();
	virtual void OnLostDevice();


};

