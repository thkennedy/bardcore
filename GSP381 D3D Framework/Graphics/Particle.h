#pragma once
#include "D3DShape.h"



// forward declaration
class D3DInstancedQuad;

class Particle : public D3DShape
{
private:

public:
	//The initial position of the particle 
	Vector3 m_vInitialPos;			
	//The initial velocity of the particle 
	Vector3 m_vInitialVelocity;
	// how big the particle starts at
	real m_rInitialSize;
	// when particle was created
	real m_rInitialTime;
	// how long particle lives before it dies
	real m_rLifetime;
	// used for uniqueness calcs
	real m_rMass;
	// initial particle color, changed in vert shader
	D3DCOLOR m_cInitialColor;
	
	static IDirect3DVertexDeclaration9* Decl;

	// number to access 
	D3DInstancedQuad* myQuad;

	Particle(void){}
	Particle(int id);
	~Particle(void);

	///////////////
	// UTILITIES
	//////////////
	virtual void CalculateInternals(){}
	virtual void Update(real dt);
	virtual void Render();
	virtual void OnResetDevice();
	virtual void OnLostDevice();

	void ResetParticle();
};

