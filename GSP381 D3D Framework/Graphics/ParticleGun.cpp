#include "ParticleGun.h"
#include "CameraManager.h"
#include "../PRNG/RandomNumber.h"


ParticleGun::ParticleGun(const std::string& fxName, 
	const std::string& techName, 
	const std::string& texName, 
	const D3DXVECTOR3& accel, 
	const AABBG& box,
	int maxNumParticles,
	float timePerParticle)
	: ParticleSystem(fxName, techName, texName, accel, box, maxNumParticles,
	timePerParticle)
{
}

void ParticleGun::InitParticle(Particle& out)
{
	// Generate at camera.
	out.m_vInitialPos = CamMgr->m_pCurrentCamView->GetPosition();

	// Set down a bit so it looks like player is carrying the gun.
	out.m_vInitialPos.y -= 3.0f;

	// Fire in camera's look direction.
	float speed = 500.0f;
	out.m_vInitialVelocity = speed*CamMgr->m_pCurrentCamView->GetLookAt();
 
	out.m_fInitialTime      = m_rTime;
	out.m_fLifeTime        = 4.0f;
	out.m_cInitialColor    = D3DWhite;
	out.m_fInitialSize     = RandNum->Uniform(80.0f, 90.0f);
	out.m_fMass            = 1.0f;
}
ParticleGun::~ParticleGun(void)
{
}
