#pragma once

#include "../stdafx.h"
#include <d3d9.h>
#include <d3dx9.h>
#include "../Physics/Vector3.h"
#include "../Physics/functions.h"
#include "../Graphics/d3dUtility.h"
#include <string>

#pragma comment(lib, "winmm.lib")

// include the Direct3D Library file
#pragma comment (lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

class D3DSprite
{
private:

	//////////////////////////////////////////////////////////////////////////
	// Direct3D Variables
	//////////////////////////////////////////////////////////////////////////
	LPDIRECT3D9 m_pD3DObject;		// the pointer to our Direct3D interface
	LPDIRECT3DDEVICE9 m_pD3DDevice; // the pointer to the device class


	//////////////////////////////////////////////////////////////////////////
	// Sprite Vars
	//////////////////////////////////////////////////////////////////////////
	ID3DXSprite*		m_pD3DSprite;	// Sprite Object
	IDirect3DTexture9*	m_pTexture;		// Texture Object for a sprite
	D3DXIMAGE_INFO		m_imageInfo;	// File details of a texture
	Vector3				m_vSpriteDimensions;		// dimensions
	bool				m_bMouseOver;	//	determines which version to draw

	//internals - position, orientation, etc
	D3DXVECTOR3 m_vRenderPos, m_vPosition,m_vCenter, m_vRotation, m_vScale;
	D3DXMATRIX m_mTransform;
	

	//unique ID for sprite management
	int m_iSpriteID;

	//bool to determine if object is onscreen
	bool m_bOnScreen;

	/////////////////////
	/// SCRATCH PAD
	/////////////////////
	D3DSprite* tempSprite;
	D3DXVECTOR3 vScratch1,vScratch2;

	

public:
	D3DSprite(LPDIRECT3D9 d3dObj, LPDIRECT3DDEVICE9 d3dDevice, ID3DXSprite* spriteObj);
	D3DSprite();
	~D3DSprite();

	// name for easier ID-ing
	LPCTSTR	Name;

	// display time for timed updates
	float fDisplayTime;

	// bool to determine if object is always rendered
	bool m_bAlwaysRender;

	//////////////////////////////////////////////////////////////////////////
	// Utilities
	//////////////////////////////////////////////////////////////////////////
	void Update(float time_elapsed);
	void Render();
	void ReleaseSprite();
	void OnLostDevice();
	void OnReset();

	//////////////////////////////////////////////////////////////////////////
	// Window Vars
	//////////////////////////////////////////////////////////////////////////
	D3DDEVICE_CREATION_PARAMETERS m_d3dcpWindowParams;
	RECT m_rDisplay;
	

	//////////////////////////////////////////////////////////////////////////
	// Accessors
	//////////////////////////////////////////////////////////////////////////
	D3DXVECTOR3 GetPosition() {return m_vRenderPos;}
	D3DXVECTOR3 GetCenter() {return m_vCenter;}
	int GetID() {return m_iSpriteID;}
	Vector3 GetDimensions() {return m_vSpriteDimensions;}
	bool GetVisible(){return m_bOnScreen;}
	


	//////////////////////////////////////////////////////////////////////////
	// Mutators
	//////////////////////////////////////////////////////////////////////////
	void SetID(int id){m_iSpriteID = id;}
	void SetPosition(D3DXVECTOR3 newPos);
	void SetPosition(float x, float y){m_vPosition.x = x; m_vPosition.y = y;}
	void SetCenter(D3DXVECTOR3 newCenter){m_vCenter = newCenter;}
	void SetTexture(LPCTSTR NewTexture);
	void SetTexture(IDirect3DTexture9 *pTexture, LPCTSTR texturePath);
	void SetDimensions(Vector3 newDims) {m_vSpriteDimensions = newDims;}
	void SetMouseOver(bool mouseOver) {m_bMouseOver = mouseOver;}
	void SetRotation(D3DXVECTOR3 rotation){m_vRotation = rotation;}
	void SetScale(D3DXVECTOR3 scale){m_vScale = scale;}


};

