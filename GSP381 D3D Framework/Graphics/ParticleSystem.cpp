#include "D3DApp.h"
#include "ParticleSystem.h"
#include "GfxStats.h"
#include "CameraManager.h"
#include "D3DVertex.h"




ParticleSystem::ParticleSystem(
        const std::string& fxName,
        const std::string& sTechName,
        const std::string& sTexName,
        const Vector3& vAccel,
        const AABBG& box,
        int iMaxNumParticles,
        real rTimePerParticle)
	:m_vAccel(vAccel), m_Box(box), m_rTime(0.0f),
	m_iMaxNumParticles(iMaxNumParticles), m_rTimePerParticle(rTimePerParticle)
{
	// Allocate memory for maximum number of particles.
	m_vParticles.resize(m_iMaxNumParticles);
	m_vAliveParticles.reserve(m_iMaxNumParticles);
	m_vDeadParticles.reserve(m_iMaxNumParticles);

	// They start off all dead.
	for(int i = 0; i < m_iMaxNumParticles; ++i)
	{
		// Since we are accessing the elements of m_vParticles here,
		// we had to use 'resize'. The vector::reserve method allocates
		// memory but does not create objects, and therefore, it is
		// illegal to access the elements.
		m_vParticles[i].m_fLifeTime = -1.0f;
		m_vParticles[i].m_fInitialTime = 0.0f;
	}

	D3DXMatrixIdentity(&m_mWorld);
	D3DXMatrixIdentity(&m_mInvWorld);

	m_vPosition = Vector3(0,0,0);
	m_vRotation = Vector3(0,0,0);

	D3DXCreateTextureFromFileA(D3DAPPI->GetD3DDevice(), sTexName.c_str(), &m_Tex);

	BuildFX(fxName,sTechName);

	D3DAPPI->GetD3DDevice()->CreateVertexBuffer(m_iMaxNumParticles*sizeof(Particle),
		D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY|D3DUSAGE_POINTS,
		0, D3DPOOL_DEFAULT, &m_VB, 0);
}


ParticleSystem::~ParticleSystem(void)
{
	ReleaseCOM(m_FX);
	ReleaseCOM(m_Tex);
	ReleaseCOM(m_VB);
	
	
}

void ParticleSystem::BuildFX(const std::string& fxName, 
							 const std::string& sTechName)
{
	
	// Create the FX.
	ID3DXBuffer* errors = 0;
	D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), fxName.c_str(),
		0, 0, D3DXSHADER_DEBUG, 0, &m_FX, &errors);
	if( errors )
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);

	m_hTech    = m_FX->GetTechniqueByName(sTechName.c_str());
	m_hWVP     = m_FX->GetParameterByName(0, "gWVP");
	m_hEyePosL = m_FX->GetParameterByName(0, "gEyePosL");
	m_hTex     = m_FX->GetParameterByName(0, "gTex");
	m_hTime    = m_FX->GetParameterByName(0, "gTime");
	m_hAccel   = m_FX->GetParameterByName(0, "gAccel");
	m_hViewportHeight = m_FX->GetParameterByName(0, "gViewportHeight");

	// We don't need to set these every frame since they do not change.
	hr = m_FX->SetTechnique(m_hTech);
	hr = m_FX->SetValue(m_hAccel, m_vAccel, sizeof(D3DXVECTOR3));
	hr = m_FX->SetTexture(m_hTex, m_Tex);
}

////////////////////
// Utilities
////////////////////
void ParticleSystem::AddParticle()
{
	// If there are no dead particles, then we are maxed out.
      if(m_vDeadParticles.size() > 0)
      {
            // Reinitialize a particle.
            Particle* p = m_vDeadParticles.back();
            InitParticle(*p);

            // No longer dead.
            m_vDeadParticles.pop_back();
            m_vAliveParticles.push_back(p);

								
      }

}
// builds transform matrix
void ParticleSystem::BuildTransform()
{
	//////////////
	//	ROTATION
	//////////////

	
	D3DXMatrixRotationX(&matRotX, m_vRotation.x);
	D3DXMatrixRotationY(&matRotY, m_vRotation.y);
	D3DXMatrixRotationZ(&matRotZ, m_vRotation.z);

	rotationMatrix = matRotX * matRotY * matRotZ;

	//////////////
	//	TRANSLATE
	//////////////
	D3DXMatrixTranslation(&matTranslate,m_vPosition.x,m_vPosition.y,m_vPosition.z);


	//////////////
	//	SCALE
	//////////////
	D3DXMatrixScaling(&matScale,m_vScale.x,m_vScale.y,m_vScale.z);

	///////////////
	//	TRANSFORM
	///////////////
	m_mWorld = matScale * rotationMatrix * matTranslate ;

	


	D3DXMatrixInverse(&m_mInvWorld, 0, &m_mWorld);


}
void ParticleSystem::Update(real dt)
{
	BuildTransform();

	 m_rTime += dt;

	 // rebuild the transform matrix
	 //BuildTransform();

      // Rebuild the dead and alive list. Note that resize(0) does
      // not deallocate memory (i.e., the capacity of the vector does
      // not change).
      m_vDeadParticles.resize(0);
      m_vAliveParticles.resize(0);

      // For each particle.
      for(int i = 0; i < m_iMaxNumParticles; ++i)
      {
            // Is the particle dead?

            if( (m_rTime - m_vParticles[i] .m_fInitialTime) >
                         m_vParticles[i] .m_fLifeTime)
            {
                  m_vDeadParticles.push_back(&m_vParticles[i]);
				  // subtract from vert count
            }
            else
            {
				m_vAliveParticles.push_back(&m_vParticles[i]);
            }
      }

	  iNumVerts = m_vAliveParticles.size();

      // A negative or zero m_rTimePerParticle value denotes
      // not to emit any particles.
      if( m_rTimePerParticle > 0.0f )
      {
            // Emit particles.
            static float timeAccum = 0.0f;
            timeAccum += dt;
            while( timeAccum >= m_rTimePerParticle )
            {
                  AddParticle();
                  timeAccum -= m_rTimePerParticle;
            }
      }

	  

}
void ParticleSystem::Draw()
{
	// Set camera position relative to world space system and make it
	// relative to the particle system's local system.
	D3DXVECTOR3 eyePosW = CamMgr->m_pCurrentCamView->GetPosition();
	D3DXVECTOR3 eyePosL;
	D3DXVec3TransformCoord(&eyePosL, &eyePosW, &m_mInvWorld);

	// Set FX parameters.
	m_FX->SetValue(m_hEyePosL, &eyePosL, sizeof(D3DXVECTOR3));
	m_FX->SetFloat(m_hTime, m_rTime);
	m_FX->SetMatrix(m_hWVP, &(m_mWorld*CamMgr->m_pCurrentCamView->GetViewProj()));	
	hr = m_FX->SetValue(m_hAccel, m_vAccel, sizeof(D3DXVECTOR3));

	// Point sprite sizes are given in pixels. So if the
	// viewport size is changed, then more or less pixels
	// become available, which alters the perceived size of
	// the particles.  For example, if the viewport is 32x32,
	// then a 32x32 sprite covers the entire viewport!  But if
	// the viewport is 1024x1024, then a 32x32 sprite only
	// covers a small portion of the viewport.  Thus, we scale
	// the particle's size by the viewport height to keep them
	// in proportion to the viewport dimensions.
	HWND hwnd = D3DAPPI->GetHWND();
	RECT clientRect;

	GetClientRect(hwnd, &clientRect);
	m_FX->SetInt(m_hViewportHeight, clientRect.bottom);

	UINT numPasses = 0;
	m_FX->Begin(&numPasses, 0);
	m_FX->BeginPass(0);

	D3DAPPI->GetD3DDevice()->SetStreamSource(0, m_VB, 0, sizeof(Particle));
	D3DAPPI->GetD3DDevice()->SetVertexDeclaration(Particle::Decl);

	/*AABBG boxWorld;
	m_Box.xform(mWorld, boxWorld);
	if( m_pCamView->isVisible( boxWorld ) )
	{*/
		// Initial lock of VB for writing.
		Particle* p = 0;
		m_VB->Lock(0, 0, (void**)&p, D3DLOCK_DISCARD);
		int vbIndex = 0;

		// For each living particle.
		for(UINT i  = 0; i < m_vAliveParticles.size(); ++i)
		{
			// Copy particle to VB
			p[vbIndex] = *m_vAliveParticles[i];
			++vbIndex;
		}
		m_VB->Unlock();

		// Render however many particles we copied over.
		if(vbIndex > 0)
		{
			D3DAPPI->GetD3DDevice()->DrawPrimitive(D3DPT_POINTLIST, 0, vbIndex);
		}
	//}

	m_FX->EndPass();
	m_FX->End();
	
}

////////////////////
// Accessors
///////////////////
real ParticleSystem::GetTime()
{
	return m_rTime;
}
const AABBG& ParticleSystem::GetAABBG()const
{
	return m_Box;
}
D3DMATRIX ParticleSystem::GetWorldMatrix()
{
	return m_mWorld;
}

//////////////////
// Mutators
//////////////////
void  ParticleSystem::SetTime(real t)
{
	m_rTime = t;
}
void ParticleSystem::SetWorldMtx(const D3DXMATRIX& world)
{
	m_mWorld = world;

	// Compute the change of coordinates matrix that changes
    // coordinates relative to world space so that they are
    // relative to the particle system's local space.
    D3DXMatrixInverse(&m_mInvWorld, 0, &m_mWorld);

}

void ParticleSystem::SetPosition(float x, float y, float z)
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;
}

void ParticleSystem::RotateSystem(float x, float y, float z)
{
	m_vRotation.x += x;
	m_vRotation.y += y;
	m_vRotation.z += z;

	m_vRotation.Normalize();
}

void ParticleSystem::SetRotation(float x, float y, float z)
{
	m_vRotation.x = x;
	m_vRotation.y = y;
	m_vRotation.z = z;
}

void ParticleSystem::SetScale(float x, float y, float z)
{
	m_vScale.x = x;
	m_vScale.y = y;
	m_vScale.z = z;
}
    
	
//////////////////
// D3D Methods
/////////////////
void ParticleSystem::OnLostDevice()
{
	m_FX->OnLostDevice();
      // Default pool resources need to be freed before reset.
   ReleaseCOM(m_VB);
}
void ParticleSystem::OnResetDevice()
{
	m_FX->OnResetDevice();

	if (m_VB == 0)
	{
		hr = D3DAPPI->GetD3DDevice()->CreateVertexBuffer(
			m_iMaxNumParticles*sizeof(Particle),
			D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY|D3DUSAGE_POINTS,
			0, D3DPOOL_DEFAULT, &m_VB, 0);
	}

}