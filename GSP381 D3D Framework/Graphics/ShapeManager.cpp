#include "ShapeManager.h"
#include "D3DBox.h"
#include "D3DSphere.h"
#include "D3DLine.h"
#include "D3DShip.h"
#include "D3DCamera.h"
#include "D3DApp.h"
#include "GfxStats.h"
#include "D3DQuad.h"
#include "D3DPyramid.h"
#include "InstancedQuad.h"
#include "../Physics/Quaternion.h"
#include "../Physics/Matrix4.h"
#include "D3DVertex.h"
//#include "Particle.h"
#include "../MultiThreading/CriticalSection.h"
#include <random>




ShapeManager::ShapeManager(void)
{
	m_iShapeID = 0;

	D3DXCreateSphere(D3DAPPI->GetD3DDevice(), 1.0f, 40, 20, &m_pSphere, NULL);
	D3DXCreateBox(D3DAPPI->GetD3DDevice(), 1.0f, 1.0f, 1.0f, &m_pBox, NULL);
	D3DXCreateLine(D3DAPPI->GetD3DDevice(),&m_pLine);
	D3DXCreateQuad();

	

	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/Colors/Red.png", &m_RedTexture);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/Colors/Green.png", &m_GreenTexture);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/Colors/Blue.png", &m_BlueTexture);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/Colors/Gray.png", &m_GrayTexture);
	
	// instanced quad texture
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/IceTexture.png", &m_InstancedQuadTexture);

	srand(time(NULL));


	BuildFX();
	
	

}


ShapeManager::~ShapeManager(void)
{
	
}

ShapeManager* ShapeManager::Instance()
{
	static ShapeManager instance;

	return &instance;
}

void ShapeManager::BuildFX()
{

	//////////////////////
	// INSTANCED QUADS
	/////////////////////
	HRESULT hr;
	
	// Create the FX from a .fx file
	ID3DXBuffer* errors = 0;
	hr = D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), "Graphics/Techniques/InstancedVS.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &m_pEffect, &errors);
	if( errors ) 
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);	

	// Obtain handles.
	m_hTech = m_pEffect->GetTechniqueByName("InstancedVSTech");
	m_hTexture = m_pEffect->GetParameterByName(0,"gTex");
	m_hWorld = m_pEffect->GetParameterByName(0, "gWorld");
	m_hView	= m_pEffect->GetParameterByName(0,"gView");
	m_hProj = m_pEffect->GetParameterByName(0,"gProj");
	m_hVP	= m_pEffect->GetParameterByName(0,"gViewProj");

	m_pEffect->SetTexture(m_hTexture, m_InstancedQuadTexture);
}

///////////////////////////
// CREATE SHAPES
//////////////////////////

int ShapeManager::CreateShip(float xPos, float yPos, float zPos)
{
	// create ship object, id
	D3DShip *tempShip = new D3DShip(m_iShapeID);

	// give it the current cam view
	tempShip->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	m_vAllShapes.push_back(tempShip);

	// add it to the shape map for later access
	m_mapShapes.insert(pair<int, D3DShape*>(m_iShapeID,tempShip) );

	return m_iShapeID++;


}
int ShapeManager::CreateSphere(float radius)
{
	// increase number of spheres
	m_iNumSpheres++;

	//create sphere object, id
	D3DSphere *tempSphere = new D3DSphere(m_pSphere, radius, m_BlueTexture, m_iShapeID);

	//give it the current cam view
	tempSphere->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	m_vAllShapes.push_back(tempSphere);

	// add it to the shape map for later access
	m_mapShapes.insert(pair<int, D3DShape*>(m_iShapeID,tempSphere) );

	//return the id so it can be stored, then increment
	return m_iShapeID++;
}

int ShapeManager::CreateBox(float x,float y, float z)
{
	// increase the number of boxes
	m_iNumBoxes++;

	//create sphere object, id
	D3DBox *tempBox = new D3DBox(m_pBox, x,y,z, m_RedTexture, m_iShapeID);

	//give it the current cam view
	tempBox->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	m_vAllShapes.push_back(tempBox);

	// add it to the shape map for later access
	m_mapShapes.insert(pair<int, D3DShape*>(m_iShapeID,tempBox) );

	//return the id so it can be stored, then increment
	return m_iShapeID++;
}




int ShapeManager::CreateLine(D3DXVECTOR3 vStartPos, D3DXVECTOR3 vEndPos)
{
	// create line object, id - DEFAULT COLOR IS WHITE
	D3DLine *tempLine = new D3DLine(m_pLine, vStartPos, vEndPos, D3DCOLOR_XRGB(255,255,255), m_iShapeID);

	// give it the current cam view
	tempLine->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	m_vAllShapes.push_back(tempLine);

	// add it to the shape map for later access
	m_mapShapes.insert(pair<int, D3DShape*>(m_iShapeID,tempLine) );

	// return the id so it can be store, then increment
	return m_iShapeID++;
}

int ShapeManager::CreateQuad(float xHalfExt, float yHalfExt, float zHalfExt)
{
	// increase number of quads on counter
	m_iNumQuads++;

	// create the quad object
	D3DQuad* tempQuad = new D3DQuad(m_iShapeID,xHalfExt, yHalfExt, zHalfExt);

	// set its texture
	tempQuad->SetTexture(m_InstancedQuadTexture);

	tempQuad->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	//m_vQuads.push_back(tempQuad);
	m_vAllShapes.push_back(tempQuad);

	// add it to the shape map for later access
	m_mapShapes.insert(pair<int, D3DShape*>(m_iShapeID,tempQuad) );

	// add vert data
	GStats->addVerts(4);
	GStats->addQuad(1);

	//return the id so it can be stored
	return m_iShapeID++;

}

int ShapeManager::CreateInstancedQuad(float xHalfExt, float yHalfExt, float zHalfExt)
{
	//;
	// increase number of quads on counter
	m_iNumQuads++;

	// create the quad object
	D3DInstancedQuad* tempQuad = new D3DInstancedQuad(m_iShapeID,xHalfExt, yHalfExt, zHalfExt);

	tempQuad->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	m_vQuads.push_back(tempQuad);
	
	// add it to the shape map for later access
	m_mapShapes.insert(pair<int, D3DShape*>(m_iShapeID,tempQuad) );

	// add vert data
	GStats->addVerts(4);
	GStats->addQuad(1);

	//return the id so it can be stored
	return m_iShapeID++;

}

int ShapeManager::CreatePyramid()
{
	// increase number of quads on counter
	//m_iNumQuads++;

	// create the quad object
	D3DPyramid* tempPyramid = new D3DPyramid(m_iShapeID);

	tempPyramid->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	m_vAllShapes.push_back(tempPyramid);
	
	// add it to the shape map for later access
	m_mapShapes.insert(pair<int, D3DShape*>(m_iShapeID,tempPyramid) );

	//return the id so it can be stored
	return m_iShapeID++;
}

int ShapeManager::CreateParticle(float fScale)
{
	//// temp particle
	////Particle* tempParticle = new Particle(m_iShapeID);

	//// create its quad and assign it
	//int id = CreateInstancedQuad(fScale,fScale,fScale);

	//tempParticle->myQuad = (D3DInstancedQuad*)(SMI->GetShape(id));

	//// add it to the shape vec so its gets called in loops
	//m_vAllShapes.push_back(tempParticle);

	// add it to the shape map for later access
	///m_mapShapes.insert(pair<int, D3DShape*>(m_iShapeID,tempParticle) );

	//// return the id
	//return m_iShapeID++;

	return 1;


}

int ShapeManager::D3DXCreateQuad()
{
	//****************************************
	// Create Quad Vert and Index Buffer
	//****************************************

	HRESULT hr;
	// Obtain a pointer to a new vertex buffer.
	hr = D3DAPPI->GetD3DDevice()->CreateVertexBuffer(	4 * sizeof(PNT), 
														D3DUSAGE_WRITEONLY,
														0, 
														D3DPOOL_MANAGED, 
														&m_QuadVB, 
														0);
	
	PNT* v = 0;
	m_QuadVB->Lock(0, 0, (void**)&v, 0);
	
	D3DXVECTOR3 normal;

	
	normal = D3DXVECTOR3(0.0f,0.0f,1.0f);
	v[0] = PNT(	-1.0f,	-1.0f,	0.0f,	normal,	0.0f,1.0f);
	v[1] = PNT(	-1.0f,	1.0f,	0.0f,	normal,	0.0f,0.0f);
	v[2] = PNT(	1.0f,	1.0f,	0.0f,	normal, 1.0f,0.0f);
	v[3] = PNT(	1.0f,	-1.0f,	0.0f,	normal, 1.0f,1.0f);

	m_QuadVB->Unlock();


	///////////////////////////
	// INDEX BUFFER
	//////////////////////////

		
	// Obtain a pointer to a new index buffer.
	hr = D3DAPPI->GetD3DDevice()->CreateIndexBuffer(6 * sizeof(WORD), D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_QuadIB, 0);

	// Now lock it to obtain a pointer to its internal data, and write the data

	WORD* k = 0;

	m_QuadIB->Lock(0, 0, (void**)&k, 0);

	// triangles
	k[0] = 0; k[1] = 1; k[2] = 2;
	k[3] = 0; k[4] = 2; k[5] = 3;


	m_QuadIB->Unlock();

	// creating instance buffer
	hr = D3DAPPI->GetD3DDevice()->CreateVertexBuffer(	250000 * sizeof(FFFFC), 
														D3DUSAGE_WRITEONLY,
														0, 
														D3DPOOL_MANAGED, 
														&m_InstanceDataVB, 
														0);

	//*****************************************
	//*****************************************

	return 1;
}



///////////////////////
// ACCESS SHAPES
//////////////////////
D3DShape*	ShapeManager::GetShape(int id)
{
	// locking down this call
	return m_mapShapes.find(id)->second;
	
}
D3DSphere*	ShapeManager::GetSphere(int id)
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		if ((*it)->GetID() == id)
		{
			return (D3DSphere*)(*it);
		}
	}

	//item wasn't found, return NULL ptr
	return NULL; 
}
D3DBox*		ShapeManager::GetBox(int id)
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		if ((*it)->GetID() == id)
		{
			return (D3DBox*)(*it);
		}
	}

	//item wasn't found, return NULL ptr
	return NULL; 
}

D3DLine*	ShapeManager::GetLine(int id)
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		if ((*it)->GetID() == id)
		{
			return (D3DLine*)(*it);
		}
	}

	//item wasn't found, return NULL ptr
	return NULL; 
}


////////////////////
// DELETE SHAPES
///////////////////
void ShapeManager::DeleteShape(int id)
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
		{
			if ((*it)->GetID() == id)
			{
				delete (*it);
				m_vAllShapes.erase(it);
				return;
			}
		}

	//item wasn't found, do nothing
	return;
}

/////////////////
// CAMERA (VIEWS)
/////////////////
void ShapeManager::SetCameraView(D3DCamera *camView)
{
	m_pCurrentCamView = camView;
}


////////////////
// COLORS
///////////////
void ShapeManager::SetColor(int id, EShapeColor newColor)
{

	switch(newColor)
	{
	case RED:
		GetShape(id)->SetTexture(m_RedTexture);
		break;
	case GREEN:
		GetShape(id)->SetTexture(m_GreenTexture);
		break;
	case BLUE:
		GetShape(id)->SetTexture(m_BlueTexture);
		break;
	case GRAY:
		GetShape(id)->SetTexture(m_GrayTexture);
		break;
	case YELLOW:
		GetShape(id)->SetTexture(m_YellowTexture);
		break;
	}

	

}

void ShapeManager::SetLineColor(int id, D3DCOLOR newColor)
{
	GetShape(id)->SetColor(newColor);
}

void ShapeManager::SetRandomColor(int id)
{
	int num = (rand() % 4) + 1;

	switch(num)
	{
	case 1:
		GetShape(id)->SetTexture(m_YellowTexture);
		break;
	case 2:
		GetShape(id)->SetTexture(m_GreenTexture);
		break;
	case 3:
		GetShape(id)->SetTexture(m_BlueTexture);
		break;
	case 4:
		GetShape(id)->SetTexture(m_GrayTexture);
		break;
	}
}

////////////////////
// CHANGES
///////////////////
void ShapeManager::SetLine(int id, Vector3 vStartPos, Vector3 vEndPos)
{
	D3DLine* temp = GetLine(id);
	temp->SetPosition(vStartPos);
	temp->SetEndPosition(vEndPos);
	
}

void ShapeManager::SetPosition(int id, Vector3 vPos)
{
	//;

	GetShape(id)->SetPosition(vPos);
}

// sets orientation   
void ShapeManager::SetOrientation(int iBodyID, const Quaternion &orientation)
{
	//;

	GetShape(iBodyID)->SetHeading(orientation);
}

// sets orientation by passing individual values
void ShapeManager::SetOrientation(int iBodyID, const float w, const float x,const float y, const float z)
{
	//;

	GetShape(iBodyID)->SetHeading(Quaternion(w,x,y,z));
}

void ShapeManager::SetHeading(int id, float yaw, float pitch, float roll)
{
	//;

	GetShape(id)->SetHeading(yaw, pitch, roll);
}

// manually rotates by the given quaternion
void ShapeManager::RotateShape(int iBodyID, Quaternion &qRotation)
{
	//;

	GetShape(iBodyID)->RotateShapeQ(qRotation);
}

void ShapeManager::RotateShape(int id, float yaw, float pitch, float roll)
{
	//;

	Quaternion temp;
	D3DXQuaternionRotationYawPitchRoll(&temp, yaw, pitch, roll);
	GetShape(id)->RotateShapeQ(temp);
}

void ShapeManager::SetInstancedQuadTexture(IDirect3DTexture9* texture)
{
	m_InstancedQuadTexture = texture;

	m_pEffect->SetTexture(m_hTexture, m_InstancedQuadTexture);
}


void ShapeManager::Update(float dt)
{
	//;

	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		(*it)->Update(dt);
	}
	auto begin = m_vQuads.begin();

	for (int i = m_vQuads.size() - 1; i >= 0; --i)
	{
		m_vQuads[i]->Update(dt);
	}
}
void ShapeManager::Render()
{
	for (int i = m_vAllShapes.size()-1; i >= 0;--i)
	{
		m_vAllShapes[i]->Render();
	}

}
void ShapeManager::InstancedQuadPreRender()
{
	// message var
	//HRESULT hr;
	
	// container to create instance data
	FFFFC* ib = NULL;

	// lock instance buffer for writing
	m_InstanceDataVB->Lock(0,0,(void**)&ib, 0);

	// scratch pad for matrix transfer
	D3DXMATRIX tempMat;
	for (int i = m_vQuads.size()-1; i >= 0; --i)
	{
		// store matrix in temp var for quick access
		tempMat = m_vQuads[i]->GetTransform();
		ib[i] = FFFFC(tempMat._11,tempMat._12,tempMat._13,tempMat._14,
						tempMat._21,tempMat._22,tempMat._23,tempMat._24,
						tempMat._31,tempMat._32,tempMat._33,tempMat._34,
						tempMat._41,tempMat._42,tempMat._43,tempMat._44,
						0,0,0);


	}

	// unlock buffer
	m_InstanceDataVB->Unlock();



}
// my instanced render method
void ShapeManager::InstancedQuadRender()
{

	HRESULT hr;

	// If the device was not created successfully, return
	if(!D3DAPPI->GetD3DDevice())
		return;


    // clear the window to a deep blue
	//hr = D3DAPPI->GetD3DDevice()->Clear(0, 0, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 10, 100), 1.0f, 0);
	//hr = D3DAPPI->GetD3DDevice()->BeginScene();    // begins the 3D scene

	// set stream source freq for first stream, using indexed geom
	
	hr = D3DAPPI->GetD3DDevice()->SetStreamSourceFreq(0,
		(D3DSTREAMSOURCE_INDEXEDDATA | m_vQuads.size()) );
	
	// set up first stream source with the vertex buffer
	// containing geometry for the geometry packet
	hr = D3DAPPI->GetD3DDevice()->SetStreamSource(0,
		m_QuadVB, 0, sizeof(PNT) );

	// set up stream source frequency for the second stream;
	// each set of instance attributes describes one instance
	// to be rendered
	hr = D3DAPPI->GetD3DDevice()->SetStreamSourceFreq(1,
		(D3DSTREAMSOURCE_INSTANCEDATA | 1) );

	// set up second stream source with the vertex buffer
	// containing all instances' attributes
	hr = D3DAPPI->GetD3DDevice()->SetStreamSource(1,
		m_InstanceDataVB, 0,sizeof(FFFFC) );

	// set geometry stream vertex declaration for the draw call
	hr = D3DAPPI->GetD3DDevice()->SetVertexDeclaration(GStream::Decl);
	hr = D3DAPPI->GetD3DDevice()->SetIndices(m_QuadIB);
	

	// Setup the rendering FX
	hr = m_pEffect->SetTechnique(m_hTech);
	
	hr = m_pEffect->SetMatrix(m_hView, &m_pCurrentCamView->GetView());
	hr = m_pEffect->SetMatrix(m_hProj, &m_pCurrentCamView->GetProj());
	hr = m_pEffect->SetTexture(m_hTexture, m_InstancedQuadTexture);
	hr = m_pEffect->SetMatrix(m_hVP, &m_pCurrentCamView->GetViewProj());

	// Begin passes.
	UINT numPasses = 0;
	m_pEffect->Begin(&numPasses, 0);
	for(UINT i = 0; i < numPasses; ++i)
	{
		m_pEffect->BeginPass(i);
		hr = D3DAPPI->GetD3DDevice()->DrawIndexedPrimitive(
			D3DPT_TRIANGLELIST, 
			0, 
			0, 
			4, 
			0, 
			2);
		m_pEffect->EndPass();
	}
	m_pEffect->End();

	//m_InstanceDataVB->Release();

	D3DAPPI->GetD3DDevice()->SetStreamSourceFreq(0,1);
	D3DAPPI->GetD3DDevice()->SetStreamSourceFreq(1,1);


}
	
void ShapeManager::OnResetDevice()
{
	m_pEffect->OnResetDevice();
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		(*it)->OnResetDevice();
	}
}
void ShapeManager::OnLostDevice()
{
	m_pEffect->OnLostDevice();
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		(*it)->OnLostDevice();
	}
}