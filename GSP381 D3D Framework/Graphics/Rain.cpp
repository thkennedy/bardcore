#include "Rain.h"
#include "../PRNG/RandomNumber.h"
#include "CameraManager.h"

Rain::Rain(const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		const D3DXVECTOR3& accel, 
		const AABBG& box,
		int maxNumParticles,
		float timePerParticle)
		: ParticleSystem(fxName, techName, texName, accel, box, maxNumParticles,
		timePerParticle)
	{
	}


void Rain::InitParticle(Particle& out)
{
	// Generate about the camera.
	out.m_vInitialPos = CamMgr->m_pCurrentCamView->GetPosition();

	// Spread the particles out on xz-plane.
	out.m_vInitialPos.x += RandNum->Uniform(-100.0f, 100.0f);
	out.m_vInitialPos.z += RandNum->Uniform(-100.0f, 100.0f);

	// Generate above the camera.
	out.m_vInitialPos.y += RandNum->Uniform(50.0f, 55.0f);

	out.m_fInitialTime = m_rTime;
	out.m_fLifeTime = RandNum->Uniform(2.0f, 2.5f);
	out.m_cInitialColor = D3DWhite;
	out.m_fInitialSize = RandNum->Uniform(6.0f, 7.0f);

	// Give them an initial falling down velocity.
	out.m_vInitialVelocity.x = RandNum->Uniform(-1.5f, 0.0f);
	out.m_vInitialVelocity.y = RandNum->Uniform(-50.0f, -45.0f);
	out.m_vInitialVelocity.z = RandNum->Uniform(-0.5f, 0.5f);
}
