#include "FireRing.h"
#include "../PRNG/RandomNumber.h"
//#include "Particle.h"


FireRing::FireRing(const std::string& fxName,
            const std::string& techName,
            const std::string& texName,

            const D3DXVECTOR3& accel,
            const AABBG& box,
            int maxNumParticles,
            float timePerParticle)
            : ParticleSystem(fxName, techName, texName, accel, box,
              maxNumParticles, timePerParticle)
{
}



FireRing::~FireRing(void)
{
}

void FireRing::InitParticle(Particle& out)
{
	// Time particle is created relative to the global running
        // time of the particle system.
        out.m_fInitialTime = m_rTime;

        // Flare lives for 2-4 seconds.
		out.m_fLifeTime = RandNum->Uniform(2.0f, 4.0f);

        // Initial size in pixels.
        out.m_fInitialSize = RandNum->Uniform(10.0f, 15.0f);

        // Give a very small initial velocity to give the flares
        // some randomness.
        RandNum->GetRandomVec(out.m_vInitialVelocity.x,out.m_vInitialVelocity.y, out.m_vInitialVelocity.z );

        // Scalar value used in vertex shader as an
        // amplitude factor.
        out.m_fMass = RandNum->Uniform(1.0f, 2.0f);

        // Start color at 50-100% intensity when born for
        // variation.
        out.m_cInitialColor = RandNum->Uniform(0.5f, 1.0f)*D3DWhite;

        // Generate random particle on the ring in polar
        // coordinates: random radius and random angle.
        float r = RandNum->Uniform(10.0f, 14.0f);
        float t = RandNum->Uniform(0, 2.0f*D3DX_PI);

        // Convert to Cartesian coordinates.
        out.m_vInitialPos.x = r*cosf(t);
        out.m_vInitialPos.y = r*sinf(t);

		// Random depth value in [-1, 1] (depth of the ring)
        out.m_vInitialPos.z = RandNum->Uniform(-1.0f, 1.0f);

		//out.m_vInitialPos += m_vPosition;

}