#pragma once
#include "ParticleSystem.h"


class Rain : public ParticleSystem
{
public:
	Rain(const std::string& fxName,
		const std::string& techName,
		const std::string& texName,
		const D3DXVECTOR3& accel,
		const AABBG& box,
		int maxNumParticles,
		float timePerParticle);

	void InitParticle(Particle& out);
};
