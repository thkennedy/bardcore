#include "D3DCamera.h"
#include "D3DApp.h"
#include "../Input/DirectInput.h"

D3DCamera::D3DCamera()
{
	//default cam, backed up on z looking down +z - up in world up.
	m_vPosition = D3DXVECTOR3(0,0,-50);
	//its free cam, not locked on a object so its origin is its own
	m_vOrigin = m_vPosition;

	//starts looking at the origin
	m_vLookAt = D3DXVECTOR3(0,0,0) - m_vPosition;

	m_vRight = D3DXVECTOR3(1,0,0);
	m_vUp = D3DXVECTOR3(0,1,0);

	m_fSpeed = 5.0f;

	m_eMode = FREE;

	m_vLockTarget = D3DXVECTOR3(0,0,0);
	
	CalculateViewMatrix();



}

D3DCamera::D3DCamera(D3DXVECTOR3 pos, D3DXVECTOR3 look, D3DXVECTOR3 up)
{
	
}

D3DCamera::D3DCamera(float xPos, float yPos, float zPos, float xLook, float yLook, float zLook, 
		float xUp, float yUp, float zUp)
{
	
}

D3DCamera::~D3DCamera()
{

}

void D3DCamera::Update(float dt)
{
	
	// Find the net direction the camera is traveling in (since the
	// camera could be running and strafing).
	D3DXVECTOR3 dir(0.0f, 0.0f, 0.0f);

	if( gDInput->keyDown(DIK_W) )
		dir += m_vLookAt;
	if( gDInput->keyDown(DIK_S) )
		dir -= m_vLookAt;
	if( gDInput->keyDown(DIK_D) )
		dir += m_vRight;
	if( gDInput->keyDown(DIK_A) )
		dir -= m_vRight;
	if( gDInput->keyDown(DIK_Q) )
		dir -= m_vUp;
	if( gDInput->keyDown(DIK_E) )
		dir += m_vUp;
	
	// Move at mSpeed along net direction.
	D3DXVec3Normalize(&dir, &dir);
	D3DXVECTOR3 newPos = m_vPosition + dir*m_fSpeed*dt;

	float pitch = 0.0;
	float yAngle = 0.0;
	if (gDInput->mouseButtonDown(1))
	{
		// We rotate at a fixed speed if rmb is down
		pitch  = gDInput->mouseDY() / 150.0f;
		yAngle = gDInput->mouseDX() / 150.0f;
	}

	if (m_eMode == FREE)
	{
		// Rotate camera's look and up vectors around the camera's right vector.
		D3DXMATRIX R;
		D3DXMatrixRotationAxis(&R, &m_vRight, pitch);
		D3DXVec3TransformCoord(&m_vLookAt, &m_vLookAt, &R);
		D3DXVec3TransformCoord(&m_vUp, &m_vUp, &R);


		// Rotate camera axes about the world's y-axis.
		D3DXMatrixRotationY(&R, yAngle);
		D3DXVec3TransformCoord(&m_vRight, &m_vRight, &R);
		D3DXVec3TransformCoord(&m_vUp, &m_vUp, &R);
		D3DXVec3TransformCoord(&m_vLookAt, &m_vLookAt, &R);
	}
	else
	{
		//set the camera's position to an offset from its target
		m_vPosition = D3DXVECTOR3(m_vLockTarget.x,m_vLockTarget.y + 20, m_vLockTarget.z + 20);
		//set the camera to lock onto the target
		LockCamera(m_vLockTarget);

		// Rotate camera's look and up vectors around the camera's right vector.
		D3DXMATRIX R;
		D3DXMatrixRotationAxis(&R, &m_vRight, pitch);
		D3DXVec3TransformCoord(&m_vLookAt, &m_vLookAt, &R);
		D3DXVec3TransformCoord(&m_vUp, &m_vUp, &R);


		// Rotate camera axes about the world's y-axis.
		D3DXMatrixRotationY(&R, yAngle);
		D3DXVec3TransformCoord(&m_vRight, &m_vRight, &R);
		D3DXVec3TransformCoord(&m_vUp, &m_vUp, &R);
		D3DXVec3TransformCoord(&m_vLookAt, &m_vLookAt, &R);
	}

	m_vPosition = newPos;

	//if cam is locked onto an object/position

	
	
	//view can change every frame - update this
	CalculateViewMatrix();



	m_mViewProj = m_mViewMatrix * m_mProjMatrix;
}

void D3DCamera::CalculateViewMatrix()
{

	// Keep camera's axes orthogonal to each other and of unit length.
	D3DXVec3Normalize(&m_vLookAt, &m_vLookAt);

	D3DXVec3Cross(&m_vUp, &m_vLookAt, &m_vRight);
	D3DXVec3Normalize(&m_vUp, &m_vUp);

	D3DXVec3Cross(&m_vRight, &m_vUp, &m_vLookAt);
	D3DXVec3Normalize(&m_vRight, &m_vRight);

	// Fill in the view matrix entries.

	float x = -D3DXVec3Dot(&m_vPosition, &m_vRight);
	float y = -D3DXVec3Dot(&m_vPosition, &m_vUp);
	float z = -D3DXVec3Dot(&m_vPosition, &m_vLookAt);

	m_mViewMatrix(0,0) = m_vRight.x; 
	m_mViewMatrix(1,0) = m_vRight.y; 
	m_mViewMatrix(2,0) = m_vRight.z; 
	m_mViewMatrix(3,0) = x;   

	m_mViewMatrix(0,1) = m_vUp.x;
	m_mViewMatrix(1,1) = m_vUp.y;
	m_mViewMatrix(2,1) = m_vUp.z;
	m_mViewMatrix(3,1) = y;  

	m_mViewMatrix(0,2) = m_vLookAt.x; 
	m_mViewMatrix(1,2) = m_vLookAt.y; 
	m_mViewMatrix(2,2) = m_vLookAt.z; 
	m_mViewMatrix(3,2) = z;   

	m_mViewMatrix(0,3) = 0.0f;
	m_mViewMatrix(1,3) = 0.0f;
	m_mViewMatrix(2,3) = 0.0f;
	m_mViewMatrix(3,3) = 1.0f;
}

void D3DCamera::CalculateProjectionMatrix()
{
	D3DXMatrixPerspectiveFovLH(&m_mProjMatrix,
                               D3DXToRadian(45),    // the horizontal field of view
							   (FLOAT)D3DAPPI->GetD3Dpp().BackBufferWidth / (FLOAT)D3DAPPI->GetD3Dpp().BackBufferHeight, // aspect ratio
                               1.0f,    // the near view-plane
                               1000.0f);    // the far view-plane
}

void D3DCamera::OnLostDevice()
{

}

void D3DCamera::OnResetDevice()
{
	CalculateProjectionMatrix();
}



const D3DXMATRIX& D3DCamera::GetView() const
{
	return m_mViewMatrix;
}

const D3DXMATRIX& D3DCamera::GetProj() const
{
	return m_mProjMatrix;
}

const D3DXMATRIX& D3DCamera::GetViewProj() const
{
	return m_mViewProj;
}

const D3DXVECTOR3& D3DCamera::GetRight() const
{
	return m_vRight;
}

const D3DXVECTOR3& D3DCamera::GetUp() const
{
	return m_vUp;
}

const D3DXVECTOR3& D3DCamera::GetLookAt() const
{
	return m_vLookAt;
}

D3DXVECTOR3& D3DCamera::GetPosition()
{
	return m_vPosition;
}

void D3DCamera::SetLookAt(D3DXVECTOR3& pos, D3DXVECTOR3& target, D3DXVECTOR3& up)
{
	D3DXVECTOR3 L = target - pos;
	D3DXVec3Normalize(&L, &L);

	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &up, &L);
	D3DXVec3Normalize(&R, &R);

	D3DXVECTOR3 U;
	D3DXVec3Cross(&U, &L, &R);
	D3DXVec3Normalize(&U, &U);

	m_vPosition   = pos;
	m_vRight = R;
	m_vUp    = U;
	m_vLookAt  = L;

	CalculateViewMatrix();
	
	m_mViewProj = m_mViewMatrix * m_mProjMatrix;
}

void D3DCamera::SetLookAt(D3DXVECTOR3& target, D3DXVECTOR3& up)
{
	D3DXVECTOR3 L = target - m_vPosition;
	D3DXVec3Normalize(&L, &L);

	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &up, &L);
	D3DXVec3Normalize(&R, &R);

	D3DXVECTOR3 U;
	D3DXVec3Cross(&U, &L, &R);
	D3DXVec3Normalize(&U, &U);

	m_vRight = R;
	m_vUp    = U;
	m_vLookAt  = L;

	CalculateViewMatrix();
	
	m_mViewProj = m_mViewMatrix * m_mProjMatrix;
}

void D3DCamera::SetSpeed(float s)
{
	m_fSpeed = s;
}

void D3DCamera::LockCamera(D3DXVECTOR3& target)
{
	
	D3DXVECTOR3 L = target - m_vPosition;
	D3DXVec3Normalize(&L, &L);

	D3DXVECTOR3 R;
	D3DXVec3Cross(&R, &D3DXVECTOR3(0,1,0), &L);
	D3DXVec3Normalize(&R, &R);

	D3DXVECTOR3 U;
	D3DXVec3Cross(&U, &L, &R);
	D3DXVec3Normalize(&U, &U);

	m_vRight = R;
	m_vUp    = U;
	m_vLookAt  = L;

	CalculateViewMatrix();
	
	m_mViewProj = m_mViewMatrix * m_mProjMatrix;
}

void D3DCamera::SetPos(D3DXVECTOR3& pos)
{
	m_vPosition = pos;


}
void D3DCamera::SetPos(float x, float y, float z)
{
	m_vPosition = D3DXVECTOR3(x,y,z);
}