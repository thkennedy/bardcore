#include "ParticleManager.h"
#include "ParticleSystem.h"
#include "FireRing.h"
#include "Rain.h"
#include "Sprinkler.h"
#include "D3DVertex.h"
#include "D3DApp.h"
#include "CameraManager.h"
#include "GfxStats.h"


ParticleManager::ParticleManager(void)
{
}

// accessor method
ParticleManager* ParticleManager::Instance()
{
	static ParticleManager instance;
	
	return &instance;
}

ParticleManager::~ParticleManager(void)
{

}

////////////////////
// Utilities
////////////////////
void ParticleManager::Update(real dt)
{
	// update all particle systems
	for (i = m_vParticleSystems.size() -1 ; i >= 0; --i)
		m_vParticleSystems[i]->Update(dt);

}

void ParticleManager::Render()
{
	static int NumParticles;
	// batched draws
	// fire rings

	NumParticles = 0;
	for (i = m_vFireRing.size()-1; i>=0; --i)
	{
		m_vFireRing[i]->Draw();
		NumParticles += m_vFireRing[i]->iNumVerts;
	}

	// rain
	for (i = m_vRain.size() - 1; i>=0; --i)
	{
		m_vRain[i]->Draw();
		NumParticles += m_vRain[i]->iNumVerts;
	}

	// sprinkler
	for (i = m_vSprinker.size()-1;i>=0; --i)
	{
		m_vSprinker[i]->Draw();
		NumParticles += m_vSprinker[i]->iNumVerts;
	}

	// gun
	for (i = m_vGun.size() - 1; i>=0; --i)
	{
		m_vGun[i]->Draw();
		NumParticles += m_vGun[i]->iNumVerts;
	}

	// flamethrower
	for (i = m_vFlameThrower.size() - 1; i>=0; --i)
	{
		m_vFlameThrower[i]->Draw();
		NumParticles += m_vFlameThrower[i]->iNumVerts;
	}

	// update particle count
	GStats->setVerts(NumParticles);
}

void ParticleManager::InstancedPreRender()
{
	

}

void ParticleManager::InstancedRender()
{
	

}
void ParticleManager::Init()
{
	// init ID system
	m_iSysID = 0;

	// init all particle systems
	
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), 
		L"Graphics/Textures/Particles/torch.dds", &m_FlameThrowerTex);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), 
		L"Graphics/Textures/Particles/torch.dds", &m_FireRingTex);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), 
		L"Graphics/Textures/Particles/snow.dds", &m_SprinklerTex);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), 
		L"Graphics/Textures/Particles/bolt.dds", &m_ParticleGunTex);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), 
		L"Graphics/Textures/Particles/raindrop.dds", &m_RainTex);

}
void ParticleManager::Cleanup()
{

}

////////////////////
// Creation
////////////////////
int ParticleManager::CreateFireRing(const std::string& fxName,
            const std::string& techName,
            const std::string& texName,

            const D3DXVECTOR3& accel,
            const AABBG& box,
            int maxNumParticles,
            float timePerParticle)
{

	// fill temp ptr with new fire ring using creation params
	tempFireRing = new FireRing(fxName,techName,texName,accel,
		box, maxNumParticles, timePerParticle);

	// give it an id
	tempFireRing->ID = m_iSysID;

	// add to the pSys vec
	m_vParticleSystems.push_back(tempFireRing);
	
	// add to the pSys map
	m_mapParticleSystems.insert(pair<int,ParticleSystem*>(m_iSysID,tempFireRing) );

	// add to the fire ring vec
	m_vFireRing.push_back(tempFireRing);

	return m_iSysID++;	
}
int ParticleManager::CreateRain(const std::string& fxName,
            const std::string& techName,
            const std::string& texName,

            const D3DXVECTOR3& accel,
            const AABBG& box,
            int maxNumParticles,
            float timePerParticle)
{
	// fill temp ptr with new fire ring using creation params
	tempRain = new Rain(fxName,techName,texName,accel,
		box, maxNumParticles, timePerParticle);

	// give it an id
	tempRain->ID = m_iSysID;

	// add to the pSys vec
	m_vParticleSystems.push_back(tempRain);
	
	// add to the pSys map
	m_mapParticleSystems.insert(pair<int,ParticleSystem*>(m_iSysID,tempRain) );

	// add to the fire ring vec
	m_vRain.push_back(tempRain);

	return m_iSysID++;
}

int ParticleManager::CreateSprinkler(const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		const D3DXVECTOR3& accel, 
		const AABBG& box,
		int maxNumParticles,
		float timePerSecond)
{
	// fill temp ptr with new fire ring using creation params
	tempSprinkler = new Sprinkler(fxName,techName,texName,
		accel,box,maxNumParticles,timePerSecond);

	// give it an id
	tempSprinkler->ID = m_iSysID;

	// add to the pSys vec
	m_vParticleSystems.push_back(tempSprinkler);
	
	// add to the pSys map
	m_mapParticleSystems.insert(pair<int,ParticleSystem*>(m_iSysID,tempSprinkler) );

	// add to the fire ring vec
	m_vSprinker.push_back(tempSprinkler);

	return m_iSysID++;
}


int ParticleManager::CreateGun(const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		const D3DXVECTOR3& accel, 
		const AABBG& box,
		int maxNumParticles,
		float timePerSecond)
{
	// fill temp ptr with new fire ring using creation params
	tempGun = new ParticleGun(fxName,techName,texName,
		accel,box,maxNumParticles,timePerSecond);

	// give it an id
	tempGun->ID = m_iSysID;

	// add to the pSys vec
	m_vParticleSystems.push_back(tempGun);
	
	// add to the pSys map
	m_mapParticleSystems.insert(pair<int,ParticleSystem*>(m_iSysID,tempGun) );

	// add to the fire ring vec
	m_vGun.push_back(tempGun);

	return m_iSysID++;
}

int ParticleManager::CreateFlameThrower(const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		const D3DXVECTOR3& accel, 
		const AABBG& box,
		int maxNumParticles,
		float timePerSecond)
{
	// fill temp ptr with new fire ring using creation params
	tempFlame = new FlameThrower(fxName,techName,texName,
		accel,box,maxNumParticles,timePerSecond);

	// give it an id
	tempFlame->ID = m_iSysID;

	// add to the pSys vec
	m_vParticleSystems.push_back(tempFlame);
	
	// add to the pSys map
	m_mapParticleSystems.insert(pair<int,ParticleSystem*>(m_iSysID,tempFlame) );

	// add to the fire ring vec
	m_vFlameThrower.push_back(tempFlame);

	return m_iSysID++;
}
////////////////////
// Accessors
///////////////////
ParticleSystem* ParticleManager::GetParticleSystem(int id)
{
	return m_mapParticleSystems.find(id)->second;
}
Vector3 ParticleManager::GetPosition(int id)
{
	return Vector3(0,0,0);
}


//////////////////
// Mutators
//////////////////
void ParticleManager::SetPosition(int id, real X, real Y, real Z)
{

}
void ParticleManager::SetPosition(int id, Vector3 vPos)
{

}

//////////////////
// D3D Methods
/////////////////
void ParticleManager::OnLostDevice()
{
	for (i = m_vParticleSystems.size() -1 ; i >= 0; --i)
		m_vParticleSystems[i]->OnLostDevice();
}
void ParticleManager::OnResetDevice()
{
	for (i = m_vParticleSystems.size() -1 ; i >= 0; --i)
		m_vParticleSystems[i]->OnResetDevice();
}