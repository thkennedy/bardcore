#pragma once
#include "D3DShape.h"
#include "../MultiThreading/CriticalSection.h"

class D3DInstancedQuad : public D3DShape
{
private:

	virtual void CalculateInternals();

	CriticalSection m_csShape;

public:
	D3DInstancedQuad(void){}
	D3DInstancedQuad(int id, float x, float y, float z);
	~D3DInstancedQuad(void);


	///////////////
	// UTILITIES
	//////////////
	void BuildFX();
	virtual void Update(float dt);
	virtual void Render();
	virtual void OnResetDevice();
	virtual void OnLostDevice();

	void SetHalfExtents(float x, float y, float z){m_vScale = D3DXVECTOR3(x,y,z);}
};

