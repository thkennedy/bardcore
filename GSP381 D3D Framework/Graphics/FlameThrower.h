#pragma once

#include "ParticleSystem.h"

class FlameThrower : public ParticleSystem
{
public:
	FlameThrower(void){}
	FlameThrower(const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		const D3DXVECTOR3& accel, 
		const AABBG& box,
		int maxNumParticles,
		float timePerSecond);
	~FlameThrower(void);

	void InitParticle(Particle& out);
};

