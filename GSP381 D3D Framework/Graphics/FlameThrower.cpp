#include "FlameThrower.h"
#include "../PRNG/RandomNumber.h"
#include "CameraManager.h"


FlameThrower::FlameThrower(const std::string& fxName, 
	const std::string& techName, 
	const std::string& texName, 
	const D3DXVECTOR3& accel, 
	const AABBG& box,
	int maxNumParticles,
	float timePerSecond)
	: ParticleSystem(fxName, techName, texName, accel, box, maxNumParticles,
	timePerSecond)
{
}

void FlameThrower::InitParticle(Particle& out)
{
	// Generate about the origin.
	out.m_vInitialPos = CamMgr->m_pCurrentCamView->GetPosition();
	out.m_vInitialPos -= CamMgr->m_pCurrentCamView->GetUp() * 2;
	tempVec1.x = RandNum->Uniform(-2.0f,2.0f);
	tempVec1.y = RandNum->Uniform(-2.0f,2.0f);
	tempVec1.z = RandNum->Uniform(-2.0f,2.0f);
 
	float speed = 50.0f;
	out.m_vInitialVelocity = speed*CamMgr->m_pCurrentCamView->GetLookAt();
	out.m_vInitialVelocity += tempVec1;
	

	out.m_fInitialTime		= m_rTime;
	out.m_fLifeTime			= .5f;//RandNum->Uniform(0.5f, .0f);
	out.m_cInitialColor		= D3DWhite;
	out.m_fInitialSize		= RandNum->Uniform(8.0f, 12.0f);
	out.m_fMass				= RandNum->Uniform(0.8f, 1.2f);
		
}


FlameThrower::~FlameThrower(void)
{
}
