#pragma once

#include "ParticleSystem.h"

class Sprinkler : public ParticleSystem
{
public:
	Sprinkler(void){}
	Sprinkler(const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		const D3DXVECTOR3& accel, 
		const AABBG& box,
		int maxNumParticles,
		float timePerSecond);
	~Sprinkler(void);

	void InitParticle(Particle& out);
};

