#pragma once
#include <vector>
#include <string>
#include "../Physics/vector3.h"
#include "../Physics/functions.h"
#include "D3DVertex.h"


// forward declarations
class D3DCamera;

// helper structs

//===============================================================
// Bounding Volumes

struct AABBG 
{
	// Initialize to an infinitely small bounding box.
	AABBG()
		: minPt(INFINITY, INFINITY, INFINITY),
		  maxPt(-INFINITY, -INFINITY, -INFINITY){}

    D3DXVECTOR3 center()
	{
		return 0.5f*(minPt+maxPt);
	}

	void xform (D3DXMATRIX mInput, D3DXMATRIX &mOutput)
	{
		mOutput *= mInput;
	}

	D3DXVECTOR3 minPt;
	D3DXVECTOR3 maxPt;
};

struct BoundingSphere 
{
	BoundingSphere()
		: pos(0.0f, 0.0f, 0.0f), radius(0.0f){}

	D3DXVECTOR3 pos;
	float radius;
};

class ParticleSystemQ
{
protected:
	HRESULT hr;

	// current camera view
	D3DCamera *m_pCamView;

	// .fx pointer and shader handles
    ID3DXEffect* m_FX;
    D3DXHANDLE m_hTech;
    D3DXHANDLE m_hWVP;
    D3DXHANDLE m_hEyePosL;
    D3DXHANDLE m_hTex;
    D3DXHANDLE m_hTime;
    D3DXHANDLE m_hAccel;
    D3DXHANDLE m_hViewportHeight;

    IDirect3DTexture9* m_Tex;		// particle texture
    IDirect3DVertexBuffer9* m_VB;	// vertex buffer
    D3DXMATRIX m_mWorld;			// world matrix
    D3DXMATRIX m_mInvWorld;			// inverse world matrix
    real m_rTime;					// how long particle system has been running
    Vector3 m_vAccel;				// constant acceleration factor
    AABBG m_Box;					// AABBG used for culling - determines what particles are rendered
    int m_iMaxNumParticles;			// max particles system can contain - passed into vbuffer
    real m_rTimePerParticle;		// how often a particle should be emitted

    std::vector<Particle> m_vParticles;			// all particles in the system, both alive and dead
    std::vector<Particle*> m_vAliveParticles;	// all alive particles - ones being rendered
    std::vector<Particle*> m_vDeadParticles;	// all dead particles - if we need more, just use these

public:
	ParticleSystemQ(void){}
	ParticleSystemQ(
        const std::string& fxName,
        const std::string& sTechName,
        const std::string& sTexName,
        const Vector3& vAccel,
        const AABBG& box,
        int iMaxNumParticles,
        real rTimePerParticle);

    virtual ~ParticleSystemQ();
	
	////////////////////
	// Utilities
	////////////////////
	// build FX file
	void BuildFX(const std::string& fxName,const std::string& sTechName);
	// adds a particle to the system, inits dead particle
	void AddParticle();
	virtual void InitParticle(Particle& out) = 0;
	virtual void Update(real dt);
    virtual void Draw();

	////////////////////
    // Accessors
	///////////////////
    real GetTime();
	const AABBG& GetAABBG()const;

	//////////////////
	// Mutators
	//////////////////
	void SetCamera(D3DCamera* cam){m_pCamView = cam;}
    void SetTime(real t);
    void SetWorldMtx(const D3DXMATRIX& world);
    
	
	//////////////////
	// D3D Methods
	/////////////////
    virtual void OnLostDevice();
    virtual void OnResetDevice();

    
    
};

