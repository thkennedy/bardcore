/*
	This is the instanced particle system manager. The main reason its in a manager
	is because the nature of instancing is a bit tricky it made more sense
	to put it in its own class versus trying to integrate into an already large
	non-instanced draw manager.

*/

#pragma once

#include <vector>
#include <string>
#include <map>
#include "../Physics/vector3.h"
#include "../Physics/functions.h"
#include "D3DVertex.h"
#include "FireRing.h"
#include "Rain.h"
#include "Sprinkler.h"
#include "ParticleGun.h"
#include "FlameThrower.h"



// forward declarations
class ParticleSystem;


// accessor
#define PMan ParticleManager::Instance()

class ParticleManager
{
private:
	// private const, only one instance of pman
	ParticleManager(void);

	// vec of all pSys - used for quick looping (render, update)
	std::vector<ParticleSystem*> m_vParticleSystems;

	// map of all pSys - used for indexed search
	std::map<int, ParticleSystem*> m_mapParticleSystems;

	// vec of all fire ring psys - needed for batching
	std::vector<FireRing*> m_vFireRing;

	// vec of all rain psys - needed for batching
	std::vector<Rain*>	m_vRain;

	std::vector<Sprinkler*> m_vSprinker;

	std::vector<ParticleGun*> m_vGun;

	std::vector<FlameThrower*> m_vFlameThrower;

	// particle system ID's - used for accessing
	int m_iSysID;

	///////////////////////////////
	// VERTEX AND INDEX BUFFERS
	///////////////////////////////
	//IDirect3DVertexBuffer9* m_VB;
	IDirect3DIndexBuffer9*	m_IB;
	IDirect3DVertexBuffer9*	m_InstanceDataVB;

	//////////////////////////
	// TEXTURES
	//////////////////////////
	IDirect3DTexture9* m_FlameThrowerTex;
	IDirect3DTexture9* m_FireRingTex;
	IDirect3DTexture9* m_SprinklerTex;
	IDirect3DTexture9* m_ParticleGunTex;
	IDirect3DTexture9* m_RainTex;
	
	///////////////////////
	// SCRATCH PAD VARS
	//////////////////////
	// for loops
	int i,j,k,l;
	// particle sys creation
	FireRing* tempFireRing;
	Rain* tempRain;
	Sprinkler* tempSprinkler;
	ParticleGun* tempGun;
	FlameThrower* tempFlame;



	/////////////////////////////////
	// FX HANDLES AND VARS
	/////////////////////////////////
	HRESULT hr;

	// .fx pointer and shader handles
    ID3DXEffect* m_FX;
    D3DXHANDLE m_hTech;
    D3DXHANDLE m_hWVP;
    D3DXHANDLE m_hEyePosL;
    D3DXHANDLE m_hTex;
    D3DXHANDLE m_hTime;
    D3DXHANDLE m_hAccel;
    D3DXHANDLE m_hViewportHeight;

    IDirect3DTexture9* m_Tex;		// particle texture
//    IDirect3DVertexBuffer9* m_VB;	// vertex buffer

public:

	// accessor method
	static ParticleManager* Instance();
	~ParticleManager(void);

	////////////////////
	// Utilities
	////////////////////
	void Update(real dt);
	void Evolve();
	void Render();
	void InstancedPreRender();
	void InstancedRender();
	void Init();
	void Cleanup();

	////////////////////
	// Creation
	////////////////////
	int CreateFireRing(const std::string& fxName,
            const std::string& techName,
            const std::string& texName,

            const D3DXVECTOR3& accel,
            const AABBG& box,
            int maxNumParticles,
            float timePerParticle);
	int CreateRain(const std::string& fxName,
            const std::string& techName,
            const std::string& texName,
			const D3DXVECTOR3& accel,
            const AABBG& box,
            int maxNumParticles,
            float timePerParticle);

	int CreateSprinkler(const std::string& fxName, 
			const std::string& techName, 
			const std::string& texName, 
			const D3DXVECTOR3& accel, 
			const AABBG& box,
			int maxNumParticles,
			float timePerSecond);

	int CreateGun(const std::string& fxName, 
			const std::string& techName, 
			const std::string& texName, 
			const D3DXVECTOR3& accel, 
			const AABBG& box,
			int maxNumParticles,
			float timePerSecond);

	int CreateFlameThrower(const std::string& fxName, 
			const std::string& techName, 
			const std::string& texName, 
			const D3DXVECTOR3& accel, 
			const AABBG& box,
			int maxNumParticles,
			float timePerSecond);

	////////////////////
    // Accessors
	///////////////////
	ParticleSystem* GetParticleSystem(int id);
	Vector3 GetPosition(int id);


	//////////////////
	// Mutators
	//////////////////
	void SetPosition(int id, real X, real Y, real Z);
	void SetPosition(int id, Vector3 vPos);

	//////////////////
	// D3D Methods
	/////////////////
	void OnLostDevice();
    void OnResetDevice();
};

