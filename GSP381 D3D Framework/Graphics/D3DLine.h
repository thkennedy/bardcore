#pragma once
#include "D3DShape.h"



class D3DLine
	:public D3DShape
{
private:
	virtual void CalculateInternals();

	//line drawer
	LPD3DXLINE m_pMeshLine;

	D3DXVECTOR3 m_vEndPosition;

	

public:
	D3DLine(void);
	D3DLine(LPD3DXLINE pLine, D3DXVECTOR3 vStartPosition, D3DXVECTOR3 vEndPosition, D3DCOLOR cColor, int id);
	~D3DLine(void);

	///////////////
	// UTILITIES
	//////////////
	void BuildFX();
	virtual void Update(float dt);
	virtual void Render();
	virtual void OnResetDevice();
	virtual void OnLostDevice();

	////////////////
	// ACCESSORS
	///////////////
	D3DXVECTOR3 GetEndPosition() {return m_vEndPosition;}

	////////////////
	// MUTATORS
	///////////////
	void SetEndPosition(D3DXVECTOR3 vEndPos) {m_vEndPosition = vEndPos;}

};

