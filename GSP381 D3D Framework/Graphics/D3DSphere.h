#pragma once
#include "D3DShape.h"

class D3DSphere :
	public D3DShape
{
private:
	virtual void CalculateInternals();

	//mesh ptr
	LPD3DXMESH m_pMeshSphere;

	D3DXVECTOR3 mLightVecW;		// light in world coords
	D3DXCOLOR   mAmbientMtrl;	// ambient mat color
	D3DXCOLOR   mAmbientLight;	// ambient light color
	D3DXCOLOR   mDiffuseMtrl;	// diffse mat color
	D3DXCOLOR   mDiffuseLight;	// diffuse light color
	D3DXCOLOR   mSpecularMtrl;	// specular mat color
	D3DXCOLOR   mSpecularLight;	// specular light color
	float       mSpecularPower;	// specular power

	

public:
	D3DSphere(void);
	D3DSphere(LPD3DXMESH pSphere, float radius, IDirect3DTexture9* pTexture, int id);
	~D3DSphere(void);

	
	///////////////
	// UTILITIES
	//////////////
	void BuildFX();
	virtual void Update(float dt);
	virtual void Render();
	virtual void OnResetDevice();
	virtual void OnLostDevice();



	///////////////
	// MUTATORS
	//////////////
	void SetRadius(float radius){m_vScale *= radius;}


};

