#include "D3DSpriteManager.h"
#include "../D3DSprite.h"
#include "../D3DApp.h"


D3DSpriteManager::D3DSpriteManager(void)
{
	m_NextID = 1;
	m_vSprites.clear();

}

D3DSpriteManager* D3DSpriteManager::Instance()
{
	static D3DSpriteManager Instance;

	return &Instance;
}

void D3DSpriteManager::RegisterSprite(D3DSprite* newSprite) 
{
	//only add sprite if sprite hasn't already been registered
	if (newSprite->GetID() == -1)
	{
		//assign sprite an id
		newSprite->SetID(GetNextSpriteID());
		//put sprite on the list to be rendered
		m_vSprites.push_back(newSprite);
	}
	
}

void D3DSpriteManager::RemoveSprite(D3DSprite* deleteSprite)
{
	std::vector<D3DSprite*>::iterator it = m_vSprites.begin();

	unsigned int i;

	for (i=0, it; i < m_vSprites.size(); i++)
	{
		if ((*it) == m_vSprites[i])
		{
			m_vSprites.erase(it);
			return;
		}

		++it;
	}
}

void D3DSpriteManager::ClearRegistry() 
{
	//release all sprites

	std::vector<D3DSprite*>::iterator it = m_vSprites.begin();

	for (it; it != m_vSprites.end(); it++)
	{
		(*it)->ReleaseSprite();
	}

	//clear registry
	m_vSprites.clear();

	m_NextID = 0;
}

void D3DSpriteManager::Update(float dt)
{
	// rebuild rendered sprites list each frame
	// clear list
	m_vRenderedSprites.clear();
	
	for (auto it = m_vSprites.begin(); it != m_vSprites.end(); it++)
	{
		(*it)->Update(dt);

		// if sprite is on-screen, add it to render vec
		if ((*it)->GetVisible())
			m_vRenderedSprites.push_back((*it));
	}
}

void D3DSpriteManager::Render()
{
	for (int i = m_vRenderedSprites.size()-1; i >= 0; --i)
	{
		m_vRenderedSprites[i]->Render();
		
	}
}

void D3DSpriteManager::OnLostDevice()
{
	D3DAPPI->GetD3DSpriteObject()->OnLostDevice();
}
void D3DSpriteManager::OnResetDevice()
{
	D3DAPPI->GetD3DSpriteObject()->OnResetDevice();

	for (i = m_vSprites.size()-1; i >= 0; --i)
		m_vSprites[i]->OnReset();
	
	
}

///////////////////////
// CREATION
///////////////////////
// creates a sprite. default pos is 0,0 and default texture is set
int D3DSpriteManager::CreateSprite(bool bAlwaysRender, 
								   LPCTSTR name, 
								   LPCTSTR texture)
{
	// create new sprite
	tempSprite = new D3DSprite();

	// set its id
	tempSprite->SetID(GetNextSpriteID());

	// set the always render bool
	tempSprite->m_bAlwaysRender = bAlwaysRender;

	// set its texture
	tempSprite->SetTexture(texture);

	// set its name
	tempSprite->Name = name;

	// add it to the sprite vec
	m_vSprites.push_back(tempSprite);	

	// return id for immediate use
	return tempSprite->GetID();
}

int D3DSpriteManager::CreateSprite(bool bAlwaysRender, 
				 LPCTSTR name , 
				 IDirect3DTexture9 *pTexture, 
				 LPCTSTR texturePath)
{
	// create new sprite
	tempSprite = new D3DSprite();

	// set its id
	tempSprite->SetID(GetNextSpriteID());

	// set the always render bool
	tempSprite->m_bAlwaysRender = bAlwaysRender;

	// set its texture
	tempSprite->SetTexture(pTexture, texturePath);

	// set its name
	tempSprite->Name = name;

	// add it to the sprite vec
	m_vSprites.push_back(tempSprite);	

	// return id for immediate use
	return tempSprite->GetID();
}

///////////////////////
// DISPLAY
///////////////////////
// Displays the sprite for a limited amount of time
void D3DSpriteManager::TimedDisplay(LPCTSTR name, float time, float x, float y)
{
	tempSprite = GetSprite(name);
	// move the sprite to the indicated position
	tempSprite->SetPosition(x,y);

	// set the display time for the sprite
	tempSprite->fDisplayTime = time;

}

///////////////////////
// ACCESSORS
///////////////////////
D3DSprite* D3DSpriteManager::GetSprite(int id)
{
	// loop search
	for (int i = m_vSprites.size()-1; i >= 0; --i)
	{
		if(m_vSprites[i]->GetID() == id)
			return m_vSprites[i];
	}

	// unsuccessful, return null ptr
	return NULL;
}

// get sprite by name
D3DSprite* D3DSpriteManager::GetSprite(LPCTSTR name)
{
	// loop search
	for (int i = m_vSprites.size()-1; i >= 0; --i)
	{
		if(m_vSprites[i]->Name == name)
			return m_vSprites[i];
	}

	// unsuccessful, return null ptr
	return NULL;
}

///////////////////////
// MUTATORS
///////////////////////