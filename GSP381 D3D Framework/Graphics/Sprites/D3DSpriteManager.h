#pragma once
#include <vector>
#include <string>
#include <d3d9.h>
#include <d3dx9.h>

#define SPRMGR D3DSpriteManager::Instance()

class D3DSprite;


class D3DSpriteManager
{
private:
	D3DSpriteManager();

	//sprite registry - all sprites
	std::vector<D3DSprite*> m_vSprites;

	// "alive" sprites, currently being rendered
	std::vector<D3DSprite*> m_vRenderedSprites;


	//for managing sprites
	int m_NextID;

	int GetNextSpriteID(){return m_NextID++;}


	/////////////////
	// SCRATCH PAD
	////////////////
	D3DSprite* tempSprite;
	int i,j,k,l;


public:
	~D3DSpriteManager() {};
	

	static D3DSpriteManager* Instance();
	
	//utilities
	void RegisterSprite(D3DSprite* newSprite);
	void RemoveSprite(D3DSprite* deleteSprite);
	void ClearRegistry();
	void Update(float dt);
	void Render();
	void OnLostDevice();
	void OnResetDevice();


	///////////////////////
	// CREATION
	///////////////////////
	// creates a sprite. default pos is 0,0 and default texture is set
	int CreateSprite(bool bAlwaysRender = true, LPCTSTR name = L"NoName", LPCTSTR texture = L"Graphics/Sprites/notexture.png");
	int CreateSprite(bool bAlwaysRender = true, LPCTSTR name = L"NoName", IDirect3DTexture9 *pTexture = NULL, LPCTSTR texturePath = L"Graphics/Sprites/notexture.png");


	///////////////////////
	// DISPLAY
	///////////////////////
	// Displays the sprite for a limited amount of time
	void TimedDisplay(LPCTSTR name, float time, float x, float y);

	///////////////////////
	// ACCESSORS
	///////////////////////
	// get sprite by id
	D3DSprite* GetSprite(int id);
	// get sprite by name
	D3DSprite* GetSprite(LPCTSTR name);

	///////////////////////
	// MUTATORS
	///////////////////////


	
};


