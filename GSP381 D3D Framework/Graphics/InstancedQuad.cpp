#include "InstancedQuad.h"
#include "D3DApp.h"
#include "D3DVertex.h"
#include "D3DCamera.h"


D3DInstancedQuad::D3DInstancedQuad(int id, float x, float y, float z)
	:D3DShape(id)
{

	/////////////////////////
	// INTERNAL VARS
	/////////////////////////
	
	m_vPosition = m_vCenter = m_vOrientation = D3DXVECTOR3(0.0f,0.0f,0.0f);

	m_vScale = D3DXVECTOR3(x,y,z);

		
	
}


D3DInstancedQuad::~D3DInstancedQuad(void)
{
}

void D3DInstancedQuad::CalculateInternals()
{
	

	//////////////
	//	ROTATION
	//////////////

	//quaternion calcs


	D3DXMATRIX rotationMatrix;

	D3DXMatrixRotationQuaternion(&rotationMatrix, &m_qOrientation);



	//////////////
	//	TRANSLATE
	//////////////

	D3DXMATRIX matTranslate;

	D3DXMatrixTranslation(&matTranslate,m_vPosition.x,m_vPosition.y,m_vPosition.z);

	//////////////
	//	SCALE
	//////////////

	D3DXMATRIX matScale;

	D3DXMatrixScaling(&matScale,m_vScale.x,m_vScale.y,m_vScale.z);

	///////////////
	//	TRANSFORM
	///////////////
	m_mTransform = matScale * rotationMatrix * matTranslate ;


}

void D3DInstancedQuad::BuildFX()
{
	HRESULT hr;
	
	// Create the FX from a .fx file.
	ID3DXBuffer* errors = 0;
	hr = D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), "Graphics/Techniques/quad.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &m_pEffect, &errors);
	if( errors ) 
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);	

	// Obtain handles.
	m_hTech = m_pEffect->GetTechniqueByName("QuadTech");
	m_hTexture = m_pEffect->GetParameterByName(0,"gTex");
	m_hVP	= m_pEffect->GetParameterByName(0, "gViewProjectionMatrix");
	m_hWorld = m_pEffect->GetParameterByName(0, "gWorld");
	m_hView	= m_pEffect->GetParameterByName(0,"gView");
	m_hProj = m_pEffect->GetParameterByName(0,"gProj");
	m_hWVP  = m_pEffect->GetParameterByName(0, "gWVP");


	m_pEffect->SetTexture(m_hTexture, m_Texture);

	


}

void D3DInstancedQuad::Update(float dt)
{
	
				
	CalculateInternals();

}

void D3DInstancedQuad::Render()
{
//	HRESULT hr;
//	
//	// Setup the rendering FX
//	hr = m_pEffect->SetTechnique(m_hTech);
//	hr = m_pEffect->SetMatrix(m_hWorld, &m_mTransform);
//	hr = m_pEffect->SetMatrix(m_hView, &m_pCamView->GetView());
//	hr = m_pEffect->SetMatrix(m_hProj, &m_pCamView->GetProj());
//
//	hr = m_pEffect->SetMatrix(m_hVP, &m_pCamView->GetViewProj());
//	hr = m_pEffect->SetMatrix(m_hWVP, &(m_mTransform * m_pCamView->GetViewProj() ) );
//
//	hr = D3DAPPI->GetD3DDevice()->SetStreamSource(0,m_VertexBuffer,0,sizeof(PNT));
//	hr = D3DAPPI->GetD3DDevice()->SetIndices(m_IndexBuffer);
//	hr = D3DAPPI->GetD3DDevice()->SetVertexDeclaration(PNT::Decl);
//
//	
//	//D3DAPPI->GetD3DDevice()->SetTexture(0,m_Texture);
//
//	// Begin passes.
//	UINT numPasses = 0;
//	m_pEffect->Begin(&numPasses, 0);
//	for(UINT i = 0; i < numPasses; ++i)
//	{
//		m_pEffect->BeginPass(i);
//		hr = D3DAPPI->GetD3DDevice()->DrawIndexedPrimitive(
//			D3DPT_TRIANGLELIST, 
//			0, 
//			0, 
//			4, 
//			0, 
//			2);
//		m_pEffect->EndPass();
//	}
//	m_pEffect->End();
	
	
}

void D3DInstancedQuad::OnResetDevice()
{
	//D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/pyramid.jpg", &m_Texture);
	//m_Texture->Release();
	//m_pEffect->OnResetDevice();
}
void D3DInstancedQuad::OnLostDevice()
{
	//m_pEffect->OnLostDevice();
}

