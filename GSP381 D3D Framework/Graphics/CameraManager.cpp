#include "CameraManager.h"
#include "D3DCamera.h"


CameraManager::CameraManager(void)
{
}


CameraManager::~CameraManager(void)
{
}

CameraManager* CameraManager::Instance()
{
	static CameraManager instance;

	return &instance;
}

void CameraManager::Update(float dt)
{
	for (int i = m_vCameras.size()-1; i >= 0; --i)
		m_vCameras[i]->Update(dt);

}

void CameraManager::OnLostDevice()
{
	for (int i = m_vCameras.size()-1; i >= 0; --i)
		m_vCameras[i]->OnLostDevice();
}

void CameraManager::OnResetDevice()
{
	for (int i = m_vCameras.size()-1; i >= 0; --i)
		m_vCameras[i]->OnResetDevice();
}

void CameraManager::CreateCamera()
{
	// create a new camera
	D3DCamera *temp = new D3DCamera();

	// switch to this camera view
	m_pCurrentCamView = temp;

	// if first cam, set previous cam view to this as well
	if (m_vCameras.size() < 1)
		m_pPreviousCamView = temp;

	// add to vec of cameras
	m_vCameras.push_back(temp);

}