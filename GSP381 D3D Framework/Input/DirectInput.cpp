//=============================================================================
// DirectInput.cpp by Frank Luna (C) 2005 All Rights Reserved.
//=============================================================================

#include "../Graphics/d3dUtility.h"
#include "DirectInput.h"
#include "../Graphics/D3DApp.h"

DirectInput* gDInput = 0;

DirectInput::DirectInput(DWORD keyboardCoopFlags, DWORD mouseCoopFlags)
{
	HRESULT hr;

	ZeroMemory(mKeyboardState, sizeof(mKeyboardState));
	ZeroMemory(&mMouseState, sizeof(mMouseState));

	hr = DirectInput8Create(D3DAPPI->GethInst(), DIRECTINPUT_VERSION, 
		IID_IDirectInput8, (void**)&mDInput, 0);

	hr = mDInput->CreateDevice(GUID_SysKeyboard, &mKeyboard, 0);
	hr = mKeyboard->SetDataFormat(&c_dfDIKeyboard);
	hr = mKeyboard->SetCooperativeLevel(D3DAPPI->GetHWND(), keyboardCoopFlags);
	hr = mKeyboard->Acquire();

	hr = mDInput->CreateDevice(GUID_SysMouse, &mMouse, 0);
	hr = mMouse->SetDataFormat(&c_dfDIMouse2);
	hr = mMouse->SetCooperativeLevel(D3DAPPI->GetHWND(), mouseCoopFlags);
	hr = mMouse->Acquire();
}

DirectInput::~DirectInput()
{
	ReleaseCOM(mDInput);
	mKeyboard->Unacquire();
	mMouse->Unacquire();
	ReleaseCOM(mKeyboard);
	ReleaseCOM(mMouse);
}

void DirectInput::poll()
{
	// Poll keyboard.
	HRESULT hr = mKeyboard->GetDeviceState(sizeof(mKeyboardState), (void**)&mKeyboardState); 
	if( FAILED(hr) )
	{
		// Keyboard lost, zero out keyboard data structure.
		ZeroMemory(mKeyboardState, sizeof(mKeyboardState));

		 // Try to acquire for next time we poll.
		hr = mKeyboard->Acquire();
	}

	// Poll mouse.
	hr = mMouse->GetDeviceState(sizeof(DIMOUSESTATE2), (void**)&mMouseState); 
	if( FAILED(hr) )
	{
		// Mouse lost, zero out mouse data structure.
		ZeroMemory(&mMouseState, sizeof(mMouseState));

		// Try to acquire for next time we poll.
		hr = mMouse->Acquire(); 
	}

	//get absolute mouse pos
	GetCursorPos(&m_pMousePos); //screen coords
	ScreenToClient(D3DAPPI->GetHWND(),&m_pMousePos);
}

bool DirectInput::keyDown(char key)
{
	return (mKeyboardState[key] & 0x80) != 0;
}

bool DirectInput::mouseButtonDown(int button)
{
	return (mMouseState.rgbButtons[button] & 0x80) != 0;
}

float DirectInput::mouseDX()
{
	return (float)mMouseState.lX;
}

float DirectInput::mouseDY()
{
	return (float)mMouseState.lY;
}

float DirectInput::mouseDZ()
{
	return (float)mMouseState.lZ;
}

POINT DirectInput::GetMousePos()
{
	return m_pMousePos;
}