#pragma once


#include <Windows.h>
#include "../Timer.h"

#define InputDevices RInput::Instance()

#define KEYPRESSDELAY .5

class RInput
{
private:

	//////////////////////////
	//IDirectInput8*       mDInput;

	//IDirectInputDevice8* mKeyboard;
	//char                 mKeyboardState[256]; 

	//IDirectInputDevice8* mMouse;
	//DIMOUSESTATE2        mMouseState;
	////////////////////////////
	RInput(void);
	
	USHORT m_keyCode;

	char mKeyboardState[256]; 

	bool m_bKeyPressed;

	float m_fDCounter;

	//////////////////////////////////////////////////////////////////////////
	// RawInput Variables
	//////////////////////////////////////////////////////////////////////////
	POINT m_pMousePos;
	bool m_leftMouseDown, m_leftMouseUp, m_rightMouseDown, m_rightMouseUp;


public:
	
	static RInput* Instance();
	~RInput(void);


	void InitRawInput();
	bool KeyPressed(USHORT key);
	void Update(float dt);

	bool keyDown(char key);
	bool mouseButtonDown(int button);
	float mouseDX();
	float mouseDY();
	float mouseDZ();
	
	///////////////////////
	// Accessors
	//////////////////////
	USHORT GetKeyCode(){return m_keyCode;}
	POINT GetMousePos();
	bool GetLeftMouseDown() {return m_leftMouseDown;}
	bool GetLeftMouseUp() {return m_leftMouseUp;}
	bool GetRightMouseDown() {return m_rightMouseDown;}
	bool GetRightMouseUp() {return m_rightMouseUp;}

	///////////////////////
	// Mutators
	//////////////////////

	void SetLeftMouseDown(bool mouseDown);
	void SetLeftMouseUp(bool mouseUp);
	void SetRightMouseDown(bool mouseDown);
	void SetRightMouseUp(bool mouseUp);


	void SetKeyCode(USHORT keyPress)
	{
		if (keyPress != m_keyCode || m_fDCounter > KEYPRESSDELAY)
		{
			//set keycode
			m_keyCode = keyPress;
			//clear counter
			m_fDCounter = 0;
		}
		
	}
	void SetKeyUp(bool keyUp)
	{
		m_bKeyPressed = keyUp;
		if (keyUp)
			m_keyCode = NULL;
	}



};

