#include "MTClient.h"

#include "MTClient.h"
#include "../MultiThreading/CriticalSection.h"
#include "PacketInfo.h"
//#include "../Graphics/ShapeManager.h"

// takes a passed in new socket and handles data

MTClient* MTClient::Instance()
{
	static MTClient instance;

	return &instance;

}


MTClient::MTClient(void)
{
}


MTClient::~MTClient(void)
{
}

int MTClient::Init()
{

	
	
	// init critical sections
	
	
	// initialize Winsock
	WSAStartup(MAKEWORD(2,2), &m_sWsaDat);
	if(WSAStartup(MAKEWORD(2,2),&m_sWsaDat)!=0)
	{
		//std::cout<<"WSA Initialization failed!\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}

	/* socket that is bound to specific transport service provider
	af - IP protocol  AF_INET is IPv4 AF_INET6 is IPv6
	type - type specification for socket - how data is expected to be sent- use SOCK_STREAM
	*/
	m_Socket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(m_Socket==INVALID_SOCKET)
	{
		//std::cout<<"Socket creation failed.\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}


	
	// init completed successfully
	return 1; 

	
}



// runs server
int MTClient::Run()
{
	// init the server, if it fails, shutdown
	if (Init() != 1)
	{
		//std::cout<<"Init failed, shutting down server.\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}


	// Resolve IP address for hostname
	struct hostent *host;
	if((host=gethostbyname("localhost"))==NULL)
	//if((host=gethostbyname("192.168.1.76"))==NULL)
	{
		//std::cout<<"Failed to resolve hostname.\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}

	// Setup our socket address structure
	SOCKADDR_IN SockAddr;
	SockAddr.sin_port=htons(8888);
	SockAddr.sin_family=AF_INET;
	SockAddr.sin_addr.s_addr=*((unsigned long*)host->h_addr);

	// Attempt to connect to server
	if(connect(m_Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr))!=0)
	{
		//std::cout<<"Failed to establish connection with server\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}

	//std::cout << "Connection to Server Successful!\n";

	
	// create worker thread for incoming messages
	CreateThread(NULL,
		0,
		ThreadIncomingMessages,
		(LPVOID)m_Socket,
		0,
		NULL);

	/*CreateThread(NULL,
		0,
		ThreadSendMessages,
		(LPVOID)m_Socket,
		0,
		NULL);*/

	

	
	//Sleep(99999);

	
	// run executed successfully
	return 1;	

}

// shuts down server
int MTClient::Shutdown()
{
	// Shutdown our socket's send ability
	shutdown(m_Socket,SD_SEND);


	// Close our socket entirely
	closesocket(m_Socket);

	return 1;
}

DWORD WINAPI MTClient::ThreadIncomingMessages(LPVOID socket)
{

	char tempBuffer[1000];

	SOCKET _socket = (SOCKET)socket;
	
	// run forever
	while(1)
	{
		// receive data
		int inDataLength = recv(_socket,tempBuffer,sizeof(tempBuffer),0);

		if (inDataLength > 0)
			// store message for retrieval
			Instance()->AddRecvMessage(tempBuffer);

			
	}

	return true;
}

DWORD WINAPI MTClient::ThreadSendMessages(LPVOID socket)
{

	SOCKET _socket = (SOCKET)socket;

	char cInput[1000] = "Test\n";

	
	// run forever
	while(1)
	{
		//send(_socket, cInput, sizeof(cInput), 0);

		Sleep(2000);
	}
}

// packet send command
void MTClient::SendPacket(int iType)
{
	char *sendBuffer; 
	int size;

	switch (iType)
	{
		case 0: // key press
		{
			sendBuffer = new char[8];
			sendBuffer[0] = 0;
			size = sizeof(char[8]);
			break;
		}			
		case 1: // sync
		{
			sendBuffer = new char[32];
			sendBuffer[0] = 1;
			size = sizeof(char[32]);
			break;
		}
		case 2: // boost
		{
			sendBuffer = new char[8];
			sendBuffer[0] = 2;
			size = sizeof(char[8]);
			break;
		}
		case 3: // roster update
		{
			sendBuffer = new char[8];
			sendBuffer[0] = 3;
			size = sizeof(char[8]);
			break;
		}
		case 4: // clientconnect
		{
			sendBuffer = new char[4];
			sendBuffer[0] = 4;
			sendBuffer[1] = 'T';
			sendBuffer[2] = 'I';
			sendBuffer[3] = 'M';
			size = sizeof(char[4]);
			break;
		}
		case 5: //gamestart
		{
			sendBuffer = new char[32];
			sendBuffer[0] = 5;
			size = sizeof(char[32]);
			break;
			//m_Roster->ConvertFromChar(m_cNextMessage);
			break;
		}
		default:
			break;
			
	}

	// send the packet
	send(m_Socket,sendBuffer,size,0);
}


/////////////////
//	ACCESSORS	
////////////////

std::vector<char*> MTClient::GetRecvMessages()
{

	ScopedCriticalSection cs(m_csCharBuffer);
	return m_vRecvMessages;
	
}

// gets next unread message
void MTClient::GetNextMessage(char* &outPut)
{
	// lock reading/writing of the char buffer vec
	ScopedCriticalSection cs(m_csCharBuffer);

	// only do stuff if there are messages to get
	if (m_vRecvMessages.size() > 0)
	{
		// fill passed in var
		outPut = m_vRecvMessages[0];

		// erase it from unrease messages
		auto it = m_vRecvMessages.begin();
	
		m_vRecvMessages.erase(it);

		return;
	}

	outPut = NULL;
	
}


/////////////////
//	MUTATORS	
////////////////
void MTClient::AddRecvMessage(char* newBuffer)
{
	ScopedCriticalSection cs(m_csCharBuffer);
	m_vRecvMessages.push_back(newBuffer);
	
}

