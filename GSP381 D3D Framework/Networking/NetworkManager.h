#pragma once

#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib")

#define MAXCONNECTIONS 50


// forward declarations
class MTServer;
class MTClient;

#define NETMAN NetworkManager::Instance()

class NetworkManager
{
private:
	
	// multi-threaded server, only created if its initialized
	MTServer* m_pServer;	
	// multi-threaded client, only created if its initialized
	MTClient* m_pClient;

	NetworkManager();

	
public:

	static NetworkManager* Instance();

	~NetworkManager(void);
		
	///////////////////
	// INIT
	//////////////////
	// creates and runs init on server
	void InitServer();
	// creates and runs init on client
	void InitClient();

	/////////////////
	// RUN
	/////////////////
	void RunServer();
	void RunClient();

	/////////////////
	// SHUTDOWN
	////////////////
	void ShutdownServer();
	void ShutdownClient();


};

