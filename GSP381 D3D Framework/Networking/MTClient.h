#pragma once

#include <winsock2.h>
#include "../MultiThreading/CriticalSection.h"
#include <vector>


#pragma comment(lib,"ws2_32.lib")



#define myClient MTClient::Instance()

class MTClient
{
private:

	//////////////////
	// GRAPHICAL
	/////////////////
	int m_iConnectStatusGID;


	MTClient(void);
	

	//struct with info about windows sockets such as version, # of sockets, etc
	WSADATA m_sWsaDat;

	// the socket that we are going to use to connect to the server
	SOCKET m_Socket;



	/* Specifies a local or remote endpoint to which to connect a socket 
		sin_family - Address family (must be AF_INET).
		sin_port - IP port.
		sin_addr - IP address.
		sin_zero - Padding to make structure the same size as SOCKADDR.
	*/
	SOCKADDR_IN m_sServerInf;

	// received data
	std::vector<char *> m_vRecvMessages;

	// critical sections for the server
	CriticalSection m_csCharBuffer;
	

public:

	static MTClient* Instance();
	
	~MTClient(void);


	// initializes Server
	int Init();

	// runs server
	int Run();

	// shuts down server
	int Shutdown();

	// creates the two threads needed for chat
	static DWORD WINAPI ThreadIncomingMessages(LPVOID arg);
	static DWORD WINAPI ThreadSendMessages(LPVOID arg);
	

	// packet send command
	void SendPacket(int iType);
	

	/////////////////
	//	ACCESSORS	
	////////////////
	// returns the shared buffer
	std::vector<char*> GetRecvMessages();
	void GetNextMessage(char* &outPut);
	
	/////////////////
	//	MUTATORS	
	////////////////
	// sets the shared char buffer for the server
	void AddRecvMessage(char* newBuffer);
	
};

