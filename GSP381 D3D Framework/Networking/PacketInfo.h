typedef unsigned char BYTE;

enum PacketType
{
    MSG_KeyPress = 0,	// client->server
    MSG_Sync,			// server->client
    MSG_Boost,			// server->client
    MSG_GameEvent,		// server->client
    MSG_ClientConnect,	// client->server
    MSG_Roster			// server->client
};

enum EventState
{ 
   ST_GameOver = 0,
   ST_Start,
   ST_GetReady
};

class Packet
{

public:
	Packet(){}
	~Packet(){}

	// all packets are going to have a type
	BYTE m_bType;

	virtual char* ConvertToChar() = 0;
	virtual void ConvertFromChar(char* input) = 0;

};



//gameplay client -> server
class KeyPress : public Packet
{
public:
	KeyPress();
	KeyPress(BYTE bType);

	// pack a char array to return
	virtual char* ConvertToChar();
	
	// read the data from the char array
	virtual void ConvertFromChar(char* input);

	BYTE m_bPadding[7];
};

//gameplay server -> client
class Sync : public Packet
{
public:
	Sync();
	Sync(BYTE bType, BYTE bPos[8], BYTE bVelocity[8], BYTE bAcceleration[8]);

	// pack a char array to return
	virtual char* ConvertToChar();
	
	// read the data from the char array
	virtual void ConvertFromChar(char* input);

	BYTE m_bPadding[7];				//byte padding
	BYTE m_bPosition[8];			//position of each of the 8 players
	BYTE m_bVelocity[8];			//velocity of each of the 8 players
	BYTE m_bAcceleration[8];		//acceleration of each of the 8 players
};

class Boost : public Packet
{
public:
	Boost();
	Boost(BYTE bType);

	// pack a char array to return
	virtual char* ConvertToChar();
	
	// read the data from the char array
	virtual void ConvertFromChar(char* input);
	
	BYTE m_bPadding[7];			//byte padding
};

class GameEvent : public Packet
{
public:
	GameEvent();
	GameEvent(BYTE bType, BYTE bState);
	~GameEvent(){}
	
	// pack a char array to return
	virtual char* ConvertToChar();
	
	// read the data from the char array
	virtual void ConvertFromChar(char* input);

	BYTE m_bState;				//state of game (gameover/start/getready)
	BYTE m_bPadding[6];			//byte padding


};


//setup client -> server
class ClientConnect : public Packet
{
public:
	ClientConnect();
	ClientConnect(BYTE bType, BYTE initials[] );
	// pack a char array to return
	virtual char* ConvertToChar();
	
	// read the data from the char array
	virtual void ConvertFromChar(char* input);
	
	BYTE ID[3];					//Initials of the player
};

//setup server -> client
class Roster : public Packet
{
public:
	Roster();
	Roster(BYTE bType, BYTE bID, BYTE bTimeLeft, char cRoster[24]);
	// pack a char array to return
	virtual char* ConvertToChar();
	
	// read the data from the char array
	virtual void ConvertFromChar(char* input);
	
	
	BYTE padding[5]; // 1-5
	BYTE yourID; // 6
	BYTE timeLeft; // 7
	char roster[24]; // 8-31
};
