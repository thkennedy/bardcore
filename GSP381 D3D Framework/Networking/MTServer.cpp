#include <iostream>
#include "MTServer.h"
#include "../MultiThreading/CriticalSection.h"

// takes a passed in new socket and handles data

MTServer* MTServer::Instance()
{
	static MTServer instance;

	return &instance;

}


MTServer::MTServer(void)
{
}


MTServer::~MTServer(void)
{
}

int MTServer::Init()
{
	// init vars
	m_iNumConnections = 0;
	m_iMaxConnections = MAXCONNECTIONS;

	m_cBuffer = new char[1000];

	// init critical sections
	InitializeCriticalSection(&m_csCharBuffer);
	InitializeCriticalSection(&m_csNumConnections);

	
	// initialize Winsock
	WSAStartup(MAKEWORD(2,2), &m_sWsaDat);
	if(WSAStartup(MAKEWORD(2,2),&m_sWsaDat)!=0)
	{
		std::cout<<"WSA Initialization failed!\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}

	/* socket that is bound to specific transport service provider
	af - IP protocol  AF_INET is IPv4 AF_INET6 is IPv6
	type - type specification for socket - how data is expected to be sent- use SOCK_STREAM
	*/
	m_ServerSocket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(m_ServerSocket==INVALID_SOCKET)
	{
		std::cout<<"Socket creation failed.\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}

	// setting up the server info - what needs to be listened for.
	// AF_INET means use IPv4, INADDR_ANY mean listen on any adapter
	// and 8888 is the port
	m_sServerInf.sin_family=AF_INET;
	m_sServerInf.sin_addr.s_addr=INADDR_ANY;
	m_sServerInf.sin_port=htons(1138);

	// bind the serverinfo to the socket
	if(bind(m_ServerSocket,(SOCKADDR*)(&m_sServerInf),sizeof(m_sServerInf))==SOCKET_ERROR)
	{
		std::cout<<"Unable to bind socket!\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}

	// begin listening on the socket
	listen(m_ServerSocket,MAXCONNECTIONS);

	// init completed successfully
	return 1; 
}

// runs server
int MTServer::Run()
{
	// init the server, if it fails, shutdown
	if (Init() != 1)
	{
		std::cout<<"Init failed, shutting down server.\r\n";
		WSACleanup();
		system("PAUSE");
		return 0;
	}

	while(1)
	{
		// keep listening for connections
		AcceptConnections(m_ServerSocket);
	}

	
		// run executed successfully
	return 1;	

}

// shuts down server
int MTServer::Shutdown()
{
	// Shutdown our socket's send ability
	shutdown(m_ServerSocket,SD_SEND);


	// Close our socket entirely
	closesocket(m_ServerSocket);

	return 1;
}

void MTServer::AcceptConnections(SOCKET ListeningSocket)
{
	
	
	std::cout<<"Waiting for incoming connections...\r\n";
	while(1)
	{
		// creates a new socket that you're connected on
		SOCKET TempSock = accept(ListeningSocket ,NULL,NULL);

		if (TempSock != INVALID_SOCKET){

			std::cout<<"Client connected!\r\n\r\n";



			// a client is connected, add to the number of connections i have
			AddNumConnections(1);

			// add the socket to the socket array
			m_aClientSockets[GetNumConnections() - 1] = TempSock;

			
			// create thread
			CreateThread(NULL,
				0,
				AcceptThreadedConnection,
				(LPVOID)TempSock,
				0,
				NULL);
		}
		
	}
}

DWORD WINAPI MTServer::AcceptThreadedConnection(LPVOID socket)
{
	// dynamic cast void ptr into socket var
	SOCKET _socket = (SOCKET)socket;
	
	
	if (!MTServer::Instance()->EchoAllClients(_socket) ){
		std::cout << std::endl << "Echo Failed." << std::endl;
		return false;
	}
	
	std::cout << "Client disconnected, shutting down" << std::endl;
	
	// shut down send operations
	shutdown(_socket,SD_SEND);

	closesocket(_socket);

	return true;

	
}


// sends buffer to all clients
bool MTServer::EchoAllClients(SOCKET connection)
{
	int inDataLength;

	char *tempBuffer = new char[1000];
	
	while (1)
	{
		inDataLength = recv(connection,tempBuffer,1000,0);
		
		if (inDataLength > 0)
		{
			std::cout<< "Received: " << tempBuffer << std::endl;

			for (int i = 0; i < m_iNumConnections; ++i)
			{		
				send(m_aClientSockets[i],tempBuffer,1000,0);
			}
		}
	}

	return true;
	
	
}

/////////////////
//	ACCESSORS	
////////////////
char* MTServer::GetBuffer()
{
	char* tempArray;
	EnterCriticalSection(&m_csCharBuffer);
	tempArray = m_cBuffer;
	LeaveCriticalSection(&m_csCharBuffer);

	return tempArray;
	
}

// gets the number of connections to the server
int MTServer::GetNumConnections( )
{
	int tempNum;
	EnterCriticalSection(&m_csNumConnections);
	tempNum = m_iNumConnections;
	LeaveCriticalSection(&m_csNumConnections);

	return tempNum;
}



/////////////////
//	MUTATORS	
////////////////
void MTServer::SetBuffer(char* newBuffer)
{
	EnterCriticalSection(&m_csCharBuffer);
	m_cBuffer = newBuffer;
	LeaveCriticalSection(&m_csCharBuffer);
}

void MTServer::AddNumConnections(int addOrSubtract)
{
	EnterCriticalSection(&m_csNumConnections);
	m_iNumConnections += addOrSubtract;
	LeaveCriticalSection(&m_csNumConnections);
}