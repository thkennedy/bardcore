#include "NetworkManager.h"


NetworkManager::NetworkManager(void)
{
}


NetworkManager::~NetworkManager(void)
{
}

NetworkManager* NetworkManager::Instance()
{
	static NetworkManager instance;

	return &instance;
}

		
///////////////////
// INIT
//////////////////
// creates and runs init on server
void NetworkManager::InitServer()
{

}
// creates and runs init on client
void InitClient();

/////////////////
// RUN
/////////////////
void RunServer();
void RunClient();

/////////////////
// SHUTDOWN
////////////////
void ShutdownServer();
void ShutdownClient();