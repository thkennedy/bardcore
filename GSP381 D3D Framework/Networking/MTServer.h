#pragma once

#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib")

#define MAXCONNECTIONS 50

#define myServer MTServer::Instance()

class MTServer
{
private:

	MTServer(void);

	

	//struct with info about windows sockets such as version, # of sockets, etc
	WSADATA m_sWsaDat;

	// the socket that we are going to use to listne for new connections
	SOCKET m_ServerSocket;

	// set of sockets we will use to communicate with clients
	SOCKET m_aClientSockets[MAXCONNECTIONS];

	/* Specifies a local or remote endpoint to which to connect a socket 
		sin_family - Address family (must be AF_INET).
		sin_port - IP port.
		sin_addr - IP address.
		sin_zero - Padding to make structure the same size as SOCKADDR.
	*/
	SOCKADDR_IN m_sServerInf;

	// received data
	char *m_cBuffer;

	// number of connections
	int m_iNumConnections;

	// max allowed connections
	int m_iMaxConnections;

	// critical sections for the server
	CRITICAL_SECTION m_csCharBuffer;
	CRITICAL_SECTION m_csNumConnections;

public:

	static MTServer* Instance();
	
	~MTServer(void);


	// initializes Server
	int Init();

	// runs server
	int Run();

	// shuts down server
	int Shutdown();

	void AcceptConnections(SOCKET ListeningSocket);

	// takes a passed in new socket and handles data
	static DWORD WINAPI AcceptThreadedConnection(LPVOID socket);

	// sends buffer to all clients
	bool EchoAllClients(SOCKET);

	/////////////////
	//	ACCESSORS	
	////////////////
	// returns the shared buffer
	char* GetBuffer();
	
	// gets the number of connections to the server
	int GetNumConnections();

	/////////////////
	//	MUTATORS	
	////////////////
	// sets the shared char buffer for the server
	void SetBuffer(char* newBuffer);
	
	// lets you add or subtract connection(s)
	void AddNumConnections(int addOrSubtract);
};

