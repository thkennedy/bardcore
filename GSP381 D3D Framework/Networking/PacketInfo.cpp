#include "PacketInfo.h"

KeyPress::KeyPress(BYTE bType)
{
	// type
	m_bType = bType;

	// fill padding array
	for (auto i = 6; i >= 0; --i)
	{
		m_bPadding[i] = '0';
	}
}

char* KeyPress::ConvertToChar()
{
	// alloc new memory	
	char* output = new char[8];

	// send type
	output[0] = m_bType;

	// copy stuff into buffer
	// fill padding array
	for (auto i = 6; i >= 0; --i)
	{
		output[i+1] = m_bPadding[i];
	}

	return output;

}

void KeyPress::ConvertFromChar(char* input)
{

}

Sync::Sync()
{
	// empty type
	m_bType = -1;

	// fill padding array
	for (auto i = 6; i >= 0; --i)
	{
		m_bPadding[i] = '0';
	}

	// positions
	for (auto i = 7; i >= 0; --i)
	{
		m_bPosition[i] = '0';
	}

	// velocities
	for (auto i = 7; i >= 0; --i)
	{
		m_bVelocity[i] = '0';
	}

	// accelerations
	for (auto i = 7; i >= 0; --i)
	{
		m_bAcceleration[i] = '0';
	}

	

}

Sync::Sync(BYTE bType, BYTE bPos[], BYTE bVelocity[], BYTE bAcceleration[])
{
	// empty type
	m_bType = bType;

	// fill padding array
	for (auto i = 6; i >= 0; --i)
	{
		m_bPadding[i] = '0';
	}

	// positions
	for (auto i = 7; i >= 0; --i)
	{
		m_bPosition[i] = bPos[i];
	}

	// velocities
	for (auto i = 7; i >= 0; --i)
	{
		m_bVelocity[i] = bVelocity[i];
	}

	// accelerations
	for (auto i = 7; i >= 0; --i)
	{
		m_bAcceleration[i] = bAcceleration[i];
	}

}

char* Sync::ConvertToChar()
{
	/*
		byte 0 - type
		bytes 1-7 - padding
		bytes 8-15 - position
		bytes 16-23 - velocity
		bytes 24-31- acceleration
	*/

	// new char array
	char* output = new char[32];

	// fill padding array
	for (auto i = 7; i >= 0; --i)
	{
		output[i+1] = '0';
	}

	// fill pos array
	for (auto i = 7; i >= 0; --i)
	{
		output[i+8] = m_bPosition[i];
	}

	// fill vel array
	for (auto i = 7; i >= 0; --i)
	{
		output[i+16] = m_bVelocity[i];
	}

	// fill accel array
	for (auto i = 7; i >= 0; --i)
	{
		output[i+24] = m_bAcceleration[i];
	}

	return output;
}

void Sync::ConvertFromChar(char* input)
{
/*
		byte 0 - type
		bytes 1-7 - padding
		bytes 8-15 - position
		bytes 16-23 - velocity
		bytes 24-31- acceleration
	*/

	m_bType = input[0];

	// fill padding array
	for (auto i = 7; i >= 0; --i)
	{
		m_bPadding[i] = input[i+1];
	}

	// fill pos array
	for (auto i = 7; i >= 0; --i)
	{
		m_bPosition[i] = input[i+8];
	}

	// fill vel array
	for (auto i = 7; i >= 0; --i)
	{
		m_bVelocity[i] = input[i+16];
	}

	// fill accel array
	for (auto i = 7; i >= 0; --i)
	{
		m_bAcceleration[i] = input[i+24];
	}

}


Boost::Boost()
{
	// empty type
	m_bType = -1;

	// fill padding array
	for (auto i = 6; i >= 0; --i)
	{
		m_bPadding[i] = '0';
	}
}

Boost::Boost(BYTE bType)
{
	// empty type
	m_bType = bType;

	// fill padding array
	for (auto i = 6; i >= 0; --i)
	{
		m_bPadding[i] = '0';
	}
}

char* Boost::ConvertToChar()
{
	// need to allocate memory
	char* output = new char[8];

	output[0] = m_bType;

	for (auto i = 6; i >= 0; --i)
		output[i+1] = m_bPadding[i];

	return output;
	
}

void Boost::ConvertFromChar(char* input)
{
	m_bType = input[0];

	for (auto i = 6; i >= 0; --i)
		m_bPadding[i] = input[i+1];
}



GameEvent::GameEvent()
{
	// empty type
	m_bType = -1;

	// empty state
	m_bState = -1;

	// fill padding array
	for (auto i = 5; i >= 0; --i)
	{
		m_bPadding[i] = '0';
	}

}

GameEvent::GameEvent(BYTE bType, BYTE bState)
{
	// empty type
	m_bType = bType;

	// empty state
	m_bState = bState;

	// fill padding array
	for (auto i = 5; i >= 0; --i)
	{
		m_bPadding[i] = '0';
	}

}

char* GameEvent::ConvertToChar()
{
	// need to allocate memory
	char* output = new char[8];

	output[0] = m_bType;

	output[1] = m_bState;

	for (auto i = 5; i >= 0; --i)
		output[i+2] = m_bPadding[i];

	return output;
	
}

void GameEvent::ConvertFromChar(char* input)
{
	m_bType = input[0];

	m_bState = input[1];

	for (auto i = 5; i >= 0; --i)
		m_bPadding[i] = input[i+2];
}

ClientConnect::ClientConnect()
{
	// undefined type
	m_bType = -1;

	// fill initials array
	for (auto i = 2; i >= 0; --i)
	{
		ID[i] = '0';
	}
}

ClientConnect::ClientConnect(BYTE bType, BYTE initials[] )
{
	// passed in type
	m_bType = bType;

	// fill initials array
	for (auto i = 2; i >= 0; --i)
	{
		ID[i] = initials[i];
	}

}

char* ClientConnect::ConvertToChar()
{
	// need to allocate memory
	char* output = new char[4];

	output[0] = m_bType;

	for (auto i = 2; i >= 0; --i)
		output[i+1] = ID[i];

	return output;
}

void ClientConnect::ConvertFromChar(char* input)
	{
		m_bType = input[0];

		for (auto i = 2; i >= 0; --i)
			ID[i] = input[i+1];
}






Roster::Roster()
:yourID(0), timeLeft(0)
{
	m_bType = 0;
	// fill padding array
	for (auto i = 4; i >= 0; --i)
	{
		padding[i] = '0';
	}

	// deep copy passed in roster into char array
	for (auto i = 24; i >= 0; --i)
	{
		roster[i] = 0;
	}
}
Roster::Roster(BYTE bm_bType, BYTE bID, BYTE bTimeLeft, char cRoster[24])
	:yourID(bID), timeLeft(bTimeLeft)
{
	m_bType = bm_bType;
	// fill padding array
	for (auto i = 4; i >= 0; --i)
	{
		padding[i] = 'a';
	}

	// deep copy passed in roster into char array
	for (auto i = 24; i >= 0; --i)
	{
		roster[i] = cRoster[i];
	}
}

char* Roster::ConvertToChar()
	{
		// need to allocate memory
		char* output = new char[32];

		output[0] = m_bType;
		for (auto i = 5; i >= 0; --i)
			output[i+1] = padding[i];

		output[6] = yourID;
		output[7] = timeLeft;
		for (auto i = 24; i >= 0; --i)
			output[i+8] = roster[i];

		return output;
	}

void Roster::ConvertFromChar(char* input)
	{
		m_bType = input[0];

		for (auto i = 5; i >= 0; --i)
			padding[i] = input[i+1];

		yourID = input[6];

		timeLeft = input[7];

		for (auto i = 24; i >= 0; --i)
			roster[i] = input[i+8];
			
	}